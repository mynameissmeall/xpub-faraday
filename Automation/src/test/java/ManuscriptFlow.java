import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.junit.*;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.JVM)

public class ManuscriptFlow {
    private static WebDriver driver = null;
    private static WebDriverWait wait = null;
    public String URL = Constants.URL;
    public static String manuscriptId;
    public static String projectId;

    Date date = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy_h:mm:ss a");
    String formattedDate = sdf.format(date);

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        //driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.firefox());
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 10);
        String window = driver.getWindowHandle();
        ((JavascriptExecutor) driver).executeScript("alert('Test')");
        driver.switchTo().alert().accept();
        driver.switchTo().window(window);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void createNewManuscript() throws Exception {
        /* Add constants.random after testing this function separably */
        Utils.validLogin( driver,  wait,  URL, Constants.email + /*Constants.randomStr*/ "auth1" + "@thinslices.com", Constants.password);

        WebElement validLogin = null;

        try {
            validLogin = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[data-test-id='new-manuscript']")));
        } catch (NoSuchElementException e) {

        }

        assertNotNull(validLogin);

        driver.findElement(By.cssSelector("button[data-test-id='new-manuscript']")).click();

        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[data-test-id='submission-next']")));
        } catch (NoSuchElementException e) {

        }

        assertNotNull(driver.findElement(By.cssSelector("button[data-test-id='submission-next']")));

        // First step submission flow

        WebElement subButton = driver.findElement(By.cssSelector("button[data-test='submission-next']"));

        Utils.scrollToElement(driver, subButton);
        driver.findElement(By.cssSelector("[data-test-id='agree-checkbox']")).click();
        driver.findElement(By.cssSelector("button[data-test-id='submission-next']")).click();

        // Second step submission flow
        try {
             wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".wizard-step >[data-test-id='row']:nth-of-type(2) span")));
        } catch (NoSuchElementException e) {

        }

        assertEquals("Please provide the details of all the authors of this manuscript, in the order that they appear on the manuscript. Your details have been prefilled as the submitting author.",driver.findElement(By.cssSelector(".wizard-step >[data-test-id='row']:nth-of-type(2) span")).getText());
        assertEquals("MANUSCRIPT TITLE\n*", driver.findElement(By.cssSelector("[data-test-id='submission-title']")).getText());

        driver.findElement(By.cssSelector("input[name='metadata.title']")).sendKeys(Constants.manusName + formattedDate);
        driver.findElement(By.cssSelector("[data-test-id='submission-type']")).click();
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[data-test-id='submission-type'] div[role=\"option\"]")));
        } catch (NoSuchElementException e) {

        }
        driver.findElement(By.cssSelector("[data-test-id='submission-type'] div[role=\"option\"]:nth-child(1)")).click();
        driver.findElement(By.cssSelector("[data-test-id='submission-abstract'] textarea")).sendKeys(Constants.manusAbstract + formattedDate);

        try {
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("//span[text()='Progress Saved']"))));
        } catch (NoSuchElementException e) {

        }

        WebElement element = driver.findElement(By.cssSelector("[name='conflicts.hasConflicts'][value='no'] + span"));
        Utils.scrollToElement(driver, element);
        element.click();

        try {
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("//span[text()='Saving changes...']"))));
        } catch (NoSuchElementException e) {

        }

        try {
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("//span[text()='Progress Saved']"))));
        } catch (NoSuchElementException e) {

        }

        driver.findElement(By.cssSelector("[name='conflicts.hasDataAvailability'][value='yes'] + span")).click();
        WebElement element1 = driver.findElement(By.cssSelector("[name='conflicts.hasFunding'][value='yes'] + span"));

        try {
            wait.until(ExpectedConditions.visibilityOf(element1));
        } catch (NoSuchElementException e) {

        }

        Utils.scrollToElement(driver, element1);
        element1.click();

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button[data-test-id='submission-next']")));
        } catch (NoSuchElementException e) {

        }

        WebElement element2 = driver.findElement(By.cssSelector("button[data-test-id='submission-next']"));

        Utils.scrollToElement(driver, element2);
        element2.click();

        // Third step submission flow

        try {
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector(".wizard-step [level='2']"))));
        } catch (NoSuchElementException e) {

        }
        WebElement pageTitle = driver.findElement(By.cssSelector(".wizard-step [level='2']"));
        assertEquals("3. Manuscript Files Upload", pageTitle.getText());

        try {
            wait.until(ExpectedConditions.visibilityOf(pageTitle));
        } catch (NoSuchElementException e) {

        }

        String mURL = driver.getCurrentUrl();
        manuscriptId = Utils.mID(mURL);
        projectId = Utils.pID(mURL);

        driver.findElement(By.cssSelector(/*"[data-test-id='upload-manuscripts']*/"input[type=\"file\"]")).sendKeys(Constants.fileManuscript);
//        driver.findElement(By.cssSelector("[data-test-id='upload-supplementary'] input[type=\"file\"]")).sendKeys(Constants.fileSupplementary);
//        driver.findElement(By.cssSelector("[data-test-id='upload-coverLetter'] input[type=\"file\"]")).sendKeys(Constants.fileCoverLetter);

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button[data-test-id='submission-next']")));
        } catch (NoSuchElementException e) {

        }

        // Confirmation modal
        WebElement element3 = driver.findElement(By.cssSelector("button[data-test-id='submission-next']"));

        try {
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("[data-test='submission-next']"))));
        } catch (NoSuchElementException e) {

        }

        WebElement element3 = driver.findElement(By.cssSelector("[data-test='submission-next']"));
        Utils.scrollToElement(driver, element3);
        try {
            wait.until(ExpectedConditions.visibilityOf(element3));
        } catch (NoSuchElementException e) {

        }
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        element3.click();


        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(.,'AGREE & SUBMIT')]")));
        } catch (NoSuchElementException e) {

        }

        driver.findElement(By.xpath("//button[contains(.,'AGREE & SUBMIT')]")).click();

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(.,'GO TO DASHBOARD')]")));
        } catch (NoSuchElementException e) {

        }

        driver.findElement(By.xpath("//button[contains(.,'GO TO DASHBOARD')]")).click();

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-test-id='fragment-" + manuscriptId + "']")));
        } catch (NoSuchElementException e) {

        }

        // Check if manuscript is displayed on dashboard
        assertNotNull(driver.findElement(By.cssSelector("[data-test-id='fragment-" + manuscriptId + "']")));

        driver.findElement(By.cssSelector("[data-test-id='fragment-" + manuscriptId + "']")).click();

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-test-id='fragment-status']")));
        } catch (NoSuchElementException e) {

        }

        // Check if manuscript status is submitted for author
        assertEquals("Submitted", driver.findElement(By.cssSelector("[data-test-id='fragment-status']")).getText());

    }

    @Test
    public void eqsApproveManuscript() throws Exception{
        Utils.validLogin(driver, wait, URL, Constants.adminEmail, Constants.password);

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-test-id=\"row\"] button[type=\"button\"]")));
        } catch (NoSuchElementException e) {

        }

        Utils.filterLatest(driver);

        driver.findElement(By.cssSelector("[data-test-id='fragment-" + manuscriptId + "']")).click();

        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[data-test-id='button-qa-manuscript-" + manuscriptId + "']")));
        } catch (NoSuchElementException e) {

        }

        assertEquals("QA", driver.findElement(By.cssSelector("[data-test-id='fragment-status']")).getText());

        driver.findElement(By.cssSelector("[data-test-id='button-qa-manuscript-" + manuscriptId + "']")).click();

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(.,'YES')]")));
        } catch (NoSuchElementException e) {

        }

        assertTrue(driver.getCurrentUrl().contains("eqs-decision"));

        driver.findElement(By.xpath("//button[contains(.,'YES')]")).click();

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[name='customId'")));
        } catch (NoSuchElementException e) {

        }

        driver.findElement(By.cssSelector("input[name='customId'")).sendKeys(Constants.customManuscriptID);
        driver.findElement(By.xpath("//button[contains(.,'OK')]")).click();

        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("img[src='/assets/logo-hindawi@2x.png']")));
        } catch (NoSuchElementException e) {

        }

        driver.get(URL);

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-test-id=\"row\"] button[type=\"button\"]")));
        } catch (NoSuchElementException e) {

        }

        Utils.filterLatest(driver);

        driver.findElement(By.cssSelector("[data-test-id='fragment-" + manuscriptId + "']")).click();

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-test-id='fragment-status']")));
        } catch (NoSuchElementException e) {

        }

        //Check if the manuscript status is assign HE
        assertEquals("Assign HE", driver.findElement(By.cssSelector("[data-test-id='fragment-status']")).getText());

    }

    @Test
    public void assignHE() throws Exception{
        Utils.validLogin(driver, wait, URL, Constants.adminEmail, Constants.password);

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-test-id=\"row\"] button[type=\"button\"]")));
        } catch (NoSuchElementException e) {

        }
        wait.until(ExpectedConditions.jsReturnsValue("return document.readyState==\"complete\";"));

        Utils.filterLatest(driver);

        driver.findElement(By.cssSelector("[data-test-id='fragment-" + manuscriptId + "']")).click();

        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[data-test-id='fragment-status']")));
        } catch (NoSuchElementException e) {

        }

        assertEquals("Assign HE", driver.findElement(By.cssSelector("[data-test-id='fragment-status']")).getText());

        driver.findElement(By.xpath("//div[@label = 'Assign Handling Editor']")).click();
        driver.findElement(By.xpath("//input[@placeholder = 'Filter by name or email']")).sendKeys(Constants.emailHE + "@thinslices.com");

        WebElement invite = driver.findElement(By.xpath("//div[@label = 'Assign Handling Editor'] //button[contains(.,'INVITE')]"));

        Actions hover = new Actions(driver);
        hover.moveToElement(invite);

        hover.build();
        hover.perform();
        invite.click();

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[id='ps-modal-root'] button:nth-child(2)")));
        } catch (NoSuchElementException e) {

        }

        driver.findElement(By.cssSelector("[id='ps-modal-root'] button:nth-child(2)")).click();

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-test-id='fragment-status']")));
        } catch (NoSuchElementException e) {

        }
        driver.navigate().refresh();

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-test-id='fragment-status']")));
        } catch (NoSuchElementException e) {

        }
        //Check if the manuscript status is HE invited
        assertEquals("HE Invited", driver.findElement(By.cssSelector("[data-test-id='fragment-status']")).getText());
    }

    @Test
    public void acceptManuscriptAsHE() throws Exception{
        Utils.validLogin(driver, wait, URL, Constants.emailHE + "@thinslices.com", Constants.password);

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-test-id=\"row\"] button[type=\"button\"]")));
        } catch (NoSuchElementException e) {

        }
        wait.until(ExpectedConditions.jsReturnsValue("return document.readyState==\"complete\";"));

        Utils.filterLatest(driver);

        driver.findElement(By.cssSelector("[data-test-id='fragment-" + manuscriptId + "']")).click();

        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[data-test-id='fragment-status']")));
        } catch (NoSuchElementException e) {

        }

        assertEquals("Respond to Invite", driver.findElement(By.cssSelector("[data-test-id='fragment-status']")).getText());

        driver.findElement(By.cssSelector("input[type=\"radio\"][name=\"decision\"][value=\"accept\"] + span")).click();
        driver.findElement(By.xpath("//button[contains(.,'RESPOND TO INVITATION')]")).click();

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[id=\"ps-modal-root\"] button:nth-child(2)")));
        } catch (NoSuchElementException e) {

        }

        assertEquals("Accept this invitation?", driver.findElement(By.cssSelector("div[id=\"ps-modal-root\"] h2")).getText());
        driver.findElement(By.cssSelector("[id='ps-modal-root'] button:nth-child(2)")).click();


        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@label = 'Reviewer Details & Reports']")));
        } catch (NoSuchElementException e) {

        }

        assertEquals("0 invited", driver.findElement(By.xpath("//span[contains(.,'0 invited')]")).getText());
    }

    @Test
    public void inviteReviewersAsHE() throws Exception{
        Utils.validLogin(driver, wait, URL, Constants.emailHE + "@thinslices.com", Constants.password);

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-test-id=\"row\"] button[type=\"button\"]")));
        } catch (NoSuchElementException e) {

        }
        wait.until(ExpectedConditions.jsReturnsValue("return document.readyState==\"complete\";"));

        Utils.filterLatest(driver);

        driver.findElement(By.cssSelector("[data-test-id='fragment-" + manuscriptId + "']")).click();

        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[data-test-id='fragment-status']")));
        } catch (NoSuchElementException e) {

        }

        assertEquals("Invite Reviewers", driver.findElement(By.cssSelector("[data-test-id='fragment-status']")).getText());

        int i;
        for( i=1; i<6; i++ ){

            try {
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h3[contains(text(),'Reviewer Details & Reports')]")));
            } catch (NoSuchElementException e) {

            }

            driver.findElement(By.xpath("//h3[contains(text(),'Reviewer Details & Reports')]")).click();

            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(text(),'Reviewer Details')]")));
            } catch (NoSuchElementException e) {

            }
            driver.findElement(By.xpath("//h4[contains(text(),'Reviewer Details')]")).click();

            driver.findElement(By.cssSelector("input[name='email']")).sendKeys(Constants.emailRev + i + "@thinslices.com");
            driver.findElement(By.cssSelector("input[name='firstName']")).sendKeys("rev" + i);
            driver.findElement(By.cssSelector("input[name='lastName']")).sendKeys("rev" + i);
            driver.findElement(By.cssSelector("input[name='affiliation']")).sendKeys("rev" + i);
            driver.findElement(By.xpath("//span[contains(.,'Choose in the list')]")).click();
            driver.findElement(By.xpath("//*[contains(text(),'Albania')]")).click();
            driver.findElement(By.xpath("//button[contains(text(),'Invite')]")).click();

            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[id='ps-modal-root'] button:nth-child(2)")));
            } catch (NoSuchElementException e) {

            }

            assertEquals("Send invitation to Review?", driver.findElement(By.cssSelector("div[id='ps-modal-root'] h2")).getText());
            driver.findElement(By.cssSelector("[id='ps-modal-root'] button:nth-child(2)")).click();

            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(text(),'Reviewer Details')]")));
            } catch (NoSuchElementException e) {

            }
            driver.navigate().refresh();
        }

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-test-id='fragment-status']")));
        } catch (NoSuchElementException e) {

        }

        assertEquals("Check Review Process", driver.findElement(By.cssSelector("[data-test-id='fragment-status']")).getText());

    }

    //@Test
    public void submitMajorReview() throws Exception{
        for(int i = 1; i<=2; i++) {
            Utils.validLogin(driver, wait, URL, Constants.emailRev + i + "@thinslices.com", Constants.password);

            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[data-test-id=\"row\"] button[type=\"button\"]")));
            } catch (NoSuchElementException e) {

            }
            wait.until(ExpectedConditions.jsReturnsValue("return document.readyState==\"complete\";"));

            Utils.filterLatest(driver);

            driver.findElement(By.cssSelector("[data-test-id='fragment-" + manuscriptId + "']")).click();

            try {
                wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[data-test-id='fragment-status']")));
            } catch (NoSuchElementException e) {

            }

            assertEquals("Respond to Invite", driver.findElement(By.cssSelector("[data-test-id='fragment-status']")).getText());

            driver.findElement(By.cssSelector("input[type=\"radio\"][name=\"decision\"][value=\"accept\"] + span")).click();
            driver.findElement(By.xpath("//button[contains(.,'RESPOND TO INVITATION')]")).click();

            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[id=\"ps-modal-root\"] button:nth-child(2)")));
            } catch (NoSuchElementException e) {

            }

            assertEquals("Accept this invitation?", driver.findElement(By.cssSelector("div[id=\"ps-modal-root\"] h2")).getText());
            driver.findElement(By.cssSelector("[id='ps-modal-root'] button:nth-child(2)")).click();

            driver.navigate().refresh();

            try {
                wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[data-test-id='fragment-status']")));
            } catch (NoSuchElementException e) {

            }

            assertEquals("Complete Review", driver.findElement(By.cssSelector("[data-test-id='fragment-status']")).getText());

            //driver.findElement(By.cssSelector("div[label=\"Your report\"] h3")).click();

            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(.,'Choose in the list')]")));
            } catch (NoSuchElementException e) {

            }

            driver.findElement(By.xpath("//span[contains(.,'Choose in the list')]")).click();

            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(),'Major Revision')]")));
            } catch (NoSuchElementException e) {

            }

            driver.findElement(By.xpath("//*[contains(text(),'Major Revision')]")).click();
            driver.findElement(By.cssSelector("input[type=\"file\"]")).sendKeys(Constants.fileManuscript);

            String mID = Utils.mID(driver.getCurrentUrl());

            try {
                wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("//span[text()='Progress Saved']"))));
            } catch (NoSuchElementException e) {

            }

            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[label='Your report'] [data-test-id^=file-" + mID + "]")));
            } catch (NoSuchElementException e) {

            }
            assertThat((driver.findElement(By.cssSelector("[label='Your report'] div[data-test-id^=file-" + mID + "]")).getText()), containsString(Constants.fileManuscriptName));

            driver.findElement(By.cssSelector("textarea[name='public']")).sendKeys(Constants.loremIpsumReport);
            driver.findElement(By.xpath("//*[contains(text(),'Add Confidential note for the Editorial Team')]")).click();

            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("textarea[name='confidential']")));
            } catch (NoSuchElementException e) {

            }

            driver.findElement(By.cssSelector("textarea[name='confidential']")).sendKeys(Constants.loremIpsumEditorialNote);

            driver.findElement(By.xpath("//button[contains(text(),'Submit report')]")).click();

            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[id='ps-modal-root'] button:nth-child(2)")));
            } catch (NoSuchElementException e) {

            }

            assertEquals("Are you done?", driver.findElement(By.cssSelector("div[id='ps-modal-root'] h2")).getText());
            driver.findElement(By.cssSelector("[id='ps-modal-root'] button:nth-child(2)")).click();
            driver.navigate().refresh();

            try {
                wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("[data-test-id='fragment-status']")));
            } catch (NoSuchElementException e) {

            }

            assertEquals("Review Completed", driver.findElement(By.cssSelector("[data-test-id='fragment-status']")).getText());

            driver.close();
        }

    }

    //@Test
    public void deleteManuscript() throws Exception{
        Utils.validLogin(driver, wait, URL, Constants.adminEmail, Constants.password);

        RestAssured.baseURI = URL;

        //Get user token after login with admin
        RequestSpecification request = RestAssured.given().header("Content-Type", "application/json");
        JSONObject requestParams = new JSONObject();
        requestParams.put("username", Constants.adminEmail);
        requestParams.put("password",  Constants.password);
        request.body(requestParams.toJSONString());
        Response response = request.post("api/users/authenticate");
        JsonPath jsonPathEvaluator = response.body().jsonPath();
        String token = jsonPathEvaluator.get("token");

        RequestSpecification deleteManuscript = RestAssured.given().header("Authorization", "Bearer " + token).contentType(ContentType.JSON);
        Response deleteResponse = deleteManuscript.delete("api/collections/" + projectId);

        assertEquals(200, deleteResponse.getStatusCode());
    }
}
