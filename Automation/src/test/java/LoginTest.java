import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class LoginTest {
    private static WebDriver driver = null;
    private static WebDriverWait wait = null;
    public String URL = Constants.URL;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        //driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.firefox());
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 10);
        String window = driver.getWindowHandle();
        ((JavascriptExecutor) driver).executeScript("alert('Test')");
        driver.switchTo().alert().accept();
        driver.switchTo().window(window);
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

    @Test
      public void validLoginWithAdmin() throws Exception {
        Utils.validLogin( driver,  wait,  URL, Constants.adminEmail, Constants.adminPass);
        WebElement validLogin = null;

        try {
          validLogin = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[data-test-id='new-manuscript']")));
        } catch (NoSuchElementException e) {

        }
        assertNotNull(validLogin);
        assertEquals("Admin", driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div[2]/div[2]/div[1]/span[1]")).getAttribute("innerText"));

    }
}

