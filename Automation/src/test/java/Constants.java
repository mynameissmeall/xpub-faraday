import org.apache.commons.lang3.RandomStringUtils;
import de.svenjacobs.loremipsum.LoremIpsum;

public class Constants {

    static LoremIpsum loremIpsum = new LoremIpsum();

    public static final String URL = "https://qa.review.hindawi.com/";
    public static final String jurnalID = "4b0eb00f-37cb-4fad-bd0d-7ba8ee600123";
    public static final String customManuscriptID = RandomStringUtils.randomNumeric(7);
    public static final String email = "adrian.onofrei+";
    public static final String firstname = "Test Author";
    public static final String lastname = "Test Author";
    public static final String affiliation = "TS affiliation";
    public static final String password = "password";
    public static final String adminEmail = "admin";
    public static final String adminPass = "password";
    public static final String randomStr = RandomStringUtils.randomAlphabetic(4);
    public static final String firstnameHE = "Test HE";
    public static final String lastnameHE = "Test HE";
    public static final String passwordHE = "Testing123";
    public static final String emailHE = "adrian.onofrei+he";
    public static final String emailRev = "adrian.onofrei+rev";
    public static final String affiliationHE = "TS HE affiliation";
    public static final String manusName = "This is test manuscript from ";
    public static final String manusAbstract = loremIpsum.getWords( 75 );
    public static final String fileManuscript = "/Users/adionofrei/Documents/Manuscript.pdf";
    public static final String fileManuscriptName = "Manuscript (1).pdf";
    public static final String fileSupplementary = "/Users/adionofrei/Documents/Manuscript.pdf";
    public static final String fileCoverLetter = "/Users/adionofrei/Documents/Manuscript.pdf";
    public static final String loremIpsumReport = loremIpsum.getParagraphs( 2 );
    public static final String loremIpsumEditorialNote = loremIpsum.getWords( 75, 2 );


    public static String uidAuth = null;
    public static String uidHE = null;


}

