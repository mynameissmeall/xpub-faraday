import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;

import static org.junit.Assert.assertEquals;


public class CreateAccounts {

    private static WebDriver driver = null;
    private static WebDriverWait wait = null;
    public String URL = Constants.URL;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        //driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.firefox());
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 10);
        String window = driver.getWindowHandle();
        ((JavascriptExecutor) driver).executeScript("alert('Test')");
        driver.switchTo().alert().accept();
        driver.switchTo().window(window);
        Utils.disableWarning();
    }


    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void createNewAuthor() throws Exception {

        Utils.createAuthor(driver,wait, URL, Constants.randomStr, Constants.firstname, Constants.lastname, Constants.affiliation, Constants.email + Constants.randomStr + "@thinslices.com",Constants.password);


        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[data-test-id='new-manuscript']")));
        } catch (NoSuchElementException e) {
            System.out.println(e.toString());
        }

        WebElement element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div[1]/div[2]/div/span[2]"));
        String username = element.getText();

        assertEquals(Constants.firstname+Constants.randomStr, username);

        RestAssured.baseURI = URL;

        //Get user token after login with new account

        RequestSpecification request = RestAssured.given().header("Content-Type", "application/json");

        JSONObject requestParams = new JSONObject();
        requestParams.put("username", Constants.email + Constants.randomStr + "@thinslices.com");
        requestParams.put("password",  Constants.password);
        request.body(requestParams.toJSONString());
        Response response = request.post("api/users/authenticate");

        JsonPath jsonPathEvaluator = response.body().jsonPath();
        String token = jsonPathEvaluator.get("token");

        //Get user ID and confirmationToken

        RequestSpecification requestUid = RestAssured.given().header("Authorization", "Bearer " + token);
        Response responseUid = requestUid.get("api/users/authenticate");
        JsonPath jsonPathEvaluatorUid = responseUid.body().jsonPath();

        String confirmationToken = jsonPathEvaluatorUid.get("confirmationToken");

        Constants.uidAuth = jsonPathEvaluatorUid.get("id");

        //Activate user

        driver.get(URL + "confirm-signup?userId=" + Constants.uidAuth + "&confirmationToken=" + confirmationToken);

        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//button[contains(text(),'" + "Go to Dashboard" + "')]")));
        } catch (NoSuchElementException e) {
            System.out.println(e.toString());
        }

        assertEquals("Your account has been successfully confirmed. Welcome to Hindawi!", driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div/div")).getText());
    }

    @Test
    public void createNewHE() throws Exception {

        Utils.createAuthor(driver,wait, URL, Constants.randomStr, Constants.firstnameHE, Constants.lastnameHE, Constants.affiliationHE, Constants.emailHE + Constants.randomStr + "@thinslices.com",Constants.passwordHE);


        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[data-test-id='new-manuscript']")));
        } catch (NoSuchElementException e) {
            System.out.println(e.toString());
        }

        WebElement element = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div[1]/div[2]/div/span[2]"));
        String username = element.getText();

        assertEquals(Constants.firstnameHE + Constants.randomStr, username);

        RestAssured.baseURI = URL;

        //Get user token after login with new account

        RequestSpecification request = RestAssured.given().header("Content-Type", "application/json");

        JSONObject requestParams = new JSONObject();
        requestParams.put("username", Constants.emailHE + Constants.randomStr + "@thinslices.com");
        requestParams.put("password",  Constants.passwordHE);
        request.body(requestParams.toJSONString());
        Response response = request.post("api/users/authenticate");

        JsonPath jsonPathEvaluator = response.body().jsonPath();
        String token = jsonPathEvaluator.get("token");

        //Get user ID and confirmationToken

        RequestSpecification requestUid = RestAssured.given().header("Authorization", "Bearer " + token);
        Response responseUid = requestUid.get("api/users/authenticate");
        JsonPath jsonPathEvaluatorUid = responseUid.body().jsonPath();

        String confirmationToken = jsonPathEvaluatorUid.get("confirmationToken");
        Constants.uidHE = jsonPathEvaluatorUid.get("id");

        //Activate user

        driver.get(URL + "confirm-signup?userId=" + Constants.uidHE + "&confirmationToken=" + confirmationToken);

        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//button[contains(text(),'" + "Go to Dashboard" + "')]")));
        } catch (NoSuchElementException e) {
            System.out.println(e.toString());
        }

        assertEquals("Your account has been successfully confirmed. Welcome to Hindawi!", driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div/div")).getText());

        driver.get(URL+"login?next=/");
        Utils.validLogin( driver,  wait,  URL, Constants.adminEmail, Constants.adminPass);
        driver.get(URL + "admin/users/edit/" + Constants.uidHE);

        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//button[contains(text(),'" + "Save user" + "')]")));
        } catch (NoSuchElementException e) {
            System.out.println(e.toString());
        }

        driver.findElement(By.cssSelector("input[name=handlingEditor] + span")).click();
        driver.findElement(By.cssSelector("button[type=submit]")).click();

        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[data-test-id='button-add-user']")));
        } catch (NoSuchElementException e) {
            System.out.println(e.toString());
        }

        assertEquals(URL + "admin/users", driver.getCurrentUrl() );
        assertEquals("  Add User", driver.findElement(By.cssSelector("button[data-test-id='button-add-user']")).getText());
    }
}
