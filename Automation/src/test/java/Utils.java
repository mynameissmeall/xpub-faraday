import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.regex.*;


public class Utils {

    public static void disableWarning() {
        try {
            Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
            theUnsafe.setAccessible(true);
            Unsafe u = (Unsafe) theUnsafe.get(null);

            Class cls = Class.forName("jdk.internal.module.IllegalAccessLogger");
            Field logger = cls.getDeclaredField("logger");
            u.putObjectVolatile(cls, u.staticFieldOffset(logger), null);
        } catch (Exception e) {
            // ignore
        }
    }
    public static void validLogin(WebDriver driver, WebDriverWait wait, String URL, String username, String password) throws Exception {

        driver.get(URL);

        try {
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("faraday-main")));

        } catch (NoSuchElementException e) {

        }
        WebElement user = driver.findElement(By.name("username"));
        user.click();
        user.sendKeys(username);
        WebElement pass = driver.findElement(By.name("password"));
        pass.click();
        pass.sendKeys(password);
        pass.sendKeys(Keys.ENTER);
        try {
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("[data-test-id='new-manuscript']")));
        } catch (NoSuchElementException e) {
            System.out.println(e.toString());
        }
    }

    public static void createAuthor(WebDriver driver, WebDriverWait wait, String URL, String s, String firstname, String lastname, String affiliation, String email, String password) throws Exception {

        driver.get(URL);
        try {
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.linkText("Sign up"))));
        } catch (NoSuchElementException e) {
            System.out.println(e.toString());
        }

        driver.findElement(By.linkText("Sign up")).click();

        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//button[contains(text(),'"+"PROCEED TO SET "+"')]")));
        } catch (NoSuchElementException e) {
            System.out.println(e.toString());
        }

        driver.findElement(By.name("firstName")).sendKeys(firstname + s);
        driver.findElement(By.name("lastName")).sendKeys(lastname + s);
        driver.findElement(By.name("affiliation")).sendKeys(affiliation + s);
        WebElement dropdown = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div/form/div[2]/div[2]/div[2]/div[1]"));
        dropdown.click();
        WebElement dpd = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div/form/div[2]/div[2]/div[2]/div[1]/div[2]/div/div/div[6]"));
        dpd.click();
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div/form/div[3]/div[1]/label/span")).click();
        driver.findElement(By.xpath(".//button[contains(text(),'"+"CONFIRM & PROCEED TO SET PASSWORD"+"')]")).click();

        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//button[contains(text(),'"+"BACK"+"')]")));
        } catch (NoSuchElementException e) {
            System.out.println(e.toString());
        }

        driver.findElement(By.name("email")).sendKeys(email);
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.name("confirmNewPassword")).sendKeys(password);

        driver.findElement(By.xpath(".//button[contains(text(),'"+"CONFIRM"+"')]")).click();

    }

    public static void  scrollToElement(WebDriver driver, WebElement el) {
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor) driver)
                    .executeScript("arguments[0].scrollIntoView(true);", el);
        }
    }

    public static String mID(String url)
    {
        String txt=url;

        String fragmentID = txt.split("/versions/")[1].split("/")[0];

        return fragmentID;
    }

    public static String pID(String url)
    {
        String txt=url;

        String projectID = txt.split("/projects/")[1].split("/")[0];

        return projectID;
    }

    public static void filterLatest(WebDriver driver){

        driver.findElement(By.cssSelector("[data-test-id='dashboard-filters'] [data-test-id='item']:nth-of-type(2) button")).click();
        driver.findElement(By.cssSelector("[data-test-id='dashboard-filters'] [data-test-id='item']:nth-of-type(1) div[role=\"option\"]:nth-child(1)")).click();

        driver.findElement(By.cssSelector("[data-test-id='dashboard-filters'] [data-test-id='item']:nth-of-type(2) button")).click();
        driver.findElement(By.cssSelector("[data-test-id='dashboard-filters'] [data-test-id='item']:nth-of-type(2) div[role=\"option\"]:nth-child(1)")).click();

    }

}

