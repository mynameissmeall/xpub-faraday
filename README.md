
## xPub-faraday  

An MVP implementation of the first design sessions which allows a user to go through the process of creating a submission, assigning editors and reviewers, submitting reviews and submitting a decision.  

## Roadmap

The major tasks we're planning to work on are the following: 
* Implement a future-proof theming setup.
* Let users go through multiple rounds of review.
* Implement roles and permissions.
* Change the data model to account for the changes that have occured in `pubsweet-server`, as well as to make it easily portable to Postgres in the future.
* Merge xpub's authentication, routing and navigation with pubsweet's.

You can follow more fine-grained lists of things that we're working on  
* [Faraday](https://gitlab.coko.foundation/xpub/xpub-faraday/boards) for tasks related to `xpub-faraday` and  
* [Xpub](https://gitlab.coko.foundation/xpub/xpub/boards?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=Xpub) for more general xpub concerns

## Installing

In the root directory, run `yarn` to install all the dependencies. 

## Configuration
Add the following values to `packages/xpub-collabra/config/local-development.json`

```json
{
  "pubsweet-server": {
    "secret": "__EDIT_THIS__"
  }
}
```

xPub-faraday is using external services as AWS, MTS-FTP, Publons, ORCID. In order to run the app locally a `.env` file is mandatory with keys and settings for each service.

Contact us at technology@hindawi.com for help getting setup.

## Running the app

1. Open Docker engine
2. `cd packages/xpub-faraday`
3. start services with `yarn services`
3. The first time you run the app, initialize the database with `yarn run setupdb` (press Enter when asked for a collection title, to skip that step).
3. `yarn start`


## Community

Join [the Mattermost channel](https://mattermost.coko.foundation/coko/channels/xpub) for discussion of xpub.


## Migrations

1. use folder `./packages/xpub-faraday/migrations`
2. add file named `${number}-${name}(.sql/.js)` e.g. `001-user-add-name-field.sql`
