const { get } = require('lodash')
const {
  services,
  Collection,
  authsome: authsomeHelper,
} = require('pubsweet-component-helper-service')

module.exports = models => async (req, res) => {
  const { collectionId } = req.params

  try {
    const collection = await models.Collection.find(collectionId)
    const collectionStatus = get(collection, 'status')
    const authsome = authsomeHelper.getAuthsome(models)
    const target = {
      path: req.route.path,
    }
    const canArchive = await authsome.can(req.user, 'PATCH', target)

    if (!canArchive) {
      return res.status(403).json({
        error: 'Unauthorized',
      })
    }

    if (!collectionStatus) {
      return res.status(400).json({
        error: 'You cannot delete manuscripts while in draft.',
      })
    }

    if (collectionStatus === 'deleted') {
      return res.status(400).json({
        error: 'Manuscript already deleted',
      })
    }

    const collectionHelper = new Collection({ collection })
    await collectionHelper.updateStatus({ newStatus: 'deleted' })

    return res.status(200).json(collection)
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'Collection')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }
}
