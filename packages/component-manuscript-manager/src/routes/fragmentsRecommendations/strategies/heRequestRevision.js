module.exports = {
  execute: async ({
    userId,
    notification,
    fragmentHelper,
    collectionHelper,
    newRecommendation,
  }) => {
    const latestUserRecommendation = fragmentHelper.getLatestUserRecommendation(
      userId,
    )
    if (
      latestUserRecommendation &&
      !fragmentHelper.canHEMakeAnotherRecommendation(latestUserRecommendation)
    ) {
      throw new Error('Cannot make another recommendation on this version.')
    }

    await fragmentHelper.addRevision()
    await collectionHelper.updateStatus({ newStatus: 'revisionRequested' })
    await fragmentHelper.addRecommendation(newRecommendation)

    notification.notifySAWhenHERequestsRevision()
    notification.notifyEiCWhenHEMakesRecommendation()

    if (fragmentHelper.hasReviewers()) {
      notification.notifyReviewersWhenHEMakesRecommendation()
    }
  },
}
