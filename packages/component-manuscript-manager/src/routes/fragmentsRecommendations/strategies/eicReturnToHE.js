module.exports = {
  execute: async ({
    notification,
    fragmentHelper,
    collectionHelper,
    newRecommendation,
  }) => {
    const latestRecommendation = fragmentHelper.getLatestRecommendation()
    if (latestRecommendation.recommendation === 'return-to-handling-editor') {
      throw new Error('Cannot return to Handling Editor again.')
    }

    if (collectionHelper.hasEQA()) {
      await collectionHelper.removeTechnicalChecks()
    }
    await collectionHelper.updateStatus({ newStatus: 'reviewCompleted' })

    await fragmentHelper.addRecommendation(newRecommendation)

    notification.notifyHEWhenEiCReturnsToHE()
  },
}
