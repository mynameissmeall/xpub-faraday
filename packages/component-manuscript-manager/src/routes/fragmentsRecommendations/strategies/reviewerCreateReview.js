module.exports = {
  execute: async ({ fragmentHelper, newRecommendation, userId }) => {
    if (fragmentHelper.getLatestUserRecommendation(userId)) {
      throw new Error('Cannot write another review on this version.')
    }

    await fragmentHelper.addRecommendation(newRecommendation)
  },
}
