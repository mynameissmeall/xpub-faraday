const config = require('config')
const { v4 } = require('uuid')

const {
  services,
  Fragment,
  Collection,
  authsome: authsomeHelper,
} = require('pubsweet-component-helper-service')

const { recommendations } = config

const rejectAsHE = require('./strategies/heReject')
const publishAsHE = require('./strategies/hePublish')
const rejectAsEiC = require('./strategies/eicReject')
const publishAsEiC = require('./strategies/eicPublish')
const returnToHE = require('./strategies/eicReturnToHE')
const Notification = require('../../notifications/notification')
const createReview = require('./strategies/reviewerCreateReview')
const requestRevisionAsHE = require('./strategies/heRequestRevision')
const requestRevisionAsEiC = require('./strategies/eicRequestRevision')

module.exports = models => async (req, res) => {
  const { recommendation, comments = [], recommendationType } = req.body
  if (!services.checkForUndefinedParams(recommendationType, recommendation))
    return res.status(400).json({ error: 'Recommendation type is required.' })

  const reqUser = await models.User.find(req.user)
  const userId = reqUser.id

  const isEditorInChief = reqUser.editorInChief || reqUser.admin

  const { collectionId, fragmentId } = req.params

  let collection, fragment

  try {
    collection = await models.Collection.find(collectionId)
    if (!collection.fragments.includes(fragmentId))
      return res.status(400).json({
        error: `Collection and fragment do not match.`,
      })
    fragment = await models.Fragment.find(fragmentId)
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'Item')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }

  const collectionHelper = new Collection({ collection })

  const authsome = authsomeHelper.getAuthsome(models)
  const target = {
    fragment,
    path: req.route.path,
  }
  const canPost = await authsome.can(req.user, 'POST', target)
  if (!canPost)
    return res.status(403).json({
      error: 'Unauthorized.',
    })

  const fragmentHelper = new Fragment({ fragment })

  if (!collectionHelper.isLatestVersion(fragmentId)) {
    const error =
      recommendationType === recommendations.type.editor
        ? 'Cannot make a recommendation on an older version.'
        : 'Cannot write a review on an older version.'
    return res.status(400).json({ error })
  }

  const newRecommendation = {
    userId,
    id: v4(),
    comments,
    recommendation,
    recommendationType,
    createdOn: Date.now(),
    updatedOn: Date.now(),
  }

  const notification = new Notification({
    fragment,
    collection,
    newRecommendation,
    UserModel: models.User,
    baseUrl: services.getBaseUrl(req),
  })

  const strategies = {
    he: {
      reject: rejectAsHE,
      publish: publishAsHE,
      major: requestRevisionAsHE,
      minor: requestRevisionAsHE,
    },
    eic: {
      reject: rejectAsEiC,
      publish: publishAsEiC,
      revision: requestRevisionAsEiC,
      'return-to-handling-editor': returnToHE,
    },
  }

  let role = ''
  switch (recommendationType) {
    case 'review':
      role = 'reviewer'
      try {
        await createReview.execute({
          userId,
          fragmentHelper,
          newRecommendation,
        })
      } catch (e) {
        return res.status(400).json({ error: e.message })
      }
      return res.status(200).json(newRecommendation)
    case 'editorRecommendation':
      role = isEditorInChief ? 'eic' : 'he'
      break
    default:
      return res.status(400).json({
        error: `Recommendation ${recommendation} is not defined.`,
      })
  }

  try {
    await strategies[role][recommendation].execute({
      userId,
      models,
      notification,
      fragmentHelper,
      collectionHelper,
      newRecommendation,
    })
  } catch (e) {
    return res.status(400).json({ error: e.message })
  }

  return res.status(200).json(newRecommendation)
}
