const config = require('config')

const journalName = config.get('journal.name')

const getEmailCopy = ({ emailType, titleText, expectedDate, customId }) => {
  let paragraph
  const hasLink = true
  const hasIntro = true
  const hasSignature = true
  switch (emailType) {
    case 'eqs-manuscript-submitted':
      paragraph = `Manuscript ID ${customId} has been submitted and a package has been sent. Please click on the link below to either approve or reject the manuscript:`
      break
    case 'coauthors-manuscript-submitted':
      paragraph = `${titleText}.<br/><br/>
        To confirm the submission and view the status of the manuscript, please verify your details by clicking the link below.<br/><br/>
        Thank you for submitting your work to ${journalName}.
      `
      break
    case 'submitting-author-manuscript-submitted':
      paragraph = `Congratulations, ${titleText} has been successfully submitted to ${journalName}.<br/><br/>
        We will confirm this submission with all authors of the manuscript, but you will be the primary recipient of communications from the journal.
        As submitting author, you will be responsible for responding to editorial queries and making updates to the manuscript. <br/><br/>
        In order to view the status of the manuscript, please visit the manuscript details page.<br/><br/>
        Thank you for submitting your work to ${journalName}.
      `
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return { paragraph, hasLink, hasIntro, hasSignature }
}

module.exports = {
  getEmailCopy,
}
