const config = require('config')
const { get, tail } = require('lodash')
const Email = require('@pubsweet/component-email-templating')

const {
  User,
  services,
  Fragment,
} = require('pubsweet-component-helper-service')

const resetPath = config.get('invite-reset-password.url')
const { name: journalName, staffEmail } = config.get('journal')
const unsubscribeSlug = config.get('unsubscribe.url')

const { getEmailCopy } = require('./emailCopy')

module.exports = {
  async sendEQSEmail({ baseUrl, fragment, UserModel, collection }) {
    const userHelper = new User({ UserModel })
    const eicName = await userHelper.getEiCName()
    const fragmentHelper = new Fragment({ fragment })
    const { title } = await fragmentHelper.getFragmentData({})

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'eqs-manuscript-submitted',
      customId: collection.customId,
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: staffEmail,
        name: 'Editorial Assistant',
      },
      content: {
        subject: `Manuscript Submitted`,
        paragraph,
        signatureName: eicName,
        signatureJournal: journalName,
        ctaLink: services.createUrl(baseUrl, config.get('eqs-decision.url'), {
          title,
          collectionId: collection.id,
          customId: collection.customId,
          token: collection.technicalChecks.token,
        }),
        ctaText: 'MAKE DECISION',
        unsubscribeLink: baseUrl,
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async sendAuthorsEmail({ baseUrl, fragment, UserModel, collection }) {
    const fragmentHelper = new Fragment({ fragment })
    const collectionOwners = get(collection, 'owners')
    const userHelper = new User({ UserModel })
    const adminOwner = collectionOwners.find(owner => userHelper.isAdmin(owner))
    const handlingEditor = get(collection, 'handlingEditor')
    const parsedFragment = await fragmentHelper.getFragmentData({
      handlingEditor,
    })
    const {
      submittingAuthor,
      activeAuthors: fragmentAuthors,
    } = await fragmentHelper.getAuthorData({
      UserModel,
    })

    const submittingAuthorName = `${submittingAuthor.firstName} ${
      submittingAuthor.lastName
    }`

    let userEmailData = await Promise.all(
      fragmentAuthors.map(async author => {
        const { paragraph, ...bodyProps } = getEmailCopy({
          emailType: author.isSubmitting
            ? 'submitting-author-manuscript-submitted'
            : 'coauthors-manuscript-submitted',
          titleText: author.isSubmitting
            ? `the manuscript titled "${parsedFragment.title}"`
            : `The manuscript titled "${
                parsedFragment.title
              }" has been submitted to ${journalName} by ${submittingAuthorName}`,
        })
        const user = await UserModel.find(author.id)
        return {
          author: Object.assign(author, {
            isConfirmed: user.isConfirmed,
            unsubscribeToken: user.accessTokens.unsubscribe,
            passwordResetToken: user.accessTokens.passwordReset,
          }),
          paragraph,
          bodyProps,
        }
      }),
    )

    if (adminOwner) {
      userEmailData = tail(userEmailData)
    }

    userEmailData.forEach(({ author, paragraph, bodyProps }) => {
      const email = new Email({
        type: 'user',
        fromEmail: `${journalName} <${staffEmail}>`,
        toUser: {
          email: author.email,
          name: `${author.lastName}`,
        },
        content: {
          subject: `Manuscript submitted to ${journalName}`,
          paragraph,
          signatureName: '',
          signatureJournal: journalName,
          ctaLink: services.createUrl(
            baseUrl,
            `/projects/${collection.id}/versions/${fragment.id}/details`,
          ),
          ctaText: 'MANUSCRIPT DETAILS',
          unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
            id: author.id,
            token: author.unsubscribeToken,
          }),
        },
        bodyProps,
      })

      if (author.isSubmitting) {
        email.content.ctaLink = services.createUrl(
          baseUrl,
          `/projects/${collection.id}/versions/${fragment.id}/details`,
        )
        email.content.ctaText = 'MANUSCRIPT DETAILS'
      } else if (author.isConfirmed) {
        email.content.ctaLink = services.createUrl(baseUrl, '')
        email.content.ctaText = 'LOG IN'
      } else {
        email.content.ctaLink = services.createUrl(baseUrl, resetPath, {
          email: author.email,
          token: author.passwordResetToken,
          firstName: author.firstName,
          lastName: author.lastName,
          affiliation: author.affiliation,
          title: author.title.toLowerCase(),
          country: author.country,
        })
        email.content.ctaText = 'CONFIRM ACCOUNT'
      }
      return email.sendEmail()
    })
  },
}
