module.exports = {
  execute: async ({
    models,
    TeamHelper,
    fragmentHelper,
    collectionHelper,
    notification,
    userHelper,
  }) => {
    const eicRequestToRevision = fragmentHelper.getLatestEiCRequestToRevision()
    if (!eicRequestToRevision) {
      throw new Error('No Editor in Chief request to revision has been found.')
    }

    let newFragment = await fragmentHelper.createFragmentFromRevision(
      models.Fragment,
    )

    await fragmentHelper.removeRevision()

    const teamHelper = new TeamHelper({
      TeamModel: models.Team,
      fragmentId: newFragment.id,
    })

    const authorIds = newFragment.authors.map(auth => auth.id)

    const { id: teamId } = await teamHelper.createTeam({
      role: 'author',
      members: authorIds,
      objectType: 'fragment',
    })
    authorIds.forEach(id => {
      userHelper.updateUserTeams({ userId: id, teamId })
    })

    await collectionHelper.updateStatus({ newStatus: 'submitted' })

    newFragment.submitted = Date.now()
    newFragment = await newFragment.save()

    await collectionHelper.addFragment(newFragment.id)

    await notification.notifyEditorInChiefWhenAuthorSubmitsRevision(newFragment)

    return newFragment
  },
}
