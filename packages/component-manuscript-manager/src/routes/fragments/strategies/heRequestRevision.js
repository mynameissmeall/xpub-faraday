module.exports = {
  execute: async ({
    models,
    baseUrl,
    userHelper,
    TeamHelper,
    notification,
    fragmentHelper,
    collectionHelper,
  }) => {
    const heRequestToRevision = fragmentHelper.getLatestHERequestToRevision()
    if (!heRequestToRevision) {
      throw new Error('No Handling Editor request to revision has been found.')
    }

    let newFragment = await fragmentHelper.createFragmentFromRevision(
      models.Fragment,
    )
    await fragmentHelper.removeRevision()

    const teamHelper = new TeamHelper({
      TeamModel: models.Team,
      fragmentId: newFragment.id,
    })

    if (heRequestToRevision.recommendation === 'major') {
      const reviewerIds = newFragment.invitations.map(inv => inv.userId)

      teamHelper.createTeam({
        role: 'reviewer',
        members: reviewerIds,
        objectType: 'fragment',
      })
      newFragment.invitations = newFragment.invitations.map(invitation => ({
        ...invitation,
        hasAnswer: false,
        invitedOn: Date.now(),
        isAccepted: false,
        respondedOn: null,
      }))
      await newFragment.save()
    } else {
      delete newFragment.invitations
      await newFragment.save()
    }

    const authorIds = newFragment.authors.map(auth => auth.id)

    const { id: teamId } = await teamHelper.createTeam({
      role: 'author',
      members: authorIds,
      objectType: 'fragment',
    })
    authorIds.forEach(id => {
      userHelper.updateUserTeams({ userId: id, teamId })
    })

    const fragments = await collectionHelper.getAllFragments({
      FragmentModel: models.Fragment,
    })

    await collectionHelper.updateStatusByRecommendation({
      recommendation: heRequestToRevision.recommendation,
      fragments,
    })

    newFragment.submitted = Date.now()
    newFragment = await newFragment.save()

    await collectionHelper.addFragment(newFragment.id)

    await notification.notifyHandlingEditorWhenAuthorSubmitsRevision(
      newFragment,
    )

    if (heRequestToRevision.recommendation === 'major') {
      await notification.notifyReviewersWhenAuthorSubmitsMajorRevision(
        newFragment,
        baseUrl,
      )
    }

    return newFragment
  },
}
