const {
  Team,
  User,
  services,
  Fragment,
  Collection,
  authsome: authsomeHelper,
} = require('pubsweet-component-helper-service')

const Notification = require('../../notifications/notification')

const eicRequestRevision = require('./strategies/eicRequestRevision')
const heRequestRevision = require('./strategies/heRequestRevision')

module.exports = models => async (req, res) => {
  const { collectionId, fragmentId } = req.params
  let collection, fragment

  try {
    collection = await models.Collection.find(collectionId)
    if (!collection.fragments.includes(fragmentId))
      return res.status(400).json({
        error: `Collection and fragment do not match.`,
      })

    fragment = await models.Fragment.find(fragmentId)
    if (!fragment.revision) {
      return res.status(400).json({
        error:
          'Your Handling Editor was changed. A new handling editor will be assigned to your manuscript soon. Sorry for the inconvenience.',
      })
    }

    const authsome = authsomeHelper.getAuthsome(models)
    const target = {
      fragment,
      path: req.route.path,
    }
    const canPatch = await authsome.can(req.user, 'PATCH', target)
    if (!canPatch)
      return res.status(403).json({
        error: 'Unauthorized.',
      })

    const collectionHelper = new Collection({ collection })
    const fragmentHelper = new Fragment({ fragment })
    const userHelper = new User({ UserModel: models.User })

    const strategies = {
      he: heRequestRevision,
      eic: eicRequestRevision,
    }

    const role = collection.handlingEditor ? 'he' : 'eic'

    const notification = new Notification({
      fragment,
      collection,
      UserModel: models.User,
      baseUrl: services.getBaseUrl(req),
    })

    const baseUrl = services.getBaseUrl(req)

    try {
      const newFragment = await strategies[role].execute({
        models,
        baseUrl,
        userHelper,
        notification,
        fragmentHelper,
        collectionHelper,
        TeamHelper: Team,
      })
      return res.status(200).json(newFragment)
    } catch (e) {
      return res.status(400).json({ error: e.message })
    }
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'Item')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }
}
