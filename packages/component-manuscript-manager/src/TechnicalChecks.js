const bodyParser = require('body-parser')

const TechnicalChecks = app => {
  app.use(bodyParser.json())
  const basePath = '/api/collections/:collectionId/status'
  const routePath = './routes/technicalChecks'

  /**
   * @api {patch} /api/collections/:collectionId/status Take a EQS or EQA decision.
   * @apiGroup TechnicalChecks
   * @apiParam {collectionId} collectionId Collection id
   *   * @apiParamExample {json} Body
   *    {
   *      "token": "12345",
   *      "agree": true,
   *      "step": "EQS"
   *    }
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *   }
   * @apiErrorExample {json} Invite user errors
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   *    HTTP/1.1 500 Internal Server Error
   */
  app.patch(basePath, require(`${routePath}/patch`)(app.locals.models))
}

module.exports = TechnicalChecks
