const bodyParser = require('body-parser')

const Collections = app => {
  app.use(bodyParser.json())
  const routePath = './routes/collections'
  const authBearer = app.locals.passport.authenticate('bearer', {
    session: false,
  })
  /**
   * @api {get} /api/collections Get latest collections
   * @apiGroup Collections
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    [
   *      {
   *        "type": "collection",
   *        "status": "draft",
   *        ...,
   *        currentVersion:
   *        {
   *          "type": "fragment",
   *          "files": {},
   *          "owners": [
   *            "2494cbdf-06a8-4403-999b-ae93e23f32c8",
   *            "ed39345d-a95f-46d8-b5e1-75a2744beefe"
   *          ],
   *          ...
   *        }
   *      }
   *    ]
   * @apiErrorExample {json} Get collections errors
   *    HTTP/1.1 403 Forbidden
   */
  app.get(
    '/api/collections',
    authBearer,
    require(`${routePath}/get`)(app.locals.models),
  )
  app.patch(
    '/api/collections/:collectionId/archive',
    authBearer,
    require(`${routePath}/patch`)(app.locals.models),
  )

  /**
   * @api {delete} /api/collections/:collectionId Delete collection with specified id
   * @apiGroup Collections
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 204 Accepted
   * @apiErrorExample {json} Get collections errors
   *    HTTP/1.1 403 Forbidden
   */
  app.delete(
    '/api/collections/:collectionId',
    authBearer,
    require(`${routePath}/delete`)(app.locals.models),
  )
}

module.exports = Collections
