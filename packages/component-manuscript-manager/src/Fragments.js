const bodyParser = require('body-parser')

const Fragments = app => {
  app.use(bodyParser.json())
  const basePath = '/api/collections/:collectionId/fragments/:fragmentId/submit'
  const routePath = './routes/fragments'
  const authBearer = app.locals.passport.authenticate('bearer', {
    session: false,
  })
  /**
   * @api {patch} /api/collections/:collectionId/fragments/:fragmentId/submit Submit a revision for a manuscript
   * @apiGroup Fragments
   * @apiParam {collectionId} collectionId Collection id
   * @apiParam {fragmentId} fragmentId Fragment id
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *
   *   }
   * @apiErrorExample {json} Invite user errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   *    HTTP/1.1 500 Internal Server Error
   */
  app.patch(
    `${basePath}`,
    authBearer,
    require(`${routePath}/patch`)(app.locals.models),
  )
  /**
   * @api {post} /api/collections/:collectionId/fragments/:fragmentId/submit Submit a manuscript
   * @apiGroup Fragments
   * @apiParam {collectionId} collectionId Collection id
   * @apiParam {fragmentId} fragmentId Fragment id
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *
   *   }
   * @apiErrorExample {json} Invite user errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   *    HTTP/1.1 500 Internal Server Error
   */
  app.post(
    `${basePath}`,
    authBearer,
    require(`${routePath}/post`)(app.locals.models),
  )
}

module.exports = Fragments
