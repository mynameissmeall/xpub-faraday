const { get } = require('lodash')

module.exports = {
  getPrivateNoteTextForAuthor: ({ newRecommendation }) => {
    const authorNote = newRecommendation.comments.find(comm => comm.public)
    const content = get(authorNote, 'content')

    return content ? `Reason & Details: "${content}"` : ''
  },
  getHEComments: ({ heRecommendation }) => {
    const heComments = get(heRecommendation, 'comments', [])

    if (heComments.length === 0) return

    const publicComment = heComments.find(comm => comm.public)

    const content = get(publicComment, 'content')
    if (!content) {
      throw new Error('a public comment cannot be without content')
    }

    return `Please find our editorial comments below.<br/> "${content}"`
  },
  getEmailTypeByRecommendationForAuthors: ({
    recommendation,
    hasPeerReview,
  }) => {
    let emailType
    switch (recommendation) {
      case 'publish':
        emailType = 'author-manuscript-published'
        break
      case 'reject':
        emailType = hasPeerReview
          ? 'author-manuscript-rejected'
          : 'authors-manuscript-rejected-before-review'
        break
      default:
        throw new Error(`Undefined recommendation: ${recommendation}`)
    }

    return emailType
  },
}
