module.exports = {
  _declaration: {
    _attributes: {
      version: '1.0',
      encoding: 'utf-8',
    },
  },
  _doctype: 'article SYSTEM "JATS-archivearticle1-mathml3.dtd"',
  article: {
    _attributes: {
      'dtd-version': '1.1d1',
      'article-type': 'Research Article',
    },
    front: {
      'journal-meta': {
        'journal-id': [
          {
            _attributes: {
              'journal-id-type': 'publisher',
            },
            _text: 'research',
          },
          {
            _attributes: {
              'journal-id-type': 'email',
            },
            _text: 'faraday@hindawi.com',
          },
        ],
        'journal-title-group': {
          'journal-title': {
            _text: 'Faraday Journal',
          },
        },
        issn: [
          {
            _attributes: {
              'pub-type': 'ppub',
            },
            _text: '2474-7394',
          },
          {
            _attributes: {
              'pub-type': 'epub',
            },
          },
        ],
      },
      'article-meta': {
        'article-id': [
          {
            _attributes: {
              'pub-id-type': 'publisher-id',
            },
            _text: 'FARADAY-D-00-00000',
          },
          {
            _attributes: {
              'pub-id-type': 'manuscript',
            },
            _text: 'FARADAY-D-00-00000',
          },
        ],
        'article-categories': {
          'subj-group': [
            {
              _attributes: {
                'subj-group-type': 'Article Type',
              },
              subject: {
                _text: 'Research Article',
              },
            },
            {
              _attributes: {
                'subj-group-type': 'Category',
              },
              subject: {
                _text: 'Information science',
              },
            },
            {
              _attributes: {
                'subj-group-type': 'Classification',
              },
              subject: {
                _text: 'Applied sciences and engineering',
              },
            },
            {
              _attributes: {
                'subj-group-type': 'Classification',
              },
              subject: {
                _text: 'Scientific community',
              },
            },
          ],
        },
        'title-group': {
          'article-title': {
            _text: 'Untitled Article Title ',
          },
          'alt-title': {
            _attributes: {
              'alt-title-type': 'running-head',
            },
            _text: "let's hope this works",
          },
        },
        'contrib-group': {
          contrib: {
            _attributes: {
              'contrib-type': 'author',
              corresp: 'yes',
            },
            role: {
              _attributes: {
                'content-type': '1',
              },
            },
            name: {
              surname: {
                _text: 'Heckner',
              },
              'given-names': {
                _text: 'Hannah',
              },
              prefix: {
                _text: 'Ms.',
              },
            },
            email: {
              _text: 'hheckner@aaas.org',
            },
            xref: {
              _attributes: {
                'ref-type': 'aff',
                rid: 'aff1',
              },
            },
          },
          aff: {
            _attributes: {
              id: 'aff1',
            },
            country: {
              _text: 'UNITED STATES',
            },
          },
        },
        history: {
          date: {
            _attributes: {
              'date-type': 'received',
            },
            day: {
              _text: '24',
            },
            month: {
              _text: '01',
            },
            year: {
              _text: '2018',
            },
          },
        },
        abstract: {
          p: {
            _text: 'No abstract provided',
          },
        },
        'kwd-group': {
          kwd: [
            {
              _text: 'manuscript',
            },
            {
              _text: 'submissions',
            },
            {
              _text: 'ftp site',
            },
          ],
        },
        'funding-group': {},
        counts: {
          'fig-count': {
            _attributes: {
              count: '0',
            },
          },
        },
        'custom-meta-group': {
          'custom-meta': [
            {
              'meta-name': {
                _text: 'Black and White Image Count',
              },
              'meta-value': {
                _text: '0',
              },
            },
            {
              'meta-name': {
                _text: 'Color Image Count',
              },
              'meta-value': {
                _text: '0',
              },
            },
          ],
        },
      },
    },
    body: {
      fig: [
        {
          label: {
            _text: 'Figure 1',
          },
          graphic: {
            _attributes: {
              'xlink:href': 'GasBulbData.eps',
              'xmlns:xlink': 'http://www.w3.org/1999/xlink',
            },
          },
        },
        {
          label: {
            _text: 'Figure 2',
          },
          graphic: {
            _attributes: {
              'xlink:href': 'ThreeSunsets.jpg',
              'xmlns:xlink': 'http://www.w3.org/1999/xlink',
            },
          },
        },
      ],
    },
  },
}
