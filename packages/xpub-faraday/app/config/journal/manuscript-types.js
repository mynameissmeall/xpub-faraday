module.exports = [
  {
    label: 'Research Article',
    value: 'research',
    author: true,
    peerReview: true,
    abstractRequired: true,
  },
  {
    label: 'Review Article',
    value: 'review',
    author: true,
    peerReview: true,
    abstractRequired: true,
  },
  {
    label: 'Letter to the editor',
    value: 'letter-to-editor',
    author: true,
    peerReview: false,
    abstractRequired: false,
  },
]
