module.exports = {
  questions: [
    {
      id: 'conflictsOfInterest',
      commentsFieldName: 'conflicts.message',
      commentsSubtitle: '',
      subtitle: null,
      commentsPlaceholder: 'Please provide the conflicts of interest',
      commentsOn: 'yes',
      radioFieldName: 'conflicts.hasConflicts',
      radioLabel: 'Do any authors have conflicts of interest to declare?',
      required: true,
    },
    {
      id: 'dataAvailability',
      commentsFieldName: 'conflicts.dataAvailabilityMessage',
      commentsSubtitle:
        'For more information about the data availability statement, please ',
      subtitle: {
        link: 'https://www.hindawi.com/journals/bca/guidelines/',
        label: 'click here',
      },
      commentsPlaceholder:
        'Please provide a statement describing the availability of the underlying data related to your submission',
      commentsOn: 'no',
      radioFieldName: 'conflicts.hasDataAvailability',
      radioLabel:
        'Have you included a data availability statement in your manuscript?',
      required: false,
    },
    {
      id: 'funding',
      commentsFieldName: 'conflicts.fundingMessage',
      commentsSubtitle: '',
      commentsPlaceholder:
        'Please provide a statement describing how the research and publication of your article is funded',
      subtitle: null,
      commentsOn: 'no',
      radioFieldName: 'conflicts.hasFunding',
      radioLabel: 'Have you provided a funding statement in your manuscript?',
      required: false,
    },
  ],
  links: {
    authorGuidelines: 'https://www.hindawi.com/journals/bca/guidelines/',
    articleProcessing: 'https://www.hindawi.com/journals/bca/apc/',
  },
}
