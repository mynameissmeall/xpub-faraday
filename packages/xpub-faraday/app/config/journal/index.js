const metadata = require('./metadata')
const decisions = require('./decisions')
const recommendations = require('./recommendations')
const sections = require('./sections')
const roles = require('./roles')
const issueTypes = require('./issues-types')
const manuscriptTypes = require('./manuscript-types')
const title = require('./title')
const wizard = require('./wizard')
const submission = require('./submission')
const statuses = require('./statuses')
const rolesAccess = require('./roles-access')

module.exports = {
  statuses,
  metadata,
  recommendations,
  decisions,
  sections,
  roles,
  issueTypes,
  manuscriptTypes,
  title,
  wizard,
  submission,
  rolesAccess,
}
