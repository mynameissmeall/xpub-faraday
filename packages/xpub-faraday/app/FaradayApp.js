import React from 'react'
import { get } from 'lodash'
import { connect } from 'react-redux'
import styled, { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withJournal } from 'xpub-journal'
import { withRouter } from 'react-router-dom'
import { compose, withHandlers } from 'recompose'
import { logoutUser } from 'pubsweet-component-login/actions'
import {
  Logo,
  AppBar,
  AppBarMenu,
  AutosaveIndicator,
  Footer,
} from 'pubsweet-component-faraday-ui'
import { userNotConfirmed } from 'pubsweet-component-faraday-selectors'
import { createDraftSubmission } from 'pubsweet-component-wizard/src/redux/conversion'

const App = ({
  autosave,
  journal = {},
  goTo,
  children,
  logout,
  currentUser,
  ...rest
}) => (
  <Root className="faraday-root">
    <AppBar
      autosave={() => (
        <AutosaveIndicator
          autosave={autosave}
          delay={5000}
          successText="Progress Saved"
        />
      )}
      currentUser={currentUser}
      journal={journal}
      logo={() => (
        <Logo
          height={54}
          onClick={() => goTo('/')}
          src={get(journal, 'metadata.logo')}
          title="Hindawi"
        />
      )}
      menu={() => (
        <AppBarMenu currentUser={currentUser} goTo={goTo} logout={logout} />
      )}
      {...rest}
    />
    <MainContainer
      canCreateDraft={rest.canCreateDraft}
      className="faraday-main"
    >
      {children}
    </MainContainer>
    <Footer />
  </Root>
)

export default compose(
  withRouter,
  withJournal,
  connect(
    state => ({
      autosave: state.autosave,
      currentUser: state.currentUser,
      canCreateDraft: !userNotConfirmed(state),
    }),
    (dispatch, { history }) => ({
      logout: () => dispatch(logoutUser()),
      createDraft: () => dispatch(createDraftSubmission(history)),
    }),
  ),
  withHandlers({
    goTo: ({ history }) => path => {
      history.push(path)
    },
  }),
)(App)

// #region styles
const Root = styled.div`
  height: 100%;
  overflow-y: auto;
  div[open] {
    width: auto;
  }
`

const appBarPaddingHelper = props =>
  props.canCreateDraft
    ? css`
        padding-top: calc(${th('appBar.height')} + ${th('gridUnit')});
        padding-right: calc(${th('gridUnit')} * 12);
        padding-left: calc(${th('gridUnit')} * 12);
        padding-bottom: calc(${th('gridUnit')} * 5);

        @media (max-width: 1200px) {
          padding-right: calc(${th('gridUnit')} * 2);
          padding-left: calc(${th('gridUnit')} * 2);
        }
      `
    : css`
        padding-top: calc(${th('appBar.height')} + ${th('gridUnit')} * 4);
        padding-right: calc(${th('gridUnit')} * 12);
        padding-left: calc(${th('gridUnit')} * 12);
        padding-bottom: calc(${th('gridUnit')} * 5);

        @media (max-width: 1200px) {
          padding-right: calc(${th('gridUnit')} * 2);
          padding-left: calc(${th('gridUnit')} * 2);
        }
      `

const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: visible;
  ${appBarPaddingHelper};
`
// #endregion
