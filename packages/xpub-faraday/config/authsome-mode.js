const config = require('config')
const { get, pickBy, last, has, pick } = require('lodash')

const statuses = config.get('statuses')
const helpers = require('./authsome-helpers')

function unauthenticatedUser(operation, object, userId) {
  // Public/unauthenticated users can GET /collections, filtered by 'published'
  if (operation === 'GET' && object && object.path === '/collections') {
    return {
      filter: collections =>
        collections.filter(collection => collection.published),
    }
  }

  // Public/unauthenticated users can GET /collections/:id/fragments, filtered by 'published'
  if (
    operation === 'GET' &&
    get(object, 'path') === '/collections/:id/fragments'
  ) {
    return {
      filter: fragments => fragments.filter(fragment => fragment.published),
    }
  }

  // and filtered individual collection's properties: id, title, source, content, owners
  if (operation === 'GET' && object && object.type === 'collection') {
    if (object.published) {
      return {
        filter: collection =>
          pickBy(collection, (_, key) =>
            ['id', 'title', 'owners'].includes(key),
          ),
      }
    }
  }

  if (operation === 'GET' && object && object.type === 'fragment') {
    if (object.published) {
      return {
        filter: fragment =>
          pickBy(fragment, (_, key) =>
            ['id', 'title', 'source', 'presentation', 'owners'].includes(key),
          ),
      }
    }
  }

  if (operation === 'PATCH') {
    // let unauthorized reviewers decline the review invitation
    if (get(object, 'type') === 'invitation') {
      return true
    }
  }

  // allow users to authenticate
  if (
    operation === 'POST' &&
    get(object, 'type') === 'user' &&
    get(object, 'id') === userId
  ) {
    return true
  }

  return false
}

const isCollectionInStatuses = (c, statuses) =>
  statuses.includes(get(c, 'status', 'draft'))

const filterCollectionInStatuses = (statuses = []) => c =>
  !statuses.includes(get(c, 'status', 'draft'))

const filterNoFragmentCollections = c => c.fragments.length !== 0

const createPaths = ['/collections', '/collections/:collectionId/fragments']

async function applyAuthenticatedUserPolicy(user, operation, object, context) {
  if (operation === 'GET') {
    if (get(object, 'path') === '/api/users') {
      return helpers.getUsersList({ UserModel: context.models.User, user })
    }

    if (get(object, 'type') === 'collection') {
      if (isCollectionInStatuses(object, ['draft', 'technicalChecks'])) {
        if (!helpers.isOwner({ user, object })) {
          return false
        }
      }
      return {
        filter: async collection => {
          const userPermissions = await helpers.getUserPermissions({
            user,
            Team: context.models.Team,
          })
          const fragmentPermissions = userPermissions
            .filter(
              up => up.objectType === 'fragment' && up.role === 'reviewer',
            )
            .map(up => up.objectId)

          const collPermission = userPermissions.find(
            p =>
              p.objectId === collection.id ||
              collection.fragments.includes(p.objectId),
          )
          const role = get(collPermission, 'role', 'author')

          const parsedStatuses = await helpers.setCollectionStatus({
            role,
            user,
            collection,
            FragmentModel: context.models.Fragment,
          })

          const parsedCollection = helpers.stripeCollectionByRole({
            collection,
            role,
          })

          return {
            ...parsedCollection,
            ...parsedStatuses,
            fragments:
              role !== 'reviewer'
                ? collection.fragments
                : collection.fragments.filter(fragId =>
                    fragmentPermissions.includes(fragId),
                  ),
          }
        },
      }
    }

    if (get(object, 'type') === 'fragment') {
      if (helpers.isInDraft(object)) {
        return helpers.isOwner({ user, object })
      }

      const userPermissions = await helpers.getUserPermissions({
        user,
        Team: context.models.Team,
      })

      const permission = userPermissions.find(
        p => p.objectId === object.id || p.objectId === object.collectionId,
      )

      if (!permission) return false
      const collectionId = get(object, 'collectionId')
      const collection = await context.models.Collection.find(collectionId)

      if (isCollectionInStatuses(collection, ['deleted'])) return false

      return {
        filter: fragment =>
          helpers.stripeFragmentByRole({
            fragment,
            role: permission.role,
            status: collection.status,
            user,
            isLast: last(collection.fragments) === fragment.id,
          }),
      }
    }

    // allow HE to get reviewer invitations
    if (get(object, 'fragment.type') === 'fragment') {
      const collectionId = get(object, 'fragment.collectionId')
      const collection = await context.models.Collection.find(collectionId)

      if (get(collection, 'handlingEditor.id') === user.id) {
        return true
      }
    }

    if (get(object, 'type') === 'user') {
      return helpers.parseUser({ user: object })
    }

    if (get(object, 'path') === '/api/collections') {
      return helpers.getCollections({
        user,
        models: context.models,
      })
    }

    if (get(object, 'path', '') === '/teams') {
      return {
        filter: teams => teams.filter(t => user.teams.includes(t.id)),
      }
    }
  }

  if (operation === 'POST') {
    // allow everyone to create manuscripts and versions
    if (createPaths.includes(object.path)) {
      return true
    }

    // allow HE to invite
    if (
      get(object, 'path') ===
      '/api/collections/:collectionId/fragments/:fragmentId/invitations'
    ) {
      return helpers.isHandlingEditor({ user, object })
    }

    // allow HE or assigned reviewers to recommend
    if (
      get(object, 'path') ===
      '/api/collections/:collectionId/fragments/:fragmentId/recommendations'
    ) {
      return helpers.hasPermissionForObject({
        user,
        object,
        Team: context.models.Team,
        roles: ['reviewer', 'handlingEditor'],
      })
    }

    // allow owner to submit a manuscript
    if (
      get(object, 'path') ===
      '/api/collections/:collectionId/fragments/:fragmentId/submit'
    ) {
      return helpers.isOwner({ user, object: object.fragment })
    }

    // allow user to authenticate itself
    if (
      get(object, 'type') === 'user' &&
      get(object, 'id') === get(user, 'id')
    ) {
      return true
    }
  }

  if (operation === 'PATCH') {
    if (get(object.current, 'type') === 'collection') {
      return helpers.isOwner({ user, object: object.current })
    }

    if (get(object.current, 'type') === 'fragment') {
      return helpers.isOwner({ user, object: object.current })
    }

    // allow reviewer to patch (accept/decline) his invitation
    if (get(object, 'type') === 'invitation') {
      return user.id === object.userId
    }

    // allow reviewer to patch his recommendation
    if (
      get(object, 'path') ===
      '/api/collections/:collectionId/fragments/:fragmentId/recommendations/:recommendationId'
    ) {
      return helpers.hasPermissionForObject({
        user,
        object,
        Team: context.models.Team,
        roles: ['reviewer'],
      })
    }

    if (
      get(object, 'current.type') === 'user' &&
      get(object, 'current.id') === user.id
    ) {
      return {
        filter: body =>
          pick(body, [
            'title',
            'country',
            'lastName',
            'firstName',
            'affiliation',
            'notifications',
          ]),
      }
    }

    // allow owner to submit a revision
    if (
      get(object, 'path') ===
      '/api/collections/:collectionId/fragments/:fragmentId/submit'
    ) {
      return helpers.isOwner({ user, object: object.fragment })
    }
  }

  if (operation === 'DELETE') {
    if (
      get(object, 'path') ===
      '/api/collections/:collectionId/fragments/:fragmentId/invitations/:invitationId'
    ) {
      return helpers.isHandlingEditor({ user, object })
    }

    if (get(object, 'type') === 'collection') {
      return helpers.isOwner({ user, object })
    }
  }

  // If no individual permissions exist (above), fallback to unauthenticated
  // user's permission
  return unauthenticatedUser(operation, object, user.id)
}

async function applyAdminPolicy(user, operation, object, context) {
  if (operation === 'GET') {
    if (get(object, 'type') === 'collection') {
      return {
        filter: collection => ({
          ...collection,
          visibleStatus: get(statuses, `${collection.status}.admin.label`),
        }),
      }
    }

    if (get(object, 'type') === 'fragment') {
      const collection = await context.models.Collection.find(
        get(object, 'collectionId'),
      )
      if (isCollectionInStatuses(collection, ['deleted'])) return false
    }

    if (get(object, 'path') === '/api/users') {
      return helpers.getUsersList({ UserModel: context.models.User, user })
    }

    if (get(object, 'type') === 'user') {
      return helpers.parseUser({ user: object })
    }

    if (get(object, 'path') === '/api/collections') {
      const collections = await context.models.Collection.all()
      return Promise.all(
        collections
          .filter(filterCollectionInStatuses(['deleted']))
          .filter(filterNoFragmentCollections)
          .map(async coll => {
            const latestFragmentId = coll.fragments[coll.fragments.length - 1]
            coll.currentVersion = await context.models.Fragment.find(
              latestFragmentId,
            )
            const status = get(coll, 'status', 'draft')
            coll.visibleStatus = get(statuses, `${status}.admin.label`)
            return coll
          }),
      )
    }
  }
  if (operation === 'PATCH') {
    if (get(object, 'current.type') === 'collection') {
      return !isCollectionInStatuses(get(object, 'current'), [
        'rejected',
        'accepted',
        'withdrawn',
      ])
    }
    if (get(object, 'current.type') === 'fragment') {
      const collection = await context.models.Collection.find(
        get(object, 'current.collectionId'),
      )
      return (
        helpers.isLastFragment(collection, get(object, 'current')) &&
        !isCollectionInStatuses(collection, [
          'rejected',
          'accepted',
          'withdrawn',
        ])
      )
    }
  }
  return true
}

async function applyEditorInChiefPolicy(user, operation, object, context) {
  if (operation === 'GET') {
    if (get(object, 'type') === 'collection') {
      if (isCollectionInStatuses(object, ['draft', 'technicalChecks']))
        return false
      return {
        filter: collection => ({
          ...collection,
          visibleStatus: get(
            statuses,
            `${collection.status}.editorInChief.label`,
          ),
        }),
      }
    }

    if (get(object, 'type') === 'fragment') {
      const collection = await context.models.Collection.find(
        get(object, 'collectionId'),
      )
      if (
        isCollectionInStatuses(collection, [
          'draft',
          'technicalChecks',
          'deleted',
        ])
      )
        return false
    }

    if (get(object, 'path') === '/api/users') {
      return helpers.getUsersList({ UserModel: context.models.User, user })
    }

    if (get(object, 'type') === 'user') {
      return helpers.parseUser({ user: object })
    }

    if (get(object, 'path') === '/api/collections') {
      const collections = await context.models.Collection.all()
      return Promise.all(
        collections
          .filter(
            filterCollectionInStatuses(['draft', 'technicalChecks', 'deleted']),
          )
          .filter(filterNoFragmentCollections)
          .map(async coll => {
            const latestFragmentId = coll.fragments[coll.fragments.length - 1]
            coll.currentVersion = await context.models.Fragment.find(
              latestFragmentId,
            )
            const status = get(coll, 'status', 'draft')
            coll.visibleStatus = get(statuses, `${status}.editorInChief.label`)
            return coll
          }),
      )
    }
  }
  if (operation === 'PATCH') {
    if (get(object, 'current.type') === 'collection') {
      return false
    }
    if (get(object, 'current.type') === 'fragment') {
      return false
    }
    if (get(object, 'path') === '/api/collections/:collectionId/archive') {
      return false
    }
  }
  if (operation === 'POST') {
    if (
      get(object, 'path') ===
      '/api/collections/:collectionId/fragments/:fragmentId/invitations'
    )
      return false
  }
  if (operation === 'DELETE') {
    if (
      get(object, 'path') ===
      '/api/collections/:collectionId/fragments/:fragmentId/invitations/:invitationId'
    )
      return false
  }
  return true
}

const authsomeMode = async (userId, operation, object, context) => {
  if (get(object, 'targetUser')) {
    if (has(object, 'targetUser.isActive')) {
      return !!object.targetUser.isActive
    }

    return true
  }

  if (!userId) {
    return unauthenticatedUser(operation, object, userId)
  }

  // It's up to us to retrieve the relevant models for our
  // authorization/authsome mode, e.g.
  const user = await context.models.User.find(userId)

  if (get(user, 'admin')) {
    return applyAdminPolicy(user, operation, object, context)
  }

  if (get(user, 'editorInChief')) {
    return applyEditorInChiefPolicy(user, operation, object, context)
  }

  if (user && user.isActive !== false) {
    return applyAuthenticatedUserPolicy(user, operation, object, context)
  }

  return false
}

module.exports = authsomeMode
