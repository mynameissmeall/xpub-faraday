## Hindawi Reviewer Invite HOC.

Injects `onInviteReviewer`, `onInvitePublonReviewer`, `onResendInviteReviewer`, `onRevokeInviteReviewer` and `onReviewerResponse` handlers as props.

### withInviteReviewer props

`inviteReviewer` namespace contains the following fields:

| Name                   | Type                                   | Description                                          |
| :--------------------- | :------------------------------------- | :--------------------------------------------------- |
| onInviteReviewer       | `(reduxFormValues, modalProps) => any` | sends an invitation to the reviewer                  |
| onInvitePublonReviewer | `(reduxFormValues, modalProps) => any` | sends an invitation to a Publon reviewer             |
| onResendInviteReviewer | `(email, modalProps) => any`           | resends an invitation to an already invited reviewer |
| onRevokeInviteReviewer | `(invitationId, modalProps) => any`    | cancels an invitation to an invited reviewer         |
| onReviewerResponse     | `(reduxFormValues, modalProps) => any` | handles the reviewer response to the invitation      |

_Note: The functions must be used withing a modal_

```javascript
const InviteReviewer = ({
  onInviteReviewer,
  onInvitePublonReviewer,
  onResendInviteReviewer,
  onRevokeInviteReviewer,
}) => (
  <Modal>
    <span>Reviewers list</span>
    <div>
      <span>Revi Ewerin</span>
      <button
        onClick={() => onInviteReviewer(values, { ...modalProps, setFetching })}
      >
        Invite
      </button>
    </div>
    <div>
      <span>Publonus re' Vyewer</span>
      <button
        onClick={() =>
          onInvitePublonReviewer(reviewerData, { ...modalProps, setFetching })
        }
      >
        Invite
      </button>
    </div>
    <div>
      <span>Rev d'Iewer</span>
      <button
        onClick={() =>
          onResendInviteReviewer(email, { ...modalProps, setFetching })
        }
      >
        Resend invitation
      </button>
      <button
        onClick={() =>
          onRevokeInviteReviewer(invitationId, { ...modalProps, setFetching })
        }
      >
        Cancel invitation
      </button>
    </div>
  </Modal>
)

const Invitation = ({ onReviewerResponse }) => (
  <Modal>
    <span>Accept invitation?</span>
    <button
      onClick={() =>
        onReviewerResponse(reduxFormValues, { ...modalProps, setFetching })
      }
    >
      Yes
    </button>
    <button
      onClick={() =>
        onReviewerResponse(reduxFormValues, { ...modalProps, setFetching })
      }
    >
      No
    </button>
  </Modal>
)
```
