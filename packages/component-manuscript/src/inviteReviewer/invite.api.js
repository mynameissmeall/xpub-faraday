import { create, remove, update } from 'pubsweet-client/src/helpers/api'

export const inviteReviewer = ({
  fragmentId,
  reviewerData,
  collectionId,
  isPublons = false,
}) =>
  create(`/collections/${collectionId}/fragments/${fragmentId}/invitations`, {
    ...reviewerData,
    role: 'reviewer',
    isPublons,
  })

export const revokeReviewer = ({ fragmentId, collectionId, invitationId }) =>
  remove(
    `/collections/${collectionId}/fragments/${fragmentId}/invitations/${invitationId}`,
  )

export const reviewerDecision = ({
  fragmentId,
  agree = true,
  collectionId,
  invitationId,
}) =>
  update(
    `/collections/${collectionId}/fragments/${fragmentId}/invitations/${invitationId}`,
    {
      isAccepted: agree,
    },
  )
