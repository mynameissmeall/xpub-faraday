import { pick } from 'lodash'
import { withHandlers, withProps, compose } from 'recompose'
import { handleError, withFetching } from 'pubsweet-component-faraday-ui'
import { parseEicDecision } from './utils'
import { createRecommendation } from './handleRecommendation.api'

export default compose(
  withFetching,
  withHandlers({
    createRecommendation: ({
      fragment,
      collection,
      fetchUpdatedCollection,
    }) => (values, { setFetching, setModalError, hideModal, reset }) => {
      const recommendation = parseEicDecision(values)
      setFetching(true)
      createRecommendation({
        recommendation,
        fragmentId: fragment.id,
        collectionId: collection.id,
      })
        .then(() => {
          setFetching(false)
          hideModal()
          reset()
          fetchUpdatedCollection()
        })
        .catch(err => {
          setFetching(false)
          handleError(setModalError)(err)
        })
    },
    onEditorialRecommendation: ({
      fragment,
      collection,
      fetchUpdatedCollection,
    }) => (
      recommendation,
      { hideModal, setFetching, setModalError, reset },
    ) => {
      setFetching(true)
      createRecommendation({
        recommendation,
        fragmentId: fragment.id,
        collectionId: collection.id,
      })
        .then(r => {
          setFetching(false)
          reset()
          hideModal()
          fetchUpdatedCollection()
        })
        .catch(e => {
          setFetching(false)
          handleError(setModalError)(e)
        })
    },
  }),
  withProps(props => ({
    recommendationHandler: {
      ...pick(props, ['createRecommendation', 'onEditorialRecommendation']),
    },
  })),
)
