import { Button } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

const defaultText = css`
  color: ${th('colorText')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
`
const Root = styled.div`
  display: flex;
  flex-direction: row;
  margin: auto;
`

const Title = styled.div`
  ${defaultText};
  font-size: ${th('fontSizeBase')};
  color: ${th('colorPrimary')};
  text-align: left;
  p {
    margin: 0;
  }
`

const Container = styled.div`
  flex: ${({ flex }) => flex || 1};
  padding: 0 ${th('subGridUnit')};
`

const SideBar = styled.div`
  background-color: ${th('colorBackground')};
  flex: ${({ flex }) => flex || 1};
  padding: ${th('subGridUnit')};
`

const Header = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  margin-bottom: ${th('subGridUnit')};
  flex-wrap: wrap;
`

const BreadCrumbs = styled.div`
  & span {
    color: ${th('colorPrimary')};
    cursor: pointer;
    font-size: ${th('fontSizeBase')};
    text-align: left;
    
    &:after {
      content: '>';
      padding: 0 calc(${th('subGridUnit')} * 2);
    }

    &:last-child {
      font-size: ${th('fontSizeBase')};
      font-weight: bold;
      margin-right: ${th('subGridUnit')};
      &:after {
        display: none;
    }
  }
`
const ManuscriptId = styled.div`
  ${defaultText};
  color: ${th('colorPrimary')};
  font-size: ${th('fontSizeBase')};
  margin-right: ${th('subGridUnit')};
  text-align: left;
  text-transform: uppercase;
  white-space: nowrap;
`

const LeftDetails = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  flex: ${({ flex }) => flex || 1};
`

const RightDetails = styled.div`
  align-items: center;
  display: flex;
  flex: ${({ flex }) => flex || 1};
  flex-direction: row;
  justify-content: flex-end;
`

const StatusLabel = styled.div`
  ${defaultText};
  border: ${th('borderDefault')};
  color: ${th('colorPrimary')};
  font-weight: bold;
  padding: 0 0.5em;
  text-align: left;
  text-transform: capitalize;
  line-height: 1.5;
`

const DateField = styled.span`
  ${defaultText};
  margin: 0 ${th('subGridUnit')};
  text-align: left;
`
const Row = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  flex-wrap: wrap;
`

const ManuscriptType = styled.div`
  ${defaultText};
  padding: ${th('subGridUnit')};
  margin-left: calc(${th('subGridUnit')} * 2);
  text-align: right;
  text-transform: capitalize;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`

const ManuscriptHeader = styled.div`
  border-bottom: ${th('borderDefault')};
  padding-bottom: calc(${th('subGridUnit')} * 2);
`

const ModalRoot = styled.form`
  align-items: center;
  background-color: ${th('backgroundColorReverse')};
  display: flex;
  flex-direction: column;
  padding: calc(${th('subGridUnit')} * 6);
  position: relative;
  width: 580px;
`

const CloseIcon = styled.div`
  cursor: pointer;
  position: absolute;
  top: ${th('subGridUnit')};
  right: ${th('subGridUnit')};
`

const ActionButton = styled(Button)`
  ${defaultText};
  align-items: center;
  background-color: ${th('colorPrimary')};
  color: ${th('colorTextReverse')};
  display: flex;
  height: calc(${th('subGridUnit')} * 5);
  padding: calc(${th('subGridUnit')} / 2) ${th('subGridUnit')};
  text-align: center;
  text-transform: uppercase;
`

const ModalTitle = styled.span`
  color: ${th('colorPrimary')};
  font-family: ${th('fontHeading')};
  font-size: ${th('fontSizeHeading5')};
  margin-bottom: ${th('gridUnit')};
`

const CustomRadioGroup = styled.div`
  width: 100%;
  div {
    flex-direction: column;
    width: 100%;
    label {
      margin-bottom: ${th('subGridUnit')}!important;
      span:last-child {
        font-style: normal;
        ${defaultText};
      }
    }
  }
`

const SpinnerContainer = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  height: calc(${th('gridUnit')} * 2);
  min-width: calc(${th('gridUnit')} * 4);
`

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: space-evenly;
  margin: ${th('gridUnit')} auto 0;
  width: 100%;
`

export {
  Row,
  Root,
  Title,
  Header,
  SideBar,
  CloseIcon,
  ModalRoot,
  Container,
  DateField,
  ModalTitle,
  StatusLabel,
  BreadCrumbs,
  LeftDetails,
  ActionButton,
  ManuscriptId,
  RightDetails,
  ManuscriptType,
  ManuscriptHeader,
  CustomRadioGroup,
  SpinnerContainer,
  ButtonsContainer,
}
