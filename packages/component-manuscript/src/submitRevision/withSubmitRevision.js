import { connect } from 'react-redux'
import { get, pick, isNull } from 'lodash'
import { withRouter } from 'react-router-dom'
import {
  MultiAction,
  handleError,
  withFetching,
  withFilePreview,
  withFileDownload,
} from 'pubsweet-component-faraday-ui'
import { DragDropContext } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import { withModal } from 'pubsweet-component-modal/src/components'
import {
  getFormSyncErrors,
  setSubmitSucceeded,
  change as changeForm,
} from 'redux-form'
import {
  compose,
  toClass,
  withProps,
  withHandlers,
  setDisplayName,
  withStateHandlers,
} from 'recompose'

import {
  addAuthor,
  deleteAuthor,
} from 'pubsweet-components-faraday/src/redux/authors'

import {
  uploadFile,
  deleteFile,
} from 'pubsweet-components-faraday/src/redux/files'

import { onChange, onSubmit, getInitialValues, validate } from './utils'

export default compose(
  withRouter,
  withFetching,
  withFilePreview,
  withFileDownload,
  connect(
    state => ({
      formErrors:
        get(state, 'form.revision.submitFailed', false) &&
        getFormSyncErrors('revision')(state),
      canSubmit: !get(state, 'form.revision.syncErrors'),
      hasFormError:
        get(state, 'form.revision.submitFailed', false) &&
        get(state, 'form.revision.syncErrors'),
    }),
    { changeForm, setSubmitSucceeded },
  ),
  withStateHandlers(
    { authorEditIndex: null },
    {
      onAuthorEdit: (_, { changeForm, setSubmitSucceeded }) => index => {
        changeForm('revision', 'editAuthors', !isNull(index))
        setSubmitSucceeded('revision')
        return {
          authorEditIndex: index,
        }
      },
    },
  ),
  withProps(() => ({
    modalKey: 'submitRevision',
  })),
  withModal(({ isFetching }) => ({
    isFetching,
    modalComponent: MultiAction,
  })),
  withHandlers({
    addResponseFile: ({
      setError,
      fragment,
      changeForm,
      setFetching,
    }) => file => {
      setFetching(true)
      return uploadFile({
        file,
        fragment,
        type: 'responseToReviewers',
      })
        .then(f => {
          setFetching(false)
          changeForm('revision', 'responseToReviewers.file', f)
        })
        .catch(err => {
          setFetching(false)
          handleError(setError)(err)
        })
    },
    deleteResponseFile: ({ setError, changeForm, setFetching }) => file => {
      setFetching(true)
      return deleteFile({ fileId: file.id }, 'responseToReviewers')
        .then(r => {
          setFetching(false)
          changeForm('revision', 'responseToReviewers.file', null)
        })
        .catch(err => {
          setFetching(false)
          handleError(setError)(err)
        })
    },
    addFile: () => uploadFile,
    addAuthor: () => addAuthor,
    deleteFile: () => deleteFile,
    deleteAuthor: () => deleteAuthor,
    previewFile: ({ previewFile }) => previewFile,
    downloadFile: ({ downloadFile }) => downloadFile,
  }),
  DragDropContext(HTML5Backend),
  toClass,
  withProps(props => ({
    submitRevision: {
      initialValues: getInitialValues(props.fragment),
      ...pick(props, [
        'addFile',
        'journal',
        'history',
        'fragment',
        'canSubmit',
        'addAuthor',
        'showModal',
        'changeForm',
        'collection',
        'isFetching',
        'deleteFile',
        'currentUser',
        'setFetching',
        'previewFile',
        'downloadFile',
        'onAuthorEdit',
        'deleteAuthor',
        'getSignedUrl',
        'hasFormError',
        'formErrors',
        'fetchingError',
        'addResponseFile',
        'deleteResponseFile',
      ]),
      isEditingAuthor: !isNull(props.authorEditIndex),
      onChange,
      onSubmit,
      validate,
      responseFile: get(
        props,
        'formValues.revision.responseToReviewers.file',
        null,
      ),
    },
  })),

  setDisplayName('SubmitRevisionHOC'),
)
