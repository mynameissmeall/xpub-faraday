## Hindawi Submit Revision HOC.

The `withSubmitRevision` HOC contains the logic for submitting a manuscript version. It contains the following utility HOCs:

* `withFetching`
* `withFilePreview`
* `withFileDownload`
* `withModal`

..as well as other `recompose` HOCs.

### withSubmitRevision props

`submitRevision` namespace contains the following fields:
Name|Type|Description
---|---|---
collection|`object`|Object containing the selected collection
fragment|`object`|Object containing the selected fragment
journal|`object`|Deprecated object containing manuscript types
currentUser|`object`|Object containing the currently logged user
initialValues|`{...fragment}`| Object containing the initial state of the fragment
isEditingAuthor|`bool`|Value representing if the current user is the editing author
canSubmit|`bool`|Value representing if the form doesn't have errors and can be submitted
hasFormError|`bool`|Value representing if the form has any errors
formErrors|`bool`|Value representing if the form has any errors
responseFile|`file`|Value containing the revision's file for the reviewer's response
addAuthor|`({ author, collectionId: string, fragmentId: string }) => any`|An async call to add an author to the manuscript
deleteAuthor|`({ authorId, fragmentId, collectionId }) => any`|An async call to remove an existing author from the manuscript
onAuthorEdit|`index => authorEditIndex: number`|Chages the form to allow editing of the selected author and returns his index
addFile|`({ file: object, type: string, fragment: object }) => any`|Uploads the file to the server
deleteFile|`({ fileId: string, type: string }) => any`|Removes the file from the server
getSignedUrl|`(id: string) => Promise({signedURL: string})`|An async call that returns the securized S3 file url
addResponseFile|`file => any`|Uploads the file then updates the form
deleteResponseFile|`file => any`|Deletes the file from the server then updates the form
onChange|`(reduxFormValues, dispatch, { collection, fragment }) => any`|Used to autosave new fragment when fields change
validate|`({ editAuthors: , files: array, responseToReviewers: object }) => errors: object`|Checks the form for required fields and returns the errors
onSubmit|`(reduxFormValues, dispatch, { history, fragment, collection, showModal, setFetching, canSubmit })`|Handles the submission of a new manuscript version

```javascript
const ManuscriptLayout = ({submitRevision}) =>(
  <SubmitRevision {...submitRevision} />
)

const SubmitRevision = ({...}) => (
  <Root>
    <DetailsAndAuthors
      addAuthor={addAuthor}
      changeForm={changeForm}
      collection={collection}
      deleteAuthor={deleteAuthor}
      formErrors={formErrors}
      fragment={fragment}
      isAuthorEdit={isEditingAuthor}
      manuscriptTypes={journal.manuscriptTypes}
      onAuthorEdit={onAuthorEdit}
    />

    <ManuscriptFiles
      changeForm={changeForm}
      collection={collection}
      deleteFile={deleteFile}
      downloadFile={downloadFile}
      formErrors={formErrors}
      formName="revision"
      fragment={fragment}
      getSignedUrl={getSignedUrl}
      previewFile={previewFile}
      token={currentUser.token}
      uploadFile={addFile}
    />

    <ResponseToReviewer
      file={responseFile}
      getSignedUrl={getSignedUrl}
      isFetching={isFetching}
      onDelete={deleteResponseFile}
      onUpload={addResponseFile}
      token={currentUser.token}
    />
  </Root>
```
