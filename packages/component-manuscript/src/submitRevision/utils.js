import { actions } from 'pubsweet-client'
import { get, debounce, omit, set } from 'lodash'
import { handleError } from 'pubsweet-component-faraday-ui'
import { autosaveRequest } from 'pubsweet-component-wizard/src/redux/autosave'
import { submitRevision } from 'pubsweet-component-wizard/src/redux/conversion'

const parseRevision = (values, fragment) => ({
  ...omit(fragment, 'recommendations'),
  revision: {
    ...values,
  },
})

const _onChange = (values, dispatch, { collection, fragment }) => {
  const newValues = omit(values, 'editAuthors')

  dispatch(autosaveRequest())
  dispatch(
    actions.updateFragment(collection, parseRevision(newValues, fragment)),
  )
}

export const onChange = debounce(_onChange, 1000, { maxWait: 5000 })

export const onSubmit = (
  values,
  dispatch,
  { history, fragment, collection, showModal, setFetching, canSubmit },
) => {
  if (!canSubmit) return

  showModal({
    title: 'Ready to submit your revision?',
    subtitle: `Once submitted, the submission can't be modified.`,
    onConfirm: ({ hideModal, setModalError }) => {
      setFetching(true)
      return submitRevision({
        fragmentId: fragment.id,
        collectionId: collection.id,
      })
        .then(
          r => {
            Promise.all([
              dispatch(actions.getCollection(collection)),
              dispatch(actions.getFragments({ id: collection.id })),
            ]).then(() => {
              setFetching(false)
              hideModal()
              history.push(
                `/projects/${r.collectionId}/versions/${r.id}/details`,
              )
            })
          },
          err => {
            throw err
          },
        )
        .catch(err => {
          setFetching(false)
          handleError(setModalError)(err)
        })
    },
  })
}

export const getInitialValues = fragment => ({
  files: get(fragment, 'revision.files', {}),
  authors: get(fragment, 'revision.authors', []),
  metadata: get(fragment, 'revision.metadata', {
    abstract: '',
    title: '',
    type: '',
  }),
  responseToReviewers: get(fragment, 'revision.responseToReviewers', {
    content: '',
    file: null,
  }),
})

export const validate = ({ editAuthors, files, responseToReviewers }) => {
  const errors = {}

  if (editAuthors) {
    set(errors, 'editAuthors', 'An unsaved author is being added or edited.')
  }

  if (get(files, 'manuscripts', []).length === 0) {
    set(errors, 'files', 'At least one main manuscript file is required.')
  }

  if (!responseToReviewers.content && !responseToReviewers.file) {
    set(
      errors,
      'responseToReviewers.content',
      'A reply or a report file is needed.',
    )
  }

  return errors
}
