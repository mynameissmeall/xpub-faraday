module.exports = {
  client: {
    components: [() => require('./components')],
    reducers: {
      editors: () => require('./redux/editors').default,
    },
  },
}
