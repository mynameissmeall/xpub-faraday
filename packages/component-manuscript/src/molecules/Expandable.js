import React from 'react'
import { Icon } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withState, withHandlers } from 'recompose'

const Expandable = ({ expanded, label, children, toggle, rightHTML }) => (
  <Root expanded={expanded}>
    <Header expanded={expanded} onClick={toggle}>
      <LeftDetails>
        <Chevron expanded={expanded}>
          <Icon primary size={3}>
            chevron_up
          </Icon>
        </Chevron>
        <SectionLabel>{label}</SectionLabel>
      </LeftDetails>
      {rightHTML && <RightDetails>{rightHTML}</RightDetails>}
    </Header>
    {expanded && <ChildrenContainer>{children}</ChildrenContainer>}
  </Root>
)

export default compose(
  withState('expanded', 'setExpanded', ({ startExpanded }) => startExpanded),
  withHandlers({
    toggle: ({ setExpanded }) => () => {
      setExpanded(e => !e)
    },
  }),
)(Expandable)

// #region styled-components
const Chevron = styled.div`
  align-items: center;
  height: calc(${th('subGridUnit')} * 5);
  display: flex;
  justify-content: center;
  transform: ${({ expanded }) => `rotateZ(${expanded ? 0 : 180}deg)`};
  transition: transform 0.2s;
  width: calc(${th('subGridUnit')} * 5);
`

const SectionLabel = styled.span`
  color: ${th('colorPrimary')};
  font-family: ${th('fontHeading')};
  font-size: ${th('fontSizeBase')};
`

const ChildrenContainer = styled.div`
  cursor: default;
  padding: calc(${th('subGridUnit')} * 2);
  padding-top: 0;
`

const Header = styled.div`
  align-items: center;
  border-width: 0;
  border-bottom-width: ${({ expanded }) => (expanded ? '1px' : 0)};
  border-style: ${th('borderStyle')};
  border-color: ${th('colorBorder')};
  cursor: pointer;
  display: flex;
  justify-content: flex-start;
  margin-bottom: ${({ expanded }) => (expanded ? th('subGridUnit') : 0)};
`

const Root = styled.div`
  border: ${th('borderDefault')};
  cursor: pointer;
  display: flex;
  flex-direction: column;
  margin-top: ${th('subGridUnit')};
  transition: all 0.3s;
`

const LeftDetails = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex: ${({ flex }) => flex || 1};
`

const RightDetails = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  flex: ${({ flex }) => flex || 1};
`
// #endregion
