import React from 'react'
import { sortBy } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'
import { setDisplayName, withProps, compose } from 'recompose'
import { DateParser } from 'pubsweet-components-faraday/src/components'

const parseRecommendationType = t => {
  switch (t) {
    case 'reject':
      return 'Rejection request'
    case 'publish':
      return 'Publish request'
    default:
      return 'Revision request'
  }
}

const parseRecommendationComments = (comments = []) =>
  sortBy(comments, c => !c.public).map(c => ({
    title: c.public ? 'Reason & Details' : 'Internal note',
    content: c.content,
  }))

const EditorialComment = ({
  reason,
  comments,
  updatedOn,
  author: { name, role },
}) => (
  <Root>
    <Row>
      <HeaderContainer>
        <ReasonText>{reason}</ReasonText>
        <CommentAuthor>{name}</CommentAuthor>
        <RoleBadge>{role}</RoleBadge>
      </HeaderContainer>
      <DateParser timestamp={updatedOn}>
        {t => <DefaultText>{t}</DefaultText>}
      </DateParser>
    </Row>
    {comments.map(({ title, content }) => (
      <EditorContainer key={title}>
        <ReasonText>{title}</ReasonText>
        <DefaultText>{content}</DefaultText>
      </EditorContainer>
    ))}
  </Root>
)

export default compose(
  setDisplayName('EditorialComment'),
  withProps(
    ({
      userId,
      editorInChief,
      recommendation,
      handlingEditor: { id: heId, name: heName },
      comments = [],
    }) => ({
      reason: parseRecommendationType(recommendation),
      comments: parseRecommendationComments(comments),
      author: {
        name: userId === heId ? heName : editorInChief,
        role: userId === heId ? 'HE' : 'EiC',
      },
    }),
  ),
)(EditorialComment)

// #region styled-components
const defaultText = css`
  color: ${th('colorPrimary')};
  font-size: ${th('fontSizeBaseSmall')};
  font-family: ${th('fontReading')};
`

const DefaultText = styled.span`
  ${defaultText};
`

const CommentAuthor = styled(DefaultText)`
  margin-left: calc(${th('subGridUnit')} * 2);
  text-decoration: underline;
`

const RoleBadge = styled.span`
  align-items: center;
  border: ${th('borderDefault')};
  color: ${th('colorPrimary')};
  display: flex;
  font-family: ${th('fontInterface')};
  font-size: 9px;
  font-weight: bold;
  height: calc(${th('subGridUnit')} * 2);
  margin-left: ${th('subGridUnit')};
  padding: calc(${th('subGridUnit')} / 3);
  text-align: center;
`

const HeaderContainer = styled.div`
  align-items: center;
  display: flex;
`

const ReasonText = styled(DefaultText)`
  font-family: ${th('fontHeading')};
  font-weight: bold;
  text-transform: uppercase;
`

const EditorContainer = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
`

const Row = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
`

const Root = styled.div`
  background-color: ${th('colorBackground')};
  border: ${th('borderDefault')};
  margin: calc(${th('subGridUnit')} * 2) 0;
  padding: calc(${th('subGridUnit')} * 2);
  transition: height 0.3s;
`
// #endregion
