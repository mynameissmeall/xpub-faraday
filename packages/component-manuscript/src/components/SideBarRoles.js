import React from 'react'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

import { HandlingEditorSection } from 'pubsweet-components-faraday/src/components/Dashboard'

const SideBarRoles = ({ project, editorInChief, currentUser }) => (
  <Root>
    <Row>
      <Text>Editor in Chief</Text>
      <Name>{editorInChief}</Name>
    </Row>
    <Row>
      <HandlingEditorSection currentUser={currentUser} project={project} />
    </Row>
  </Root>
)

export default SideBarRoles

// #region styled-components
const defaultText = css`
  color: ${th('colorText')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
`
const Root = styled.div`
  display: flex;
  flex-direction: column;
  padding: calc(${th('subGridUnit')} * 3) ${th('subGridUnit')};
`
const Text = styled.div`
  ${defaultText};
  text-transform: uppercase;
`

const Name = styled.div`
  ${defaultText};
  text-decoration: underline;
`

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  line-height: 1.5;
  margin-bottom: calc(${th('subGridUnit')} * 2);
  div {
    justify-content: space-between;
  }
  button {
    &:first-child {
      align-items: center;
      background-color: ${th('colorBackground')};
      color: ${th('colorText')};
      height: calc(${th('subGridUnit')} * 5);
      margin: 0;
      padding: calc(${th('subGridUnit')} / 2) ${th('subGridUnit')};
      text-align: center;
      text-transform: uppercase;
    }
    &:last-child {
      margin: 0 0 0 ${th('subGridUnit')};
    }
  }
`
// #endregion
