import React, { Fragment } from 'react'
import { ReviewReportCard } from './'

const ReviewReportsList = ({ recommendations, showBorder }) => (
  <Fragment>
    {recommendations.length ? (
      recommendations.map((r, index) => (
        <ReviewReportCard
          i={index + 1}
          key={r.id}
          report={r}
          showBorder={showBorder}
        />
      ))
    ) : (
      <div>No reports submitted yet.</div>
    )}
  </Fragment>
)

export default ReviewReportsList
