import React from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { setDisplayName, withProps, compose } from 'recompose'

import { Expandable } from '../molecules'
import { EditorialComment } from './'

const EditorialComments = ({
  recommendations,
  handlingEditor,
  editorInChief,
}) => (
  <Root>
    <Expandable label="Editorial comments" startExpanded>
      {recommendations.map(r => (
        <EditorialComment
          key={r.id}
          {...r}
          editorInChief={editorInChief}
          handlingEditor={handlingEditor}
        />
      ))}
    </Expandable>
  </Root>
)

export default compose(
  setDisplayName('EditorialComments'),
  withProps(({ project }) => ({
    handlingEditor: get(project, 'handlingEditor'),
  })),
)(EditorialComments)

// #region styled-components
const Root = styled.div`
  background-color: ${th('colorBackground')};
  margin-top: calc(${th('subGridUnit')} * 2);
  transition: height 0.3s;
`
// #endregion
