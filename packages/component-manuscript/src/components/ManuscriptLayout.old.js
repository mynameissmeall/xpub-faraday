import React, { Fragment } from 'react'
import { isEmpty } from 'lodash'

import {
  Root,
  Header,
  SideBar,
  Container,
  LeftDetails,
  BreadCrumbs,
  ManuscriptId,
  RightDetails,
} from '../atoms'
import {
  SideBarRoles,
  SideBarActions,
  SubmitRevision,
  ReviewsAndReports,
  ManuscriptVersion,
  EditorialComments,
  ResponseToReviewers,
} from './'

const ManuscriptLayout = ({
  journal,
  history,
  currentUser,
  editorInChief,
  canMakeRevision,
  editorialRecommendations,
  hasResponseToReviewers,
  project = {},
  version = {},
}) => (
  <Root>
    {!isEmpty(project) && !isEmpty(version) ? (
      <Fragment>
        <Container flex={3}>
          <Header>
            <LeftDetails>
              <BreadCrumbs>
                <span onClick={() => history.push('/')}>Dashboard</span>
                <span>Manuscript Details</span>
              </BreadCrumbs>
              <ManuscriptId>{`- ID ${project.customId}`}</ManuscriptId>
            </LeftDetails>
            <RightDetails>
              <ManuscriptVersion project={project} version={version} />
            </RightDetails>
          </Header>
          {editorialRecommendations.length > 0 && (
            <EditorialComments
              editorInChief={editorInChief}
              project={project}
              recommendations={editorialRecommendations}
            />
          )}
          {hasResponseToReviewers && <ResponseToReviewers version={version} />}
          <ReviewsAndReports project={project} version={version} />
          {canMakeRevision && (
            <SubmitRevision project={project} version={version} />
          )}
        </Container>
        <SideBar flex={1}>
          <SideBarActions project={project} version={version} />
          <SideBarRoles
            currentUser={currentUser}
            editorInChief={editorInChief}
            project={project}
            version={version}
          />
        </SideBar>
      </Fragment>
    ) : (
      <div>Loading...</div>
    )}
  </Root>
)

export default ManuscriptLayout
