import React from 'react'
import {
  ReviewerReport,
  ContextualBox,
  withFilePreview,
  withFileDownload,
} from 'pubsweet-component-faraday-ui'

const ReviewReportCard = ({ journal, report, previewFile, downloadFile }) => (
  <ContextualBox label="Your report" mb={2} startExpanded>
    <ReviewerReport
      journal={journal}
      onDownload={downloadFile}
      onPreview={previewFile}
      report={report}
    />
  </ContextualBox>
)

export default withFileDownload(withFilePreview(ReviewReportCard))
