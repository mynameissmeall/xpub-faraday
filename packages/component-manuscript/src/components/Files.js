import React, { Fragment } from 'react'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

const Files = ({
  files: {
    manuscripts = [],
    coverLetter = [],
    supplementary = [],
    responseToReviewers = [],
  },
}) => (
  <Root>
    {!!manuscripts.length && (
      <Fragment>
        <Header>
          <span>Main manuscript</span>
          <div />
        </Header>
      </Fragment>
    )}
    {!!supplementary.length && (
      <Fragment>
        <Header>
          <span>Supplemetary files</span>
          <div />
        </Header>
      </Fragment>
    )}
    {!!coverLetter.length && (
      <Fragment>
        <Header>
          <span>Cover letter</span>
          <div />
        </Header>
      </Fragment>
    )}
  </Root>
)

export default Files

// #region styled-component
const defaultText = css`
  color: ${th('colorPrimary')};
  font-family: ${th('fontHeading')};
  font-size: ${th('fontSizeBaseSmall')};
`

const Header = styled.div`
  align-self: stretch;
  align-items: center;
  display: flex;
  flex-direction: row;

  & span {
    margin-right: ${th('subGridUnit')};
    margin-top: ${th('subGridUnit')};
    ${defaultText};
    text-transform: uppercase;
  }

  & div {
    background: ${th('colorPrimary')};
    flex: 1;
    height: 1px;
  }
`

const Root = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`
// #endregion
