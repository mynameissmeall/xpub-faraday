import React, { Fragment } from 'react'
import styled from 'styled-components'
import { isEmpty, get } from 'lodash'
import {
  Text,
  paddingHelper,
  AuthorReviews,
  SubmitRevision,
  ReviewerDetails,
  HERecommendation,
  ManuscriptHeader,
  ManuscriptAssignHE,
  ManuscriptMetadata,
  ManuscriptDetailsTop,
  ResponseToInvitation,
  ManuscriptEicDecision,
  ResponseToRevisionRequest,
} from 'pubsweet-component-faraday-ui'

import ReviewerReportForm from './ReviewerReportForm'
import EditorialCommentCard from './EditorialCommentCard'
import ReviewerReports from './ReviewerReports'

const messagesLabel = {
  'return-to-handling-editor': 'Comments for Handling Editor',
  reject: 'Comments for Author',
  revision: 'Comments for Author',
}

const cannotViewReviewersDetails = ['revisionRequested', 'pendingApproval']

const ManuscriptLayout = ({
  history,
  journal = {},
  fragment = {},
  versions,
  isFetching,
  changeForm,
  formValues,
  heExpanded,
  collection = {},
  currentUser,
  getSignedUrl,
  shouldReview,
  editorInChief,
  fetchingError,
  toggleAssignHE,
  isFetchingData,
  submitRevision,
  inviteReviewer,
  isLatestVersion,
  publonsFetching,
  publonReviewers,
  reviewerReports,
  handlingEditors,
  canHEOnlyReject,
  toggleHEResponse,
  toggleEicDecision,
  heResponseExpanded,
  toggleReviewReport,
  eicDecisionExpanded,
  reviewReportExpanded,
  inviteHandlingEditor,
  toggleReviewerDetails,
  recommendationHandler,
  isFetchingFromAutosave,
  toggleHERecommentation,
  toggleReviewerResponse,
  reviewerDetailsExpanded,
  toggleEditorialComments,
  toggleAbstractMetadata,
  abstractMetadataExpanded,
  toggleConflictsOfInterest,
  conflictsOfInterestExpanded,
  toggleFilesMetadata,
  filesMetadataExpanded,
  reviewerRecommendations,
  HERecommendationExpanded,
  invitationsWithReviewers,
  reviewerResponseExpanded,
  pendingOwnRecommendation,
  editorialRecommendations,
  responseToRevisionRequest,
  editorialCommentsExpanded,
  submittedOwnRecommendation,
  toggleReviewerRecommendations,
  reviewerRecommendationExpanded,
  authorResponseToRevisonRequest,
  toggleResponeToRevisionRequest,
  toggleResponseToRevisionRequest,
  canHEMakeRecommendationToPublish,
  responseToRevisionRequestExpanded,
}) => (
  <Root pb={30}>
    {!isEmpty(collection) && !isEmpty(fragment) ? (
      <Fragment>
        <ManuscriptDetailsTop
          collection={collection}
          currentUser={currentUser}
          fragment={fragment}
          getSignedUrl={getSignedUrl}
          history={history}
          versions={versions}
        />
        <ManuscriptHeader
          collection={collection}
          currentUser={currentUser}
          editorInChief={editorInChief}
          fragment={fragment}
          handlingEditors={handlingEditors}
          inviteHE={toggleAssignHE}
          isFetching={isFetchingData.editorsFetching}
          isLatestVersion={isLatestVersion}
          journal={journal}
          resendInvitation={inviteHandlingEditor.assignHE}
          revokeInvitation={inviteHandlingEditor.revokeHE}
          versions={versions}
        />
        <ManuscriptMetadata
          abstractMetadataExpanded={abstractMetadataExpanded}
          conflictsOfInterestExpanded={conflictsOfInterestExpanded}
          currentUser={currentUser}
          filesMetadataExpanded={filesMetadataExpanded}
          fragment={fragment}
          getSignedUrl={getSignedUrl}
          toggleAbstractMetadata={toggleAbstractMetadata}
          toggleConflictsOfInterest={toggleConflictsOfInterest}
          toggleFilesMetadata={toggleFilesMetadata}
        />

        {get(currentUser, 'permissions.canViewEditorialComments', true) && (
          <EditorialCommentCard
            collection={collection}
            expanded={editorialCommentsExpanded}
            journal={journal}
            reports={editorialRecommendations}
            toggle={toggleEditorialComments}
          />
        )}

        {get(currentUser, 'permissions.authorCanViewReportsDetails', false) && (
          <AuthorReviews
            currentUser={currentUser}
            getSignedUrl={getSignedUrl}
            invitations={invitationsWithReviewers}
            journal={journal}
            reviewerReports={reviewerReports}
            token={get(currentUser, 'token')}
          />
        )}

        {get(
          currentUser,
          'permissions.reviewersCanViewReviewerReports',
          false,
        ) && (
          <ReviewerReports
            currentUser={currentUser}
            expanded={reviewReportExpanded}
            getSignedUrl={getSignedUrl}
            invitations={invitationsWithReviewers}
            isLatestVersion={isLatestVersion}
            journal={journal}
            reviewerReports={reviewerRecommendations}
            toggle={toggleReviewReport}
            token={get(currentUser, 'token')}
          />
        )}

        {get(currentUser, 'permissions.canViewResponseFromAuthor', false) && (
          <ResponseToRevisionRequest
            authorReply={get(fragment, 'responseToReviewers', {})}
            expanded={responseToRevisionRequestExpanded}
            fragment={fragment}
            getSignedUrl={getSignedUrl}
            toggle={toggleResponseToRevisionRequest}
            token={get(currentUser, 'token')}
          />
        )}

        {isLatestVersion &&
          get(currentUser, 'permissions.canReview', false) && (
            <ReviewerReportForm
              changeForm={changeForm}
              expanded={reviewerRecommendationExpanded}
              formValues={get(formValues, 'reviewerReport', {})}
              isFetchingFromAutosave={isFetchingFromAutosave}
              modalKey="reviewer-report"
              project={collection}
              review={pendingOwnRecommendation}
              toggle={toggleReviewerRecommendations}
              token={get(currentUser, 'token')}
              version={fragment}
            />
          )}

        {isLatestVersion &&
          get(currentUser, 'isInvitedHE', false) && (
            <ResponseToInvitation
              commentsOn="decline"
              expanded={heResponseExpanded}
              formValues={formValues.responseToInvitation}
              label="Do you agree to be the handling editor for this manuscript?"
              onResponse={inviteHandlingEditor.onHEResponse}
              title="Respond to Editorial Invitation"
              toggle={toggleHEResponse}
            />
          )}

        {get(currentUser, 'isInvitedToReview', false) && (
          <ResponseToInvitation
            expanded={reviewerResponseExpanded}
            label="Do you agree to review this manuscript?"
            onResponse={inviteReviewer.onReviewerResponse}
            title="Respond to Invitation to Review"
            toggle={toggleReviewerResponse}
          />
        )}

        {isLatestVersion && (
          <ManuscriptAssignHE
            assignHE={inviteHandlingEditor.assignHE}
            currentUser={currentUser}
            expanded={heExpanded}
            handlingEditors={handlingEditors}
            isFetching={isFetchingData.editorsFetching}
            toggle={toggleAssignHE}
          />
        )}

        {get(currentUser, 'permissions.canViewReviewersDetails', false) && (
          <ReviewerDetails
            currentUser={currentUser}
            expanded={reviewerDetailsExpanded}
            fetchingError={fetchingError}
            fragment={fragment}
            getSignedUrl={getSignedUrl}
            highlight={
              reviewerReports.length === 0 &&
              currentUser.handlingEditor &&
              !cannotViewReviewersDetails.includes(
                get(collection, 'status', 'draft'),
              )
            }
            invitations={invitationsWithReviewers}
            isFetching={isFetchingData.publonsFetching}
            isLatestVersion={isLatestVersion}
            journal={journal}
            mb={2}
            publonReviewers={publonReviewers}
            reviewerReports={reviewerReports}
            scrollIntoView
            toggle={toggleReviewerDetails}
            token={get(currentUser, 'token')}
            {...inviteReviewer}
          />
        )}

        {isLatestVersion &&
          get(currentUser, 'permissions.canSubmitRevision', false) && (
            <SubmitRevision {...submitRevision} />
          )}

        {isLatestVersion &&
          get(currentUser, 'permissions.canMakeHERecommendation', false) && (
            <HERecommendation
              canHEMakeRecommendationToPublish={
                canHEMakeRecommendationToPublish
              }
              canHEOnlyReject={canHEOnlyReject}
              expanded={HERecommendationExpanded}
              formValues={get(formValues, 'editorialRecommendation', {})}
              highlight={
                editorialRecommendations.length === 0 &&
                reviewerRecommendations.length > 0
              }
              modalKey="heRecommendation"
              onRecommendationSubmit={
                recommendationHandler.onEditorialRecommendation
              }
              toggle={toggleHERecommentation}
            />
          )}
        {isLatestVersion &&
          get(currentUser, 'permissions.canMakeDecision', false) && (
            <ManuscriptEicDecision
              collection={collection}
              data-test-id="contextual-box-manuscript-eic-decision"
              expanded={eicDecisionExpanded}
              formValues={get(formValues, 'eicDecision')}
              fragment={fragment}
              highlight={editorialRecommendations.length > 0}
              messagesLabel={messagesLabel}
              mt={2}
              submitDecision={recommendationHandler.createRecommendation}
              toggle={toggleEicDecision}
            />
          )}
      </Fragment>
    ) : (
      <Text>Loading...</Text>
    )}
  </Root>
)

export default ManuscriptLayout

// #region styles
const Root = styled.div`
  overflow-y: visible;
  min-height: 70vh;
  ${paddingHelper};
`
// #endregion
