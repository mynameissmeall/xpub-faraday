import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const TR = ({
  name,
  email,
  index,
  affiliation,
  isSubmitting,
  isCorresponding,
}) => (
  <Row>
    <td width="20">{index + 1}</td>
    <td>
      <AuthorName>{name}</AuthorName>
      {isSubmitting && <AuthorStatus>SA</AuthorStatus>}
      {isCorresponding && !isSubmitting && <AuthorStatus>CA</AuthorStatus>}
    </td>
    <td>{email || ''}</td>
    <td>{affiliation}</td>
  </Row>
)
const Authors = ({ authors }) => (
  <Table>
    <thead>
      <tr>
        <td colSpan="2">Full Name</td>
        <td>{authors[0].email ? 'Email' : ''}</td>
        <td>Affiliation</td>
      </tr>
    </thead>
    <tbody>
      {authors.map(
        (
          {
            userId,
            email = '',
            isSubmitting,
            lastName = '',
            firstName = '',
            affiliation = '',
            isCorresponding,
          },
          index,
        ) => (
          <TR
            affiliation={affiliation}
            email={email}
            index={index}
            isCorresponding={isCorresponding}
            isSubmitting={isSubmitting}
            key={`${userId}`}
            name={`${firstName} ${lastName}`}
          />
        ),
      )}
    </tbody>
  </Table>
)

export default Authors

// #region styled-components
const Row = styled.tr`
  border-bottom: ${th('borderDefault')};
  color: ${th('colorPrimary')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
  height: calc(${th('subGridUnit')} * 7);
  text-align: left;
  td:first-child {
    font-weight: bold;
  }
  &:hover {
    background-color: ${th('backgroundColorReverse')};
  }
`

const AuthorName = styled.span`
  text-decoration: underline;
`

const AuthorStatus = styled.span`
  border: ${th('borderDefault')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
  margin-left: ${th('subGridUnit')};
  padding: 0 calc(${th('subGridUnit')} / 2);
  text-align: center;
  text-transform: uppercase;
`

const Table = styled.table`
  border-spacing: 0;
  border-collapse: collapse;
  width: 100%;

  & thead tr {
    border-bottom: ${th('borderDefault')};
    color: ${th('colorPrimary')};
    font-family: ${th('fontReading')};
    font-size: ${th('fontSizeBaseSmall')};
    font-weight: bold;
    height: calc(${th('subGridUnit')} * 7);
    text-align: left;
  }
`
// #endregion
