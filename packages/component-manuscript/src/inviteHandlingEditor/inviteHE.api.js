import { create, update, remove } from 'pubsweet-client/src/helpers/api'

export const handlingEditorDecision = ({
  reason,
  isAccepted,
  collectionId,
  invitationId,
}) =>
  update(`/collections/${collectionId}/invitations/${invitationId}`, {
    isAccepted,
    reason,
  })

export const assignHandlingEditor = ({ email, collectionId }) =>
  create(`/collections/${collectionId}/invitations`, {
    email,
    role: 'handlingEditor',
  }).then(
    res => res,
    err => {
      throw err
    },
  )

export const revokeHandlingEditor = ({ invitationId, collectionId }) =>
  remove(`/collections/${collectionId}/invitations/${invitationId}`).then(
    res => res,
    err => {
      throw err
    },
  )
