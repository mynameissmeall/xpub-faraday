import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const boxShadow = props => {
  if (props.transparent) {
    return css`
      box-shadow: transparent;
    `
  }

  if (props.highlight) {
    return css`
      border: 1px solid ${th('colorPrimary')};
      box-shadow: 0 0 2px 0 ${th('colorPrimary')};
    `
  }

  return css`
    box-shadow: ${th('boxShadow')};
  `
}

export default {
  Root: css`
    background-color: ${props =>
      props.transparent ? 'transparent' : th('accordion.backgroundColor')};
    border-radius: ${th('borderRadius')};
    flex: 1;

    ${boxShadow};
  `,
  Header: {
    Root: css`
      background-color: ${props =>
        !props.transparent
          ? th('accordion.headerBackgroundColor')
          : 'transparent'};
      border-radius: ${props => (props.expanded ? 0 : th('borderRadius'))};
      border-top-left-radius: ${th('borderRadius')};
      border-top-right-radius: ${th('borderRadius')};
      border-bottom: ${props =>
        props.expanded && !props.transparent ? th('accordion.border') : 'none'};
    `,
    Label: css`
      color: ${th('colorText')};
      font-size: ${th('accordion.headerFontSize')};
      font-family: ${th('accordion.headerFontFamily')};
    `,
    Icon: css`
      color: ${th('colorText')};
    `,
  },
}
