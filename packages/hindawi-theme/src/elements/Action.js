import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export default css`
  color: ${th('action.color')};

  &:hover {
    color: ${th('colorSecondary')};
    text-decoration: none;
  }

  &:active,
  &:focus {
    color: ${th('action.colorActive')};
    text-decoration: none;
  }
`
