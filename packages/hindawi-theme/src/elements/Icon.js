import { css } from 'styled-components'
import { darken } from '@pubsweet/ui-toolkit'

export default css`
  & svg {
    &:hover {
      stroke: ${props => darken(props.color, 0.1)};
    }
    &:active {
      stroke: ${props => darken(props.color, 0.2)};
    }
  }
  display: initial;
`
