import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export default {
  Main: css`
    background: ${th('colorBackgroundHue')};
    height: calc(${th('gridUnit')} * 4);
    border-radius: ${th('borderRadius')};
  `,
  Value: css`
    border: none;
    font-family: ${th('fontHeading')};

    &:hover {
      color: ${th('colorSecondary')};
    }
  `,
  Placeholder: css`
    color: ${th('colorText')};
    line-height: ${th('gridUnit')};
    padding: 0 ${th('gridUnit')};
    font-family: ${th('fontWriting')};
  `,
  Opener: css`
    border-color: ${props =>
      props.open ? th('menu.hoverColor') : th('colorBorder')};
    height: calc(${th('gridUnit')} * 4);

    &:hover {
      border-color: ${th('menu.hoverColor')};
    }

    &:active,
    &:focus {
      outline: none;
    }
  `,

  OptionsContainer: css`
    margin-top: calc(${th('gridUnit')} / 2);
  `,
  Options: css`
    background: ${th('colorBackgroundHue')};
    box-shadow: ${th('menu.openerShadow')};
    min-width: fit-content;
  `,
  Option: css`
    border: none;
    color: ${th('colorText')};
    font-family: ${th('fontReading')};
    font-size: ${th('fontSizeBase')};
    line-height: ${th('lineHeightBase')};
    height: calc(${th('gridUnit')} * 4);
    padding: ${th('gridUnit')};

    &:hover {
      background-color: ${th('menu.optionBackground')};
    }
  `,
}
