import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { marginHelper, displayHelper } from 'pubsweet-component-faraday-ui'

export default {
  H1: css`
    color: ${th('heading.h1Color')};
    ${marginHelper};
    font-weight: normal;
  `,
  H2: css`
    color: ${th('heading.h2Color')};
    ${marginHelper};
    font-weight: normal;
  `,
  H3: css`
    color: ${th('heading.h3Color')};
    ${marginHelper};
    font-weight: normal;
  `,
  H4: css`
    color: ${th('heading.h4Color')};
    ${marginHelper};
    ${displayHelper};
    font-weight: normal;
  `,
}
