# Publons Component

### Retrieve a list of suggested reviewers [GET]

This endpoint allow to call the Publons API and get a list of suggested reviewers.

#### Publons Reviewers Request

`GET /api/fragments/:fragmentId/publons`

| URI Parameter | Requiered | Requirements | Description |
| ------------- | --------- | ------------ | ------------------ |
| fragmentId | No | String | The ID of the fragment |

#### Publons Reviewers Response

```javascript
HTTP/1.1 200 OK
{
  "name": "Bradley Bundy",
  "profileUrl": "https://publons.com/author/1249425/bradley-bundy",
  "reviews": 16,
  "institution": "Brigham Young University",
  "email": "bundy@byu.edu"
 }
```