process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const helpers = require('../../routes/publons/helpers')

const publonsReviewers = [
  {
    contact: {
      emails: [{ email: 'email@email.com' }],
    },
    publishingName: 'John Smith',
    profileUrl: 'url',
    numVerifiedReviews: 16,
    recentOrganizations: [
      {
        name: 'MIT',
      },
    ],
  },
  {
    contact: {
      emails: [{ email: 'email2@email.com' }],
    },
    publishingName: 'Iures Marcel',
    profileUrl: 'url',
    numVerifiedReviews: 2,
    recentOrganizations: [
      {
        name: 'CalTech',
      },
    ],
  },
]

const existingReviewers = [
  { email: 'rev@email.com' },
  { email: 'rev2@email.com' },
]

describe('Publons helpers', () => {
  it('should return a properly formatted list of publons reviewers', () => {
    const reviewers = helpers.parseReviewers({
      publonsReviewers,
      existingReviewers,
    })

    expect(reviewers).toHaveLength(2)
    expect(reviewers[0]).toHaveProperty('affiliation')
    expect(reviewers[0]).toHaveProperty('reviews')
    expect(reviewers[0]).toHaveProperty('name')
    expect(reviewers[0]).toHaveProperty('email')
    expect(reviewers[0]).toHaveProperty('profileUrl')
  })
  it('should return reviewers that do not already exist in the system', () => {
    existingReviewers[0].email = publonsReviewers[0].contact.emails[0].email
    const reviewers = helpers.parseReviewers({
      publonsReviewers,
      existingReviewers,
    })

    expect(reviewers).toHaveLength(1)
    const matchingReviewer = reviewers.find(
      rev => rev.email === existingReviewers[0].email,
    )
    expect(matchingReviewer).toBeUndefined()
  })
})
