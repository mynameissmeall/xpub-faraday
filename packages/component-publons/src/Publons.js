const bodyParser = require('body-parser')

const Publons = app => {
  app.use(bodyParser.json())
  const basePath = '/api/fragments/:fragmentId/publons'
  const routePath = './routes/publons'
  const authBearer = app.locals.passport.authenticate('bearer', {
    session: false,
  })
  /**
   * @api {get} /api/fragments/:fragmentId/publons List publons reviewers
   * @apiGroup Publons
   * @apiParam {id} fragmentId Fragment id
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    [{
   *      "name": "Bradley Bundy",
   *      "profileUrl": "https://publons.com/author/1249425/bradley-bundy",
   *      "reviews": 16,
   *      "institution": "Brigham Young University",
   *      "email": "bundy@byu.edu"
   *    }]
   * @apiErrorExample {json} List errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   */
  app.get(
    `${basePath}`,
    authBearer,
    require(`${routePath}/get`)(app.locals.models),
  )
}

module.exports = Publons
