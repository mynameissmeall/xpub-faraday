import React from 'react'
import { isEmpty } from 'lodash'
import { Route } from 'react-router-dom'
import { AuthenticatedComponent } from 'pubsweet-client'

import { parseSearchParams } from './utils'
import { ReviewerInviteDecision, ReviewerDecline } from './'

const PrivateRoute = ({ component: Component, token, ...rest }) => (
  <Route
    render={props =>
      isEmpty(token) ? (
        <AuthenticatedComponent>
          <Component {...props} {...rest} />
        </AuthenticatedComponent>
      ) : (
        <Component {...props} {...rest} token={token} />
      )
    }
  />
)

const ReviewerSignUp = ({ location }) => {
  const { invitationToken, ...rest } = parseSearchParams(location.search)
  return isEmpty(invitationToken) ? (
    <PrivateRoute component={ReviewerInviteDecision} {...rest} />
  ) : (
    <Route
      component={props => (
        <ReviewerDecline
          invitationToken={invitationToken}
          {...props}
          {...rest}
        />
      )}
    />
  )
}

export default ReviewerSignUp
