import React, { Fragment } from 'react'
import { reduxForm } from 'redux-form'
import { required } from 'xpub-validators'
import { Button, ValidatedField, TextField } from '@pubsweet/ui'
import {
  Row,
  Item,
  Label,
  Text,
  PasswordValidation,
  passwordValidator,
} from 'pubsweet-component-faraday-ui'

import { emailValidator } from '../utils'

const EmailField = input => <TextField {...input} type="email" />

const SignUpForm = () => (
  <Fragment>
    <Row mb={2}>
      <Item data-test-id="sign-up-email" vertical>
        <Label required>Email</Label>
        <ValidatedField
          component={EmailField}
          name="email"
          validate={[required, emailValidator]}
        />
      </Item>
    </Row>
    <PasswordValidation formLabel="Password" formName="signUpInvitation" />
  </Fragment>
)

const InviteForm = () => (
  <PasswordValidation formLabel="Password" formName="signUpInvitation" />
)

const ForgotEmailForm = () => (
  <Fragment>
    <Row mb={2} mt={2}>
      <Item vertical>
        <Label required>Email</Label>
        <ValidatedField
          component={EmailField}
          name="email"
          validate={[required, emailValidator]}
        />
      </Item>
    </Row>
  </Fragment>
)

const withoutBack = ['setPassword']
const Step1 = ({
  error,
  onBack,
  submitting,
  onSubmitText,
  handleSubmit,
  type = 'invite',
}) => (
  <Fragment>
    {type === 'signup' && <SignUpForm />}
    {type === 'setPassword' && <InviteForm />}
    {type === 'forgotPassword' && <ForgotEmailForm />}
    {type === 'invite' && <InviteForm />}
    {error && (
      <Row justify="flex-start" mb={2}>
        <Text error>{error}</Text>
      </Row>
    )}
    <Row />

    <Row justify={!withoutBack.includes(type) ? 'space-between' : 'center'}>
      {!withoutBack.includes(type) && (
        <Button data-test-id="sign-up-back-button" onClick={onBack}>
          BACK
        </Button>
      )}
      <Button
        data-test-id="sign-up-confirm-button"
        disabled={submitting}
        onClick={handleSubmit}
        primary
      >
        {onSubmitText}
      </Button>
    </Row>
  </Fragment>
)

export default reduxForm({
  form: 'signUpInvitation',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate: passwordValidator,
})(Step1)
