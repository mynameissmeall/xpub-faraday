import { withJournal } from 'xpub-journal'
import { compose, withState, withProps, withHandlers } from 'recompose'

import SignUpInvitation from './SignUpInvitationForm'
import {
  signUpUser,
  confirmUser,
  setNewPassword,
  resetUserPassword,
} from './utils'

export default compose(
  withJournal,
  withState(
    'step',
    'changeStep',
    ({ type }) => (type === 'forgotPassword' || type === 'setPassword' ? 1 : 0),
  ),
  withProps(({ location }) => {
    const params = new URLSearchParams(location.search)
    const email = params.get('email') || ''
    const token = params.get('token') || ''
    const title = params.get('title') || ''
    const lastName = params.get('lastName') || ''
    const firstName = params.get('firstName') || ''
    const affiliation = params.get('affiliation') || ''
    const country = params.get('country') || ''

    return {
      initialValues: {
        email,
        title,
        token,
        lastName,
        firstName,
        affiliation,
        country,
      },
    }
  }),
  withHandlers({
    nextStep: ({ changeStep }) => () => changeStep(step => step + 1),
    prevStep: ({ changeStep }) => () => changeStep(step => step - 1),
    confirmInvitation: ({
      initialValues: { email = '', token = '' },
      history,
    }) => confirmUser(email, token, history),
    signUp: ({ history }) => signUpUser(history),
    forgotPassword: ({ history }) => resetUserPassword(history),
    setNewPassword: ({ history }) => setNewPassword(history),
  }),
  withProps(
    ({
      type,
      signUp,
      history,
      prevStep,
      forgotPassword,
      setNewPassword,
      confirmInvitation,
    }) => {
      switch (type) {
        case 'forgotPassword':
          return {
            onBack: history.goBack,
            onSubmit: forgotPassword,
            onSubmitText: 'SEND EMAIL',
          }
        case 'signup':
          return { onSubmit: signUp, onSubmitText: 'CONFIRM', onBack: prevStep }
        case 'setPassword':
          return {
            onSubmitText: 'CONFIRM',
            onSubmit: setNewPassword,
            onBack: () => history.push('/'),
          }
        default:
          return {
            onSubmitText: 'CONFIRM',
            onSubmit: confirmInvitation,
            onBack: prevStep,
          }
      }
    },
  ),
)(SignUpInvitation)
