import React, { Fragment } from 'react'
import { compose } from 'recompose'
import { reduxForm } from 'redux-form'
import { get, isUndefined } from 'lodash'
import { required as requiredValidator } from 'xpub-validators'
import { Menu, Button, Checkbox, TextField, ValidatedField } from '@pubsweet/ui'

import {
  Row,
  Text,
  Item,
  Label,
  ActionLink,
  MenuCountry,
  ItemOverrideAlert,
} from 'pubsweet-component-faraday-ui'

const AgreeCheckbox = ({ value, onChange }) => (
  <Row alignItems="center" justify="flex-start" mb={3} mt={3}>
    <Checkbox checked={value} onChange={onChange} value={value} />
    <Text>
      I agree with the{' '}
      <ActionLink to="https://www.hindawi.com/terms/">
        Terms of Service
      </ActionLink>{' '}
    </Text>
  </Row>
)

const Step0 = ({ type, error, journal, handleSubmit, initialValues }) =>
  !isUndefined(initialValues) ? (
    <Fragment>
      <Row mb={2} mt={3}>
        <Item data-test-id="sign-up-first-name" mr={1} vertical>
          <Label required>First Name</Label>
          <ValidatedField
            component={TextField}
            name="firstName"
            validate={[requiredValidator]}
          />
        </Item>
        <Item data-test-id="sign-up-last-name" ml={1} vertical>
          <Label required>Last Name</Label>
          <ValidatedField
            component={TextField}
            name="lastName"
            validate={[requiredValidator]}
          />
        </Item>
      </Row>

      <Row mb={2}>
        <ItemOverrideAlert data-test-id="sign-up-title" mr={1} vertical>
          <Label required>Title</Label>
          <ValidatedField
            component={input => (
              <Menu
                {...input}
                options={get(journal, 'title', [])}
                placeholder="Please select"
              />
            )}
            name="title"
            validate={[requiredValidator]}
          />
        </ItemOverrideAlert>
        <ItemOverrideAlert data-test-id="sign-up-country" ml={1} vertical>
          <Label required>Country</Label>
          <ValidatedField
            component={MenuCountry}
            name="country"
            placeholder="Please select"
            validate={[requiredValidator]}
          />
        </ItemOverrideAlert>
      </Row>

      <Row>
        <Item data-test-id="sign-up-affiliation" vertical>
          <Label required>Affiliation</Label>
          <ValidatedField
            component={TextField}
            name="affiliation"
            validate={[requiredValidator]}
          />
        </Item>
      </Row>

      <Row data-test-id="sign-up-agree-TC" justify="flex-start" mb={2}>
        <ValidatedField
          component={AgreeCheckbox}
          name="agreeTC"
          validate={[requiredValidator]}
        />
      </Row>

      <Row>
        <Text secondary small>
          This account information will be processed by us in accordance with
          our Privacy Policy for the purpose of registering your account and
          allowing you to use the services available via the platform. Please
          read our{' '}
          <ActionLink to="https://www.hindawi.com/privacy/">
            Privacy Policy{' '}
          </ActionLink>{' '}
          for further information.
        </Text>
      </Row>

      <Button
        data-test-id="sign-up-proceed-to-set-email-and-password"
        mt={4}
        onClick={handleSubmit}
        primary
      >
        PROCEED TO SET {type === 'signup' && 'EMAIL AND'} PASSWORD
      </Button>
      <Row mt={3}>
        <Text display="flex">
          Already have an account?
          <ActionLink data-test-id="go-to-sign-in" internal pl={1 / 2} to="/">
            Sign in
          </ActionLink>
        </Text>
      </Row>
    </Fragment>
  ) : (
    <div>{!isUndefined(error) && 'Loading...'}</div>
  )

export default compose(
  reduxForm({
    form: 'signUpInvitation',
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
    enableReinitialize: true,
  }),
)(Step0)
