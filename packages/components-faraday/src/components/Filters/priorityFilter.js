import { get } from 'lodash'

import { utils } from './'
import cfg from '../../../../xpub-faraday/config/default'

const statuses = get(cfg, 'statuses')

export const FILTER_VALUES = {
  ALL: 'all',
  NEEDS_ATTENTION: 'needsAttention',
  IN_PROGRESS: 'inProgress',
  ARCHIVED: 'archived',
}

const options = [
  { label: 'All', value: FILTER_VALUES.ALL },
  { label: 'Needs Attention', value: FILTER_VALUES.NEEDS_ATTENTION },
  { label: 'In Progress', value: FILTER_VALUES.IN_PROGRESS },
  { label: 'Archived', value: FILTER_VALUES.ARCHIVED },
]

const archivedStatuses = ['withdrawn', 'accepted', 'rejected']

const filterFn = (filterValue, { currentUser, userPermissions = [] }) => ({
  id = '',
  fragments = [],
  status = 'draft',
}) => {
  if (filterValue === FILTER_VALUES.ARCHIVED) {
    return archivedStatuses.includes(status)
  }
  const permission = userPermissions.find(
    ({ objectId }) => objectId === id || fragments.includes(objectId),
  )
  const userRole = utils.getUserRole(currentUser, get(permission, 'role'))
  switch (filterValue) {
    case FILTER_VALUES.NEEDS_ATTENTION:
      return get(statuses, `${status}.${userRole}.needsAttention`)
    case FILTER_VALUES.IN_PROGRESS:
      return (
        !archivedStatuses.includes(status) &&
        !get(statuses, `${status}.${userRole}.needsAttention`)
      )
    default:
      return true
  }
}

export default {
  options,
  filterFn,
  type: 'filter',
}
