import { get } from 'lodash'
import { compose, withState, withHandlers } from 'recompose'

import { utils } from './'

export default config => Component => {
  const filterFns = utils.makeFilterFunctions(config)
  const filterValues = utils.makeFilterValues(config)
  const { sortKey, sortFn } = utils.makeSortFunction(config)

  return compose(
    withState(
      'filterValues',
      'setFilterValues',
      utils.hydrateFilters(filterValues),
    ),
    withHandlers({
      getFilterOptions: () => key => get(config, `${key}.options`, []),
      getDefaultFilterValue: ({ filterValues }) => key =>
        get(filterValues, key, ''),
      changeFilterValue: ({ setFilterValues }) => filterKey => value => {
        // ugly but recompose doesn't pass the new state in the callback function
        let newState = {}
        setFilterValues(
          v => {
            newState = {
              ...v,
              [filterKey]: value,
            }
            return newState
          },
          () => {
            localStorage.setItem('filterValues', JSON.stringify(newState))
          },
        )
      },
      filterItems: ({ filterValues, ...props }) => items =>
        filterFns
          .reduce(
            (acc, { key, fn }) => acc.filter(fn(filterValues[key], props)),
            items,
          )
          .sort(sortFn(filterValues[sortKey], props)),
    }),
  )(Component)
}
