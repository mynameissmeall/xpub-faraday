const options = [
  { label: 'Newest first', value: 'asc' },
  { label: 'Oldest first', value: 'desc' },
]

const sortFn = orderValue => (i1, i2) => {
  if (orderValue === 'desc') {
    return i1.created - i2.created
  }
  return i2.created - i1.created
}

export default {
  sortFn,
  options,
  type: 'order',
}
