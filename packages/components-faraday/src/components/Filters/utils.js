import { get } from 'lodash'

export const hydrateFilters = defaultValues => {
  const filterValues = localStorage.getItem('filterValues')
  if (filterValues) return JSON.parse(filterValues)
  return defaultValues
}

export const makeFilterFunctions = config =>
  Object.entries(config)
    .filter(([filterKey, { type }]) => type === 'filter')
    .map(([filterKey, { filterFn }]) => ({
      key: filterKey,
      fn: filterFn,
    }))

export const makeSortFunction = config => {
  const [sortKey, { sortFn }] = Object.entries(config).find(
    ([filterKey, { type }]) => type !== 'filter',
  )
  return {
    sortKey,
    sortFn,
  }
}

export const makeFilterValues = config =>
  Object.keys(config).reduce((acc, el) => ({ ...acc, [el]: '' }), {})

export const getUserRole = (user, role) => {
  if (user.admin) return 'admin'
  if (user.editorInChief) return 'editorInChief'
  return role
}

export const parsePermission = permission => ({
  objectId: permission.object.id,
  role: permission.group,
})

export const getCollectionImportance = (statuses, item) =>
  get(statuses, `${get(item, 'status', 'draft')}.importance`)
