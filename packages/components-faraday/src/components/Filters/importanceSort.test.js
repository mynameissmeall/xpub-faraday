import fixturesService from 'pubsweet-component-fixture-service'

import { importanceSort } from './'
import { SORT_VALUES } from './importanceSort'

const { sortFn } = importanceSort
const { fixtures: { collections: { collection } } } = fixturesService

describe('Importance sort', () => {
  describe('Important items first', () => {
    // the more important collection is already before the less important one
    it('should return a negative value', () => {
      const sortResult = sortFn(SORT_VALUES.MORE_IMPORTANT)(
        { ...collection, status: 'pendingApproval' },
        { ...collection, status: 'heAssigned' },
      )
      expect(sortResult).toBeLessThan(0)
    })
    // the more important collection is after a less important one
    it('should return a positive value', () => {
      const sortResult = sortFn(SORT_VALUES.MORE_IMPORTANT)(
        { ...collection, status: 'heAssigned' },
        { ...collection, status: 'pendingApproval' },
      )
      expect(sortResult).toBeGreaterThan(0)
    })
  })

  describe('Less important items first', () => {
    it('should return a positive value', () => {
      const sortResult = sortFn(SORT_VALUES.LESS_IMPORTANT)(
        { ...collection, status: 'pendingApproval' },
        { ...collection, status: 'heAssigned' },
      )
      expect(sortResult).toBeGreaterThan(0)
    })
    it('should return a negative value', () => {
      const sortResult = sortFn(SORT_VALUES.LESS_IMPORTANT)(
        { ...collection, status: 'heAssigned' },
        { ...collection, status: 'pendingApproval' },
      )
      expect(sortResult).toBeLessThan(0)
    })
  })

  describe('Sort by date if both have the same', () => {
    it('should place older item before newer item', () => {
      const sortResult = sortFn(SORT_VALUES.LESS_IMPORTANT)(
        { ...collection, status: 'heAssigned', created: Date.now() + 2000 },
        { ...collection, status: 'heAssigned', created: Date.now() },
      )
      expect(sortResult).toBeGreaterThan(0)
    })

    it('should not move items', () => {
      const sortResult = sortFn(SORT_VALUES.LESS_IMPORTANT)(
        { ...collection, status: 'heAssigned', created: Date.now() },
        { ...collection, status: 'heAssigned', created: Date.now() + 2000 },
      )
      expect(sortResult).toBeLessThan(0)
    })
  })
})
