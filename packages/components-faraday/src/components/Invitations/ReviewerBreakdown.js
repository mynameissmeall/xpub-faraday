import React from 'react'
import { get } from 'lodash'
import { connect } from 'react-redux'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'
import { compose, withHandlers, withProps } from 'recompose'
import { selectFragment, selectCollection } from 'xpub-selectors'
import { selectReviewers } from 'pubsweet-components-faraday/src/redux/reviewers'

const ReviewerBreakdown = ({
  fragment,
  compact = false,
  getCompactReport,
  getExtendedReport,
}) => (compact ? getCompactReport() : getExtendedReport())

const acceptedInvitationFilter = i => i.hasAnswer && i.isAccepted
const submittedFilter = r =>
  r.status === 'accepted' && r.review && r.review.submittedOn
const roleFilter = role => i => i.role === role

const reviewerReduce = (acc, r) => ({
  ...acc,
  [r.status]: acc[r.status] + 1,
  submitted: submittedFilter(r) ? acc.submitted + 1 : acc.submitted,
})

export default compose(
  connect((state, { versionId, collectionId }) => ({
    reviewers: selectReviewers(state),
    fragment: selectFragment(state, versionId),
    collection: selectCollection(state, collectionId),
  })),
  withProps(({ fragment }) => ({
    invitations: get(fragment, 'invitations', []),
    recommendations: get(fragment, 'recommendations', []),
  })),
  withHandlers({
    getCompactReport: ({ invitations }) => () => {
      const reviewerInvitations = invitations.filter(roleFilter('reviewer'))
      const accepted = reviewerInvitations.filter(acceptedInvitationFilter)
        .length
      return (
        <ReviewerText>
          Reviewer invitations ({accepted}/{reviewerInvitations.length})
        </ReviewerText>
      )
    },
    getExtendedReport: ({ recommendations, reviewers = [] }) => () => {
      const mappedValues = reviewers.map(r => ({
        ...r,
        review: recommendations.find(rec => rec.userId === r.userId),
      }))
      const report = mappedValues.reduce(reviewerReduce, {
        accepted: 0,
        declined: 0,
        submitted: 0,
      })

      return (
        <BreakdownText>
          <b>{reviewers.length}</b> invited,
          <b> {report.accepted}</b> agreed,
          <b> {report.declined}</b> declined,
          <b> {report.submitted}</b> submitted
        </BreakdownText>
      )
    },
  }),
)(ReviewerBreakdown)

// #region styled-components
const defaultText = css`
  color: ${th('colorText')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
`

const BreakdownText = styled.div`
  ${defaultText};
  margin-right: ${th('subGridUnit')};
`

const ReviewerText = BreakdownText.extend`
  text-transform: uppercase;
`
// #endregion
