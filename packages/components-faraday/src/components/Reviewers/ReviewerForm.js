import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, Spinner } from '@pubsweet/ui'
import { compose, withHandlers } from 'recompose'
import { get, pick, differenceWith } from 'lodash'
import { reduxForm, change as changeForm, initialize } from 'redux-form'

import { ReviewersSelect } from './'
import { inviteReviewer } from '../../redux/reviewers'

const ReviewerForm = ({
  users,
  clearForm,
  isFetching,
  handleSubmit,
  filteredUsers,
  reviewerError,
  selectReviewer,
}) => (
  <Root>
    <Row>
      <ReviewersSelect onSelect={selectReviewer} values={filteredUsers()} />
    </Row>

    {reviewerError && (
      <CenterRow>
        <Err>{reviewerError}</Err>
      </CenterRow>
    )}
    <ButtonsContainer>
      <FormButton onClick={clearForm}>Clear</FormButton>
      {isFetching ? (
        <SpinnerContainer>
          <Spinner size={4} />
        </SpinnerContainer>
      ) : (
        <FormButton onClick={handleSubmit} primary>
          Send
        </FormButton>
      )}
    </ButtonsContainer>
  </Root>
)

export default compose(
  connect(
    state => ({
      users: get(state, 'users.users', []),
    }),
    { changeForm, initialize, inviteReviewer },
  ),
  reduxForm({
    form: 'inviteReviewer',
    onSubmit: (
      values,
      dispatch,
      { inviteReviewer, collectionId, versionId, getReviewers, reset },
    ) => {
      const reviewerData = pick(values, [
        'email',
        'lastName',
        'firstName',
        'affiliation',
      ])
      inviteReviewer(reviewerData, collectionId, versionId).then(() => {
        reset()
        getReviewers()
      })
    },
  }),
  withHandlers({
    selectReviewer: ({ changeForm, initialize }) => reviewer => () => {
      Object.entries(reviewer).forEach(([key, value]) => {
        changeForm('inviteReviewer', key, value)
      })
    },
    clearForm: ({ reset }) => () => {
      reset()
    },
    filteredUsers: ({ users, reviewers }) => () =>
      differenceWith(
        users,
        reviewers.filter(r => r.status !== 'pending'),
        (user, reviewer) => user.email === reviewer.email,
      ),
  }),
)(ReviewerForm)

// #region styled-components
const FormButton = styled(Button)`
  height: calc(${th('subGridUnit')} * 5);
  margin: ${th('subGridUnit')};
  padding: 0;
`

const Err = styled.span`
  color: ${th('colorError')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
`

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: ${th('subGridUnit')} auto 0;
  width: 100%;
`

const SpinnerContainer = styled.div`
  height: calc(${th('subGridUnit')} * 5);
  margin: ${th('subGridUnit')};
  min-width: calc(${th('gridUnit')} * 4);
`

const Row = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: row;
`

const CenterRow = Row.extend`
  justify-content: center;
`

const Root = styled.div`
  align-self: stretch;
  border: ${th('borderDefault')};
  display: flex;
  flex-direction: column;
  margin-bottom: ${th('gridUnit')};
  padding: ${th('subGridUnit')} calc(${th('subGridUnit')} * 3)
    calc(${th('subGridUnit')} * 3) calc(${th('subGridUnit')} * 3);
`
// #endregion
