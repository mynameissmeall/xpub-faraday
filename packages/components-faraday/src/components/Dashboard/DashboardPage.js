import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { actions } from 'pubsweet-client'
import { withJournal } from 'xpub-journal'
import { ConnectPage } from 'xpub-connect'
import { get } from 'lodash'
import { withRouter } from 'react-router-dom'
import { selectCurrentUser } from 'xpub-selectors'
import { handleError, withFetching } from 'pubsweet-component-faraday-ui'
import { compose, withHandlers, withContext, shouldUpdate } from 'recompose'
import { update } from 'pubsweet-client/src/helpers/api'
import {
  getUserPermissions,
  newestFirstParseDashboard,
} from 'pubsweet-component-faraday-selectors'

import { Dashboard } from './'
import { priorityFilter, orderFilter, withFiltersHOC } from '../Filters'

const deleteManuscript = ({ collectionId, comments }) =>
  update(`/collections/${collectionId}/archive`, { comments })

export default compose(
  ConnectPage(() => [
    actions.getCollections(),
    actions.getUsers(),
    actions.getTeams(),
  ]),
  connect(
    state => {
      const { collections, conversion } = state
      const currentUser = selectCurrentUser(state)
      const dashboard = newestFirstParseDashboard(state)

      const userPermissions = getUserPermissions(state)
      return {
        dashboard,
        conversion,
        collections,
        currentUser,
        userPermissions,
        isAdmin: get(state, 'currentUser.user.admin', 'false'),
      }
    },
    {
      getCollections: actions.getCollections,
      deleteCollection: actions.deleteCollection,
    },
  ),
  shouldUpdate(
    (props, nextProps) =>
      props.collections.length !== nextProps.collections.length,
  ),
  withRouter,
  withJournal,
  withFetching,
  withFiltersHOC({
    priority: priorityFilter,
    order: orderFilter,
  }),
  withContext(
    {
      journal: PropTypes.object,
      currentUser: PropTypes.object,
    },
    ({ journal, currentUser }) => ({ journal, currentUser }),
  ),
  withHandlers({
    deleteCollection: ({
      setFetching,
      deleteCollection,
      getCollections,
    }) => collection => ({ hideModal, setModalError }) => {
      setFetching(true)
      deleteCollection(collection)
        .then(() => {
          setFetching(false)
          hideModal()
          getCollections()
        })
        // again, the error is not being thrown from deleteCollection action and
        // the catch is never run
        .catch(err => {
          setFetching(false)
          handleError(setModalError)(err)
        })
    },
    deleteManuscript: ({ getCollections }) => (
      { comments },
      dispatch,
      { setFetching, setError, hideModal, collectionId },
    ) => {
      setFetching(true)
      deleteManuscript({ collectionId, comments })
        .then(() => {
          setFetching(false)
          hideModal()
          getCollections()
        })
        .catch(e => {
          setFetching(false)
          handleError(setError)(e)
        })
    },
  }),
)(Dashboard)
