import React from 'react'
import { compose, withHandlers } from 'recompose'
import {
  Row,
  Item,
  Label,
  OpenModal,
  ActionLink,
  ShadowedBox,
  handleError,
  withFetching,
} from 'pubsweet-component-faraday-ui'

const subbedTitle = `Unsubscribe from emails`
const subbedSubtitle = `Are you sure you want to unsubscribe from emails?`
const unsubbedTitle = `Subscribe to emails`
const unsubbedSubtitle = `Are you sure you want to subscribe to emails?`

const EmailNotifications = ({ isFetching, isSubscribed, setSubscription }) => (
  <ShadowedBox mt={2}>
    <Row alignItems="center">
      <Item>
        <Label>Email Notifications </Label>
      </Item>
      <Item justify="flex-end">
        <OpenModal
          isFetching={isFetching}
          onConfirm={setSubscription(!isSubscribed)}
          subtitle={isSubscribed ? subbedSubtitle : unsubbedSubtitle}
          title={isSubscribed ? subbedTitle : unsubbedTitle}
        >
          {showModal => (
            <ActionLink onClick={showModal}>
              {isSubscribed ? 'Unsubscribe' : 'Re-subscribe'}
            </ActionLink>
          )}
        </OpenModal>
      </Item>
    </Row>
  </ShadowedBox>
)

export default compose(
  withFetching,
  withHandlers({
    setSubscription: ({
      token,
      userId,
      setFetching,
      changeEmailSubscription,
    }) => value => ({ hideModal, setModalError }) => {
      setFetching(true)
      changeEmailSubscription(userId, value, token)
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch(err => {
          setFetching(false)
          handleError(setModalError)(err)
        })
    },
  }),
)(EmailNotifications)
