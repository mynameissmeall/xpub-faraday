import React, { Fragment } from 'react'
import { get } from 'lodash'
import { connect } from 'react-redux'
import { Button, H2 } from '@pubsweet/ui'
import { withJournal } from 'xpub-journal'
import { Row } from 'pubsweet-component-faraday-ui'
import { compose, lifecycle, withState } from 'recompose'

import { parseSearchParams } from '../utils'
import { changeEmailSubscription } from '../../redux/users'

const Unsubscribe = ({ message, history }) => (
  <Fragment>
    <Row mt={3}>
      <H2>{message}</H2>
    </Row>
    <Row mt={3}>
      <Button onClick={() => history.replace('/')} primary>
        Go to Dashboard
      </Button>
    </Row>
  </Fragment>
)

export default compose(
  connect(null, { changeEmailSubscription }),
  withState('message', 'setConfirmMessage', 'Loading...'),
  withJournal,
  lifecycle({
    componentDidMount() {
      const {
        journal,
        location,
        setConfirmMessage,
        changeEmailSubscription,
      } = this.props
      const { id, token } = parseSearchParams(location.search)
      const confirmMessage = `You have successfully unsubscribed. To re-subscribe go to your profile.`
      const errorMessage = `Something went wrong. Please try again or contact ${get(
        journal,
        'metadata.email',
        'technology@hindawi.com',
      )}.`

      changeEmailSubscription(id, false, token)
        .then(() => {
          setConfirmMessage(confirmMessage)
        })
        .catch(() => {
          setConfirmMessage(errorMessage)
        })
    },
  }),
)(Unsubscribe)
