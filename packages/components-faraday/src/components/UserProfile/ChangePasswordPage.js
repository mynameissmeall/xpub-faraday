import React from 'react'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { required } from 'xpub-validators'
import { selectCurrentUser } from 'xpub-selectors'
import { Button, H2, TextField, ValidatedField } from '@pubsweet/ui'
import {
  Row,
  Item,
  Text,
  Label,
  ShadowedBox,
  PasswordValidation,
  changePasswordValidator,
} from 'pubsweet-component-faraday-ui'

import { onSubmitChangePassword as onSubmit } from '../utils'

const PasswordField = input => <TextField {...input} type="password" />

const containerPadding = {
  pt: 4,
  pb: 4,
  pl: 4,
  pr: 4,
}
const ChangePassword = ({ history, handleSubmit, error }) => (
  <ShadowedBox center mt={10} {...containerPadding}>
    <H2>Change Password</H2>
    <Row mb={2} mt={3}>
      <Item vertical>
        <Label required>Current Password</Label>
        <ValidatedField
          component={PasswordField}
          name="currentPassword"
          validate={[required]}
        />
      </Item>
    </Row>
    <PasswordValidation formLabel="New Password" formName="changePassword" />
    {error && (
      <Row mt={1}>
        <Item>
          <Text error>{error}</Text>
        </Item>
      </Row>
    )}
    <Row />
    <Row justify="space-between">
      <Button onClick={history.goBack}>Back</Button>
      <Button onClick={handleSubmit} primary>
        Update password
      </Button>
    </Row>
  </ShadowedBox>
)

export default compose(
  connect(state => ({
    user: selectCurrentUser(state),
  })),
  reduxForm({
    onSubmit,
    form: 'changePassword',
    validate: changePasswordValidator,
  }),
)(ChangePassword)

// #region styles
// #endregion
