import React from 'react'
import { get } from 'lodash'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { reduxForm } from 'redux-form'
import { th } from '@pubsweet/ui-toolkit'
import { withProps, lifecycle, compose } from 'recompose'
import { required as requiredValidator } from 'xpub-validators'
import { Button, H2, TextField, ValidatedField } from '@pubsweet/ui'
import { loginUser, logoutUser } from 'pubsweet-component-login/actions'
import {
  Row,
  Item,
  Text,
  Label,
  ActionLink,
  withFetching,
} from 'pubsweet-component-faraday-ui'

const PasswordField = input => <TextField {...input} type="password" />

const Login = ({ handleSubmit, fetchingError }) => (
  <Root onSubmit={handleSubmit}>
    <CustomH2>Login</CustomH2>
    <Row mt={3}>
      <Item vertical>
        <Label required>Email</Label>
        <ValidatedField
          component={TextField}
          data-test-id="login-username"
          name="username"
          validate={[requiredValidator]}
        />
      </Item>
    </Row>

    <Row mt={3}>
      <Item vertical>
        <Label required>Password</Label>
        <ValidatedField
          component={PasswordField}
          data-test-id="login-password"
          name="password"
          validate={[requiredValidator]}
        />
      </Item>
    </Row>

    <Row justify="flex-end" mb={3} mt={1 / 2}>
      <ActionLink internal to="/password-reset">
        Forgot your password?
      </ActionLink>
    </Row>

    <Button data-test-id="login-button" primary type="submit">
      LOG IN
    </Button>

    {fetchingError && (
      <Row justify="flex-start" mt={1}>
        <Text error>{fetchingError}</Text>
      </Row>
    )}

    <Row mt={2}>
      <Text display="flex" secondary>
        {`Don't have an account? `}
        <ActionLink internal pl={1 / 2} to="/signup">
          Sign up
        </ActionLink>
      </Text>
    </Row>
  </Root>
)

const LoginPage = compose(
  withFetching,
  connect(null, {
    logoutUser,
  }),
  withProps({ passwordReset: true }),
  lifecycle({
    componentDidMount() {
      const { logoutUser } = this.props
      logoutUser()
    },
  }),
  reduxForm({
    form: 'login',
    enableReinitialize: false,
    onSubmit: (values, dispatch, { location, setError }) => {
      const redirectTo = get(location, 'state.from.pathname', '/dashboard')
      dispatch(loginUser(values, redirectTo, setError))
    },
  }),
)(Login)

export default LoginPage

// #region styles
const Root = styled.form`
  background-color: ${th('colorBackgroundHue')};
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  margin-top: calc(${th('gridUnit')} * 10);
  padding: calc(${th('gridUnit')} * 5);
  width: calc(${th('gridUnit')} * 46);
`

const CustomH2 = styled(H2)`
  text-align: center;
`
// #endregion
