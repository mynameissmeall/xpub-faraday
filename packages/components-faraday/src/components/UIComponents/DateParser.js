import moment from 'moment'
import { compose, withProps, withHandlers } from 'recompose'
import { string, instanceOf, number, oneOfType } from 'prop-types'

const getDuration = timestamp => {
  const today = moment()
  const stamp = moment(timestamp)
  return moment.duration(today.diff(stamp))
}

const D = ({ children, timestamp, daysAgo }) => children(timestamp, daysAgo)

const DateParser = compose(
  withHandlers({
    renderTimestamp: ({
      timestamp,
      durationThreshold = 1,
      dateFormat = 'DD.MM.YYYY',
    }) => () => {
      if (!timestamp) return ''
      const duration = getDuration(timestamp)

      if (duration.asDays() < durationThreshold) {
        return `${duration.humanize()} ago`
      }
      return moment(timestamp).format(dateFormat)
    },
    renderDaysAgo: ({ timestamp }) => () => {
      if (!timestamp) return ''
      const duration = getDuration(timestamp)
      return duration.humanize()
    },
  }),
  withProps(({ renderTimestamp, renderDaysAgo }) => ({
    daysAgo: renderDaysAgo(),
    timestamp: renderTimestamp(),
  })),
)(D)

DateParser.propTypes = {
  dateFormat: string,
  durationThreshold: number,
  timestamp: oneOfType([string, number, instanceOf(Date)]),
}

export default DateParser
