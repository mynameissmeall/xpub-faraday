import React from 'react'
import { get } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

const borderColor = ({ theme, validationStatus = 'default' }) =>
  ({
    error: theme.colorError,
    success: theme.colorSuccess,
    default: theme.colorBorder,
  }[validationStatus])

const defaultText = css`
  color: ${th('colorText')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
`
export const DefaultText = styled.div`
  ${defaultText};
`

export const LinkText = styled.div`
  ${defaultText};
  color: ${th('colorPrimary')};
  cursor: pointer;
  text-decoration: underline;
  &:hover {
    color: ${th('colorText')};
  }
`

export const RootContainer = styled.div`
  background-color: ${th('backgroundColorReverse')};
  border: ${({ bordered }) => (bordered ? th('borderDefault') : 'none')};
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  max-width: 600px;
  min-width: 500px;
  padding: calc(${th('subGridUnit')} * 2) calc(${th('subGridUnit')} * 4);
`

export const Title = styled.div`
  color: ${th('colorPrimary')};
  font-family: ${th('fontHeading')};
  font-size: ${th('fontSizeHeading5')};
  font-weight: bold;
  margin: ${th('subGridUnit')} auto;
  text-align: center;
`
export const Subtitle = styled.div`
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBase')};
  font-weight: normal;
  margin: ${th('subGridUnit')} auto;
  text-align: center;
`

export const Email = styled.div`
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBase')};
  font-weight: normal;
  margin: ${th('subGridUnit')} auto;
  text-align: center;
`

export const FormContainer = styled.form``

export const Row = styled.div.attrs(props => ({
  className: 'form-row',
}))`
  align-items: ${props => get(props, 'alignItems', 'flex-start')};
  display: flex;
  flex-direction: row;
  justify-content: ${({ justify }) => justify || 'space-evenly'};
  margin: ${({ noMargin }) =>
    noMargin ? 0 : css`calc(${th('subGridUnit')} * 2) 0`};
  width: 100%;

  label + div[role='alert'] {
    margin-top: 0;
  }
`

export const RowItem = styled.div.attrs(props => ({
  className: 'form-row-item',
}))`
  display: flex;
  flex: ${({ flex }) => flex || 1};
  flex-direction: ${({ vertical }) => (vertical ? 'column' : 'row')};
  justify-content: ${({ centered }) => (centered ? 'center' : 'initial')};
  margin-right: ${({ withRightMargin }) =>
    withRightMargin ? th('gridUnit') : 0};

  & > div {
    flex: 1;
  }

  div[role='alert'] {
    margin-top: 0;
  }
`

export const Label = styled.div.attrs(props => ({
  className: 'form-label',
}))`
  align-items: center;
  color: ${th('colorPrimary')};
  display: flex;
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
  justify-content: ${({ justify }) => justify || 'flex-start'};
  margin: ${th('subGridUnit')} 0;
  text-transform: uppercase;
`

export const LabelTitle = styled(Label)`
  margin: calc(${th('subGridUnit')} * 2) 0 0;
  line-height: 1;
`

export const Err = styled.span`
  color: ${th('colorError')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBase')};
  margin-top: 0;
  text-align: ${({ align }) => align || 'center'};
`

export const Textarea = styled.textarea`
  border: ${th('borderDefault')};
  border-color: ${({ hasError }) =>
    hasError ? th('colorError') : th('colorPrimary')};
  font-size: ${th('fontSizeBaseSmall')};
  font-family: ${th('fontWriting')};
  padding: ${th('subGridUnit')};
  outline: none;
  transition: all 300ms linear;

  width: ${({ width }) => `${width || '500px'}`};
  height: ${({ height }) => `${height || 150}px`};

  &:active,
  &:focus {
    border-color: #54a5fa;
  }

  &:read-only {
    background-color: ${th('colorBackgroundHue')};
  }
`

export const CustomRadioGroup = styled.div`
  div {
    flex-direction: row;
    justify-content: ${({ justify }) => justify || 'space-between'};
    label {
      span:last-child {
        font-style: normal;
        ${defaultText};
      }
    }
  }
  & ~ div {
    margin-top: 0;
  }
`

export const PrivatePolicy = styled.div`
  ${defaultText};
  text-align: justify;
`

export const TextAreaField = input => (
  <Textarea {...input} height={70} width="100%" />
)

export const LabelHeader = styled.div`
  color: ${th('colorPrimary')};
  font-family: ${th('fontHeading')};
  font-size: ${th('fontSizeBaseSmall')};
  margin: ${th('subGridUnit')} 0;
  font-weight: bold;
  text-transform: uppercase;
`

export const TextField = styled.input`
  border: ${th('borderWidth')} ${th('borderStyle')} ${borderColor};

  border-radius: ${th('borderRadius')};

  font-family: inherit;
  font-size: inherit;

  height: calc(${th('gridUnit')} * 2);
  padding: 0 calc(${th('gridUnit')} / 2);
  width: 100%;

  &::placeholder {
    color: ${th('colorTextPlaceholder')};
  }

  ${th('cssOverrides.TextField.Input')};
`
