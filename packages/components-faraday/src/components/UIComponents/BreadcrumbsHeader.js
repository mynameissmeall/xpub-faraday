import React from 'react'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

const BreadcrumbsHeader = ({
  history,
  showBack,
  title = '',
  info = '',
  underlined,
}) => (
  <Root underlined={underlined}>
    {showBack && <BackButton onClick={() => history.goBack()}>Back</BackButton>}
    {title && <Title>{title}</Title>}
    {info && <Content>{info}</Content>}
  </Root>
)

export default BreadcrumbsHeader

// #region styles
const Root = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: ${th('gridUnit')};
  border-bottom: ${({ underlined }) =>
    underlined ? th('borderDefault') : 'none'};
  padding-bottom: ${({ underlined }) => (underlined ? th('gridUnit') : '0')};
`

const defaultText = css`
  color: ${th('colorPrimary')};
  font-family: ${th('fontHeading')};
  font-size: ${th('fontSizeBase')};
  text-align: left;
  padding: 0 calc(${th('subGridUnit')}*2);
`

const Title = styled.span`
  ${defaultText};
  font-size: ${th('fontSizeHeading5')};
  font-weight: bold;
`

const Content = styled.span`
  ${defaultText};
`
const BackButton = styled.span`
  color: ${th('colorPrimary')};
  font-size: ${th('fontSizeBase')};
  text-align: left;
  cursor: pointer;
  padding-right: calc(${th('subGridUnit')}*2);

  &:before {
    content: '<';
    padding-right: calc(${th('subGridUnit')}*2);
  }
`

// #endregion
