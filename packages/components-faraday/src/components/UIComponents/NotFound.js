import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Icon, Button } from '@pubsweet/ui'

const NotFound = ({ history }) => (
  <Root>
    <div>
      <Icon size={6}>cloud-off</Icon>
    </div>
    <H2>The page cannot be found</H2>
    <H3>
      The page you are looking for might have been removed, had its name
      changed, or is temporarily unavailable.
    </H3>
    <RedirectButton onClick={() => history.push('/')} primary>
      Go to dashboard
    </RedirectButton>
  </Root>
)

export default NotFound

const Root = styled.div`
  margin: 0 auto;
  text-align: center;
  width: 90vw;
`
const RedirectButton = styled(Button)`
  display: inline-flex;
  align-self: center;
`
const H2 = styled.h2`
  font-size: ${th('fontSizeHeading2')};
`
const H3 = styled.h3`
  font-size: ${th('fontSizeHeading3')};
`
