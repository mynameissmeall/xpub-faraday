module.exports = {
  client: {
    components: [() => require('./components')],
    reducers: {
      authors: () => require('./redux/authors').default,
      customError: () => require('./redux/errors').default,
      files: () => require('./redux/files').default,
    },
  },
}
