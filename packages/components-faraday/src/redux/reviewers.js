import { get } from 'lodash'
import { selectCurrentUser } from 'xpub-selectors'
import {
  create,
  remove,
  update,
  get as apiGet,
} from 'pubsweet-client/src/helpers/api'

// reviewer decision constants and action creators
const initialState = {
  fetching: {
    decision: false,
    invite: false,
    reviewers: false,
  },
  error: null,
  reviewers: [],
}

const CLEAR_ERROR = 'reviewers/CLEAR_ERROR'
export const clearReviewersError = () => ({
  type: CLEAR_ERROR,
})

// #region Selectors
export const selectReviewers = state => get(state, 'reviewers.reviewers', [])
export const selectReviewersError = state => get(state, 'reviewers.error')
export const selectFetchingReviewers = state =>
  get(state, 'reviewers.fetching.reviewers', false)
export const selectFetchingInvite = state =>
  get(state, 'reviewers.fetching.invite', false)
export const selectFetchingDecision = state =>
  get(state, 'reviewers.fetching.decision', false)

export const selectInvitation = (state, fragmentId) => {
  const currentUser = selectCurrentUser(state)
  const invitations = get(state, `fragments.${fragmentId}.invitations`, [])
  return invitations.find(
    i => i.userId === currentUser.id && i.role === 'reviewer' && !i.hasAnswer,
  )
}

export const currentUserIsReviewer = (state, fragmentId) => {
  const currentUser = selectCurrentUser(state)
  const invitations = get(state, `fragments.${fragmentId}.invitations`, [])
  return !!invitations.find(
    i =>
      i.userId === currentUser.id &&
      i.role === 'reviewer' &&
      i.hasAnswer &&
      i.isAccepted,
  )
}

export const getCollectionReviewers = (collectionId, fragmentId) => dispatch =>
  apiGet(
    `/collections/${collectionId}/fragments/${fragmentId}/invitations?role=reviewer`,
  )
// #endregion

export const inviteReviewer = ({
  fragmentId,
  reviewerData,
  collectionId,
  isPublons = false,
}) =>
  create(`/collections/${collectionId}/fragments/${fragmentId}/invitations`, {
    ...reviewerData,
    role: 'reviewer',
    isPublons,
  })

// #region Actions - invitations
export const setReviewerPassword = reviewerBody =>
  create(`/users/reset-password`, reviewerBody)

export const revokeReviewer = ({ fragmentId, collectionId, invitationId }) =>
  remove(
    `/collections/${collectionId}/fragments/${fragmentId}/invitations/${invitationId}`,
  )
// #endregion

// #region Actions - decision
export const reviewerDecision = ({
  fragmentId,
  agree = true,
  collectionId,
  invitationId,
}) =>
  update(
    `/collections/${collectionId}/fragments/${fragmentId}/invitations/${invitationId}`,
    {
      isAccepted: agree,
    },
  )

export const reviewerDecline = ({
  fragmentId,
  collectionId,
  invitationId,
  invitationToken,
}) =>
  update(
    `/collections/${collectionId}/fragments/${fragmentId}/invitations/${invitationId}/decline`,
    {
      invitationToken,
    },
  )
// #endregion

// #region Reducer
export default (state = initialState, action = {}) => {
  switch (action.type) {
    case CLEAR_ERROR: {
      return {
        ...state,
        error: null,
      }
    }
    default:
      return state
  }
}
// #endregion
