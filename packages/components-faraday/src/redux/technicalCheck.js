import { update } from 'pubsweet-client/src/helpers/api'

export const technicalDecision = ({
  step,
  agree,
  token,
  customId,
  comments,
  collectionId,
}) =>
  update(`/collections/${collectionId}/status`, {
    step,
    token,
    agree,
    customId,
    comments,
  })
