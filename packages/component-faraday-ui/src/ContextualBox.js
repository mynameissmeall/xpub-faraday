import React from 'react'
import PropTypes from 'prop-types'
import { isUndefined } from 'lodash'
import styled from 'styled-components'
import { Icon, H3 } from '@pubsweet/ui'
import { override, th } from '@pubsweet/ui-toolkit'

import Accordion from './pending/Accordion'
import ControlledAccordion from './pending/ControlledAccordion'

const CustomHeader = ({
  label,
  toggle,
  expanded,
  transparent,
  rightChildren,
  ...props
}) => (
  <Header
    expanded={expanded}
    hasChildren={rightChildren}
    onClick={toggle}
    transparent={transparent}
  >
    <div>
      <Icon secondary size={2}>
        {expanded ? 'minus' : 'plus'}
      </Icon>
      <H3>{label}</H3>
    </div>
    {typeof rightChildren === 'function'
      ? rightChildren({ ...props, expanded, transparent })
      : rightChildren}
  </Header>
)

const ContextualBox = ({
  label,
  children,
  rightChildren,
  toggle,
  expanded,
  ...rest
}) =>
  !isUndefined(expanded) ? (
    <ControlledAccordion
      expanded={expanded}
      header={CustomHeader}
      label={label}
      rightChildren={rightChildren}
      toggle={toggle}
      {...rest}
    >
      {children}
    </ControlledAccordion>
  ) : (
    <Accordion
      header={CustomHeader}
      label={label}
      rightChildren={rightChildren}
      {...rest}
    >
      {children}
    </Accordion>
  )

export default ContextualBox

ContextualBox.propTypes = {
  /** Label of the contextual box. */
  label: PropTypes.string,
  /** Component or html to be rendered on the right side. */
  // rightChildren: PropTypes.any,
  rightChildren: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.func,
    PropTypes.string,
  ]),
  /** The state of the contextual box. If passed from a parent then the component
   * is controlled and can be expanded/collapsed remotely.
   */
  expanded: PropTypes.bool, // eslint-disable-line
  /** Callback function used to control the state of the component.
   * To be used together with the `expanded` prop.
   */
  toggle: PropTypes.func, // eslint-disable-line
}

ContextualBox.defaultProps = {
  label: '',
  rightChildren: undefined,
}

// #region styles
const Header = styled.div.attrs(props => ({
  'data-test-id': props['data-test-id'] || 'accordion-header',
}))`
  align-items: center;
  cursor: pointer;
  display: flex;
  justify-content: ${props =>
    props.hasChildren ? 'space-between' : 'flex-start'};
  ${override('ui.Accordion.Header')};

  ${H3} {
    margin: ${th('gridUnit')} 0;
  }

  & div {
    align-items: center;
    display: flex;
  }
`
// #endregion
