A radio group with a comment.

```js
const { Fragment } = require('react')
const { compose } = require('recompose')
const { connect } = require('react-redux')
const { reduxForm, getFormValues } = require('redux-form')

const Wrapper = compose(
  connect(state => ({
    formValues: getFormValues('styleguide')(state),
  })),
  reduxForm({
    form: 'styleguide',
  }),
)(({ formValues }) => (
  <Fragment>
    <RadioWithComments
      required
      formValues={formValues}
      radioFieldName="hasConflicts"
      commentsFieldName="conflictsComments"
      commentsOn="yes"
      commentsLabel="Conflict of interest details"
      radioLabel="Do any authors have conflicts of interest to declare?"
    />
    <RadioWithComments
      required
      formValues={formValues}
      radioFieldName="hasDataAvailability"
      commentsFieldName="dataAvailabilityComments"
      commentsOn="no"
      commentsLabel="Data availability statement"
      radioLabel="Have you included a data availability statement in your manuscript?"
    />
    <RadioWithComments
      required
      formValues={formValues}
      radioFieldName="hasFunding"
      commentsFieldName="fundingComments"
      commentsOn="no"
      commentsLabel="Funding statement"
      radioLabel="Have you provided a funding statement in your manuscript?"
    />
  </Fragment>
))
;<Wrapper />
```
