import React from 'react'
import PropTypes from 'prop-types'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { reduxForm } from 'redux-form'
import { th } from '@pubsweet/ui-toolkit'

import { ContextualBox, Row, Text } from '../'
import { ManuscriptFiles, DetailsAndAuthors, ResponseToReviewer } from './'

const SubmitRevision = ({
  journal,
  addFile,
  fragment,
  addAuthor,
  deleteFile,
  collection,
  changeForm,
  isFetching,
  currentUser,
  previewFile,
  hasFormError,
  deleteAuthor,
  getSignedUrl,
  handleSubmit,
  responseFile,
  downloadFile,
  fetchingError,
  addResponseFile,
  deleteResponseFile,
  onAuthorEdit,
  isEditingAuthor,
  formErrors,
  formName,
}) => (
  <ContextualBox
    data-test-id="submit-revision"
    highlight
    label="Submit Revision"
    mb={2}
  >
    <Root data-test-id="submit-revision-details">
      <DetailsAndAuthors
        addAuthor={addAuthor}
        changeForm={changeForm}
        collection={collection}
        deleteAuthor={deleteAuthor}
        formErrors={formErrors}
        fragment={fragment}
        isAuthorEdit={isEditingAuthor}
        manuscriptTypes={journal.manuscriptTypes}
        onAuthorEdit={onAuthorEdit}
      />

      <ManuscriptFiles
        changeForm={changeForm}
        collection={collection}
        deleteFile={deleteFile}
        downloadFile={downloadFile}
        formErrors={formErrors}
        formName={formName}
        fragment={fragment}
        getSignedUrl={getSignedUrl}
        previewFile={previewFile}
        token={currentUser.token}
        uploadFile={addFile}
      />

      <ResponseToReviewer
        file={responseFile}
        getSignedUrl={getSignedUrl}
        isFetching={isFetching}
        onDelete={deleteResponseFile}
        onUpload={addResponseFile}
        token={currentUser.token}
      />

      <Row>
        {hasFormError && (
          <Text align="center" error mt={1}>
            There are some missing required fields.
          </Text>
        )}
      </Row>

      <Row justify="flex-end" mt={1}>
        <Button ml={2} onClick={handleSubmit} primary size="medium">
          Submit revision
        </Button>
      </Row>
    </Root>
  </ContextualBox>
)

SubmitRevision.propTypes = {
  /** Object containing the list of recommendations. */
  journal: PropTypes.object, //eslint-disable-line
  /** Uploads the file to the server. */
  addFile: PropTypes.func,
  /** Object containing the selected fragment. */
  fragment: PropTypes.object, //eslint-disable-line
  /** An async call to add an author to the manuscript. */
  addAuthor: PropTypes.func,
  /** Removes the file from the server. */
  deleteFile: PropTypes.func,
  /** Object containing the selected collection. */
  collection: PropTypes.object, //eslint-disable-line
  /** Change added form. */
  changeForm: PropTypes.func,
  /** Object containing token for current user. */
  currentUser: PropTypes.object, //eslint-disable-line
  /** View content of the uploaded file. */
  previewFile: PropTypes.func,
  /** Value representing if the form has any errors. */
  hasFormError: PropTypes.bool,
  /** An async call to remove an existing author from the manuscript. */
  deleteAuthor: PropTypes.func,
  /** An async call that returns the securized S3 file url. */
  getSignedUrl: PropTypes.func,
  /** Value containing the revision's file for the reviewer's response. */
  responseFile: PropTypes.object, //eslint-disable-line
  /** Downloads the file from the server. */
  downloadFile: PropTypes.func,
  /** Uploads the file then updates the form. */
  addResponseFile: PropTypes.func,
  /** Deletes the file from the server then updates the form. */
  deleteResponseFile: PropTypes.func,
  /** Chages the form to allow editing of the selected author and returns his index */
  onAuthorEdit: PropTypes.func,
  /** Value representing if the form has any errors */
  formErrors: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
}
SubmitRevision.defaultProps = {
  journal: {},
  addFile: () => {},
  fragment: {},
  addAuthor: () => {},
  deleteFile: () => {},
  collection: {},
  changeForm: () => {},
  currentUser: {},
  previewFile: () => {},
  hasFormError: false,
  deleteAuthor: () => {},
  getSignedUrl: () => {},
  responseFile: {},
  downloadFile: () => {},
  addResponseFile: () => {},
  deleteResponseFile: () => {},
  onAuthorEdit: () => {},
  formErrors: {},
}

const Root = styled.div`
  background-color: ${th('colorBackgroundHue')};
  padding: calc(${th('gridUnit')} * 2);
`

export default reduxForm({ form: 'revision' })(SubmitRevision)
