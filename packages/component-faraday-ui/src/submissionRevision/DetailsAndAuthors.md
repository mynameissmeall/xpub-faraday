```js
const { compose, withProps, withHandlers } = require('recompose')
const { connect } = require('react-redux')
const { reduxForm, getFormValues, change } = require('redux-form')

const Wrapper = compose(
  withHandlers({
    onAuthorEdit: props => () => console.log('Edit author'),
  }),
  withProps({
    manuscriptTypes: [
      {
        label: 'Research',
        value: 'research',
        author: true,
        peerReview: true,
        abstractRequired: true,
      },
      {
        label: 'Review',
        value: 'review',
        author: true,
        peerReview: true,
        abstractRequired: true,
      },
    ],
    fragment: {
      id: 'a9dc38fe-5524-4728-b97f-9495a2eb4bee',
      type: 'fragment',
      files: {
        coverLetter: [],
        manuscripts: [
          {
            id:
              'a9dc38fe-5524-4728-b97f-9495a2eb4bee/4a452733-e05d-485a-a0be-7c199c5eb4a1',
            name: 'manuscris.pdf',
            size: 39973,
            originalName: 'manuscris.pdf',
          },
        ],
        supplementary: [],
      },
      owners: [
        {
          id: '81586d97-d2b4-4423-a1e3-efd228fc67b8',
          username: 'mihail.hagiu+re@thinslices.com',
        },
        {
          id: '96673581-5916-46c5-8a57-d9e69c3e713d',
          username: 'admin',
        },
      ],
      authors: [
        {
          id: '81586d97-d2b4-4423-a1e3-efd228fc67b8',
          email: 'mihail.hagiu+re@thinslices.com',
          country: 'AX',
          lastName: 'ihail',
          firstName: 'M',
          affiliation: 'TS',
          isSubmitting: true,
          isCorresponding: true,
        },
      ],
      created: '2018-10-11T08:04:47.636Z',
      version: 1,
      metadata: {
        type: 'research',
        title: 'czxcxzc',
        journal: 'Bioinorganic Chemistry and Applications',
        abstract: 'xdzczxc',
      },
      conflicts: {
        hasFunding: '',
        hasConflicts: 'no',
        hasDataAvailability: '',
      },
      submitted: 1539606240257,
      collectionId: 'e69cddda-74be-47aa-8f99-c388ef5c8a77',
      declarations: {
        agree: true,
      },
      fragmentType: 'version',
      recommendations: [],
    },
    collection: {
      id: 'e69cddda-74be-47aa-8f99-c388ef5c8a77',
      type: 'collection',
      owners: [
        '96673581-5916-46c5-8a57-d9e69c3e713d',
        '81586d97-d2b4-4423-a1e3-efd228fc67b8',
      ],
      status: 'rejected',
      created: 1539245087543,
      customId: '5074586',
      fragments: ['a9dc38fe-5524-4728-b97f-9495a2eb4bee'],
      technicalChecks: {},
      currentVersion: {
        id: 'a9dc38fe-5524-4728-b97f-9495a2eb4bee',
        type: 'fragment',
        files: {
          coverLetter: [],
          manuscripts: [
            {
              id:
                'a9dc38fe-5524-4728-b97f-9495a2eb4bee/4a452733-e05d-485a-a0be-7c199c5eb4a1',
              name: 'manuscris.pdf',
              size: 39973,
              originalName: 'manuscris.pdf',
            },
          ],
          supplementary: [],
        },
        owners: [
          '81586d97-d2b4-4423-a1e3-efd228fc67b8',
          '96673581-5916-46c5-8a57-d9e69c3e713d',
        ],
        authors: [
          {
            id: '81586d97-d2b4-4423-a1e3-efd228fc67b8',
            email: 'mihail.hagiu+re@thinslices.com',
            country: 'AX',
            lastName: 'ihail',
            firstName: 'M',
            affiliation: 'TS',
            isSubmitting: true,
            isCorresponding: true,
          },
        ],
        created: '2018-10-11T08:04:47.636Z',
        version: 1,
        metadata: {
          type: 'research',
          title: 'czxcxzc',
          journal: 'Bioinorganic Chemistry and Applications',
          abstract: 'xdzczxc',
        },
        conflicts: {
          hasFunding: '',
          hasConflicts: 'no',
          hasDataAvailability: '',
        },
        submitted: 1539606240257,
        collectionId: 'e69cddda-74be-47aa-8f99-c388ef5c8a77',
        declarations: {
          agree: true,
        },
        fragmentType: 'version',
        recommendations: [],
      },
      visibleStatus: 'Rejected',
    },
  }),
  connect(
    state => ({
      formValues: getFormValues('styleguide')(state),
    }),
    {
      changeForm: change,
    },
  ),
  reduxForm({
    form: 'styleguide',
  }),
)(props => console.log('Padadas', props) || <DetailsAndAuthors {...props} />)
;<Wrapper />
```
