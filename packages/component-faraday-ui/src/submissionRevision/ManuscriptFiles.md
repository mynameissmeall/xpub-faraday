```js
const { compose, withHandlers } = require('recompose')
const { connect } = require('react-redux')
const { reduxForm, getFormValues } = require('redux-form')

const Wrapper = compose(
  withHandlers({
    uploadFile: props => file => Promise.resolve(file),
    deleteFile: props => file => console.log('Deleted', file),
  }),
  connect(state => ({
    formValues: getFormValues('styleguide')(state),
  })),
  reduxForm({
    form: 'styleguide',
  }),
)(props => <ManuscriptFiles {...props} />)
;<Wrapper />
```
