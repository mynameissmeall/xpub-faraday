```js
const { reduxForm, getFormValues, change } = require('redux-form')
const { compose } = require('recompose')
const { connect } = require('react-redux')

const allowedFileExtensions = ['pdf', 'doc', 'docx']

const onUpload = file => console.log('Upload', file)

const Wrapper = compose(
  connect(state => ({
    formValues: getFormValues('Response to reviewer')(state),
  })),
  reduxForm({
    form: 'Response to reviewer',
  }),
)(props => (
  <ResponseToReviewer
    onUpload={onUpload}
    allowedFileExtensions={allowedFileExtensions}
  />
))
;<Wrapper />
```
