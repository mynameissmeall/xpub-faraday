import React from 'react'
import PropTypes from 'prop-types'
import { get, has } from 'lodash'
import { Field } from 'redux-form'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { required } from 'xpub-validators'
import { Menu, TextField, ValidatedField } from '@pubsweet/ui'

import {
  Row,
  Item,
  Text,
  Label,
  Textarea,
  WizardAuthors,
  ContextualBox,
  RowOverrideAlert,
  ItemOverrideAlert,
} from '../'

const Empty = () => <div />

const DetailsAndAuthors = ({
  journal,
  fragment,
  addAuthor,
  collection,
  changeForm,
  formValues,
  formErrors,
  isAuthorEdit,
  authorsError,
  deleteAuthor,
  onAuthorEdit,
  manuscriptTypes,
  getTooltipContent,
  isAuthorsFetching,
  ...rest
}) => (
  <ContextualBox label="Details and Authors" startExpanded transparent>
    <Root>
      <Row mb={3}>
        <Item data-test-id="submission-title" flex={3} mr={1} vertical>
          <Label required>MANUSCRIPT TITLE</Label>
          <ValidatedField
            component={TextField}
            name="metadata.title"
            validate={[required]}
          />
        </Item>
        <ItemOverrideAlert data-test-id="submission-type" vertical>
          <Label required>MANUSCRIPT TYPE</Label>
          <ValidatedField
            component={input => (
              <Menu
                options={manuscriptTypes}
                {...input}
                placeholder="Please select"
              />
            )}
            name="metadata.type"
            validate={[required]}
          />
        </ItemOverrideAlert>
      </Row>

      <RowOverrideAlert mb={2}>
        <Item data-test-id="submission-abstract" vertical>
          <Label required>ABSTRACT</Label>
          <ValidatedField
            component={Textarea}
            minHeight={15}
            name="metadata.abstract"
            validate={[required]}
          />
        </Item>
      </RowOverrideAlert>

      <Field component={Empty} name="editAuthors" />
      <Field component={Empty} name="authors" />
      <WizardAuthors
        addAuthor={addAuthor}
        authors={get(fragment, 'revision.authors', [])}
        changeForm={changeForm}
        collection={collection}
        deleteAuthor={deleteAuthor}
        error={authorsError}
        formName="revision"
        fragment={fragment}
        isAuthorEdit={isAuthorEdit}
        isAuthorsFetching={isAuthorsFetching}
        journal={journal}
        onAuthorEdit={onAuthorEdit}
      />

      {has(formErrors, 'editAuthors') && (
        <Row justify="flex-start" mb={1} mt={1}>
          <Text error>{get(formErrors, 'editAuthors', '')}</Text>
        </Row>
      )}
    </Root>
  </ContextualBox>
)

const Root = styled.div`
  border-left: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  padding-left: calc(${th('gridUnit')} * 1);
`

DetailsAndAuthors.propTypes = {
  /** Deprecated object containing manuscript types. */
  journal: PropTypes.string,
  /** Object containing the selected fragment */
  fragment: PropTypes.PropTypes.shape({
    id: PropTypes.string,
    type: PropTypes.string,
    files: PropTypes.shape({
      coverLetter: PropTypes.array,
      manuscripts: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string,
          name: PropTypes.string,
          size: PropTypes.number,
          originalName: PropTypes.string,
        }),
      ),
      supplementary: PropTypes.array,
    }),
    owners: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
        username: PropTypes.string,
      }),
    ),
    authors: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
        email: PropTypes.string,
        country: PropTypes.string,
        lastName: PropTypes.string,
        firstName: PropTypes.string,
        affiliation: PropTypes.string,
        isSubmitting: PropTypes.bool,
        isCorresponding: PropTypes.bool,
      }),
    ),
    created: PropTypes.string,
    version: PropTypes.number,
    metadata: PropTypes.shape({
      type: PropTypes.string,
      title: PropTypes.string,
      journal: PropTypes.string,
      abstract: PropTypes.string,
    }),
    conflicts: PropTypes.shape({
      hasFunding: PropTypes.string,
      hasConflicts: PropTypes.string,
      hasDataAvailability: PropTypes.string,
    }),
    submitted: PropTypes.number,
    collectionId: PropTypes.string,
    declarations: PropTypes.shape({
      agree: PropTypes.bool,
    }),
    fragmentType: PropTypes.string,
    recomandations: PropTypes.array,
  }),
  /** Object containing the selected collection. */
  collection: PropTypes.shape({
    id: PropTypes.string,
    type: PropTypes.string,
    owners: PropTypes.array,
    status: PropTypes.string,
    created: PropTypes.number,
    customId: PropTypes.string,
    fragments: PropTypes.arrayOf(PropTypes.string),
    technicalChecks: PropTypes.object,
    CurrentVersion: PropTypes.shape({
      id: PropTypes.string,
      type: PropTypes.string,
      files: PropTypes.shape({
        coverLetter: PropTypes.array,
        manuscripts: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
            size: PropTypes.number,
            originalName: PropTypes.string,
          }),
        ),
        supplementary: PropTypes.array,
      }),
      owners: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string,
          username: PropTypes.string,
        }),
      ),
      authors: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string,
          email: PropTypes.string,
          country: PropTypes.string,
          lastName: PropTypes.string,
          firstName: PropTypes.string,
          affiliation: PropTypes.string,
          isSubmitting: PropTypes.bool,
          isCorresponding: PropTypes.bool,
        }),
      ),
      created: PropTypes.string,
      version: PropTypes.number,
      metadata: PropTypes.shape({
        type: PropTypes.string,
        title: PropTypes.string,
        journal: PropTypes.string,
        abstract: PropTypes.string,
      }),
      conflicts: PropTypes.shape({
        hasFunding: PropTypes.string,
        hasConflicts: PropTypes.string,
        hasDataAvailability: PropTypes.string,
      }),
      submitted: PropTypes.number,
      collectionId: PropTypes.string,
      declarations: PropTypes.shape({
        agree: PropTypes.bool,
      }),
      fragmentType: PropTypes.string,
      recomandations: PropTypes.array,
    }),
    visibleStatus: PropTypes.string,
  }),
  /** Chages the form to allow editing of the selected author and returns his index. */
  onAuthorEdit: PropTypes.func,
  /** Manuscript types that you can chose from. */
  manuscriptTypes: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.string,
      author: PropTypes.bool,
      peerReview: PropTypes.bool,
      abstractRequired: PropTypes.bool,
    }),
  ),
}
DetailsAndAuthors.defaultProps = {
  journal: '',
  fragment: {},
  collection: {},
  onAuthorEdit: () => {},
  manuscriptTypes: [],
}

export default DetailsAndAuthors
