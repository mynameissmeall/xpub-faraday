import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { withHandlers } from 'recompose'
import { th } from '@pubsweet/ui-toolkit'
import { Action, Icon } from '@pubsweet/ui'

import { paddingHelper, marginHelper } from './styledHelpers'

const ActionLink = ({
  to,
  icon,
  size,
  onClick,
  fontIcon,
  disabled,
  children,
  renderLink,
  paddingBottom,
  paddingRight,
  iconSize = 1.8,
  iconPosition = 'left',
  ...rest
}) => (
  <Root {...rest} to={to}>
    {icon &&
      iconPosition === 'left' && (
        <Icon onClick={onClick} secondary={to} size={iconSize} warning={!to}>
          {icon}
        </Icon>
      )}
    {fontIcon &&
      (iconPosition === 'left' && (
        <FontIconButton
          className={`${fontIcon} fontIconStyle`}
          onClick={onClick}
          paddingBottom={paddingBottom}
          paddingRight={paddingRight}
          size={size}
        />
      ))}
    {renderLink()}
    {icon &&
      iconPosition === 'right' && (
        <Icon onClick={onClick} secondary={to} size={iconSize} warning={!to}>
          {icon}
        </Icon>
      )}
    {fontIcon &&
      (iconPosition === 'right' && (
        <FontIconButton
          className={`${fontIcon} fontIconStyle`}
          onClick={onClick}
          paddingBottom={paddingBottom}
          paddingRight={paddingRight}
          size={size}
        />
      ))}
  </Root>
)

ActionLink.propTypes = {
  /** Link/URL specifying where to navigate, outside or inside the app.
   * If present the component will behave like a navigation link. */
  to: PropTypes.string,
  /** What icon to be used. */
  icon: PropTypes.string,
  /** Size of the icon. */
  iconSize: PropTypes.number,
  /** Position of the icon. */
  iconPosition: PropTypes.oneOf(['left', 'right']),
  /** Callback function fired when the component is clicked. */
  onClick: PropTypes.func,
  /** If true the component will be disabled (can't be interacted with). */
  disabled: PropTypes.bool,
}

ActionLink.defaultProps = {
  iconSize: 2,
  to: '',
  disabled: true,
  icon: '',
  onClick: () => {},
  iconPosition: 'left',
}

export default withHandlers({
  renderLink: ({ to, internal, disabled, onClick, size, children }) => () => {
    if (to && !internal) {
      return (
        <ExternalLink href={to} target="_blank">
          {children}
        </ExternalLink>
      )
    }

    if (to && internal) {
      return <CustomLink to={to}>{children}</CustomLink>
    }
    return (
      <Action disabled={disabled} onClick={onClick} size={size}>
        {children}
      </Action>
    )
  },
})(ActionLink)

// #region styles
const ExternalLink = styled.a`
  color: ${th('colorSecondary')};
  font-family: ${th('fontReading')};
  text-decoration: underline;
`
const CustomLink = ExternalLink.withComponent(Link)

const Root = styled.div`
  align-items: center;
  justify-content: center;
  display: ${props => (props.to ? 'inline-flex' : 'flex')};
  &:hover * {
    color: ${th('colorSecondary')};
  }

  ${marginHelper};
  ${paddingHelper};

  height: ${props => (props.height ? `${props.height}px` : 'max-content')};
  width: max-content;
`
const FontIconButton = styled.span`
  padding-bottom: calc(${props => props.paddingBottom} * ${th('gridUnit')} / 2);
  padding-right: calc(${props => props.paddingRight} * ${th('gridUnit')} / 2);
  font-size: calc(${props => props.size} * ${th('gridUnit')});
`

// #endregion
