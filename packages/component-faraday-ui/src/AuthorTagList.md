A list of author tags. Used on manuscript card and details.

```js
const authors = [
  {
    id: 1,
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    affiliation: 'University of California',
    isSubmitting: true,
  },
  {
    id: 2,
    email: 'michael.felps@gmail.com',
    firstName: 'Michael',
    lastName: 'Felps',
    affiliation: 'US Presidency',
    isSubmitting: true,
    isCorresponding: true,
  },
  {
    id: 3,
    email: 'barrack.obama@gmail.com',
    firstName: 'Barrack',
    lastName: 'Obama',
    affiliation: 'US Presidency',
  },
]
;<AuthorTagList authors={authors} />
```

A list of author tags with affiliation and tooltip

```js
const authors = [
  {
    id: 1,
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    affiliation: 'University of California',
    isSubmitting: true,
    affiliationNumber: 1,
  },
  {
    id: 2,
    email: 'michael.felps@gmail.com',
    firstName: 'Michael',
    lastName: 'Felps',
    affiliation: 'US Presidency',
    isSubmitting: true,
    isCorresponding: true,
    affiliationNumber: 2,
  },
  {
    id: 3,
    email: 'barrack.obama@gmail.com',
    firstName: 'Barrack',
    lastName: 'Obama',
    affiliation: 'US Presidency',
    affiliationNumber: 2,
  },
  {
    id: 4,
    email: 'barrack.obama@gmail.com',
    firstName: 'Sebi',
    lastName: 'Mihalache',
    affiliation: 'US Presidency',
    affiliationNumber: 2,
  },
]
;<AuthorTagList authors={authors} withTooltip withAffiliations />
```

A list of author tags with tooltip

```js
const authors = [
  {
    id: 1,
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
  },
  {
    id: 2,
    email: 'michael.felps@gmail.com',
    firstName: 'Michael',
    lastName: 'Felps',
    isSubmitting: true,
    isCorresponding: true,
  },
  {
    id: 3,
    email: 'barrack.obama@gmail.com',
    firstName: 'Barrack',
    lastName: 'Obama',
  },
]
;<AuthorTagList authors={authors} withTooltip />
```

Use a different separator and key for mapping the authors.

```js
const authors = [
  {
    id: 1,
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
  },
  {
    id: 2,
    email: 'michael.felps@gmail.com',
    firstName: 'Michael',
    lastName: 'Felps',
    isSubmitting: true,
    isCorresponding: true,
  },
  {
    id: 3,
    email: 'barrack.obama@gmail.com',
    firstName: 'Barrack',
    lastName: 'Obama',
  },
]
;<AuthorTagList separator="* * *" authors={authors} authorKey="firstName" />
```
