Sections on top of each other.

```js
const files = [
  {
    id: 'file1',
    name: 'myfile.docx',
    size: 51312,
  },
  {
    id: 'file2',
    name: 'another_pdf.pdf',
    size: 133127,
  },
];

<div style={{ display: 'flex', flexDirection: 'column' }}>
  <FileSection
    isFirst
    required
    listId="mainManuscript"
    files={files}
    title="Main Manuscript"
    allowedFileExtensions={['pdf', 'doc', 'docx']}
    onFileDrop={f => console.log('dropped a native file', f)}
    onFilePick={f => console.log('picked a file', f)}
    moveItem={
      (dragIndex, hoverIndex) => console.log('moving the item from', dragIndex, hoverIndex)
    }
    changeList={
      (from, to, fileId) => console.log('change from to', from, to, fileId)
    }
    onDelete={f => console.log('delete', f)}
    onPreview={f => console.log('preview', f)}
    onDownload={f => console.log('download', f)}
  />
  <FileSection
    required
    title="Cover Letter"
    files={files}
    listId="coverLetter"
    allowedFileExtensions={['pdf', 'doc', 'docx']}
    onFileDrop={f => console.log('dropped a native file', f)}
    onFilePick={f => console.log('picked a file', f)}
    moveItem={
      (dragIndex, hoverIndex) => console.log('moving the item from', dragIndex, hoverIndex)
    }
    changeList={
      (from, to, fileId) => console.log('change from to', from, to, fileId)
    }
    onDelete={f => console.log('delete', f)}
    onPreview={f => console.log('preview', f)}
    onDownload={f => console.log('download', f)}
  />
  <FileSection
    files={[]}
    title="Supplimental Files"
    listId="supplimentalFiles"
    required isLast onFileDrop={f => console.log('dropped a native file', f)}
    onFilePick={f => console.log('picked a file', f)}
    moveItem={
      (dragIndex, hoverIndex) => console.log('moving the item from', dragIndex, hoverIndex)
    }
    changeList={
      (from, to, fileId) => console.log('change from to', from, to, fileId)
    }
    onDelete={f => console.log('delete', f)}
    onPreview={f => console.log('preview', f)}
    onDownload={f => console.log('download', f)}
    />
</div>
```
