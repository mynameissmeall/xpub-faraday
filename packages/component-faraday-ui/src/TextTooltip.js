import PropTypes from 'prop-types'
import 'react-tippy/dist/tippy.css'
import { Tooltip } from 'react-tippy'
import React, { Fragment } from 'react'
import { Text, Row } from 'pubsweet-component-faraday-ui'
import { ThemeProvider, withTheme } from 'styled-components'

const TitleTooltip = ({ theme = {}, title = '' }) => (
  <ThemeProvider theme={theme}>
    <Fragment>
      <Row mt={1}>
        <Text>{title}</Text>
      </Row>
    </Fragment>
  </ThemeProvider>
)

const TextTooltip = ({ theme = {}, children, ...rest }) => (
  <Tooltip
    arrow
    html={<TitleTooltip theme={theme} {...rest} />}
    position="bottom"
    theme="light"
  >
    {children}
  </Tooltip>
)

TextTooltip.propTypes = {
  /** User can hover over an item, without clicking it, and a new window will appear with a new title */
  title: PropTypes.string,
}
TextTooltip.defaultProps = {
  title:
    'β-Carboline Silver Compound Binding Studies with Human Serum Albumin: A Comprehensive Multispectroscopic Analysis and Molecular Modeling Study',
}

export default withTheme(TextTooltip)
