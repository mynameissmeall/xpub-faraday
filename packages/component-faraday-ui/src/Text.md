A piece of text. (Body 1)

```js
<Text>my boy is amazing</Text>
```

A piece of text with a bullet.

```js
<Text bullet>
  I am like a list item{' '}
  <ActionLink to="https://www.google.com">google</ActionLink>
</Text>
```

A secondary text. (Body 2)

```js
<Text secondary>my boy is amazing</Text>
```

Error text.

```js
<Text error>why error?</Text>
```

A text used for manuscript custom IDs.

```js
<Text customId>ID 444222</Text>
```

A text used for journal.

```js
<Text journal>text for journal</Text>
```

A small text.

```js
<Text small>my boy is amazing</Text>
```

A small secondary text.

```js
<Text small secondary>
  my boy is amazing
</Text>
```

Text used as a label line.

```js
<Text labelLine>SUPPLEMENTARY FILES</Text>
```
