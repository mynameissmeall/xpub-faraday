import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { IconButton, Label, marginHelper } from './'

const IconCard = ({ label, icon, iconSize, onClick, ...rest }) => (
  <Root onClick={onClick} {...rest}>
    <IconButton icon={icon} iconSize={iconSize} />
    <Label>{label}</Label>
  </Root>
)

export default IconCard

// #region styles
const Root = styled.div`
  align-items: center;
  background-color: ${th('colorBackgroundHue')};
  box-shadow: ${th('boxShadow')};
  border-radius: ${th('borderRadius')};
  cursor: pointer;
  display: flex;
  flex-direction: column;
  justify-content: center;

  height: calc(${th('gridUnit')} * 19);
  width: calc(${th('gridUnit')} * 26);
  svg {
    stroke: ${th('colorPrimary')};
  }

  ${marginHelper};
`
// #endregion
