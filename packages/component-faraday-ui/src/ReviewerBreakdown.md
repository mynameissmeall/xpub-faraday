Reviewer Breakdown Component

```js
const fragment = {
  invitations: [
    {
      id: 'a69c1273-4073-4529-9e65-5424ad8034ea',
      role: 'reviewer',
      type: 'invitation',
      userId: '9c79c3bf-ccba-4540-aad8-ce4609325826',
      hasAnswer: false,
      invitedOn: 1534506511008,
      isAccepted: false,
      respondedOn: null,
    },
    {
      id: 'ca1c08bb-4da6-4cb4-972d-d16e3b66b884',
      role: 'reviewer',
      type: 'invitation',
      userId: 'd3ab9a0b-d8e8-41e5-ab3b-72b13f84fba1',
      hasAnswer: true,
      invitedOn: 1534506542522,
      isAccepted: true,
      respondedOn: 1534506569565,
    },
  ],
  recommendations: [
    {
      id: '87fb2c45-2685-47cc-9952-d58435ff8f3a',
      userId: 'd3ab9a0b-d8e8-41e5-ab3b-72b13f84fba1',
      comments: [
        {
          files: [],
          public: true,
          content: 'This is a great manuscript',
        },
      ],
      createdOn: 1534506583815,
      updatedOn: 1534506592527,
      submittedOn: 1534506591684,
      recommendation: 'publish',
      recommendationType: 'review',
    },
    {
      id: '9853453f-1049-4369-8543-1b812930430d',
      userId: 'ede6770d-8dbf-4bf9-bbe2-98facfd0a114',
      comments: [
        {
          public: true,
          content: 'nice job',
        },
        {
          public: false,
          content: 'please publish this',
        },
      ],
      createdOn: 1534506624583,
      updatedOn: 1534506624583,
      recommendation: 'publish',
      recommendationType: 'editorRecommendation',
    },
    {
      id: '64c5b596-fbfc-485c-9068-f3a58306efd7',
      userId: '5b53da0d-3f88-4e94-b8f9-7eae6a754168',
      createdOn: 1534506644873,
      updatedOn: 1534506644873,
      recommendation: 'publish',
      recommendationType: 'editorRecommendation',
    },
    {
      id: '3d43ff74-9a20-479d-a218-23bf8eac0b6a',
      userId: '5b53da0d-3f88-4e94-b8f9-7eae6a754168',
      createdOn: 1534506813446,
      updatedOn: 1534506813446,
      recommendation: 'publish',
      recommendationType: 'editorRecommendation',
    },
  ],
}
;
<ReviewerBreakdown fragment={fragment} />
```

Without invitations
```js
<ReviewerBreakdown fragment={{}} />
```
