```js
const React = require('react')
const reduxForm = require('redux-form').reduxForm;

const PasswordField = () => (
  <PasswordValidation formLabel="Password" formName="signUpInvitation" />
)

const Form = reduxForm({
  form: 'signUpInvitation',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
})(PasswordField);

<Form />
```
