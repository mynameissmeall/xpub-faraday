import qs from 'querystring'
import { withHandlers } from 'recompose'

const cache = {}
const reviewerFiles = ['manuscripts', 'supplementary']
const defaultFiles = [...reviewerFiles, 'coverLetter']

const createAnchorElement = (file, filename) => {
  const url = URL.createObjectURL(file)
  const a = document.createElement('a')

  a.href = url
  a.download = filename
  document.body.appendChild(a)

  return {
    a,
    url,
  }
}

const removeAnchorElement = (a, url) => {
  document.body.removeChild(a)
  URL.revokeObjectURL(url)
}

export const withFileDownload = withHandlers({
  downloadFile: ({ token }) => file => {
    if (!token) return
    const { id: fileId, name: fileName } = file
    const fileURL = `${
      window.location.origin
    }/api/files/${fileId}?${qs.stringify({
      download: true,
    })}`

    const xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function onXhrStateChange() {
      if (this.readyState === 4) {
        if (this.status >= 200 && this.status < 300) {
          const f = new File([this.response], fileName)

          const { a, url } = createAnchorElement(f, fileName)
          a.click()
          removeAnchorElement(a, url)
        }
      }
    }
    xhr.open('GET', fileURL)
    xhr.responseType = 'blob'
    xhr.setRequestHeader('Authorization', `Bearer ${token}`)
    xhr.send()
  },
})

export const withZipDownload = withHandlers({
  downloadFiles: ({
    token,
    isReviewer,
    fragmentId,
    setFetching,
    archiveName,
  }) => () => {
    const getUrl = `${
      window.location.origin
    }/api/files/${fragmentId}?${qs.stringify({
      fileTypes: isReviewer ? reviewerFiles : defaultFiles,
    })}`
    if (cache[fragmentId]) {
      const fileName = archiveName || `${fragmentId}-archive.zip`

      const { a, url } = createAnchorElement(cache[fragmentId], fileName)
      a.click()
      removeAnchorElement(a, url)
    } else {
      setFetching(fetching => true)
      const xhr = new XMLHttpRequest()
      xhr.onreadystatechange = function onXhrStateChange() {
        if (this.readyState === 4) {
          setFetching(fetching => false)
          if (this.status >= 200 && this.status < 300) {
            const fileName = archiveName || `${fragmentId}-archive.zip`
            const f = new File([this.response], fileName, {
              type: 'application/zip',
            })
            cache[fragmentId] = f

            const { a, url } = createAnchorElement(f, fileName)
            a.click()
            removeAnchorElement(a, url)
          }
        }
      }
      xhr.open('GET', getUrl)
      xhr.responseType = 'blob'
      xhr.setRequestHeader('Authorization', `Bearer ${token}`)
      xhr.send()
    }
  },
})
