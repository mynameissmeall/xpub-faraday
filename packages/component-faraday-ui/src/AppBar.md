A sticky app bar. For demo purposes it's not fixed to the top at the moment.

```js
const currentUser = {
  user: {
    admin: true,
    username: 'cocojambo',
    firstName: 'Alex',
    lastName: 'Munteanu',
  },
  isAuthenticated: true,
}

const autosave = {
  isFetching: true,
}

const HindawiLogo = () => (
  <Logo
    onClick={() => console.log('Hindawi best publish!')}
    title="Anca"
    src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ca/Hindawi.svg/1200px-Hindawi.svg.png"
  />
)

const MenuComponent = () => (
  <AppBarMenu
    currentUser={currentUser}
    goTo={path => console.log(`navigating to ${path}`)}
    logout={() => console.log('logging out')}
  />
)

const AutosaveComponent = () => (
  <AutosaveIndicator isVisible autosave={autosave} />
)
;<AppBar
  autosave={AutosaveComponent}
  logo={HindawiLogo}
  menu={MenuComponent}
  currentUser={currentUser}
  journal={{
    metadata: {
      nameText: 'Chemistry Awesomeness',
      backgroundImage:
        'https://images.hindawi.com/journals/jchem/jchem.banner.jpg',
    },
  }}
  fixed={false}
/>
```

Without a journal background.

```js
const currentUser = {
  user: {
    admin: true,
    username: 'cocojambo',
    firstName: 'Alex',
    lastName: 'Munteanu',
    isConfirmed: true,
  },
  isAuthenticated: true,
}

const HindawiLogo = () => (
  <Logo
    onClick={() => console.log('Hindawi best publish!')}
    title="Hindawi"
    src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ca/Hindawi.svg/1200px-Hindawi.svg.png"
  />
)

const autosave = {
  isFetching: false,
  lastUpdate: new Date(),
}

const MenuComponent = () => (
  <AppBarMenu
    currentUser={currentUser}
    goTo={path => console.log(`navigating to ${path}`)}
    logout={() => console.log('logging out')}
  />
)

const AutosaveComponent = () => (
  <AutosaveIndicator isVisible autosave={autosave} />
)
;<AppBar
  autosave={AutosaveComponent}
  logo={HindawiLogo}
  menu={MenuComponent}
  currentUser={currentUser}
  journal={{
    metadata: {
      nameText: 'Chemistry Awesomeness',
    },
  }}
  fixed={false}
/>
```

With submit manuscript button

```js
const currentUser = {
  user: {
    admin: false,
    username: 'cocojambo',
    firstName: 'Alex',
    lastName: 'Munteanu',
    isConfirmed: true,
  },
  isAuthenticated: true,
}

const HindawiLogo = () => (
  <Logo
    onClick={() => console.log('Hindawi best publish!')}
    title="Hindawi"
    src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ca/Hindawi.svg/1200px-Hindawi.svg.png"
  />
)

const autosave = {
  isFetching: false,
  lastUpdate: new Date(),
}

const MenuComponent = () => (
  <AppBarMenu
    currentUser={currentUser}
    goTo={path => console.log(`navigating to ${path}`)}
    logout={() => console.log('logging out')}
  />
)

const AutosaveComponent = () => (
  <AutosaveIndicator isVisible autosave={autosave} />
)
;<AppBar
  autosave={AutosaveComponent}
  logo={HindawiLogo}
  menu={MenuComponent}
  currentUser={currentUser}
  createDraft={() => alert('Submit manuscript')}
  journal={{
    metadata: {
      nameText: 'Chemistry Awesomeness',
    },
  }}
  fixed={false}
/>
```

With account not confirmed

```js
const currentUser = {
  user: {
    admin: false,
    username: 'cocojambo',
    firstName: 'Alex',
    lastName: 'Munteanu',
    isConfirmed: true,
  },
  isAuthenticated: true,
}

const HindawiLogo = () => (
  <Logo
    onClick={() => console.log('Hindawi best publish!')}
    title="Hindawi"
    src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ca/Hindawi.svg/1200px-Hindawi.svg.png"
  />
)

const autosave = {
  isFetching: false,
  lastUpdate: new Date(),
}

const MenuComponent = () => (
  <AppBarMenu
    currentUser={currentUser}
    goTo={path => console.log(`navigating to ${path}`)}
    logout={() => console.log('logging out')}
  />
)

const AutosaveComponent = () => (
  <AutosaveIndicator isVisible autosave={autosave} />
)
;<AppBar
  autosave={AutosaveComponent}
  logo={HindawiLogo}
  menu={MenuComponent}
  currentUser={currentUser}
  canCreateDraft={false}
  createDraft={() => alert('Submit manuscript')}
  journal={{
    metadata: {
      nameText: 'Chemistry Awesomeness',
    },
  }}
  fixed={false}
/>
```
