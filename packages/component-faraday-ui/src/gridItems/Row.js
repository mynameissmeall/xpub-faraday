import { get } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { heightHelper, marginHelper, paddingHelper } from '../styledHelpers'

/** @component */
const Row = styled.div.attrs(props => ({
  'data-test-id': props['data-test-id'] || 'row',
}))`
  align-items: ${props => get(props, 'alignItems', 'flex-start')};
  background-color: ${props => props.bgColor || 'transparent'};
  display: flex;
  flex-wrap: ${props => get(props, 'flexWrap', 'initial')};
  justify-content: ${props => get(props, 'justify', 'space-evenly')};
  width: ${props => (props.fitContent ? 'fit-content' : '100%')};
  height: ${props => get(props, 'height', 'auto')};

  ${heightHelper};
  ${marginHelper};
  ${paddingHelper};
`

Row.propTypes = {
  /** Defines how flex items are laid out along the seconday axis. */
  alignItems: PropTypes.string,
  /** Defines the background color. */
  bgColor: PropTypes.string,
  /** Sets whether flex items are forced onto one line or can wrap on multiple ones. */
  flexWrap: PropTypes.string,
  /** Specifies alignment along the main axis. */
  justifyContent: PropTypes.string,
}

Row.defaultProps = {
  alignItems: 'flex-start',
  bgColor: 'transparent',
  flexWrap: 'initial',
  justifyContent: 'space-evenly',
}

export default Row
