import { isNumber } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { marginHelper, paddingHelper } from 'pubsweet-component-faraday-ui'

/** @component */
const Item = styled.div.attrs(props => ({
  'data-test-id': props['data-test-id'] || 'item',
}))`
  align-items: ${({ alignItems }) => alignItems || 'initial'};
  display: flex;
  flex: ${({ flex }) => (isNumber(flex) ? flex : 1)};
  flex-direction: ${({ vertical }) => (vertical ? 'column' : 'row')};
  flex-wrap: ${props => props.flexWrap || 'initial'};
  justify-content: ${({ justify }) => justify || 'initial'};

  ${marginHelper};
  ${paddingHelper};
`

Item.propTypes = {
  /** Defines how flex items are laid out along the secondary axis. */
  alignItems: PropTypes.string,
  /** How much space should this item take relative to the other items. */
  flex: PropTypes.number,
  /** Sets the flex direction. If true items are layed out in a column. */
  vertical: PropTypes.bool,
  /** Sets whether flex items are forced onto one line or can wrap on multiple ones. */
  flexWrap: PropTypes.string,
  /** Specifies alignment along the main axis. */
  justify: PropTypes.string,
}

Item.defaultProps = {
  alignItems: 'initial',
  flex: 1,
  vertical: false,
  flexWrap: 'initial',
  justify: 'initial',
}

export default Item
