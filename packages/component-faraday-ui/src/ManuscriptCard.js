import React, { Fragment } from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withJournal } from 'xpub-journal'
import { H3, H4, DateParser } from '@pubsweet/ui'
import PropTypes from 'prop-types'
import { compose, withHandlers, setDisplayName, withProps } from 'recompose'
import {
  Tag,
  Text,
  Row,
  Item,
  Label,
  IconButton,
  ActionLink,
  TextTooltip,
  AuthorTagList,
  ReviewerBreakdown,
  DeleteManuscriptModal,
} from './'
import { OpenModal } from './modals'

const ManuscriptCard = ({
  isDraft,
  onDelete,
  isFetching,
  onCardClick,
  fragment = {},
  manuscriptType = {},
  canViewReports,
  deleteManuscript,
  canDeleteManuscript,
  collection: { visibleStatus = 'Draft', handlingEditor, customId, id: collId },
}) => {
  const {
    authors = [],
    metadata = {},
    id: fragmentId,
    submitted = null,
  } = fragment
  const { title = 'No title', journal, type = '' } = metadata
  return (
    <Root data-test-id={`fragment-${fragmentId}`} onClick={onCardClick}>
      <MainContainer>
        <Row alignItems="center" justify="space-between">
          <TextTooltip title={title}>
            <H3>{title}</H3>
          </TextTooltip>
          <Tag data-test-id="fragment-status" status>
            {visibleStatus}
          </Tag>
        </Row>
        {authors.length > 0 && (
          <Row alignItems="center" justify="flex-start" mb={1}>
            <AuthorTagList authors={authors} withTooltip />
          </Row>
        )}
        <Row alignItems="center" justify="flex-start">
          {customId && <Text customId mr={1}>{`ID ${customId}`}</Text>}
          {submitted && (
            <DateParser humanizeThreshold={0} timestamp={submitted}>
              {(timestamp, timeAgo) => (
                <Text
                  mr={3}
                >{`Submitted on ${timestamp} (${timeAgo} ago)`}</Text>
              )}
            </DateParser>
          )}
          <Text>{manuscriptType.label || type}</Text>
          {journal &&
            (manuscriptType.label || type) && (
              <Text journal ml={1}>
                {journal}
              </Text>
            )}
        </Row>
        <Row alignItems="center" height={5} justify="flex-start">
          <H4>Handling editor</H4>
          <Text ml={1} mr={3} whiteSpace="nowrap">
            {get(handlingEditor, 'name', 'Unassigned')}
          </Text>
          {canViewReports && (
            <Fragment>
              <Label mr={1}>Reviewers Reports</Label>
              <ReviewerBreakdown fragment={fragment} label="Reviewer Reports" />
            </Fragment>
          )}
          {isDraft && (
            <Item justify="flex-end" onClick={e => e.stopPropagation()}>
              <OpenModal
                confirmText="Delete"
                isFetching={isFetching}
                modalKey={`delete-${collId}`}
                onConfirm={onDelete}
                title="Are you sure you want to delete this submission?"
              >
                {showModal => (
                  <DeleteIcon
                    fontIcon="deleteIcon"
                    onClick={showModal}
                    paddingBottom={1}
                    paddingRight={0}
                    size={1.65}
                  >
                    Delete
                  </DeleteIcon>
                )}
              </OpenModal>
            </Item>
          )}

          {canDeleteManuscript && (
            <Item justify="flex-end" onClick={e => e.stopPropagation()}>
              <DeleteManuscriptModal
                collectionId={collId}
                modalKey={`archive-${collId}`}
                onSubmit={deleteManuscript}
              />
            </Item>
          )}
        </Row>
      </MainContainer>
      <SideNavigation>
        <IconButton fontIcon="arrowRight" iconSize={2} pl={0.2} />
      </SideNavigation>
    </Root>
  )
}
export default compose(
  withJournal,
  withHandlers({
    onCardClick: ({ onClick, collection = {}, fragment = {} }) => () => {
      onClick(collection, fragment)
    },
  }),
  withProps(
    ({
      fragment: { metadata },
      journal = {},
      collection: { status = 'draft' },
    }) => ({
      manuscriptType: get(journal, 'manuscriptTypes', []).find(
        t => t.value === get(metadata, 'type', ''),
      ),
      isDraft: status === 'draft',
      isDeleted: status === 'deleted',
    }),
  ),
  withProps(({ isAdmin, isDraft, isDeleted }) => ({
    canDeleteManuscript: isAdmin && !isDraft && !isDeleted,
  })),
  setDisplayName('ManuscriptCard'),
)(ManuscriptCard)
// #region styles

ManuscriptCard.propTypes = {
  fragment: PropTypes.shape({
    authors: PropTypes.arrayOf(PropTypes.object),
    created: PropTypes.string,
    submitted: PropTypes.number,
    metadata: PropTypes.object,
  }), // eslint-disable-line
  collection: PropTypes.shape({
    customId: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    visibleStatus: PropTypes.string,
    handlingEditor: PropTypes.object,
  }), // eslint-disable-line
}

ManuscriptCard.defaultProps = {
  fragment: {},
  collection: {},
}

const MainContainer = styled.div`
  justify-content: flex-start;
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: calc(${th('gridUnit')} * 2);
  padding-bottom: ${th('gridUnit')};
  width: calc(100% - (${th('gridUnit')} * 5 / 2));
  overflow: hidden;
  ${Row} {
    [data-tooltipped] {
      overflow: hidden;
    }
    ${H4} {
      white-space: nowrap;
    }
    ${H3} {
      margin-bottom: 0;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
      padding-right: ${th('gridUnit')};
      flex: 11;
    }
  }
`
const DeleteIcon = styled(ActionLink)`
  cursor: default;
  color: ${th('colorWarning')};
`

const SideNavigation = styled.div`
  align-items: center;
  background-color: ${th('colorBackgroundHue2')
    ? th('colorBackgroundHue2')
    : th('colorBackgroundHue')};
  border-top-right-radius: ${th('borderRadius')};
  border-bottom-right-radius: ${th('borderRadius')};
  display: flex;
  width: calc(${th('gridUnit')} * 5 / 2);
`
const Root = styled.div`
  background-color: #fff;
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  cursor: pointer;
  display: flex;
  margin: calc(${th('gridUnit')} / 4) calc(${th('gridUnit')} / 4)
    ${th('gridUnit')} calc(${th('gridUnit')} / 4);
  &:hover {
    box-shadow: ${th('dashboardCard.hoverShadow')};
  }
  ${H3} {
    margin: 0;
    margin-bottom: ${th('gridUnit')};
  }
`
