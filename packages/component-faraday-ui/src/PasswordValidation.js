import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { required } from 'xpub-validators'
import { connect } from 'react-redux'
import { getFormValues } from 'redux-form'
import { ValidatedField, TextField } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { Row, Item, Label, IconButton } from 'pubsweet-component-faraday-ui'
import styled, { css } from 'styled-components'
import { get } from 'lodash'
import { withProps, compose } from 'recompose'
import {
  minLength,
  atLeastOneDigit,
  atLeastOneUppercase,
  atLeastOneLowerrcase,
  atLeastOnePunctuation,
} from './Utils.js'

const PasswordField = input => <TextField {...input} type="password" />
const PasswordValidation = ({
  formName,
  formLabel,
  minLength,
  atLeastOneDigit,
  atLeastOneUppercase,
  submitFailed,
  atLeastOneLowerrcase,
  atLeastOnePunctuation,
}) => (
  <Fragment>
    <Row mb={2}>
      <Item data-test-id="sign-up-password" vertical>
        <Label required>{formLabel}</Label>
        <ValidatedField
          component={PasswordField}
          name="password"
          validate={[required]}
        />
      </Item>
    </Row>

    <Row mb={1}>
      <Item data-test-id="sign-up-confirm-password" vertical>
        <Label required>Retype Password</Label>
        <ValidatedField
          component={PasswordField}
          name="confirmNewPassword"
          validate={[required]}
        />
      </Item>
    </Row>

    <Row alignItems="center" justify="flex-start">
      <IconButton
        fontIcon="infoIcon"
        iconSize={2}
        mb={1 / 2}
        mr={1 / 2}
        primary
        unclickable
      />
      <RulesTitle>The password must contain: </RulesTitle>
    </Row>

    <Rules>
      <Row justify="flex-start">
        <Rule error={submitFailed && !minLength} fulfilled={minLength}>
          at least 6 characters
        </Rule>
      </Row>

      <Row justify="flex-start">
        <Rule
          error={submitFailed && !atLeastOneUppercase}
          fulfilled={atLeastOneUppercase}
        >
          at least 1 uppercase character (A-Z)
        </Rule>
      </Row>

      <Row justify="flex-start">
        <Rule
          error={submitFailed && !atLeastOneLowerrcase}
          fulfilled={atLeastOneLowerrcase}
        >
          at least 1 lowercase character (a-z)
        </Rule>
      </Row>

      <Row justify="flex-start">
        <Rule
          error={submitFailed && !atLeastOneDigit}
          fulfilled={atLeastOneDigit}
        >
          at least 1 digit (0-9)
        </Rule>
      </Row>

      <Row justify="flex-start" mb={3}>
        <Rule
          error={submitFailed && !atLeastOnePunctuation}
          fulfilled={atLeastOnePunctuation}
        >
          at least 1 special character (punctuation)
        </Rule>
      </Row>
    </Rules>
  </Fragment>
)
PasswordValidation.propTypes = {
  /** Name of the redux form. */
  formName: PropTypes.string,
  /** Label name for password input */
  formLabel: PropTypes.string,
  /** If the password requirements are not fulfilled it will return true. */
  submitFailed: PropTypes.bool,
  /** If the password's length is greater or equal than the minimum value, it will return true. */
  minLength: PropTypes.bool,
  /** If the password has at least one digit it will return true. */
  atLeastOneDigit: PropTypes.bool,
  /** If the password has at least one uppercase it will return true. */
  atLeastOneUppercase: PropTypes.bool,
  /** If the password has at least one lowercase it will return true. */
  atLeastOneLowerrcase: PropTypes.bool,
  /** If the password has at least one punctuation it will return true. */
  atLeastOnePunctuation: PropTypes.bool,
}
PasswordValidation.defaultProps = {
  formName: '',
  formLabel: '',
  submitFailed: false,
  minLength: false,
  atLeastOneDigit: false,
  atLeastOneUppercase: false,
  atLeastOneLowerrcase: false,
  atLeastOnePunctuation: false,
}

export default compose(
  connect((state, props) => ({
    formValues: getFormValues(props.formName)(state),
    submitFailed: get(state, `form.${props.formName}.submitFailed`, false),
  })),
  withProps(({ formValues }) => ({
    minLength: minLength(get(formValues, 'password', ''), 6),
    atLeastOneUppercase: atLeastOneUppercase(get(formValues, 'password', '')),
    atLeastOneLowerrcase: atLeastOneLowerrcase(get(formValues, 'password', '')),
    atLeastOneDigit: atLeastOneDigit(get(formValues, 'password', '')),
    atLeastOnePunctuation: atLeastOnePunctuation(
      get(formValues, 'password', ''),
    ),
  })),
)(PasswordValidation)

const RulesTitle = styled.p`
  margin: 0px;
  background-color: ${th('colorBackgroundHue')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseMedium')};
  line-height: ${th('lineHeightHeading3')};
  color: ${th('colorSecondary')};
`
const Rules = styled.div`
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseMedium')};
  line-height: ${th('lineHeightHeading4')};
  color: ${th('colorText')};
`
const ruleHelper = props => {
  const textDecoration = props.fulfilled ? 'line-through' : 'none'

  let textColor = 'inherit'
  if (props.error) {
    textColor = '#FC6A4B'
  } else if (props.fulfilled) {
    textColor = '#63A945'
  }

  return css`
    text-decoration: ${textDecoration};
    color: ${textColor};
  `
}

const Rule = styled.p`
  margin: 0px;
  ${ruleHelper};
`
