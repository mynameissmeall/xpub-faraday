A pdf file.

```js
<FileItem
  item={{
    id: 'file1',
    name: 'myfile.pdf',
    size: 1231312,
  }}
  onPreview={file => console.log('clicked preview', file)}
  onDownload={file => console.log('download me', file)}
/>
```

A Word document (no preview available).

```js
<FileItem
  item={{
    id: 'file1',
    name: 'myfile.docx',
    size: 51312,
  }}
  onPreview={() => console.log('clicked preview')}
  onDownload={() => console.log('download me')}
/>
```

With a drag handle.

```js
<FileItem
  item={{
    id: 'file1',
    name: 'myfile.pdf',
    size: 1231312,
  }}
  dragHandle={DragHandle}
  onPreview={file => console.log('clicked preview', file)}
  onDownload={file => console.log('download me', file)}
/>
```
