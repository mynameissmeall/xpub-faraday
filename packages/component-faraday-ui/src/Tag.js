import { get } from 'lodash'
import PropTypes from 'prop-types'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

import { marginHelper } from './styledHelpers'

const tagCSS = props => {
  if (get(props, 'oldStatus')) {
    return css`
      background-color: ${th('colorFurnitureHue')};
      height: calc(${th('gridUnit')} * 3)
      font-weight: ${th('tag.fontWeight')};
      padding: calc(${th('gridUnit')} / 2) ${th('gridUnit')} 0px
        ${th('gridUnit')};
    `
  }

  if (get(props, `status`)) {
    return css`
      background-color: ${th('tag.statusBackgroundColor')};
      padding: calc(${th('gridUnit')} / 4) ${th('gridUnit')};
      height: 24px;
      font-family: ${th('fontHeading')};
    `
  }

  return css`
    background-color: ${th('tag.backgroundColor')};
    height: 14px;
    font-size: 85%;
    padding: calc(${th('gridUnit')} / 4) calc(${th('gridUnit')} / 4) 0px;
    margin-right: 1px;
    font-family: ${th('fontInterface')};
  `
}

/** @component */
const Tag = styled.div`
  border-radius: ${th('tag.borderRadius')
    ? th('tag.borderRadius')
    : th('borderRadius')};
  color: ${th('tag.color')};
  font-weight: ${th('tag.fontWeight')};
  font-size: ${th('tag.fontSize')};
  text-align: center;
  vertical-align: baseline;
  width: fit-content;
  white-space: nowrap;
  display: inline-flex;
  align-items: center;

  ${tagCSS};
  ${marginHelper};
`

Tag.propTypes = {
  /** Old status of the corresponding user. */
  oldStatus: PropTypes.bool,
  /** New status of the corresponding user. */
  status: PropTypes.bool,
}

Tag.defaultProps = {
  oldStatus: false,
  status: false,
}

export default Tag
