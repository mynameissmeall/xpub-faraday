import React, { Fragment } from 'react'
import styled from 'styled-components'
import { compose } from 'recompose'
import { reduxForm } from 'redux-form'
import { th } from '@pubsweet/ui-toolkit'
import { required } from 'xpub-validators'
import { H2, Button, ValidatedField, TextArea, Spinner } from '@pubsweet/ui'
import {
  Row,
  Text,
  Label,
  IconButton,
  OpenModal,
  ActionLink,
  ItemOverrideAlert,
  withFetching,
} from 'pubsweet-component-faraday-ui'

const Form = compose(
  withFetching,
  reduxForm({
    form: 'deleteManuscript',
  }),
)(({ fetchingError, hideModal, handleSubmit, isFetching }) => (
  <Root data-test-id="delete-manuscript-modal">
    <IconButton
      icon="x"
      iconSize={2}
      onClick={hideModal}
      right={5}
      secondary
      top={5}
    />
    <H2 ml={4} mr={4}>
      Are you sure you want to remove this manuscript?
    </H2>

    <Row mt={2}>
      <ItemOverrideAlert data-test-id="manuscript-return-reason" vertical>
        <Label required>
          Reason for removing the manuscript from the platform:
        </Label>
        <ValidatedField
          component={TextArea}
          name="comments"
          style={{ maxWidth: '440px', minWidth: '440px' }}
          validate={[required]}
        />
      </ItemOverrideAlert>
    </Row>
    {fetchingError && (
      <Text align="center" error mt={1}>
        {fetchingError}
      </Text>
    )}
    <Row mt={3}>
      {isFetching ? (
        <Spinner size={3} />
      ) : (
        <Fragment>
          <Button data-test-id="modal-cancel" onClick={hideModal}>
            NO
          </Button>
          <Button data-test-id="modal-confirm" onClick={handleSubmit} primary>
            OK
          </Button>
        </Fragment>
      )}
    </Row>
  </Root>
))
const DeleteManuscriptModal = props => (
  <OpenModal component={Form} {...props}>
    {showModal => (
      <DeleteIcon
        fontIcon="deleteIcon"
        onClick={showModal}
        paddingBottom={1}
        paddingRight={0}
        size={1.65}
      >
        Delete
      </DeleteIcon>
    )}
  </OpenModal>
)

export default DeleteManuscriptModal

DeleteManuscriptModal.defaultProps = {
  onSubmit: () => {},
  collectionId: 'santa-claus',
  modalKey: 'deleteManuscriptModal',
}

// #region styles
const DeleteIcon = styled(ActionLink)`
  cursor: default;
  color: ${th('colorWarning')};
`
const Root = styled.div`
  align-items: center;
  background: ${th('colorBackgroundHue')};
  border: ${th('borderWidth')} ${th('borderStyle')} transparent;
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  position: relative;
  padding: calc(${th('gridUnit')} * 5);
  width: calc(${th('gridUnit')} * 65);

  ${H2} {
    text-align: center;
  }
`
// #endregion
