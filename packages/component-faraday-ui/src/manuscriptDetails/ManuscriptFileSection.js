import React, { Fragment } from 'react'
import { Text, FileItem, Item, Row } from 'pubsweet-component-faraday-ui'
import PropTypes from 'prop-types'

const ManuscriptFileSection = ({ list, label, ...rest }) => (
  <Fragment>
    {!!list.length && (
      <Fragment>
        {label && (
          <Text labelLine mb={1} mt={1}>
            {label}
          </Text>
        )}
        <Row flexWrap="wrap" justify="flex-start" mb={1}>
          {list.map(file => (
            <Item
              alignItems="flex-start"
              flex={0}
              key={file.id}
              mr={1}
              vertical
            >
              <FileItem item={file} {...rest} mb={1} />
            </Item>
          ))}
        </Row>
      </Fragment>
    )}
  </Fragment>
)

ManuscriptFileSection.propTypes = {
  /** List of uploaded files */
  list: PropTypes.arrayOf(PropTypes.object),
  /** Category name of uploaded files. */
  label: PropTypes.string,
  /** Callback function fired when download icon is pressed. */
  onDownload: PropTypes.func, // eslint-disable-line
  /** Callback function fired when preview icon is pressed. */
  onPreview: PropTypes.func, // eslint-disable-line
}

ManuscriptFileSection.defaultProps = {
  list: [],
  label: '',
}

export default ManuscriptFileSection
