import React from 'react'
import { ContextualBox, AssignHE } from 'pubsweet-component-faraday-ui'
import PropTypes from 'prop-types'

const ManuscriptAssignHE = ({
  toggle,
  assignHE,
  expanded,
  isFetching,
  currentUser: { permissions: { canAssignHE = false } },
  handlingEditors = [],
  inviteHandlingEditor,
}) =>
  canAssignHE ? (
    <ContextualBox
      data-test-id="assign-handling-editor"
      expanded={expanded}
      label="Assign Handling Editor"
      scrollIntoView
      toggle={toggle}
    >
      <AssignHE
        handlingEditors={handlingEditors}
        inviteHandlingEditor={assignHE}
        isFetching={isFetching}
      />
    </ContextualBox>
  ) : null

ManuscriptAssignHE.propTypes = {
  /** Handling editors you want to be displayed. */
  handlingEditors: PropTypes.arrayOf(PropTypes.object),
  /** Callback function fired when the handling editor is invited. */
  inviteHandlingEditor: PropTypes.func,
}

ManuscriptAssignHE.defaultProps = {
  handlingEditors: [],
  inviteHandlingEditor: () => {},
}
export default ManuscriptAssignHE
