A contextual box for EiC decision.

```js
const eicDecisions = [
  { value: 'return-to-handling-editor', label: 'Return to Handling Editor' },
  { value: 'publish', label: 'Publish' },
  { value: 'reject', label: 'Reject' },
];

const messagesLabel = {
  'return-to-handling-editor': 'Comments for Handling Editor',
  reject: 'Comments for Author',
  publish: 'Comments for Author',
};

<ManuscriptEicDecision
  messagesLabel={messagesLabel}
  options={eicDecisions}
  submitDecision={v => console.log('submit decision', v)}
/>
```
