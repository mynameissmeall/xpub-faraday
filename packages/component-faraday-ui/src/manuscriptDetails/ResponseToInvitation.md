A Handling Editor response to an invitation.

```js
const formValues = {
  decision: 'accept',
}
;<RemoteOpener>
  {({ toggle }) => (
    <ResponseToInvitation
      commentsOn="decline"
      label="Do you agree to be the handling editor for this manuscript?"
      formValues={formValues}
      onResponse={(values, { setFetching }) => {
        console.log('on response: ', values)
        setFetching(true)
      }}
      title="Respond to Editorial Invitation"
      toggle={toggle}
    />
  )}
</RemoteOpener>
```

A Reviewer response to an invitation.

```js
<RemoteOpener>
  {({ toggle }) => (
    <ResponseToInvitation
      label="Do you agree to review this manuscript?"
      onResponse={(values, { setFetching }) => {
        console.log('on response: ', values)
        setFetching(true)
      }}
      title="Respond to Invitation to Review"
      toggle={toggle}
    />
  )}
</RemoteOpener>
```
