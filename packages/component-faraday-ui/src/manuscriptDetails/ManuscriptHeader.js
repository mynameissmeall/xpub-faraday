import React, { Fragment } from 'react'
import { get, chain, isEmpty } from 'lodash'
import PropTypes from 'prop-types'
import { H2, H4, DateParser, Button } from '@pubsweet/ui'
import {
  compose,
  withProps,
  defaultProps,
  withHandlers,
  setDisplayName,
} from 'recompose'

import {
  Tag,
  Row,
  Text,
  AuthorTagList,
  PersonInvitation,
} from 'pubsweet-component-faraday-ui'

const ManuscriptHeader = ({
  renderHE,
  fragment = {},
  manuscriptType = {},
  editorInChief = 'Unassigned',
  collection: { visibleStatus = 'Draft', customId, handlingEditor },
  isLatestVersion,
}) => {
  const { authors = [], metadata = {}, submitted = null } = fragment
  const { title = 'No title', journal = '', type = '' } = metadata
  return (
    <Fragment>
      <Row
        alignItems="baseline"
        data-test-id="manuscript-title"
        justify="space-between"
      >
        <H2 mb={1}>{title}</H2>
        {isLatestVersion ? (
          <Tag data-test-id="fragment-status" status>
            {visibleStatus}
          </Tag>
        ) : (
          <Tag oldStatus>Viewing an Older Version</Tag>
        )}
      </Row>
      {authors.length > 0 && (
        <Row
          alignItems="center"
          data-test-id="authors-row"
          justify="flex-start"
          mb={1}
        >
          <AuthorTagList authors={authors} withAffiliations withTooltip />
        </Row>
      )}
      <Row alignItems="center" justify="flex-start" mb={1}>
        {customId && (
          <Text
            customId
            data-test-id="manuscript-id"
            mr={1}
          >{`ID ${customId}`}</Text>
        )}
        {submitted && (
          <DateParser durationThreshold={0} timestamp={submitted}>
            {timestamp => <Text mr={3}>Submitted on {timestamp}</Text>}
          </DateParser>
        )}
        <Text>{manuscriptType.label || type}</Text>
        <Text journal ml={1}>
          {journal}
        </Text>
      </Row>
      <Row alignItems="center" justify="flex-start" mb={1}>
        <H4>Editor in Chief</H4>
        <Text ml={1} mr={3}>
          {editorInChief}
        </Text>
        <H4>Handling editor</H4>
        {renderHE()}
      </Row>
    </Fragment>
  )
}

ManuscriptHeader.propTypes = {
  /** Component that takes a func that returns a react elem and calls it instead of implementing its own rander. */
  renderHE: PropTypes.func,
  /** Object containing the selected fragment. */
  fragment: PropTypes.object, //eslint-disable-line
  /** Manuscript types that you can chose from. */
  manuscriptType: PropTypes.object, //eslint-disable-line
  /** Status for editor in chief. */
  editorInChief: PropTypes.string,
  /** Object containing the selected collection. */
  collection: PropTypes.object, //eslint-disable-line
  /** Specifies if manuscript is at the latest version. */
  isLatestVersion: PropTypes.bool,
}
ManuscriptHeader.defaultProps = {
  renderHE: () => {},
  fragment: {},
  manuscriptType: {},
  editorInChief: 'Unassigned',
  collection: {},
  isLatestVersion: false,
}

export default compose(
  defaultProps({
    inviteHE: () => {},
    revokeInvitation: () => {},
    resendInvitation: () => {},
  }),
  withProps(
    ({
      journal = {},
      fragment: { metadata },
      collection: { invitations = [] },
    }) => ({
      manuscriptType: get(journal, 'manuscriptTypes', []).find(
        t => t.value === get(metadata, 'type', ''),
      ),
      heInvitation: invitations.find(
        i => i.role === 'handlingEditor' && i.isAccepted,
      ),
      pendingInvitation: invitations.find(
        i => i.role === 'handlingEditor' && !i.hasAnswer,
      ),
    }),
  ),
  withHandlers({
    resendInvitation: ({ resendInvitation }) => (email, props) =>
      resendInvitation(email, props),
    revokeInvitation: ({ revokeInvitation }) => (id, props) =>
      revokeInvitation(id, props),
  }),
  withHandlers({
    renderHE: ({
      inviteHE,
      isFetching,
      heInvitation,
      resendInvitation,
      revokeInvitation,
      pendingInvitation = {},
      handlingEditors = [],
      isLatestVersion,
      currentUser: {
        permissions: { canAssignHE },
        id: currentUserId,
        admin,
        editorInChief,
      },
      collection: { handlingEditor },
      collection,
      currentUser,
    }) => () => {
      if (pendingInvitation.userId === currentUserId) {
        return <Text ml={1}>Invited</Text>
      }
      const invitedHeId =
        get(pendingInvitation, 'userId', false) ||
        get(heInvitation, 'userId', false)

      if (invitedHeId && (admin || editorInChief)) {
        const person = chain(handlingEditors)
          .filter(he => he.id === invitedHeId)
          .map(he => ({ ...he, name: `${he.firstName} ${he.lastName}` }))
          .first()
          .value()

        let invitedHe = {}
        if (get(pendingInvitation, 'userId', false)) {
          invitedHe = pendingInvitation
        } else if (get(heInvitation, 'userId', false)) {
          invitedHe = heInvitation
        }
        return (
          <PersonInvitation
            isFetching={isFetching}
            isLatestVersion={isLatestVersion}
            ml={1}
            withName
            {...invitedHe}
            onResend={resendInvitation}
            onRevoke={revokeInvitation}
            person={person}
          />
        )
      }
      if (!isEmpty(pendingInvitation)) {
        return <Text ml={1}>{handlingEditor.name}</Text>
      }
      if (heInvitation) {
        return <Text ml={1}>{handlingEditor.name}</Text>
      }

      if (canAssignHE) {
        return (
          <Button
            data-test-id="manuscript-invite-he-button"
            ml={1}
            onClick={inviteHE}
            primary
            size="small"
          >
            Invite
          </Button>
        )
      }
      return <Text ml={1}>Unassigned</Text>
    },
  }),
  setDisplayName('ManuscriptHeader'),
)(ManuscriptHeader)
