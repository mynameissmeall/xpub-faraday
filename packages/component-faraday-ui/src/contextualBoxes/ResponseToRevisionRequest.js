import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'

import {
  ContextualBox,
  AuthorReply,
  withFilePreview,
  withFileDownload,
} from '../'

const ResponseToRevisionRequest = ({
  fragment,
  authorReply,
  toggle,
  expanded,
  downloadFile,
  previewFile,
  ...rest
}) => (
  <ContextualBox
    data-test-id="response-to-revision-request"
    expanded={expanded}
    label="Response to Revision Request"
    mb={2}
    toggle={toggle}
    {...rest}
  >
    <AuthorReply
      authorReply={authorReply}
      fragment={fragment}
      onDownload={downloadFile}
      onPreview={previewFile}
    />
  </ContextualBox>
)

ResponseToRevisionRequest.propTypes = {
  /** Object containing the selected fragment. */
  fragment: PropTypes.object, //eslint-disable-line
  /** Callback function used to control the state of the component.
   * To be used together with the `expanded` prop.
   */
  toggle: PropTypes.func,
  /** Prop used together with toggle. */
  expanded: PropTypes.bool,
}
ResponseToRevisionRequest.defaultProps = {
  fragment: {},
  toggle: () => {},
  expanded: false,
}

export default compose(withFilePreview, withFileDownload)(
  ResponseToRevisionRequest,
)
