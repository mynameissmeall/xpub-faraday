AuthReviews.

```js
const reports = [
  {
    id: 'f9d3621f-1689-4afb-965f-98be38d99d9f',
    userId: '84dddb49-177f-4163-89c8-0179184e4395',
    comments: [
      {
        files: [
          {
            id:
              '5c0a233b-2569-443b-8110-ef98a18a60a4/2cac524e-0259-45fb-ad3c-9ebc94af8acc',
            name: '1508309142.png',
            size: 35249,
            originalName: '1508309142.png',
          },
        ],
        public: true,
        content: 'Public report here.',
        public: true,
        content:
          'CEVA, altceva mult mai mult text pentru a vedea unde anume este limita CEVA, altceva mult mai mult text pentru a vedea unde anume este limita CEVA, altceva mult mai mult text pentru a vedea unde anume este limita',
      },
    ],
    createdOn: 1539339578446,
    updatedOn: 1539339580846,
    submittedOn: 1539339580826,
    recommendation: 'minor',
    recommendationType: 'review',
    reviewerNumber: 1,
  },
  {
    id: '21258b47-aba5-4597-926e-765458c4fda2',
    userId: '078c43af-39d8-4ef2-8ca2-f38b57e5c072',
    comments: [
      {
        files: [],
        public: true,
        content: 'ceva',
      },
    ],
    createdOn: 1539689165760,
    updatedOn: 1539689169634,
    submittedOn: 1539689169611,
    recommendation: 'publish',
    recommendationType: 'review',
    reviewerNumber: 2,
  },
]

const journal = {
  recommendations: [
    {
      value: 'publish',
      label: 'Publish',
    },
    {
      value: 'major',
      label: 'Major revision',
    },
    {
      value: 'minor',
      label: 'Minor revision',
    },
    {
      value: 'reject',
      label: 'Reject',
    },
  ],
}
;<AuthorReviews reports={reports} journal={journal} />
```
