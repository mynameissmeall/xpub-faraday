import React, { Fragment } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { required } from 'xpub-validators'
import { Button, FilePicker, Menu, Spinner, ValidatedField } from '@pubsweet/ui'
import { initial } from 'lodash'

import {
  Row,
  Text,
  Item,
  Label,
  FileItem,
  Textarea,
  ActionLink,
  ContextualBox,
  ItemOverrideAlert,
} from 'pubsweet-component-faraday-ui/src'

const allowedFileExtensions = ['pdf', 'doc', 'docx', 'txt', 'rdf', 'odt']
const ReviewerReportForm = ({
  toggle,
  hasNote,
  addNote,
  addFile,
  expanded,
  fileError,
  isFetching,
  removeNote,
  removeFile,
  previewFile,
  downloadFile,
  handleSubmit,
  fetchingError,
  review = {},
  formValues = {},
  journal: { recommendations },
  isFetchingFromAutosave,
}) => (
  <ContextualBox
    data-test-id="submit-report-box"
    expanded={expanded}
    highlight
    label="Your report"
    scrollIntoView
    toggle={toggle}
  >
    <Root data-test-id="report-box-response">
      <Row justify="flex-start">
        <ItemOverrideAlert
          data-test-id="form-report-recommendation"
          flex={0}
          vertical
        >
          <Label required>Recommendation</Label>
          <ValidatedField
            component={input => (
              <Menu {...input} options={initial(recommendations)} />
            )}
            name="recommendation"
            validate={[required]}
          />
        </ItemOverrideAlert>
      </Row>

      <Row
        alignItems="center"
        data-test-id="form-report-upload-file"
        justify="space-between"
        mt={1}
      >
        <Item>
          <Label required>Your report</Label>
          {!formValues.file && (
            <FilePicker
              allowedFileExtensions={allowedFileExtensions}
              onUpload={addFile}
            >
              <ActionLink icon="plus">UPLOAD FILE</ActionLink>
            </FilePicker>
          )}
        </Item>

        <Item justify="flex-end">
          <ActionLink
            fontIcon="linkIcon"
            iconPosition="right"
            to="https://about.hindawi.com/authors/peer-review-at-hindawi/"
          >
            Hindawi Reviewer Guidelines
          </ActionLink>
        </Item>
      </Row>

      <Row mb={1 / 2}>
        <ItemOverrideAlert data-test-id="form-report-textarea" vertical>
          <ValidatedField component={Textarea} name="public" />
        </ItemOverrideAlert>
      </Row>

      {formValues.file && (
        <Row justify="flex-start" mb={1}>
          <Item flex={0}>
            <FileItem
              data-test-id="form-report-file-item-actions"
              item={formValues.file}
              onDelete={removeFile}
              onDownload={downloadFile}
              onPreview={previewFile}
            />
          </Item>
        </Row>
      )}

      <Row alignItems="center">
        {hasNote ? (
          <Fragment>
            <Item>
              <Label>Confidential note for the Editorial Team</Label>
            </Item>
            <Item justify="flex-end">
              <ActionLink
                data-test-id="form-report-remove-note"
                icon="x"
                onClick={removeNote}
              >
                Remove
              </ActionLink>
            </Item>
          </Fragment>
        ) : (
          <Item>
            <ActionLink data-test-id="form-report-add-note" onClick={addNote}>
              Add Confidential note for the Editorial Team
            </ActionLink>
          </Item>
        )}
      </Row>

      {hasNote && (
        <Row>
          <ItemOverrideAlert
            data-test-id="textarea-form-report-add-note"
            vertical
          >
            <ValidatedField component={Textarea} name="confidential" />
          </ItemOverrideAlert>
        </Row>
      )}

      {fetchingError && (
        <Row justify="flex-start">
          <Text error>{fetchingError}</Text>
        </Row>
      )}
      <Row justify="flex-end" mt={1}>
        {isFetching || isFetchingFromAutosave ? (
          <Spinner />
        ) : (
          <Button
            data-test-id="button-submit-report"
            onClick={handleSubmit}
            primary
            size="medium"
          >
            Submit report
          </Button>
        )}
      </Row>
    </Root>
  </ContextualBox>
)

export default ReviewerReportForm

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  padding: calc(${th('gridUnit')} * 2);
`
// #endregion
