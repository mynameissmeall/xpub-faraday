import React from 'react'
import PropTypes from 'prop-types'
import { withProps, compose } from 'recompose'
import { get } from 'lodash'

import {
  ContextualBox,
  ReviewerReportAuthor,
  Row,
  Text,
  indexReviewers,
} from '../'

const SubmittedReportsNumberForAuthorReviews = ({ reports }) => (
  <Row fitContent justify="flex-end">
    <Text customId mr={1 / 2}>
      {reports}
    </Text>
    <Text mr={1 / 2} pr={1 / 2} secondary>
      {' '}
      submitted
    </Text>
  </Row>
)

const AuthorReviews = ({
  token,
  journal,
  reports,
  fragment,
  invitations,
  getSignedUrl,
  reviewerReports,
}) =>
  reports.length > 0 && (
    <ContextualBox
      label="Reviewer Reports"
      mb={2}
      rightChildren={
        <SubmittedReportsNumberForAuthorReviews reports={reports.length} />
      }
    >
      {reports.map(r => (
        <ReviewerReportAuthor
          getSignedUrl={getSignedUrl}
          journal={journal}
          key={r.id}
          report={r}
          token={token}
        />
      ))}
    </ContextualBox>
  )

AuthorReviews.propTypes = {
  /** The list of available reports. */
  reports: PropTypes.arrayOf(PropTypes.object),
  /** Returns the url of the selected file. */
  getSignedUrl: PropTypes.func,
  /** Object containing the list of recommendations. */
  journal: PropTypes.object, //eslint-disable-line
  /** Contains the token of the currently logged user. */
  token: PropTypes.string,
}

AuthorReviews.defaultProps = {
  reports: [],
  getSignedUrl: () => {},
  journal: {},
  token: '',
}

export default compose(
  withProps(
    ({
      invitations = [],
      publonReviewers = [],
      reviewerReports = [],
      currentUser,
    }) => ({
      token: get(currentUser, 'token', ''),
      publonReviewers,
      invitations: invitations.map(i => ({
        ...i,
        review: reviewerReports.find(r => r.userId === i.userId),
      })),
      reports: indexReviewers(
        reviewerReports.filter(r => r.submittedOn),
        invitations,
      ),
    }),
  ),
)(AuthorReviews)
