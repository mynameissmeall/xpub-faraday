import React from 'react'
import { H4 } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import PropTypes from 'prop-types'

import { marginHelper } from './'

const Label = ({ children, required, ...rest }) => (
  <Root {...rest}>
    <H4>{children}</H4>
    {required && <Required>*</Required>}
  </Root>
)

Label.propTypes = {
  /** If true the label is required. */
  required: PropTypes.bool,
}

Label.defaultProps = {
  required: false,
}
export default Label

// #region styles
const Required = styled.span`
  color: ${th('colorError')};
  margin-left: calc(${th('gridUnit')} / 4);
`

const Root = styled.div`
  align-items: center;
  display: flex;

  ${marginHelper};

  ${H4} {
    margin: 0;
  }
`
// #endregion
