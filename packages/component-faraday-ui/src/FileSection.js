import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { FilePicker, Spinner } from '@pubsweet/ui'
import { compose, withState, withHandlers, withProps } from 'recompose'
import PropTypes from 'prop-types'

import { radiusHelpers } from './styledHelpers'
import { withNativeFileDrop, withFileSectionDrop } from './helpers'
import {
  Row,
  Item,
  Text,
  Label,
  FileItem,
  ActionLink,
  DragHandle,
  SortableList,
} from './'

const EXTENSIONS = {
  pdf: 'PDF',
  doc: 'Doc',
  docx: 'DocX',
  png: 'PNG',
  txt: 'TXT',
  rdf: 'RDF',
  odt: 'ODT',
}

const FileSection = ({
  error,
  title,
  isLast,
  listId,
  isFirst,
  required,
  moveItem,
  maxFiles,
  isFetching,
  isFileItemOver,
  canDropFileItem,
  connectFileDrop,
  supportedFormats,
  connectDropTarget,
  allowedFileExtensions,
  files = [],
  onFilePick,
  onPreview,
  onDownload,
  onDelete,
  testId,
}) => (
  <Root
    data-test-id={testId}
    isFileItemOver={isFileItemOver && canDropFileItem}
    isFirst={isFirst}
    isLast={isLast}
    ref={instance => {
      connectFileDrop(instance)
      connectDropTarget(instance)
    }}
  >
    <Row alignItems="center">
      <Item>
        <Label required={required}>{title}</Label>
        {isFetching ? (
          <Spinner />
        ) : (
          <FilePicker
            allowedFileExtensions={allowedFileExtensions}
            disabled={files.length >= maxFiles}
            onUpload={onFilePick}
          >
            <ActionLink
              data-test-id={`add-file-${listId}`}
              disabled={files.length >= maxFiles}
              icon="plus"
              size="small"
            >
              UPLOAD FILE
            </ActionLink>
          </FilePicker>
        )}
      </Item>
      {supportedFormats && (
        <Item justify="flex-end">
          <Text fontStyle="italic" secondary>
            Supported file formats: {supportedFormats}
          </Text>
        </Item>
      )}
    </Row>
    <SortableList
      beginDragProps={['id', 'index', 'name', 'listId']}
      dragHandle={DragHandle}
      items={files}
      listId={listId}
      listItem={FileItem}
      mb={1}
      moveItem={moveItem}
      onDelete={onDelete}
      onDownload={onDownload}
      onPreview={onPreview}
      shadow
    />
    {error && (
      <Row>
        <Text error>{error}</Text>
      </Row>
    )}
  </Root>
)

FileSection.propTypes = {
  /** Files that are uploaded. */
  files: PropTypes.arrayOf(PropTypes.object),
  /** Error you get on uploading. */
  error: PropTypes.string,
  /** Titles of manuscript, cover letter and supplimental files. */
  title: PropTypes.string,
  /** Id of manuscript, cover letter and supplimental files. */
  listId: PropTypes.string,
  /** Function used for draging and hovering over items. */
  moveItem: PropTypes.func,
  /** Extensions allowed to be uploaded. */
  allowedFileExtensions: PropTypes.arrayOf(PropTypes.string),
  /** Callback function fired when a file is picked. */
  onFilePick: PropTypes.func,
  /** Callback function fired when preview icon is pressed. */
  onPreview: PropTypes.func,
  /** Callback function fired when download icon is pressed. */
  onDownload: PropTypes.func,
  /** Callback function fired when delete icon is pressed. */
  onDelete: PropTypes.func,
}

FileSection.defaultProps = {
  files: {},
  error: '',
  title: '',
  listId: '',
  moveItem: () => {},
  allowedFileExtensions: [],
  onFilePick: () => {},
  onPreview: () => {},
  onDownload: () => {},
  onDelete: () => {},
}

export default compose(
  withState('error', 'setStateError', ''),
  withHandlers({
    clearError: ({ setStateError }) => () => {
      setStateError(e => '')
    },
  }),
  withHandlers({
    setError: ({ setStateError, clearError }) => err => {
      setStateError(e => err, () => setTimeout(clearError, 2000))
    },
  }),
  withProps(({ allowedFileExtensions = [] }) => ({
    supportedFormats: allowedFileExtensions
      .map(ext => EXTENSIONS[ext.toLowerCase()])
      .join(', '),
  })),
  withFileSectionDrop,
  withNativeFileDrop,
)(FileSection)

// #region styles
const Root = styled.div`
  background: ${props =>
    props.isFileItemOver ? th('colorFurniture') : th('colorBackground')};
  min-height: calc(${th('gridUnit')} * 22);
  padding: 0 ${th('gridUnit')};
  width: 100%;

  ${radiusHelpers};
  border-bottom: ${props => (!props.isLast ? '1px dashed #dbdbdb' : 'none')};
`
// #endregion
