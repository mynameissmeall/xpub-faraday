import React, { Fragment } from 'react'
import { get, last, head } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, Spinner } from '@pubsweet/ui'
import { compose, withHandlers, withProps } from 'recompose'
import PropTypes from 'prop-types'
import { Label, OpenModal, Text, withFetching, ActionLink } from '../'

const TableView = ({
  reviewers,
  onInviteReviewer,
  setFetching,
  isFetching,
  publonsError,
}) => {
  if (publonsError) {
    return (
      <Text align="center" error>
        {publonsError}
      </Text>
    )
  }
  return reviewers.length === 0 ? (
    <Text align="center">No suggestions yet.</Text>
  ) : (
    <Table>
      <thead>
        <tr>
          <th>
            <Label>Full Name</Label>
          </th>
          <th>
            <Label>Affiliation</Label>
          </th>
          <th>
            <Label>No. of Reviews</Label>
          </th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        {reviewers.map(reviewer => (
          <TableRow key={reviewer.email}>
            <td>
              <ActionLink
                to={get(reviewer, 'profileUrl', 'https://www.publons.com/')}
              >
                {`${get(reviewer, 'name', '')}`}
              </ActionLink>
            </td>
            <td>
              <Text>{`${get(reviewer, 'affiliation', '')}`}</Text>
            </td>
            <td>
              <Text>{`${get(reviewer, 'reviews', '')}`}</Text>
            </td>
            <HiddenCell>
              <OpenModal
                confirmText="Invite"
                onConfirm={modalProps => onInviteReviewer(reviewer, modalProps)}
                publonsFetching={isFetching}
                setFetching={setFetching}
                title="Send invitation to review?"
              >
                {showModal => (
                  <Button onClick={showModal} primary size="small">
                    SEND
                  </Button>
                )}
              </OpenModal>
            </HiddenCell>
          </TableRow>
        ))}
      </tbody>
    </Table>
  )
}

const PublonsTable = ({ publonsFetching, ...rest }) => (
  <Fragment>{publonsFetching ? <Spinner /> : <TableView {...rest} />}</Fragment>
)

export default compose(
  withFetching,
  withProps(({ reviewers = [] }) => ({
    reviewers,
  })),
  withHandlers({
    onInviteReviewer: ({ onInvite }) => (reviewer, modalProps) => {
      const names = reviewer.name.split(' ')
      const newReviewer = {
        email: reviewer.email,
        role: 'reviewer',
        firstName: head(names),
        lastName: last(names),
      }
      onInvite(newReviewer, modalProps)
    },
  }),
)(PublonsTable)

TableView.propTypes = {
  reviewers: PropTypes.arrayOf(PropTypes.object),
  /** Sends an invitation to the reviewer. */
  onInviteReviewer: PropTypes.func,
}

TableView.defaultProps = {
  reviewers: [],
  onInviteReviewer: () => {},
}

// #region styles
const Table = styled.table`
  border-collapse: collapse;

  & thead {
    border: 1px solid ${th('colorBorder')};
    background-color: ${th('colorBackgroundHue2')};
    padding-top: calc(${th('gridUnit')} * 2);
  }

  & th,
  & td {
    border: none;
    padding-left: calc(${th('gridUnit')} * 2);
    text-align: start;
    vertical-align: middle;

    height: calc(${th('gridUnit')} * 5);
    min-width: calc(${th('gridUnit')} * 12);
  }
`

const HiddenCell = styled.td`
  opacity: 0;
  padding-top: ${th('gridUnit')};
`

const HidableCell = styled.td`
  opacity: 1;
  padding-top: ${th('gridUnit')};
`

const TableRow = styled.tr`
  background-color: ${th('colorBackgroundHue2')};
  border-bottom: 1px solid ${th('colorBorder')};

  & td:first-child {
    min-width: calc(${th('gridUnit')} * 20);
  }

  & td:last-child {
    vertical-align: top;
    text-align: right;
    padding-right: calc(8px * 2);
  }

  &:hover {
    background: ${th('colorBackgroundHue3')};

    ${HiddenCell} {
      opacity: 1;
    }
    ${HidableCell} {
      opacity: 0;
    }
  }
`
// #endregion
