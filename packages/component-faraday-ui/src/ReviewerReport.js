import React, { Fragment } from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import { withProps } from 'recompose'
import styled from 'styled-components'
import { DateParser } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { Label, Item, FileItem, Row, Text } from './'

const ReviewerReport = ({
  journal,
  showOwner,
  onPreview,
  onDownload,
  reportFile,
  currentUser,
  publicReport,
  privateReport,
  reviewerName,
  reviewerNumber,
  recommendation,
  report: { submittedOn },
}) => (
  <Root>
    <Row justify="space-between" mb={2}>
      <Item vertical>
        <Label mb={1 / 2}>Recommendation</Label>
        <Text>{recommendation}</Text>
      </Item>

      <Item justify="flex-end">
        <Fragment>
          {showOwner && <Text>{reviewerName}</Text>}
          <Text customId ml={1} mr={1}>
            {`Reviewer ${reviewerNumber}`}
          </Text>
        </Fragment>
        <DateParser timestamp={submittedOn}>
          {date => <Text>{date}</Text>}
        </DateParser>
      </Item>
    </Row>

    {publicReport && (
      <Row mb={2}>
        <Item vertical>
          <Label mb={1 / 2}>Report</Label>
          <Text whiteSpace="pre-wrap">{publicReport}</Text>
        </Item>
      </Row>
    )}

    {reportFile && (
      <Fragment>
        <Label mb={1 / 2}>Report file</Label>
        <Row justify="flex-start" mb={2}>
          <Item flex={0} mr={1}>
            <FileItem
              item={reportFile}
              onDownload={onDownload}
              onPreview={onPreview}
            />
          </Item>
        </Row>
      </Fragment>
    )}

    {privateReport && (
      <Row mb={2}>
        <Item vertical>
          <Label mb={1 / 2}>Confidential note for the Editorial Team</Label>
          <Text whiteSpace="pre-wrap">{privateReport}</Text>
        </Item>
      </Row>
    )}
  </Root>
)

export default withProps(
  ({ report, currentUser, journal: { recommendations = [] } }) => ({
    recommendation: get(
      recommendations.find(r => r.value === report.recommendation),
      'label',
    ),
    reportFile: get(report, 'comments.0.files.0'),
    publicReport: get(report, 'comments.0.content'),
    privateReport: get(report, 'comments.1.content'),
    reviewerName: `${get(currentUser, 'firstName', '')} ${get(
      currentUser,
      'lastName',
      '',
    )}`,
  }),
)(ReviewerReport)

ReviewerReport.propTypes = {
  /** True when you want to show reviewer name. */
  showOwner: PropTypes.bool,
  /** Pass object with informations about the report. */
  report: PropTypes.shape({
    /** Unique id for report. */
    id: PropTypes.string,
    /** Unique id for user. */
    userId: PropTypes.string,
    /** Comments by reviewers. */
    comments: PropTypes.arrayOf(PropTypes.object),
    /** When the comment was created. */
    createdOn: PropTypes.number,
    /** When the comment was updated. */
    updatedOn: PropTypes.number,
    /** When the comment was submited. */
    submittedOn: PropTypes.number,
    /** The recommendation given by reviewer. */
    recommendation: PropTypes.string,
    /** Type of recommendation. */
    recommendationType: PropTypes.string,
  }),
  /** Pass object with informations about recommendation.  */
  journal: PropTypes.shape({
    recommendations: PropTypes.arrayOf(PropTypes.object),
  }),
}

ReviewerReport.defaultProps = {
  showOwner: false,
  report: {},
  journal: { recommendation: [] },
}
// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBackgroundHue3')};
  border-radius: ${th('borderRadius')};
  padding: calc(${th('gridUnit')} * 2);
  margin: ${th('gridUnit')};
`
// #endregion
