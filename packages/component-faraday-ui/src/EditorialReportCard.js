import React, { Fragment } from 'react'
import { get } from 'lodash'
import { withProps, withHandlers, compose } from 'recompose'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { DateParser } from '@pubsweet/ui'
import PropTypes from 'prop-types'

import { Label, Item, Row, Text, Tag } from './'
import { getReportComments } from './helpers'

const EditorialReportCard = ({
  publicLabel,
  privateLabel,
  journal,
  publicReport,
  privateReport,
  recommendation,
  editorName,
  editorRole,
  handlingEditorName,
  collection,
  report: { createdOn, reviewer },
}) => (
  <Root data-test-id="editorial-report-card">
    <Row justify="space-between" mb={2}>
      <Item vertical>
        <Label mb={1 / 2}>
          {editorRole === 'HE' ? 'Recommendation' : 'Decision'}
        </Label>
        <Text>{recommendation}</Text>
      </Item>

      <Item justify="flex-end">
        {reviewer && (
          <Fragment>
            <Text mr={1 / 2}>
              {handlingEditorName !== 'Assigned' ? editorName : ''}
            </Text>
            <Tag mr={2}>{editorRole}</Tag>
          </Fragment>
        )}
        <DateParser timestamp={createdOn}>
          {date => <Text>{date}</Text>}
        </DateParser>
      </Item>
    </Row>

    {publicReport && (
      <Row mb={2}>
        <Item vertical>
          <Label mb={1 / 2}>{publicLabel}</Label>
          <Text whiteSpace="pre-wrap">{publicReport}</Text>
        </Item>
      </Row>
    )}

    {privateReport && (
      <Row mb={2}>
        <Item vertical>
          <Label mb={1 / 2}>{privateLabel}</Label>
          <Text whiteSpace="pre-wrap">{privateReport}</Text>
        </Item>
      </Row>
    )}
  </Root>
)

export default compose(
  withHandlers({
    getEditorRole: ({ report }) => () => {
      if (get(report, 'reviewer.handlingEditor')) {
        return 'HE'
      }
      return get(report, 'reviewer.editorInChief') ? 'EiC' : ''
    },
    getEditorName: ({ report }) => () =>
      `${get(report, 'reviewer.firstName', '')} ${get(
        report,
        'reviewer.lastName',
        '',
      )}`,
    getHandlingEditorName: ({ collection }) => () =>
      `${get(collection, 'handlingEditor.name', '')}`,
    getRecommendationLabel: ({
      report,
      journal: { recommendations = [] },
    }) => () =>
      get(
        recommendations.find(r => r.value === report.recommendation),
        'label',
        'Return to HE',
      ),
  }),
  withProps(
    ({
      report,
      getEditorRole,
      getEditorName,
      getHandlingEditorName,
      getRecommendationLabel,
    }) => ({
      recommendation: getRecommendationLabel(),
      publicReport: getReportComments({ report, isPublic: true }),
      privateReport: getReportComments({ report, isPublic: false }),
      handlingEditorName: getHandlingEditorName(),
      editorName: getEditorName(),
      editorRole: getEditorRole(),
    }),
  ),
)(EditorialReportCard)

EditorialReportCard.propTypes = {
  /** Label that will be publicly viewed, for example, a message for the author will be seen by other roles also. */
  publicLabel: PropTypes.string,
  /** Label that will only be viewed as private message, for example, by the Editorial Team. */
  privateLabel: PropTypes.string,
  /** Message by editorial team and other information. */
  report: PropTypes.shape({
    id: PropTypes.string,
    userId: PropTypes.string,
    comments: PropTypes.arrayOf(PropTypes.object),
    createdOn: PropTypes.number,
    updatedOn: PropTypes.number,
    submittedOn: PropTypes.number,
    recommendation: PropTypes.string,
    recommendationType: PropTypes.string,
    reviewer: PropTypes.object,
  }),
  /** Object containing the list of recommendations. */
  journal: PropTypes.shape({
    recommendation: PropTypes.arrayOf(PropTypes.object),
  }),
}

EditorialReportCard.defaultProps = {
  publicLabel: '',
  privateLabel: '',
  report: {},
  journal: {},
}
// #region styles
const Root = styled.div`
  box-shadow: ${th('boxShadow')};
  padding: calc(${th('gridUnit')} * 2);
  margin: ${th('gridUnit')};
`
// #endregion
