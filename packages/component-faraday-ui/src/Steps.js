import React from 'react'
import { Steps } from '@pubsweet/ui'

export default () => (
  <Steps currentStep={1}>
    <Steps.Step key="step-1" title="Previous" />
    <Steps.Step key="step-2" title="Current step" />
    <Steps.Step key="step-3" title="Next step" />
  </Steps>
)
