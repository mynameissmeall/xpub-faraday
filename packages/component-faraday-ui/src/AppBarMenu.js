import React from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import {
  compose,
  withProps,
  withHandlers,
  setDisplayName,
  withStateHandlers,
} from 'recompose'

import { Text, IconButton } from './'

const AppBarMenu = ({
  goTo,
  logout,
  expanded,
  username,
  toggleMenu,
  currentUser = {},
}) =>
  currentUser.user ? (
    <Root>
      <User data-test-id="admin-menu-button" onClick={toggleMenu}>
        <Text>{username}</Text>
        <IconButton
          fontIcon={expanded ? 'arrowUp' : 'arrowDown'}
          ml={1}
          secondary
          size={2}
        />
      </User>
      {expanded && (
        <Dropdown data-test-id="admin-menu-dropdown">
          {currentUser.user.admin && (
            <DropdownOption
              data-test-id="admin-dropdown-dashboard"
              onClick={goTo('/admin')}
            >
              Admin Dashboard
            </DropdownOption>
          )}
          <DropdownOption
            data-test-id="admin-dropdown-profile"
            onClick={goTo('/profile')}
          >
            My Profile
          </DropdownOption>
          <DropdownOption data-test-id="admin-dropdown-logout" onClick={logout}>
            Logout
          </DropdownOption>
        </Dropdown>
      )}
      {expanded && <ToggleOverlay onClick={toggleMenu} />}
    </Root>
  ) : null

export default compose(
  withStateHandlers(
    { expanded: false },
    { toggleMenu: ({ expanded }) => () => ({ expanded: !expanded }) },
  ),
  withProps(({ currentUser }) => ({
    username: get(
      currentUser,
      'user.firstName',
      get(currentUser, 'user.username', 'User'),
    ),
  })),
  withHandlers({
    goTo: ({ toggleMenu, goTo }) => path => () => {
      toggleMenu()
      goTo(path)
    },
    logout: ({ logout, toggleMenu }) => () => {
      toggleMenu()
      logout()
    },
  }),
  setDisplayName('AppBarMenu'),
)(AppBarMenu)

// #region styles
const User = styled.div`
  align-items: center;
  display: flex;
  cursor: pointer;
`
const Dropdown = styled.div.attrs(props => ({
  'data-test-id': props['data-test-id'] || 'admin-dropdown',
}))`
  background-color: ${th('appBar.colorBackground')};
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};

  position: absolute;
  top: calc(${th('gridUnit')} * 4);
  width: calc(${th('gridUnit')} * 18);
  z-index: 10;
`

const DropdownOption = styled.div.attrs(props => ({
  'data-test-id': props['data-test-id'] || 'dropdown-option',
}))`
  align-items: center;
  color: ${th('colorText')};
  cursor: pointer;
  display: flex;
  justify-content: flex-start;

  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBase')};
  line-height: ${th('lineHeightBase')};

  height: calc(${th('gridUnit')} * 4);
  padding: ${th('gridUnit')};

  &:hover {
    background-color: ${th('menu.optionBackground')};
  }
`

const Root = styled.div`
  align-items: center;
  display: flex;
  justify-content: flex-end;
  margin-left: ${th('gridUnit')};
  position: relative;
`

const ToggleOverlay = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  opacity: 0;
`
// #endregion
