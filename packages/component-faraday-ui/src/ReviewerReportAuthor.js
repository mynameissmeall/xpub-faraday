import React, { Fragment } from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { DateParser } from '@pubsweet/ui'
import { withProps, compose } from 'recompose'

import {
  Row,
  Text,
  Item,
  Label,
  FileItem,
  withFilePreview,
  withFileDownload,
} from './'

const ReviewerReportAuthor = ({
  journal,
  showOwner,
  reviewFile,
  previewFile,
  downloadFile,
  publicReport,
  reviewerName,
  reviewerNumber,
  recommendation,
  report: { submittedOn },
}) => (
  <Root data-test-id="reviewer-reports-for-author">
    <Row justify="space-between" mb={2}>
      <Item justify="flex-end">
        {publicReport && (
          <Row mb={1}>
            <Item vertical>
              <Label mb={1 / 2}>Report</Label>
              <Text whiteSpace="pre-wrap">{publicReport}</Text>
            </Item>
          </Row>
        )}
        <Text customId ml={1} mr={1} whiteSpace="nowrap">
          {`Reviewer ${reviewerNumber}`}
        </Text>
        <DateParser timestamp={submittedOn}>
          {date => <Text>{date}</Text>}
        </DateParser>
      </Item>
    </Row>

    {reviewFile && (
      <Fragment>
        <Label mb={1 / 2}>Files</Label>
        <Row justify="flex-start" mb={1 / 2}>
          <Item flex={0} mr={1}>
            <FileItem
              item={reviewFile}
              onDownload={downloadFile}
              onPreview={previewFile}
            />
          </Item>
        </Row>
      </Fragment>
    )}
  </Root>
)

export default compose(
  withFilePreview,
  withFileDownload,
  withProps(({ report, journal: { recommendations = [] } }) => ({
    recommendation: get(
      recommendations.find(r => r.value === report.recommendation),
      'label',
    ),
    reviewFile: get(report, 'comments.0.files.0'),
    publicReport: get(report, 'comments.0.content'),
    reviewerName: `${get(report, 'reviewer.firstName', '')} ${get(
      report,
      'reviewer.lastName',
      '',
    )}`,
    reviewerNumber: get(report, 'reviewerNumber', ''),
  })),
)(ReviewerReportAuthor)

ReviewerReportAuthor.propTypes = {
  /** True when you want to show reviewer name. */
  showOwner: PropTypes.bool,
  /** Pass object with informations about the report. */
  report: PropTypes.shape({
    /** Unique id for report. */
    id: PropTypes.string,
    /** Unique id for user. */
    userId: PropTypes.string,
    /** Comments by reviewers. */
    comments: PropTypes.arrayOf(PropTypes.object),
    /** When the comment was created. */
    createdOn: PropTypes.number,
    /** When the comment was updated. */
    updatedOn: PropTypes.number,
    /** When the comment was submited. */
    submittedOn: PropTypes.number,
    /** The recommendation given by reviewer. */
    recommendation: PropTypes.string,
    /** Type of recommendation. */
    recommendationType: PropTypes.string,
  }),
  /** Pass object with informations about recommendation.  */
  journal: PropTypes.shape({
    recommendations: PropTypes.arrayOf(PropTypes.object),
  }),
}

ReviewerReportAuthor.defaultProps = {
  showOwner: false,
  report: {},
  journal: { recommendation: [] },
}

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBackgroundHue3')};
  border-radius: ${th('borderRadius')};
  padding: calc(${th('gridUnit')} * 2);
  margin: ${th('gridUnit')};
`
// #endregion
