A three section file component used in the submission flow.

```js
const files = {
  manuscripts: [
    {
      id: 'file1',
      name: 'myfile.docx',
      size: 51312,
    },
    {
      id: 'file2',
      name: 'another_pdf.pdf',
      size: 133127,
    },
    {
      id: 'file3',
      name: 'another_pdf1231.pdf',
      size: 1337,
    },
  ],
  coverLetter: [
    {
      id: 'cover1',
      name: 'myfile.pdf',
      size: 312,
    },
  ],
  supplementary: [],
};

<WizardFiles files={files} uploadFile={f => console.log('upload file ', f)} />
```
