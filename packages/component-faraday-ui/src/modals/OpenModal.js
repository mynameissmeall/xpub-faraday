import React from 'react'
import { compose, withHandlers, withProps } from 'recompose'
import { withModal } from 'pubsweet-component-modal/src/components'
import PropTypes from 'prop-types'
import { MultiAction, SingleActionModal } from './'

const OpenModal = ({
  showModal,
  title,
  subtitle,
  confirmText,
  cancelText,
  children,
  onConfirm,
  onCancel,
  onClose,
  content,
  modalKey,
}) => <div>{children(showModal)}</div>

const selectModalComponent = props => {
  if (props.component) {
    return {
      modalComponent: props.component,
    }
  }
  if (props.single) {
    return {
      modalComponent: SingleActionModal,
    }
  }
  return {
    modalComponent: MultiAction,
  }
}

OpenModal.propTypes = {
  /** Title that will be showed on the card. */
  title: PropTypes.string,
  /** Subtitle that will be showed on the card. */
  subtitle: PropTypes.string,
  /** The text you want to see on the button when someone submit the report. */
  confirmText: PropTypes.string,
  /** The text you want to see on the button when someone cancel the report. */
  cancelText: PropTypes.string,
  /** Callback function fired when cancel confirmation card. */
  onCancel: PropTypes.func,
  /** Callback function fired when confirm confirmation card. */
  onConfirm: PropTypes.func,
  /** When is true will show a spinner.  */
  onClose: PropTypes.func,
  /** The text you want to show on the card. */
  content: PropTypes.func,
  /** Unique key for modal box. */
  modalKey: PropTypes.string,
}
OpenModal.defaultProps = {
  title: '',
  subtitle: '',
  confirmText: 'OK',
  cancelText: 'Cancel',
  onCancel: () => {},
  onConfirm: () => {},
  onClose: () => {},
  content: null,
  modalKey: 'aModal',
}

export default compose(
  withProps(selectModalComponent),
  withModal(({ isFetching, modalKey, modalComponent }) => ({
    isFetching,
    modalComponent,
  })),
  withHandlers({
    showModal: props => () => {
      props.showModal(props)
    },
  }),
)(OpenModal)
