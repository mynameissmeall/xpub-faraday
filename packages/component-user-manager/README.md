# User Manager Component

### Add Author to a Collection [POST]

This endpoint allows creates a new Author, linking it to a Collection through a Team.

#### Add Author Request

`POST /api/collections/:collectionId/users`

| URI Parameter | Requiered | Requirements | Description |
| ------------- | --------- | ------------ | ------------------ |
| collectionId | No | String | The ID of the collection |

#### Add Author Body

```json
{
  "email": "email@example.com",
  "role": "author", [acceptedValues: author],
  "isSubmitting": true,
  "isCorresponding": false
}
```

#### Add Author Response

```javascript
HTTP/1.1 200 OK
{
  "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
  "type": "user",
  "admin": false,
  "email": "email@example.com",
  "teams": [
    "c576695a-7cda-4e27-8e9c-31f3a0e9d592"
  ],
  "username": "email@example.com",
  "fragments": [],
  "collections": [],
  "isConfirmed": false,
  "editorInChief": false,
  "handlingEditor": false,
  "passwordResetToken": "04590a2b7f6c1f37cb84881d529e516fa6fc309c205a07f1341b2bfaa6f2b46c"
}
```

---

### Get Collection Authors [GET]

This endpoint allows you to get a list of authors for a collection.

#### Get Collection Authors Request

`GET /api/collections/:collectionId/users`

| Query Parameter | Requiered | Requirements | Description              |
| --------------- | --------- | ------------ | ------------------------ |
| collectionId    | Yes       | String       | Collection ID         |

#### Get Collection Authors Response

```javascript
HTTP/1.1 200 OK
[
  {
    "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
    "type": "user",
    "admin": false,
    "email": "email@example.com",
    "teams": [
      "c576695a-7cda-4e27-8e9c-31f3a0e9d592"
    ],
    "username": "email@example.com",
    "fragments": [],
    "collections": [],
    "isConfirmed": false,
    "editorInChief": false,
    "handlingEditor": false,
    "passwordResetToken": "04590a2b7f6c1f37cb84881d529e516fa6fc309c205a07f1341b2bfaa6f2b46c",
    "isSubmitting": true,
    "isCorresponding": false,
  },
  {
    "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
    "type": "user",
    "admin": false,
    "email": "email2@example.com",
    "teams": [
      "c576695a-7cda-4e27-8e9c-31f3a0e9d592"
    ],
    "username": "email2@example.com",
    "fragments": [],
    "collections": [],
    "isConfirmed": true,
    "editorInChief": false,
    "handlingEditor": false,
    "isSubmitting": false,
    "isCorresponding": false,
  }
]
```

---
### Update Author [PATCH]

This endpoint allows you to update an author's data.

#### Update Author Request

`PATCH /api/collections/:collectionId/invitations/:invitationId`

| URI Parameter | Requiered | Requirements | Description              |
| --------------- | --------- | ------------ | ------------------------ |
| collectionId    | Yes       | String       | Collection ID         |
| userId    | Yes       | String       | User ID |

#### Update Author Body

```json
{
  "isSubmitting": false,
  "isCorresponding": true,
  "firstName": "John",
  "lastName": "Smith",
  "affiliation": "UCLA"
}
```

#### Update Author Response

```javascript
HTTP/1.1 200 OK
{
  "id": "7e8a77f9-8e5c-4fa3-b717-8df9932df128",
  "type": "collection",
  "owners": [
    {
       "id": "69ac1ee9-08a8-4ee6-a57c-c6c8be8d3c4f",
       "username": "admin"
    }
  ],
  "authors": [
    {
      "userId": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
      "isSubmitting": false,
      "isCorresponding": true
    }
  ],
  "created": 1522829424474,
  "customId": "9424466",
  "fragments": [
    "c35d0bd8-be03-4c16-b869-bd69796c5a21"
  ],
  "invitations": [
    {
      "id": "9043a836-0d49-4b8d-be0b-df39071b5c57",
      "hasAnswer": false,
      "timestamp": 1522831123430,
      "isAccepted": false
    },
  ]
}
```
---
### Delete Author [DELETE]

This endpoint allows you to delete an author from a collection.

#### Delete Author Request

`DELETE /api/collections/:collectionId/users/:userId`

| Query Parameter | Requiered | Requirements | Description              |
| --------------- | --------- | ------------ | ------------------------ |
| collectionId    | Yes       | String       | Collection ID         |
| userId    | Yes       | String       | User ID |

#### Delete Author Response

```javascript
HTTP/1.1 200 OK
{}
```
---
### Reset Password [POST]

This endpoint allows you to reset your password.

#### Reset Password Request

`POST /api/users/reset-password`


####  Reset Password Body

```json
{
    "email": "email@example.com",
    "token": "123123123",
    "password": "password",
    "firstName": "John",
    "lastName": "Smith",
    "affiliation": "UCLA",
    "title": "Mr"
}
```

#### Reset Password Response

```javascript
HTTP/1.1 200 OK
{
  "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
  "type": "user",
  "admin": false,
  "email": "email@example.com",
  "teams": [],
  "username": "email@example.com",
  "fragments": [],
  "collections": [],
  "isConfirmed": true,
  "editorInChief": false,
  "handlingEditor": false
}
```