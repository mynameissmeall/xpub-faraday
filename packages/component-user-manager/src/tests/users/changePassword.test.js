process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const httpMocks = require('node-mocks-http')
const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')

const { Model, fixtures } = fixturesService

const { user } = fixtures.users
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const reqBody = {
  currentPassword: 'password',
  password: 'N3wPassword!',
}

const notFoundError = new Error()
notFoundError.name = 'NotFoundError'
notFoundError.status = 404
const changePasswordPath = '../../routes/users/changePassword'

describe('Users password reset route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })
  it('should return an error when some parameters are missing', async () => {
    delete body.password
    const req = httpMocks.createRequest({ body })
    req.user = user.id

    const res = httpMocks.createResponse()
    await require(changePasswordPath)(models)(req, res)

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Missing required params.')
  })
  it('should return an error when the password is too small', async () => {
    body.password = 'small'
    const req = httpMocks.createRequest({ body })
    req.user = user.id

    const res = httpMocks.createResponse()
    await require(changePasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(
      'Password is too weak. Please check password requirements.',
    )
  })
  it('should return an error when user is not found', async () => {
    const req = httpMocks.createRequest({ body })
    req.user = 'invalid-user-id'

    const res = httpMocks.createResponse()
    await require(changePasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('User not found')
  })
  it('should return an error when the current password is incorrect', async () => {
    body.currentPassword = 'invalid-password'
    const req = httpMocks.createRequest({ body })
    req.user = user.id

    const res = httpMocks.createResponse()
    await require(changePasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Wrong username or password.')
  })
  it('should return success when the body is correct', async () => {
    const req = httpMocks.createRequest({ body })
    req.user = user.id

    const res = httpMocks.createResponse()
    await require(changePasswordPath)(models)(req, res)

    expect(res.statusCode).toBe(200)
    const savedUser = JSON.parse(res._getData())

    savedUser.validPassword = user.validPassword
    expect(savedUser.token).not.toEqual(user.token)

    expect(savedUser.validPassword(body.password)).toBeTruthy()
  })
})
