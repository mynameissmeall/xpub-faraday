process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const httpMocks = require('node-mocks-http')
const cloneDeep = require('lodash/cloneDeep')
const Chance = require('chance')

const fixturesService = require('pubsweet-component-fixture-service')

const { Model, fixtures } = fixturesService
const chance = new Chance()

const { user } = fixtures.users
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const reqBody = {
  id: user.id,
  subscribe: chance.bool(),
  token: user.accessTokens.unsubscribe,
}

const notFoundError = new Error()
notFoundError.name = 'NotFoundError'
notFoundError.status = 404
const forgotPasswordPath = '../../routes/users/subscribe'

describe('Users subscribe/unsubscribe route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })

  it('should return an error when some parameters are missing', async () => {
    delete body.id
    const req = httpMocks.createRequest({ body })

    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Missing required params.')
  })
  it('should return an error when the unsubscribe token does not match', async () => {
    body.token = 'invalid-token'

    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(`Invalid token: ${body.token}`)
  })
  it('should return an error when the user does not exist', async () => {
    body.id = 'invalid-user-id'

    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('User not found')
  })
  it('should return success when the body is correct', async () => {
    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)
    const data = JSON.parse(res._getData())

    expect(data.user.notifications.email.user).toEqual(body.subscribe)
  })
})
