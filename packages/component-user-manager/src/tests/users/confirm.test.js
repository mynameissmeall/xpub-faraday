process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const httpMocks = require('node-mocks-http')
const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')

const { Model, fixtures } = fixturesService

const { user, author, inactiveUser } = fixtures.users
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const reqBody = {
  userId: user.id,
  confirmationToken: user.accessTokens.confirmation,
}

const notFoundError = new Error()
notFoundError.name = 'NotFoundError'
notFoundError.status = 404
const forgotPasswordPath = '../../routes/users/confirm'

describe('Users confirm route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })

  it('should return an error when some parameters are missing', async () => {
    delete body.userId
    const req = httpMocks.createRequest({ body })

    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Missing required params')
  })
  it('should return an error when the confirmation token does not match', async () => {
    body.confirmationToken = 'invalid-token'

    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Wrong confirmation token.')
  })
  it('should return an error when the user does not exist', async () => {
    body.userId = 'invalid-user-id'

    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('User not found')
  })
  it('should return an error when the user is already confirmed', async () => {
    body.userId = author.id
    body.confirmationToken = author.accessTokens.confirmation
    author.isConfirmed = true

    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('User is already confirmed.')
  })
  it('should return success when the body is correct', async () => {
    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)
    const data = JSON.parse(res._getData())
    expect(data.token).toBeDefined()
  })
  it('should return an error when the user is inactive', async () => {
    body.userId = inactiveUser.id
    body.confirmationToken = inactiveUser.accessTokens.confirmation
    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Unauthorized.')
  })
})
