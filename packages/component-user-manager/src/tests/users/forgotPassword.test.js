process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const httpMocks = require('node-mocks-http')
const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')

const { Model, fixtures } = fixturesService

const { user, author, inactiveUser } = fixtures.users
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const reqBody = {
  email: user.email,
}

const notFoundError = new Error()
notFoundError.name = 'NotFoundError'
notFoundError.status = 404
const forgotPasswordPath = '../../routes/users/forgotPassword'

describe('Users forgot password route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })

  it('should return an error when some parameters are missing', async () => {
    delete body.email
    const req = httpMocks.createRequest({ body })

    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Email address is required.')
  })
  it('should return an error when the user has already requested a password reset', async () => {
    body.email = author.email

    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('A password reset has already been requested.')
  })
  it('should return an error when the user is inactive', async () => {
    body.email = inactiveUser.email
    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)

    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Unauthorized.')
  })
  it('should return success when the body is correct', async () => {
    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)

    // expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())
    expect(data.message).toEqual(
      `A password reset email has been sent to ${body.email}.`,
    )
  })
  it('should return success if the email is non-existent', async () => {
    body.email = 'email@example.com'
    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(forgotPasswordPath)(models)(req, res)

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())
    expect(data.message).toEqual(
      `A password reset email has been sent to ${body.email}.`,
    )
  })
})
