process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const httpMocks = require('node-mocks-http')
const Chance = require('chance')
const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')

const { Model, fixtures } = fixturesService

jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const chance = new Chance()

const { author, submittingAuthor, inactiveUser } = fixtures.users
const { collection } = fixtures.collections
const postPath = '../../routes/fragmentsUsers/post'
const reqBody = {
  email: chance.email(),
  role: 'author',
  isSubmitting: true,
  isCorresponding: false,
  firstName: chance.first(),
  lastName: chance.last(),
  country: chance.country(),
  affiliation: chance.company(),
}
describe('Post fragments users route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })
  it('should return success when an author adds a new user to a fragment', async () => {
    const req = httpMocks.createRequest({
      body,
    })
    req.user = submittingAuthor.id
    req.params.collectionId = collection.id
    const [fragmentId] = collection.fragments
    req.params.fragmentId = fragmentId
    const res = httpMocks.createResponse()
    await require(postPath)(models)(req, res)

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())
    expect(data.email).toEqual(body.email)
    expect(data.invitations).toBeUndefined()
  })
  it('should return success when an author adds an existing user as co author to a fragment', async () => {
    body.email = author.email
    const req = httpMocks.createRequest({
      body,
    })
    req.user = submittingAuthor.id
    req.params.collectionId = collection.id
    const [fragmentId] = collection.fragments
    req.params.fragmentId = fragmentId
    const res = httpMocks.createResponse()
    await require(postPath)(models)(req, res)

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())
    expect(data.email).toEqual(body.email)
  })
  it('should return an error when the an author is added to the same fragment', async () => {
    body.email = submittingAuthor.email
    const req = httpMocks.createRequest({
      body,
    })
    req.user = submittingAuthor.id
    req.params.collectionId = collection.id
    const [fragmentId] = collection.fragments
    req.params.fragmentId = fragmentId
    const res = httpMocks.createResponse()
    await require(postPath)(models)(req, res)

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(
      `User ${submittingAuthor.email} is already an author`,
    )
  })
  it('should return an error when params are missing', async () => {
    delete body.email
    const req = httpMocks.createRequest({
      body,
    })
    req.user = submittingAuthor.id
    req.params.collectionId = collection.id
    const [fragmentId] = collection.fragments
    req.params.fragmentId = fragmentId
    const res = httpMocks.createResponse()
    await require(postPath)(models)(req, res)

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Missing parameters.')
    body.email = author.email
  })
  it('should return an error when the role is not author and the user exists', async () => {
    body.role = 'invalid-role'
    const req = httpMocks.createRequest({
      body,
    })
    req.user = submittingAuthor.id
    req.params.collectionId = collection.id
    const [fragmentId] = collection.fragments
    req.params.fragmentId = fragmentId
    const res = httpMocks.createResponse()
    await require(postPath)(models)(req, res)

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('invalid-role is not defined')
  })
  it('should return an error when the role is not author and the user is new', async () => {
    body.role = 'invalid-role'
    body.email = chance.email()
    const req = httpMocks.createRequest({
      body,
    })
    req.user = submittingAuthor.id
    req.params.collectionId = collection.id
    const [fragmentId] = collection.fragments
    req.params.fragmentId = fragmentId
    const res = httpMocks.createResponse()
    await require(postPath)(models)(req, res)

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('invalid-role is not defined')
  })
  it('should return an error when the fragment does not exist', async () => {
    const req = httpMocks.createRequest({
      body,
    })
    req.user = submittingAuthor.id
    req.params.collectionId = collection.id
    req.params.fragmentId = 'invalid-fragment-id'
    req.params.userId = author.id
    const res = httpMocks.createResponse()
    await require(postPath)(models)(req, res)

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(
      `Fragment invalid-fragment-id does not match collection ${collection.id}`,
    )
  })
  it('should return an error when the author invites an inactive user', async () => {
    body.email = inactiveUser.email
    const req = httpMocks.createRequest({
      body,
    })
    req.user = submittingAuthor.id
    req.params.collectionId = collection.id
    const [fragmentId] = collection.fragments
    req.params.fragmentId = fragmentId
    const res = httpMocks.createResponse()
    await require(postPath)(models)(req, res)

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Invited user is inactive.')
  })
})
