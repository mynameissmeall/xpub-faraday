process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const cloneDeep = require('lodash/cloneDeep')

const httpMocks = require('node-mocks-http')
const fixturesService = require('pubsweet-component-fixture-service')

const { Model, fixtures } = fixturesService

const { author, submittingAuthor } = fixtures.users
const { collection } = fixtures.collections
const deletePath = '../../routes/fragmentsUsers/delete'
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

describe('Delete fragments users route handler', () => {
  let testFixtures = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    models = Model.build(testFixtures)
  })
  it('should return success when an author is deleted', async () => {
    const req = httpMocks.createRequest({})
    req.user = submittingAuthor.id
    req.params.collectionId = collection.id
    const [fragmentId] = collection.fragments
    req.params.fragmentId = fragmentId
    req.params.userId = author.id
    const res = httpMocks.createResponse()
    await require(deletePath)(models)(req, res)

    expect(res.statusCode).toBe(200)
  })
  it('should return an error when the collection does not exist', async () => {
    const req = httpMocks.createRequest({})
    req.user = submittingAuthor.id
    req.params.collectionId = 'invalid-id'
    req.params.userId = author.id
    const res = httpMocks.createResponse()
    await require(deletePath)(models)(req, res)

    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('item not found')
  })
  it('should return an error when the user does not exist', async () => {
    const req = httpMocks.createRequest({})
    req.user = submittingAuthor.id
    req.params.collectionId = collection.id
    const [fragmentId] = collection.fragments
    req.params.fragmentId = fragmentId
    req.params.userId = 'invalid-id'
    const res = httpMocks.createResponse()
    await require(deletePath)(models)(req, res)

    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('item not found')
  })
  it('should return an error when the fragment does not exist', async () => {
    const req = httpMocks.createRequest()
    req.user = submittingAuthor.id
    req.params.collectionId = collection.id
    req.params.fragmentId = 'invalid-fragment-id'
    req.params.userId = author.id
    const res = httpMocks.createResponse()
    await require(deletePath)(models)(req, res)

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(
      `Fragment invalid-fragment-id does not match collection ${collection.id}`,
    )
  })
})
