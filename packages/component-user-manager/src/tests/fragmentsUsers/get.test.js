process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const cloneDeep = require('lodash/cloneDeep')

const httpMocks = require('node-mocks-http')
const fixturesService = require('pubsweet-component-fixture-service')

const { Model, fixtures } = fixturesService

const { collection } = fixtures.collections
const { submittingAuthor } = fixtures.users

jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const getPath = '../../routes/fragmentsUsers/get'
describe('Get fragments users route handler', () => {
  let testFixtures = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    models = Model.build(testFixtures)
  })
  it('should return success when the request data is correct', async () => {
    const req = httpMocks.createRequest()
    req.params.collectionId = collection.id
    const [fragmentId] = collection.fragments
    req.params.fragmentId = fragmentId
    req.user = submittingAuthor.id
    const res = httpMocks.createResponse()
    await require(getPath)(models)(req, res)

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())

    expect(data).toHaveLength(1)
    expect(data[0].isSubmitting).toBeDefined()
  })
  it('should return an error when the collection does not exist', async () => {
    const req = httpMocks.createRequest()
    req.params.collectionId = 'invalid-id'
    req.user = submittingAuthor.id
    const res = httpMocks.createResponse()
    await require(getPath)(models)(req, res)
    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('item not found')
  })
  it('should return an error when the fragment does not exist', async () => {
    const req = httpMocks.createRequest()
    req.user = submittingAuthor.id
    req.params.collectionId = collection.id
    req.params.fragmentId = 'invalid-fragment-id'
    const res = httpMocks.createResponse()
    await require(getPath)(models)(req, res)

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(
      `Fragment invalid-fragment-id does not match collection ${collection.id}`,
    )
  })
})
