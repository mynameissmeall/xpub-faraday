const config = require('config')
const Email = require('@pubsweet/component-email-templating')

const { services } = require('pubsweet-component-helper-service')

const { getEmailCopy } = require('./emailCopy')

const forgotPath = config.get('forgot-password.url')
const { name: journalName, staffEmail } = config.get('journal')
const unsubscribeSlug = config.get('unsubscribe.url')

module.exports = {
  async sendForgotPasswordEmail({ user, baseUrl }) {
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'user-forgot-password',
    })

    const email = new Email({
      type: 'system',
      toUser: {
        email: user.email,
      },
      fromEmail: `${journalName} <${staffEmail}>`,
      content: {
        subject: 'Forgot Password',
        ctaLink: services.createUrl(baseUrl, forgotPath, {
          email: user.email,
          token: user.accessTokens.passwordReset,
        }),
        ctaText: 'RESET PASSWORD',
        paragraph,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.accessTokens.unsubscribe,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
}
