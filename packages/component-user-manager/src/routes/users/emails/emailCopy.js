const getEmailCopy = ({ emailType }) => {
  let paragraph
  switch (emailType) {
    case 'user-forgot-password':
      paragraph = `You have requested to reset your password. In order to reset your password please click the link below.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return { paragraph, hasLink: true }
}

module.exports = {
  getEmailCopy,
}
