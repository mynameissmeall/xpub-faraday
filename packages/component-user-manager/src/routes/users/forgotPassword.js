const logger = require('@pubsweet/logger')
const {
  services,
  authsome: authsomeHelper,
} = require('pubsweet-component-helper-service')
const notifications = require('./emails/notifications')

module.exports = models => async (req, res) => {
  const { email } = req.body
  if (!services.checkForUndefinedParams(email))
    return res.status(400).json({ error: 'Email address is required.' })

  try {
    const user = await models.User.findByEmail(email)
    const authsome = authsomeHelper.getAuthsome(models)
    const canRequest = await authsome.can(req.user, '', {
      targetUser: user,
    })
    if (!canRequest) {
      return res.status(403).json({ error: 'Unauthorized.' })
    }

    if (user.passwordResetTimestamp) {
      const resetDate = new Date(user.passwordResetTimestamp)
      const hoursPassed = Math.floor(
        (new Date().getTime() - resetDate) / 3600000,
      )
      if (hoursPassed < 24) {
        return res
          .status(400)
          .json({ error: 'A password reset has already been requested.' })
      }
    }

    user.accessTokens.passwordReset = services.generateHash()
    user.passwordResetTimestamp = Date.now()
    await user.save()

    notifications.sendForgotPasswordEmail({
      user,
      baseUrl: services.getBaseUrl(req),
    })
  } catch (e) {
    logger.error(
      `A forgot password request has been made on an non-existent email: ${email}`,
    )
  }

  res.status(200).json({
    message: `A password reset email has been sent to ${email}.`,
  })
}
