const { set } = require('lodash')
const { services } = require('pubsweet-component-helper-service')

module.exports = models => async (req, res) => {
  const { subscribe, id, token } = req.body

  if (!services.checkForUndefinedParams(subscribe, id, token))
    return res.status(400).json({ error: 'Missing required params.' })

  let user
  try {
    user = await models.User.find(id)
    if (user.accessTokens.unsubscribe !== token) {
      return res.status(400).json({ error: `Invalid token: ${token}` })
    }
    set(user, 'notifications.email.user', subscribe)
    user = await user.save()

    return res.status(200).json({ user })
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'User')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }
}
