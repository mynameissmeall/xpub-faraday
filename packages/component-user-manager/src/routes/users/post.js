const { pick } = require('lodash')
const Chance = require('chance')
const { passwordStrengthRegex } = require('config')

const chance = new Chance()

module.exports = models => async (req, res) => {
  if (req.user) {
    const admin = await models.User.find(req.user)
    if (!admin.admin) {
      return res.status(403).json({ error: 'Unauthorized' })
    }
  } else {
    if (!req.body.agreeTC) {
      return res.status(403).json({
        error: 'Terms & Conditions must be read and approved.',
      })
    }
    if (!passwordStrengthRegex.test(req.body.password))
      return res.status(400).json({
        error: 'Password is too weak. Please check password requirements.',
      })
    req.body = pick(req.body, [
      'email',
      'title',
      'country',
      'firstName',
      'lastName',
      'password',
      'affiliation',
    ])
    req.body = {
      ...req.body,
      admin: false,
      isActive: true,
      isConfirmed: false,
      editorInChief: false,
      handlingEditor: false,
      username: req.body.email,
      notifications: {
        email: {
          system: true,
          user: true,
        },
      },
      accessTokens: {
        confirmation: chance.hash(),
        unsubscribe: chance.hash(),
      },
    }
  }
  let user = new models.User(req.body)

  try {
    user = await user.save()
    return res.status(201).json(user)
  } catch (err) {
    return res.status(400).json({ error: err.message })
  }
}
