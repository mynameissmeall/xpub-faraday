const { pick } = require('lodash')

const {
  User,
  Team,
  services,
  Fragment,
  authsome: authsomeHelper,
} = require('pubsweet-component-helper-service')

const notifications = require('./emails/notifications')

const authorKeys = [
  'id',
  'email',
  'title',
  'lastName',
  'firstName',
  'affiliation',
  'country',
]

module.exports = models => async (req, res) => {
  const {
    email,
    role,
    country,
    lastName,
    firstName,
    affiliation,
    isSubmitting,
    isCorresponding,
  } = req.body

  if (
    !services.checkForUndefinedParams(
      email,
      role,
      country,
      lastName,
      firstName,
      affiliation,
      isSubmitting,
      isCorresponding,
    )
  )
    return res.status(400).json({ error: 'Missing parameters.' })

  const { collectionId, fragmentId } = req.params
  let collection, fragment
  try {
    collection = await models.Collection.find(collectionId)
    if (!collection.fragments.includes(fragmentId))
      return res.status(400).json({
        error: `Fragment ${fragmentId} does not match collection ${collectionId}`,
      })
    fragment = await models.Fragment.find(fragmentId)
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'item')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }

  const UserModel = models.User
  const teamHelper = new Team({ TeamModel: models.Team, fragmentId })
  const fragmentHelper = new Fragment({ fragment })
  const reqUser = await UserModel.find(req.user)

  try {
    let user = await UserModel.findByEmail(email)
    const authsome = authsomeHelper.getAuthsome(models)

    const canInvite = await authsome.can(req.user, '', {
      targetUser: user,
    })
    if (!canInvite) {
      return res.status(400).json({ error: 'Invited user is inactive.' })
    }

    if (role !== 'author') {
      return res.status(400).json({
        error: `${role} is not defined`,
      })
    }

    await teamHelper.setupTeam({ user, role, objectType: 'fragment' })
    user = await UserModel.find(user.id)

    fragment.authors = fragment.authors || []
    const match = fragment.authors.find(author => author.id === user.id)

    if (match) {
      return res.status(400).json({
        error: `User ${user.email} is already an author`,
      })
    }

    fragmentHelper.addAuthor({
      user,
      isSubmitting,
      isCorresponding,
    })

    if (!collection.owners.includes(user.id)) {
      collection.owners.push(user.id)
      collection.save()
    }

    // send email to SA when an Admin submits a manuscript on his behalf
    if ((reqUser.admin || reqUser.editorInChief) && isSubmitting) {
      notifications.sendNotifications({
        fragment,
        UserModel,
        collection,
        submittingAuthor: user,
        baseUrl: services.getBaseUrl(req),
      })
    }

    return res.status(200).json({
      ...pick(user, authorKeys),
      isSubmitting,
      isCorresponding,
    })
  } catch (e) {
    if (role !== 'author')
      return res.status(400).json({
        error: `${role} is not defined`,
      })

    if (e.name === 'NotFoundError') {
      const userHelper = new User({ UserModel })
      const newUser = await userHelper.createUser({
        role,
        body: req.body,
      })

      await teamHelper.setupTeam({
        user: newUser,
        role,
        objectType: 'fragment',
      })

      await fragmentHelper.addAuthor({
        user: newUser,
        isSubmitting,
        isCorresponding,
      })

      // send email to SA when an Admin submits a manuscript on his behalf
      if ((reqUser.admin || reqUser.editorInChief) && isSubmitting) {
        notifications.sendNotifications({
          fragment,
          UserModel,
          collection,
          submittingAuthor: newUser,
          baseUrl: services.getBaseUrl(req),
        })
      }

      if (!collection.owners.includes(newUser.id)) {
        collection.owners.push(newUser.id)
        collection.save()
      }

      return res.status(200).json({
        ...pick(newUser, authorKeys),
        isSubmitting,
        isCorresponding,
      })
    }
    return res.status(e.status).json({
      error: `Something went wrong: ${e.name}`,
    })
  }
}
