const getEmailCopy = ({ emailType, titleText }) => {
  let paragraph
  switch (emailType) {
    case 'submitting-author-added-by-admin':
      paragraph = `${titleText}<br/><br/>
        Please verify your details by clicking the link below.<br/><br/>
        To confirm the submission and view the status of the manuscript, please verify your details by clicking the link below. <br/><br/>`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return { paragraph, hasLink: true, hasIntro: true, hasSignature: true }
}

module.exports = {
  getEmailCopy,
}
