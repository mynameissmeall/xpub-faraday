const bodyParser = require('body-parser')

const FragmentsUsers = app => {
  app.use(bodyParser.json())
  const basePath = '/api/collections/:collectionId/fragments/:fragmentId/users'
  const routePath = './routes/fragmentsUsers'
  const authBearer = app.locals.passport.authenticate('bearer', {
    session: false,
  })
  /**
   * @api {post} /api/collections/:collectionId/fragments/:fragmentId/users Add a user to a fragment
   * @apiGroup FragmentsUsers
   * @apiParam {collectionId} collectionId Collection id
   * @apiParam {fragmentId} fragmentId Fragment id
   * @apiParamExample {json} Body
   *    {
   *      "email": "email@example.com",
   *      "role": "author", [acceptedValues: author],
   *      "isSubmitting": true,
   *      "isCorresponding": false
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    {
   *      "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
   *      "email": "email@example.com",
   *      "firstName": "John",
   *      "lastName": "Smith",
   *      "affiliation": "MIT",
   *      "isSubmitting": true,
   *      "isCorresponding": false
   *    }
   * @apiErrorExample {json} Invite user errors
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 500 Internal Server Error
   */
  app.post(
    basePath,
    authBearer,
    require(`${routePath}/post`)(app.locals.models),
  )
  /**
   * @api {delete} /api/collections/:collectionId/fragments/:fragmentId/user/:userId Delete a user from a fragment
   * @apiGroup FragmentsUsers
   * @apiParam {collectionId} collectionId Collection id
   * @apiParam {fragmentId} fragmentId Fragment id
   * @apiParam {userId} userId User id
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 {}
   * @apiErrorExample {json} Delete errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 404 Not Found
   */
  app.delete(
    `${basePath}/:userId`,
    authBearer,
    require(`${routePath}/delete`)(app.locals.models),
  )
  /**
   * @api {get} /api/collections/:collectionId/fragments/:fragmentId/users List fragment users
   * @apiGroup FragmentsUsers
   * @apiParam {collectionId} collectionId Collection id
   * @apiParam {fragmentId} fragmentId Fragment id
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    [{
   *      "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
   *      "type": "user",
   *      "admin": false,
   *      "email": "email@example.com",
   *      "teams": [
   *        "c576695a-7cda-4e27-8e9c-31f3a0e9d592"
   *      ],
   *      "username": "email@example.com",
   *      "fragments": [],
   *      "collections": [],
   *      "isConfirmed": false,
   *      "editorInChief": false,
   *      "handlingEditor": false,
   *      "passwordResetToken": "04590a2b7f6c1f37cb84881d529e516fa6fc309c205a07f1341b2bfaa6f2b46c",
   *      "isSubmitting": true,
   *      "isCorresponding": false,
   *    },
   *    {
   *      "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
   *      "type": "user",
   *      "admin": false,
   *      "email": "email2@example.com",
   *      "teams": [
   *        "c576695a-7cda-4e27-8e9c-31f3a0e9d592"
   *      ],
   *      "username": "email2@example.com",
   *      "fragments": [],
   *      "collections": [],
   *      "isConfirmed": true,
   *      "editorInChief": false,
   *      "handlingEditor": false",
   *      "isSubmitting": false,
   *      "isCorresponding": false,
   *    }]
   * @apiErrorExample {json} List errors
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   */
  app.get(basePath, authBearer, require(`${routePath}/get`)(app.locals.models))
}

module.exports = FragmentsUsers
