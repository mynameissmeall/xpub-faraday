module.exports = {
  backend: () => app => {
    require('./src/Users')(app)
    require('./src/FragmentsUsers')(app)
  },
}
