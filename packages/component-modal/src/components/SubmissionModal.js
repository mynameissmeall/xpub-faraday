import React from 'react'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'
import { Button, Spinner } from '@pubsweet/ui'
import { IconButton } from '../../../component-faraday-ui/src/IconButton'

const SubmissionModal = ({
  title,
  content,
  onClose,
  subtitle,
  hideModal,
  onConfirm,
  modalError,
  isFetching,
  confirmText = 'OK',
  cancelText = 'Cancel',
}) => (
  <Root>
    <CloseIcon onClick={hideModal}>
      <IconButton primary>x</IconButton>
    </CloseIcon>
    <Title>{title}</Title>
    <Text>
      The corresponding author confirms that all co-authors are included, and
      that everyone listed as co-author agrees to that role and all the
      following requirements and acknowledgements. The submission represents
      original work and that sources are given proper attribution.
    </Text>
    <Text>
      The journal employs <b>CrossCheck</b> to compare submissions agaist a
      large and growing database of published scholarly content. If in the
      judgement of a senior editor a submission is genuinely suspected of
      plagiarism, it will be returned to the author(s) with a request for
      explanation.
    </Text>
    <Text>
      The research was conducted in accordance with ethical principles.
    </Text>
    <Text>
      There is a Data Accessibility Statement, containing information about the
      location of open data and materials, in the manuscript.
    </Text>
    <Text>
      A conflict of interest statement is present in the manuscript, even if to
      state no conflicts of interest.
    </Text>
    {modalError && <ErrorMessage>{modalError}</ErrorMessage>}
    <ButtonsContainer>
      <Button data-test-id="button-modal-hide" onClick={hideModal}>
        {cancelText}
      </Button>
      {onConfirm &&
        (isFetching ? (
          <SpinnerContainer>
            <Spinner size={4} />
          </SpinnerContainer>
        ) : (
          <Button
            data-test-id="button-modal-confirm"
            onClick={onConfirm}
            primary
          >
            {confirmText}
          </Button>
        ))}
    </ButtonsContainer>
  </Root>
)

export default SubmissionModal

// #region styled-components
const defaultText = css`
  color: ${th('colorPrimary')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
`

const SpinnerContainer = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  height: calc(${th('gridUnit')} * 2);
  min-width: calc(${th('gridUnit')} * 4);
`

const Root = styled.div`
  background-color: ${th('backgroundColor')};
  border: ${th('borderDefault')};
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: calc(${th('gridUnit')} * 2);
  position: relative;
  overflow-y: scroll;
  width: calc(${th('gridUnit')} * 25);
`

const Title = styled.div`
  ${defaultText};
  font-size: ${th('fontSizeHeading5')};
  margin-bottom: ${th('gridUnit')};
  text-align: center;
`

const Text = styled.span`
  ${defaultText};
  font-size: ${th('fontSizeBase')};
  margin-bottom: calc(${th('gridUnit')} / 2);

  & b {
    display: inline;
  }
`

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: space-evenly;
  margin: ${th('gridUnit')} auto 0;
  width: 100%;
`
const CloseIcon = styled.div`
  cursor: pointer;
  position: absolute;
  top: ${th('subGridUnit')};
  right: ${th('subGridUnit')};
`

const ErrorMessage = styled.div`
  color: ${th('colorError')};
  margin: ${th('subGridUnit')};
  text-align: center;
`
// #endregion
