module.exports = {
  backend: () => app => {
    require('./src/CollectionsInvitations')(app)
    require('./src/FragmentsInvitations')(app)
  },
}
