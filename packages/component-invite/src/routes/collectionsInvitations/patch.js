const {
  Team,
  services,
  Collection,
  Invitation,
} = require('pubsweet-component-helper-service')
const notifications = require('./emails/notifications')

module.exports = models => async (req, res) => {
  const { collectionId, invitationId } = req.params
  const { isAccepted, reason } = req.body

  const user = await models.User.find(req.user)

  try {
    const collection = await models.Collection.find(collectionId)
    collection.invitations = collection.invitations || []
    const invitation = collection.invitations.find(
      invitation => invitation.id === invitationId,
    )

    const invitationHelper = new Invitation({
      userId: user.id,
      role: 'handlingEditor',
      invitation,
    })

    const invitationValidation = invitationHelper.validateInvitation()
    if (invitationValidation.error)
      return res.status(invitationValidation.status).json({
        error: invitationValidation.error,
      })

    const collectionHelper = new Collection({ collection })

    const teamHelper = new Team({ TeamModel: models.Team, collectionId })

    await collectionHelper.updateHandlingEditor({ isAccepted })
    invitation.respondedOn = Date.now()
    invitation.hasAnswer = true
    invitation.isAccepted = isAccepted

    if (!isAccepted) {
      await teamHelper.deleteHandlingEditor({
        collection,
        role: invitation.role,
        user,
      })

      if (reason) invitation.reason = reason
    }

    await collection.save()

    notifications.sendEiCEmail({
      models,
      reason,
      collection,
      isAccepted,
      invitedHE: user,
      baseUrl: services.getBaseUrl(req),
    })

    return res.status(200).json(invitation)
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'collection')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }
}
