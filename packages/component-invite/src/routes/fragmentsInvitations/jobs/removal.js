const moment = require('moment')

const { Team, Collection } = require('pubsweet-component-helper-service')
const {
  Team: TeamModel,
  User: UserModel,
  Fragment: FragmentModel,
  Collection: CollectionModel,
} = require('pubsweet-server')

const Job = require('pubsweet-component-jobs')

const scheduleRemovalJob = async ({
  days,
  userId,
  timeUnit,
  invitation,
  fragmentId,
  collectionId,
}) => {
  const executionDate = moment()
    .add(days, timeUnit)
    .toISOString()

  const queue = `removal-${userId}-${invitation.id}`

  const params = {
    days,
    timeUnit,
    invitation,
    fragmentId,
    collectionId,
    executionDate,
  }

  await Job.schedule({ queue, executionDate, jobHandler, params })
}

const jobHandler = async job => {
  const {
    days,
    timeUnit,
    invitation,
    fragmentId,
    collectionId,
    executionDate,
  } = job.data

  const collection = await CollectionModel.find(collectionId)
  const fragment = await FragmentModel.find(fragmentId)

  const collectionHelper = new Collection({ collection })
  const teamHelper = new Team({
    TeamModel,
    collectionId,
    fragmentId,
  })

  const team = await teamHelper.getTeam({
    role: invitation.role,
    objectType: 'fragment',
  })

  fragment.invitations = fragment.invitations.filter(
    inv => inv.id !== invitation.id,
  )

  await collectionHelper.updateStatusByNumberOfReviewers({
    invitations: fragment.invitations,
  })

  await teamHelper.removeTeamMember({
    teamId: team.id,
    userId: invitation.userId,
  })

  const user = await UserModel.find(invitation.userId)
  if (!user.isConfirmed) {
    await user.delete()
  } else {
    user.teams = user.teams.filter(userTeamId => team.id !== userTeamId)
    await user.save()
  }

  await fragment.save()

  return `Job ${
    job.name
  }: the ${days} ${timeUnit} removal has been executed at ${executionDate} for user ${
    user.id
  }`
}
module.exports = { scheduleRemovalJob }
