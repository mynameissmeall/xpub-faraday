const moment = require('moment')
const { cloneDeep } = require('lodash')
const Job = require('pubsweet-component-jobs')

const { getEmailCopy } = require('../emails/emailCopy')
const Email = require('@pubsweet/component-email-templating')

const scheduleReminderJob = async ({
  days,
  email,
  order,
  userId,
  subject,
  timeUnit,
  titleText,
  invitationId,
  expectedDate,
}) => {
  const executionDate = moment()
    .add(days, timeUnit)
    .toISOString()

  const queue = `reminders-${userId}-${invitationId}`

  const { paragraph, ...bodyProps } = getEmailCopy({
    emailType: `reviewer-resend-invitation-${order}-reminder`,
    titleText,
    expectedDate,
  })

  email.bodyProps = bodyProps
  email.content.subject = subject

  const params = {
    days,
    timeUnit,
    executionDate,
    emailProps: cloneDeep(email),
  }

  await Job.schedule({ queue, params, executionDate, jobHandler })
}

const jobHandler = async job => {
  const { days, timeUnit, executionDate, emailProps } = job.data
  const email = new Email(emailProps)

  await email.sendEmail()

  return `Job ${job.id}: the ${days} ${timeUnit} reminder has been sent to ${
    email.toUser.email
  } at ${executionDate}`
}
module.exports = { scheduleReminderJob }
