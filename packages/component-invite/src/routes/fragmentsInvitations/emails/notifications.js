const config = require('config')
const { get } = require('lodash')
const Email = require('@pubsweet/component-email-templating')

const unsubscribeSlug = config.get('unsubscribe.url')
const staffEmail = config.get('journal.staffEmail')
const journalName = config.get('journal.name')

const {
  User,
  services,
  Fragment,
} = require('pubsweet-component-helper-service')

const { getEmailCopy } = require('./emailCopy')

module.exports = {
  sendHandlingEditorEmail: async ({
    baseUrl,
    fragment,
    reviewer,
    UserModel,
    emailType,
    collection,
  }) => {
    const fragmentHelper = new Fragment({ fragment })
    const { title } = await fragmentHelper.getFragmentData({
      handlingEditor: collection.handlingEditor,
    })
    const { submittingAuthor } = await fragmentHelper.getAuthorData({
      UserModel,
    })

    const titleText = `the manuscript titled "${title}" by ${
      submittingAuthor.firstName
    } ${submittingAuthor.lastName}`

    const handlingEditor = get(collection, 'handlingEditor')
    const userHelper = new User({ UserModel })
    const eicName = await userHelper.getEiCName()

    const heUser = await UserModel.find(handlingEditor.id)

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      titleText,
      expectedDate: services.getExpectedDate({
        timestamp: Date.now(),
        daysExpected: 14,
      }),
      targetUserName: reviewer.lastName,
    })

    const email = new Email({
      fromEmail: `${eicName} <${staffEmail}>`,
      type: 'user',
      toUser: {
        email: heUser.email,
        name: heUser.lastName,
      },
      content: {
        subject:
          emailType === 'reviewer-accepted'
            ? `${collection.customId}: A reviewer has agreed`
            : `${collection.customId}: A reviewer has declined`,
        paragraph,
        ctaText: 'MANUSCRIPT DETAILS',
        signatureJournal: '',
        signatureName: eicName,
        ctaLink: services.createUrl(
          baseUrl,
          `/projects/${collection.id}/versions/${fragment.id}/details`,
        ),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: heUser.id,
          token: heUser.accessTokens.unsubscribe,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  sendReviewerEmail: async ({
    baseUrl,
    fragment,
    reviewer,
    UserModel,
    collection,
    isCanceled = false,
  }) => {
    const fragmentHelper = new Fragment({ fragment })
    const { title } = await fragmentHelper.getFragmentData({
      handlingEditor: collection.handlingEditor,
    })
    const { submittingAuthor } = await fragmentHelper.getAuthorData({
      UserModel,
    })

    const titleText = `the manuscript titled "${title}" by ${
      submittingAuthor.firstName
    } ${submittingAuthor.lastName}`

    const handlingEditor = get(collection, 'handlingEditor')

    const emailType = isCanceled
      ? 'reviewer-cancel-invitation'
      : 'reviewer-thank-you'

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      titleText,
    })

    const email = new Email({
      fromEmail: `${handlingEditor.name} <${staffEmail}>`,
      type: 'user',
      toUser: {
        email: reviewer.email,
        name: `${reviewer.lastName}`,
      },
      content: {
        subject: isCanceled
          ? `${collection.customId}: Review no longer required`
          : `${collection.customId}: Thank you for agreeing to review`,
        paragraph,
        ctaText: 'MANUSCRIPT DETAILS',
        signatureJournal: journalName,
        signatureName: handlingEditor.name,
        ctaLink: services.createUrl(
          baseUrl,
          `/projects/${collection.id}/versions/${fragment.id}/details`,
        ),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: reviewer.id,
          token: reviewer.accessTokens.unsubscribe,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
}
