process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')
const requests = require('../requests')

const { Model, fixtures } = fixturesService

jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const chance = new Chance()
const reqBody = {
  email: chance.email(),
  role: 'handlingEditor',
  firstName: chance.first(),
  lastName: chance.last(),
  title: 'Mr',
  affiliation: chance.company(),
  admin: false,
}
const route = {
  path: '/api/collections/:collectionId/invitations',
}

const path = '../routes/collectionsInvitations/post'
describe('Post collections invitations route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })
  it('should return an error params are missing', async () => {
    const { admin } = testFixtures.users
    delete body.email
    const res = await requests.sendRequest({
      body,
      userId: admin.id,
      route,
      models,
      path,
    })

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Email and role are required')
  })
  it('should return success when the editor in chief invites a handlingEditor with a collection', async () => {
    const { user, editorInChief } = testFixtures.users
    const { collection } = testFixtures.collections
    collection.status = 'submitted'

    body = {
      email: user.email,
      role: 'handlingEditor',
    }
    const res = await requests.sendRequest({
      body,
      userId: editorInChief.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())
    expect(data.role).toEqual(body.role)
  })
  it('should return an error when inviting his self', async () => {
    const { editorInChief } = testFixtures.users
    body.email = editorInChief.email
    const res = await requests.sendRequest({
      body,
      userId: editorInChief.id,
      route,
      models,
      path,
    })
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Cannot invite yourself.')
  })
  it('should return an error when the role is invalid', async () => {
    const { editorInChief } = testFixtures.users
    body.role = 'someRandomRole'
    const res = await requests.sendRequest({
      body,
      userId: editorInChief.id,
      route,
      models,
      path,
    })
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(
      `Role ${body.role} is invalid. Only handlingEditor is allowed.`,
    )
  })
  it('should return success when the EiC resends an invitation to a handlingEditor with a collection', async () => {
    const { handlingEditor, editorInChief } = testFixtures.users
    const { collection } = testFixtures.collections
    collection.status = 'submitted'

    body = {
      email: handlingEditor.email,
      role: 'handlingEditor',
    }
    const res = await requests.sendRequest({
      body,
      userId: editorInChief.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())
    expect(data.role).toEqual(body.role)
  })
  it('should return an error when the invitation is already answered', async () => {
    const { answerHE, editorInChief } = testFixtures.users
    const { collection } = testFixtures.collections
    collection.status = 'submitted'

    body = {
      email: answerHE.email,
      role: 'handlingEditor',
    }
    const res = await requests.sendRequest({
      body,
      userId: editorInChief.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
      },
    })
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(
      `User has already replied to a previous invitation.`,
    )
  })
  it('should return an error when the user does not have invitation rights', async () => {
    const { author } = testFixtures.users
    const { collection } = testFixtures.collections

    const res = await requests.sendRequest({
      body,
      userId: author.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Unauthorized.')
  })
  it('should return an error when the collection is not in the submitted status', async () => {
    const { user, editorInChief } = testFixtures.users
    const { collection } = testFixtures.collections
    collection.status = 'pendingApproval'

    body = {
      email: user.email,
      role: 'handlingEditor',
    }
    const res = await requests.sendRequest({
      body,
      userId: editorInChief.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())

    expect(data.error).toEqual(
      `Cannot invite HE while collection is in the status: ${
        collection.status
      }.`,
    )
  })
  it('should return an error when the collection is in the revision requested status', async () => {
    const { user, editorInChief } = testFixtures.users
    const { collection } = testFixtures.collections
    collection.status = 'revisionRequested'

    body = {
      email: user.email,
      role: 'handlingEditor',
    }
    const res = await requests.sendRequest({
      body,
      userId: editorInChief.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())

    expect(data.error).toEqual(
      `Cannot invite HE while collection is in the status: ${
        collection.status
      }.`,
    )
  })
})
