process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const httpMocks = require('node-mocks-http')
const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')

const { Model, fixtures } = fixturesService
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

jest.mock('pubsweet-component-jobs', () => ({
  schedule: jest.fn(),
  cancelQueue: jest.fn(),
}))

const reqBody = {
  isAccepted: true,
}
const patchPath = '../../routes/fragmentsInvitations/patch'
describe('Patch fragments invitations route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })
  it('should return success when the reviewer agrees work on a collection', async () => {
    const { reviewer } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments
    const req = httpMocks.createRequest({
      body,
    })
    req.user = reviewer.id
    req.params.collectionId = collection.id
    req.params.fragmentId = fragment.id
    const reviewerInv = fragment.invitations.find(
      inv => inv.role === 'reviewer' && inv.hasAnswer === false,
    )
    req.params.invitationId = reviewerInv.id
    const res = httpMocks.createResponse()
    await require(patchPath)(models)(req, res)
    expect(res.statusCode).toBe(200)
  })
  it('should return success when the reviewer declines work on a collection', async () => {
    const { reviewer } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    body.isAccepted = false
    const req = httpMocks.createRequest({
      body,
    })
    req.user = reviewer.id
    req.params.collectionId = collection.id
    req.params.fragmentId = fragment.id

    const inv = fragment.invitations.find(
      inv => inv.role === 'reviewer' && inv.hasAnswer === false,
    )
    req.params.invitationId = inv.id
    const res = httpMocks.createResponse()
    await require(patchPath)(models)(req, res)

    expect(res.statusCode).toBe(200)
    expect(reviewer.teams).toHaveLength(1)
  })
  it('should return an error if the collection does not exist', async () => {
    const { handlingEditor } = testFixtures.users
    const req = httpMocks.createRequest({
      body,
    })
    req.user = handlingEditor.id
    req.params.collectionId = 'invalid-id'
    const res = httpMocks.createResponse()
    await require(patchPath)(models)(req, res)

    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Item not found')
  })
  it('should return an error when the invitation does not exist', async () => {
    const { user } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    const req = httpMocks.createRequest({
      body,
    })
    req.user = user.id
    req.params.collectionId = collection.id
    req.params.fragmentId = fragment.id
    req.params.invitationId = 'invalid-id'
    const res = httpMocks.createResponse()
    await require(patchPath)(models)(req, res)

    // expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Invitation not found.')
  })
  it("should return an error when a user tries to patch another user's invitation", async () => {
    const { handlingEditor } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments
    const req = httpMocks.createRequest({
      body,
    })
    req.user = handlingEditor.id
    req.params.collectionId = collection.id
    req.params.fragmentId = fragment.id
    const inv = fragment.invitations.find(
      inv => inv.role === 'reviewer' && inv.hasAnswer === false,
    )
    req.params.invitationId = inv.id
    const res = httpMocks.createResponse()
    await require(patchPath)(models)(req, res)
    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(`Unauthorized.`)
  })
  it('should return an error when the invitation is already answered', async () => {
    const { handlingEditor } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    const req = httpMocks.createRequest({
      body,
    })
    req.user = handlingEditor.id
    req.params.collectionId = collection.id
    req.params.fragmentId = fragment.id

    const inv = fragment.invitations.find(inv => inv.hasAnswer)
    req.params.invitationId = inv.id
    const res = httpMocks.createResponse()
    await require(patchPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(`Invitation has already been answered.`)
  })
  it('should return an error when the invited user is inactive', async () => {
    const { handlingEditor, inactiveReviewer } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    const req = httpMocks.createRequest({
      body,
    })
    req.user = handlingEditor.id
    req.params.collectionId = collection.id
    req.params.fragmentId = fragment.id

    const reviewerInv = fragment.invitations.find(
      inv =>
        inv.role === 'reviewer' &&
        inv.hasAnswer === false &&
        inv.userId === inactiveReviewer.id,
    )
    req.params.invitationId = reviewerInv.id
    const res = httpMocks.createResponse()
    await require(patchPath)(models)(req, res)
    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(`Unauthorized.`)
  })
})
