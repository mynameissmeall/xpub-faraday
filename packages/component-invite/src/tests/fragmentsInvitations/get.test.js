process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')
const requests = require('../requests')

const { Model, fixtures } = fixturesService
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const path = '../routes/fragmentsInvitations/get'
const route = {
  path:
    '/api/collections/:collectionId/fragments/:fragmentId/invitations/:invitationId?',
}
describe('Get fragment invitations route handler', () => {
  let testFixtures = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    models = Model.build(testFixtures)
  })
  it('should return success when the request data is correct', async () => {
    const { handlingEditor } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    const res = await requests.sendRequest({
      userId: handlingEditor.id,
      route,
      models,
      path,
      query: {
        role: 'reviewer',
      },
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
      },
    })

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())

    expect(data).toHaveLength(3)
  })
  it('should return an error when parameters are missing', async () => {
    const { handlingEditor } = testFixtures.users
    const res = await requests.sendRequest({
      userId: handlingEditor.id,
      route,
      models,
      path,
    })
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Role is required')
  })
  it('should return an error when the collection does not exist', async () => {
    const { handlingEditor } = testFixtures.users
    const res = await requests.sendRequest({
      userId: handlingEditor.id,
      route,
      models,
      path,
      query: {
        role: 'reviewer',
      },
      params: {
        collectionId: 'invalid-id',
      },
    })
    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Item not found')
  })
  it('should return an error when the fragment does not exist', async () => {
    const { handlingEditor } = testFixtures.users
    const { collection } = testFixtures.collections
    const res = await requests.sendRequest({
      userId: handlingEditor.id,
      route,
      models,
      path,
      query: {
        role: 'reviewer',
      },
      params: {
        collectionId: collection.id,
        fragmentId: 'invalid-id',
      },
    })
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(
      `Fragment invalid-id does not match collection ${collection.id}.`,
    )
  })
  it('should return an error when the role is invalid', async () => {
    const { handlingEditor } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    const res = await requests.sendRequest({
      userId: handlingEditor.id,
      route,
      models,
      path,
      query: {
        role: 'invalidRole',
      },
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
      },
    })
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(`Role invalidRole is invalid`)
  })
  it('should return an error when a user does not have invitation rights', async () => {
    const { user } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    const res = await requests.sendRequest({
      userId: user.id,
      route,
      models,
      path,
      query: {
        role: 'reviewer',
      },
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
      },
    })
    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Unauthorized.')
  })
})
