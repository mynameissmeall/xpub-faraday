process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')
const requests = require('../requests')

const { Model, fixtures } = fixturesService

jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))
jest.mock('pubsweet-component-jobs', () => ({
  schedule: jest.fn(),
  cancelQueue: jest.fn(),
}))

const chance = new Chance()
const reqBody = {
  email: chance.email(),
  role: 'reviewer',
  firstName: chance.first(),
  lastName: chance.last(),
  title: 'Mr',
  affiliation: chance.company(),
  admin: false,
  country: chance.country(),
}
const route = {
  path: '/api/collections/:collectionId/fragments/:fragmentId/invitations',
}

const path = '../routes/fragmentsInvitations/post'
describe('Post fragments invitations route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })
  it('should return an error params are missing', async () => {
    const { admin } = testFixtures.users
    delete body.email
    const res = await requests.sendRequest({
      body,
      userId: admin.id,
      route,
      models,
      path,
    })

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Missing parameters.')
  })

  it('should return success when a reviewer is invited', async () => {
    const { user, handlingEditor } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    body = Object.assign(body, {
      email: user.email,
      role: 'reviewer',
    })

    const res = await requests.sendRequest({
      body,
      userId: handlingEditor.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
      },
    })

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())
    expect(data.role).toEqual(body.role)
  })

  it('should return success when resending an invitation to an existing reviewer', async () => {
    const { handlingEditor, reviewer } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    const body = {
      email: reviewer.email,
      role: 'reviewer',
      isPublons: false,
    }

    const res = await requests.sendRequest({
      body,
      userId: handlingEditor.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
      },
    })

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())
    expect(data.role).toEqual(body.role)
  })

  it('should return an error when resending an invitation to reviewer and the params are missing.', async () => {
    const { editorInChief, reviewer } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    const body = {
      email: reviewer.email,
    }

    const res = await requests.sendRequest({
      body,
      userId: editorInChief.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
      },
    })

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Missing parameters.')
  })

  it('should return an error when inviting his self', async () => {
    const { editorInChief } = testFixtures.users
    body.email = editorInChief.email
    const res = await requests.sendRequest({
      body,
      userId: editorInChief.id,
      route,
      models,
      path,
    })
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Cannot invite yourself.')
  })
  it('should return an error when the role is invalid', async () => {
    const { editorInChief } = testFixtures.users
    body.role = 'someRandomRole'
    const res = await requests.sendRequest({
      body,
      userId: editorInChief.id,
      route,
      models,
      path,
    })
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(
      `Role ${body.role} is invalid. Only reviewer is accepted.`,
    )
  })
  it('should return an error when the invitation is already answered', async () => {
    const { answerReviewer, handlingEditor } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    body = Object.assign(body, {
      email: answerReviewer.email,
      role: 'reviewer',
    })

    const res = await requests.sendRequest({
      body,
      userId: handlingEditor.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
      },
    })
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(
      `User has already replied to a previous invitation.`,
    )
  })
  it('should return an error when the user does not have invitation rights', async () => {
    const { author } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    const res = await requests.sendRequest({
      body,
      userId: author.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
      },
    })

    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Unauthorized.')
  })
  it('should return an error when the invited user is inactive', async () => {
    const { inactiveUser, handlingEditor } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    body = Object.assign(body, {
      email: inactiveUser.email,
      role: 'reviewer',
    })

    const res = await requests.sendRequest({
      body,
      userId: handlingEditor.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
      },
    })
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(`Invited user is inactive.`)
  })
})
