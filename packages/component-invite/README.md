# Invite Component

### Invite a User to a Collection [POST]

This endpoint allows creates a new User, linking it to a Collection through a Team.

#### Invite User Request

`POST /api/collections/:collectionId/invitations`

| URI Parameter | Requiered | Requirements | Description |
| ------------- | --------- | ------------ | ------------------ |
| collectionId | No | String | The ID of the collection |

#### Invite User Request Body

```json
{
  "email": "email@example.com",
  "role": "handlingEditor", [acceptedValues: handlingEditor, reviewer]
}
```

#### Invite User Response

```javascript
HTTP/1.1 200 OK
{
  "id": "7b2431af-210c-49f9-a69a-e19271066ebd",
  "role": "handlingEditor",
  "userId": "4c3f8ee1-785b-4adb-87b4-407a27f652c6",
  "hasAnswer": false,
  "invitedOn": 1525428890167,
  "isAccepted": false,
  "respondedOn": null
 }
```

---

### Get Invitations [GET]

This endpoint allows you to get a list of invitations based on role.

#### Get Invitations Request

`GET /api/collections/:collectionId/invitations/:invitationId?role=:role`

| Query Parameter | Requiered | Requirements | Description              |
| --------------- | --------- | ------------ | ------------------------ |
| collectionId    | Yes       | String       | Collection ID         |
| invitationId    | No       | String       | Invitation ID |
| role           | Yes       | String       | The role to search for: handlingEditor, reviewer, author |

#### Get Invitations Response

```javascript
HTTP/1.1 200 OK
[
  {
    "name": "John Smith",
    "invitedOn": 1525428890167,
    "respondedOn": 1525428890299,
    "email": "email@example.com",
    "status": "pending",
    "invitationId": "1990881"
  }
]
```

---
### Accept or Decline an Invitation [PATCH]

This endpoint allows you to accept or to decline an invitation.

#### Accept/Decline Request

`PATCH /api/collections/:collectionId/invitations/:invitationId`

| URI Parameter | Requiered | Requirements | Description              |
| --------------- | --------- | ------------ | ------------------------ |
| collectionId    | Yes       | String       | Collection ID         |
| invitationId    | Yes       | String       | Invitation ID |

#### Accept/Decline Body

```json
{
  "isAccepted": false/true,
  "reason": "I am not ready" [optional]
}
```

#### Accept/Decline Response

```javascript
HTTP/1.1 200 OK
{
  "id": "7b2431af-210c-49f9-a69a-e19271066ebd",
  "role": "reviewer",
  "userId": "4c3f8ee1-785b-4adb-87b4-407a27f652c6",
  "hasAnswer": true,
  "invitedOn": 1525428890167,
  "isAccepted": false,
  "respondedOn": 1525428890299
}
```
---
### Decline an Invitation Unauthenticated [PATCH]

This endpoint allows you to decline an invitation without authenticating.

#### Decline Request

`PATCH /api/collections/:collectionId/invitations/:invitationId/decline`

| URI Parameter | Requiered | Requirements | Description              |
| --------------- | --------- | ------------ | ------------------------ |
| collectionId    | Yes       | String       | Collection ID         |
| invitationId    | Yes       | String       | Invitation ID |

#### Decline Body

```json
{
  "invitationToken": "f2d814f0-67a5-4590-ba4f-6a83565feb4f"
}
```

#### Decline Response

```javascript
HTTP/1.1 200 OK
{}
```
---
### Delete Invitation [DELETE]

This endpoint allows you to delete an invitation.

#### Delete Invitation Request

`DELETE /api/collections/:collectionId/invitations/:invitationId`

| Query Parameter | Requiered | Requirements | Description              |
| --------------- | --------- | ------------ | ------------------------ |
| collectionId    | Yes       | String       | Collection ID         |
| invitationId    | Yes       | String       | Invitation ID |

#### Delete Invitation Response

```javascript
HTTP/1.1 204 No Content
```