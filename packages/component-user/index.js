const resolvers = require('./server/resolvers')
const typeDefs = require('./server/typeDefs')

module.exports = {
  typeDefs,
  resolvers,
}
