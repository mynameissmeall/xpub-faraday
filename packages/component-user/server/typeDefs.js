module.exports = `
  extend type User {
    affiliation: String
    country: String
    editorInChief: Boolean
    handlingEditor: Boolean
    isActive: Boolean
    isConfirmed: Boolean
    firstName: String
    lastName: String
    title: String
  }

  extend input UserInput {
    firstName: String
    lastName: String
    title: String
    country: String
    affiliation: String
    role: AllowedRole
  }

  input UserStatusInput {
    email: String!
    username: String!
    isActive: Boolean
  }

  extend type Mutation {
    addUserAsAdmin(input: UserInput!): User
    editUserAsAdmin(id: ID!, input: UserInput!): User
    toggleUserActiveStatusAsAdmin(id: ID!, input: UserStatusInput): User
  }

  enum AllowedRole {
    editorInChief
    handlingEditor
    admin
    author
  }
`
