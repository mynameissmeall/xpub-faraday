module.exports.initialize = ({ User, Notification, ctx }) => ({
  execute: async ({ input }) => {
    const { admin } = await User.fetchOne(ctx.user, ctx)
    if (!admin) {
      throw new Error('Unauthorized')
    }

    const user = await User.create(ctx.parseUserFromAdmin(input), ctx)
    const notification = new Notification(user)
    await notification.notifyUserAddedByAdmin(input.role)

    return user
  },
})
