const activateUser = require('./activateUser')
const deactivateUser = require('./deactivateUser')
const editUser = require('./editUser')
const createUserAsAdmin = require('./createUserAsAdmin')

module.exports = {
  editUser,
  activateUser,
  deactivateUser,
  createUserAsAdmin,
}
