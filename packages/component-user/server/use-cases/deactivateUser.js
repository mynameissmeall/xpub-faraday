module.exports.initialize = ({ User, ctx }) => ({
  execute: async ({ id, input }) => {
    const username = `invalid***${input.username}`

    return User.update(id, { ...input, username, isActive: false }, ctx)
  },
})
