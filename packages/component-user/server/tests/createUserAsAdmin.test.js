const { cloneDeep } = require('lodash')
const { Model, fixtures } = require('pubsweet-component-fixture-service')

const { createUserAsAdmin } = require('../use-cases')

class MockNotification {
  constructor(user) {
    this.user = user
  }

  async notifyUserAddedByAdmin(role) {
    jest.fn(() => this)
  }
}

describe('create user', () => {
  let testFixtures = {}
  let models

  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    models = Model.build(testFixtures)
  })

  it('create an user as admin', async () => {
    const input = {
      email: 'alexandrescu@gmail.com',
      username: 'alexandrescu@gmail.com',
    }
    const ctx = {
      parseUserFromAdmin: i => i,
      user: {
        admin: true,
      },
    }

    const result = await createUserAsAdmin
      .initialize({ User: models.User, Notification: MockNotification, ctx })
      .execute({
        input,
      })

    expect(result).toMatchObject(input)
  })

  it('does not allow non admins to create user', async (...props) => {
    const ctx = {
      user: {
        admin: false,
      },
    }

    try {
      await createUserAsAdmin
        .initialize({
          User: models.User,
          Notification: MockNotification,
          ctx,
        })
        .execute({})
    } catch (e) {
      expect(e.message).toBe('Unauthorized')
    }
  })
})
