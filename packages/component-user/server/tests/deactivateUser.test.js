const { cloneDeep } = require('lodash')
const { Model, fixtures } = require('pubsweet-component-fixture-service')

const { deactivateUser } = require('../use-cases')

describe('deactivate use case', () => {
  let testFixtures = {}
  let models
  const invalidPrefix = 'invalid***'

  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    models = Model.build(testFixtures)
  })

  it('deactivate an user', async () => {
    const { user } = testFixtures.users
    user.isActive = false

    const result = await deactivateUser
      .initialize({ User: models.User })
      .execute({ id: user.id, input: user })

    expect(result.isActive).toBeFalsy()
    // invalid prefix should be present in the username
    expect(result.username.indexOf(invalidPrefix)).toBeGreaterThanOrEqual(0)
  })
})
