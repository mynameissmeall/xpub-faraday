import React, { Fragment } from 'react'
import { get } from 'lodash'
import { Formik } from 'formik'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { H2, Menu, Button, Spinner, TextField } from '@pubsweet/ui'
import { compose, setDisplayName, withHandlers, withProps } from 'recompose'
import {
  Row,
  Item,
  Text,
  Label,
  IconButton,
  MenuCountry,
  RowOverrideAlert,
  ItemOverrideAlert,
  ValidatedFormField,
  withRoles,
  withFetching,
  withCountries,
  validators,
} from 'pubsweet-component-faraday-ui'

// #region helpers
const setInitialRole = a => {
  if (get(a, 'admin', false)) {
    return 'admin'
  }
  if (get(a, 'handlingEditor', false)) {
    return 'handlingEditor'
  }
  if (get(a, 'editorInChief', false)) {
    return 'editorInChief'
  }
  return 'author'
}

const validate = values => {
  const errors = {}

  if (get(values, 'email', '') === '') {
    errors.email = 'Required'
  }

  return errors
}
// #endregion

const FormModal = ({
  edit,
  roles,
  title,
  titles,
  onClose,
  onSubmit,
  onConfirm,
  countries,
  isFetching,
  fetchingError,
  initialValues,
  confirmText = 'OK',
  cancelText = 'Cancel',
  user,
}) => (
  <Root>
    <IconButton icon="x" onClick={onClose} right={5} secondary top={5} />
    <H2>{title}</H2>
    {edit && (
      <Text mb={1} secondary>
        {get(user, 'email', '')}
      </Text>
    )}
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validate={validate}
    >
      {({ handleSubmit, ...rest }) => (
        <Fragment>
          {!edit && (
            <Row alignItems="baseline" mb={1} mt={1}>
              <ItemOverrideAlert mr={1} vertical>
                <Label required>Email</Label>
                <ValidatedFormField
                  component={TextField}
                  inline
                  name="email"
                  validate={[validators.emailValidator]}
                />
              </ItemOverrideAlert>

              <ItemOverrideAlert ml={1} vertical>
                <Label required>Role</Label>
                <ValidatedFormField
                  component={Menu}
                  name="role"
                  options={roles}
                />
              </ItemOverrideAlert>
            </Row>
          )}

          <Row mb={2}>
            <Item mr={1} vertical>
              <Label>First Name</Label>
              <ValidatedFormField
                component={TextField}
                inline
                name="firstName"
              />
            </Item>

            <Item ml={1} vertical>
              <Label>Last Name</Label>
              <ValidatedFormField
                component={TextField}
                inline
                name="lastName"
              />
            </Item>
          </Row>

          <RowOverrideAlert alignItems="center" mb={2}>
            <ItemOverrideAlert data-test-id="title" mr={1} vertical>
              <Label>Title</Label>
              <ValidatedFormField
                component={Menu}
                name="title"
                options={titles}
              />
            </ItemOverrideAlert>

            <ItemOverrideAlert data-test-id="country" ml={1} vertical>
              <Label>Country</Label>
              <ValidatedFormField component={MenuCountry} name="country" />
            </ItemOverrideAlert>
          </RowOverrideAlert>

          <Row mb={3}>
            {edit && (
              <ItemOverrideAlert data-test-id="role" mr={1} vertical>
                <Label required>Role</Label>
                <ValidatedFormField
                  component={Menu}
                  name="role"
                  options={roles}
                />
              </ItemOverrideAlert>
            )}
            <Item ml={edit && 1} vertical>
              <Label>Affiliation</Label>
              <ValidatedFormField
                component={TextField}
                inline
                name="affiliation"
              />
            </Item>
          </Row>

          {fetchingError && (
            <Row mb={1}>
              <Text error>{fetchingError}</Text>
            </Row>
          )}

          {isFetching ? (
            <Spinner />
          ) : (
            <Row>
              <Button onClick={onClose}>Cancel</Button>
              <Button onClick={handleSubmit} primary>
                {confirmText}
              </Button>
            </Row>
          )}
        </Fragment>
      )}
    </Formik>
  </Root>
)

export default compose(
  withRoles,
  withFetching,
  withCountries,
  withProps(({ user, edit }) => ({
    initialValues: {
      ...user,
      role: setInitialRole(user),
    },
    confirmText: edit ? 'EDIT USER' : 'SAVE USER',
    title: edit ? 'Edit User' : 'Add User',
  })),
  withHandlers({
    onSubmit: ({ onSubmit, ...props }) => (values, formProps) => {
      if (typeof onSubmit === 'function') {
        onSubmit(values, { ...formProps, ...props })
      }
    },
    onClose: ({ onCancel, ...props }) => () => {
      if (typeof onCancel === 'function') {
        onCancel(props)
      }
      props.hideModal()
    },
  }),

  setDisplayName('AdminUserForm'),
)(FormModal)

// #region styles
const Root = styled.div`
  align-items: center;
  background: ${th('colorBackgroundHue')};
  border: ${th('borderWidth')} ${th('borderStyle')} transparent;
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  position: relative;
  padding: calc(${th('gridUnit')} * 3);
  width: calc(${th('gridUnit')} * 60);

  ${H2} {
    margin: 0;
    text-align: center;
  }
`
// #endregion
