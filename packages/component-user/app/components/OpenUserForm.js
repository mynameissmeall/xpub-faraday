import React from 'react'

import {
  OpenModal,
  ActionLink,
  IconButton,
} from 'pubsweet-component-faraday-ui'

import AdminUserForm from './AdminUserForm'

const OpenUserForm = ({ edit, user, onSubmit, modalKey }) => (
  <OpenModal
    component={AdminUserForm}
    edit={edit}
    modalKey={modalKey}
    onSubmit={onSubmit}
    user={user}
  >
    {showModal =>
      edit ? (
        <IconButton fontIcon="editIcon" iconSize={2} onClick={showModal} />
      ) : (
        <ActionLink icon="plus" onClick={showModal}>
          ADD USER
        </ActionLink>
      )
    }
  </OpenModal>
)

export default OpenUserForm
