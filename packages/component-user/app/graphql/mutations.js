import gql from 'graphql-tag'

import { userFragment } from './fragments'

export const addUserAsAdmin = gql`
  mutation addUserAsAdmin($input: UserInput!) {
    addUserAsAdmin(input: $input) {
      ...userDetails
    }
  }
  ${userFragment}
`

export const editUserAsAdmin = gql`
  mutation editUserAsAdmin($id: ID!, $input: UserInput!) {
    editUserAsAdmin(id: $id, input: $input) {
      ...userDetails
    }
  }
  ${userFragment}
`

export const activateUserAsAdmin = gql`
  mutation activateUserAsAdmin($id: ID!, $input: ActivateUserInput) {
    activateUserAsAdmin(id: $id, input: $input) {
      ...userDetails
    }
  }
  ${userFragment}
`
