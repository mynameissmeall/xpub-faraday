import gql from 'graphql-tag'

export const userFragment = gql`
  fragment userDetails on User {
    id
    admin
    email
    title
    country
    username
    lastName
    isActive
    firstName
    affiliation
    editorInChief
    handlingEditor
  }
`
