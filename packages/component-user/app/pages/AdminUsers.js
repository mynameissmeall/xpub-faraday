import React, { Fragment } from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withJournal } from 'xpub-journal'
import { compose, withHandlers, withProps } from 'recompose'

import {
  Row,
  Text,
  Item,
  Label,
  Pagination,
  ActionLink,
  withPagination,
} from 'pubsweet-component-faraday-ui'

import withUsersGQL from '../graphql'
import { OpenUserForm, OpenStatusModal } from '../components'

const Users = ({
  page,
  users,
  history,
  journal,
  addUser,
  updateUser,
  getUserName,
  getUserRoles,
  itemsPerPage,
  getStatusLabel,
  paginatedItems,
  toggleUserStatus,
  ...rest
}) => (
  <Fragment>
    <Row alignItems="center" justify="space-between" mb={1}>
      <Item alignItems="center">
        <ActionLink
          data-test-id="go-to-dashboard"
          fontIcon="arrowLeft"
          mr={2}
          onClick={history.goBack}
          paddingBottom={1.2}
        >
          Admin Dashboard
        </ActionLink>
        <OpenUserForm modalKey="addUser" onSubmit={addUser} />
      </Item>

      <Pagination {...rest} itemsPerPage={itemsPerPage} page={page} />
    </Row>

    <Table>
      <thead>
        <tr>
          <th>
            <Label>Full Name</Label>
          </th>
          <th colSpan={2}>
            <Label>Email</Label>
          </th>
          <th>
            <Label>Affiliation</Label>
          </th>
          <th>
            <Label>Roles</Label>
          </th>
          <th>
            <Label>Status</Label>
          </th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        {paginatedItems.map(user => (
          <UserRow key={user.id}>
            <td>
              <Text pl={1}>{getUserName(user)}</Text>
            </td>
            <td colSpan={2}>
              <Text>{user.email}</Text>
            </td>
            <td>
              <Text>{user.affiliation}</Text>
            </td>
            <td>
              <Text customId>{getUserRoles(user)}</Text>
            </td>
            <td>
              <Text secondary>{getStatusLabel(user)}</Text>
            </td>

            <HiddenCell>
              <Item alignItems="center" justify="flex-end">
                <OpenUserForm
                  edit
                  modalKey={`edit-${user.id}`}
                  onSubmit={updateUser}
                  user={user}
                />

                <OpenStatusModal onConfirm={toggleUserStatus} user={user} />
              </Item>
            </HiddenCell>
          </UserRow>
        ))}
      </tbody>
    </Table>
  </Fragment>
)

export default compose(
  withJournal,
  withUsersGQL,
  withProps(({ journal: { roles = {} }, users }) => ({
    roles: Object.keys(roles),
    items: users,
  })),
  withPagination,
  withHandlers({
    getStatusLabel: () => ({ admin, isConfirmed, isActive = true }) => {
      if (admin) return 'ACTIVE'
      if (!isActive) {
        return 'INACTIVE'
      }
      return isConfirmed ? 'ACTIVE' : 'INVITED'
    },
    addUser: ({ addUser }) => (
      {
        __typename,
        id,
        admin,
        isActive,
        editorInChief,
        handlingEditor,
        ...input
      },
      { setFetching, hideModal, setError },
    ) => {
      setFetching(true)
      addUser({
        variables: {
          input: {
            ...input,
            username: input.email,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
    updateUser: ({ updateUser }) => (
      {
        __typename,
        id,
        admin,
        isActive,
        handlingEditor,
        editorInChief,
        ...input
      },
      { setFetching, hideModal, setError },
    ) => {
      setFetching(true)
      updateUser({
        variables: {
          id,
          input,
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
    toggleUserStatus: ({ activateUser }) => (
      { id, email, username, isActive },
      { setFetching, hideModal, setModalError },
    ) => {
      setFetching(true)
      activateUser({
        variables: {
          id,
          input: {
            email,
            username,
            isActive,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch(e => {
          setFetching(false)
          setModalError(e.message)
        })
    },
    getUserName: () => user => {
      if (user.admin) {
        return 'Admin'
      }
      return `${get(user, 'firstName', '')} ${get(user, 'lastName', '')}`
    },
    getUserRoles: ({ journal: { roles = {} } }) => user => {
      const parsedRoles = Object.entries(roles)
        .reduce((acc, role) => (user[role[0]] ? [...acc, role[1]] : acc), [])
        .join(', ')

      return parsedRoles || 'Author'
    },
  }),
)(Users)

// #region styled-components
const Table = styled.table`
  border-collapse: collapse;

  & th,
  & td {
    border: none;
    text-align: start;
    vertical-align: middle;
    height: calc(${th('gridUnit')} * 5);
  }
`

const HiddenCell = styled.td`
  opacity: 0;
`

const UserRow = styled.tr`
  background-color: ${th('colorBackgroundHue2')};
  border-bottom: 1px solid ${th('colorBorder')};

  &:hover {
    background-color: ${th('colorBackgroundHue3')};

    ${HiddenCell} {
      opacity: 1;
    }
  }
`
