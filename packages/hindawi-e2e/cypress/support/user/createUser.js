const createUser = ({ admin, user }) => {
  cy.loginApi(admin.username, admin.password)
  cy.visit(`/`)

  cy.wait(5000)

  cy
    .get('[data-test-id="admin-menu-button"]')
    .click()
    .get('[data-test-id="admin-dropdown-dashboard"]')
    .first()
    .click()
    .title('contains', 'Admin dashboard')
    .get('[role="img"]')
    .first()
    .click()

    .visit('/admin/users')
    .url()
    .should('include', '/admin/users')
    .get('[type="button"]')
    .eq(2)
    .click()

    .get('[data-test-id="row"]')
    .get('[name="email"]')
    .type('email')
    .blur()

    .get('div[role="alert"]')
    .children('div')
    .contains('Invalid email')

    .get('[data-test-id="row"]')
    .get('[name="email"]')
    .clear()
    .type(
      `${Cypress.env('email') +
        Math.random()
          .toString(22)
          .substring(8)}@thinslices.com`,
    )

    .get('[role="listbox"]')
    .first()
    .click()
    .get('[role="option"]')
    .eq(user.roleOption)
    .click()
    .get('[name="firstName"]')
    .type(user.firstName)
    .get('[name="lastName"]')
    .type(user.lastName)
    .get('[role="listbox"]')
    .eq(1)
    .click()
    .get('[role="option"]')
    .contains(user.title)
    .click()
    .get('[role="listbox"]')
    .eq(2)
    .click()
    .get('[role="option"]')
    .contains(user.country)
    .click()
    .get('[name="affiliation"]')
    .type(user.affiliation)
    .get('[type="button"]')
    .contains('SAVE USER')
    .click()

    .wait(5000)
    .get('[data-test-id="new-manuscript"]')
    .should('be.visible')
}

Cypress.Commands.add('createUser', createUser)
