// import { binaryStringToBlob } from 'blob-util'

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

const fileTypes = {
  pdf: 'application/pdf',
  doc: 'application/msword',
  docx:
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  txt: 'text/plain',
  excel: 'application/vnd.ms-excel',
  xlm: 'application/vnd.ms-excel',
}

Cypress.Commands.add(
  'uploadFile',
  ({ fileName, fileType = 'pdf', filePicker }) => {
    cy
      .fixture(fileName, 'base64')
      .then(Cypress.Blob.base64StringToBlob)
      .then(blob => {
        const el = filePicker[0]
        const testFile = new File([blob], fileName, {
          type: fileTypes[fileType],
        })
        const dataTransfer = new DataTransfer()
        dataTransfer.items.add(testFile)
        el.files = dataTransfer.files
      })
  },
)

Cypress.Commands.add('form_request', (url, formData, token) =>
  cy
    .server()
    .route('POST', url)
    .as('form_request')
    .window()
    .then(win => {
      const xhr = new win.XMLHttpRequest()
      xhr.open('POST', url)
      xhr.setRequestHeader('Accept', `text/plain`)
      xhr.setRequestHeader('Authorization', `Bearer ${token}`)
      xhr.send(formData)
    })
    .wait('@form_request'),
)

Cypress.Commands.add(
  'uploadFileAPI',
  ({ fragmentId, fileType = 'manuscripts', token }) => {
    const data = new FormData()
    const file = new File(['some text'], 'filename.pdf', { type: 'text/plain' })
    data.append('fileType', fileType)
    data.append('fragmentId', fragmentId)
    data.append('file', file)
    data.append('newName', 'filename.pdf')

    cy.form_request('/api/files', data, token).then(res => res.response.body)
  },
)

Cypress.Commands.add('loginApi', (user, pwd) => {
  const username = user === 'admin' ? user : Cypress.env('email') + user
  cy
    .request({
      method: 'POST',
      url: '/api/users/authenticate',
      body: {
        username,
        password: pwd,
      },
    })
    .then(response => {
      const { token } = response.body
      window.localStorage.setItem('token', token)
      return token
    })
})

Cypress.Commands.add('useFilters', () => {
  cy.get('[data-test-id="row"]').contains('Filters')
  cy.get('[data-test-id="dashboard-filter-priority"]').click()
  cy
    .get('[role="option"]')
    .contains('Needs Attention')
    .click()
  cy.get('[data-test-id="fragment-status"]').then($fragment => {
    Cypress._.each($fragment => {
      expect($fragment).contain('Submit Revision')
    })
  })
})

Cypress.Commands.add('searchManuscriptByFragmentId', () => {
  cy.get('[data-test-id="/^fragment-w+/"]')
})

Cypress.Commands.add(
  'createManuscriptViaAPI',
  (
    author = {
      email: `admin`,
      password: 'password',
    },
  ) => {
    let fragmentID = null
    let collectionID = null
    let fragment = {}

    cy.loginApi(author.email, author.password).then(userToken => {
      cy
        .request({
          method: 'POST',
          url: '/api/collections',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${userToken}`,
          },
          body: {
            customId: Math.random()
              .toString()
              .slice(-7),
          },
        })
        .then(response => {
          const { id } = response.body
          collectionID = id
          Cypress.env('collectionId', collectionID)
          return collectionID
        })
        .then(() => {
          cy.request({
            method: 'POST',
            url: `/api/collections/${collectionID}/fragments`,
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${userToken}`,
            },
            body: {
              created: Date.now(),
              files: {
                coverLetter: [],
                manuscripts: [],
                supplementary: [],
              },
              fragmentType: 'version',
              metadata: {
                type: 'research',
                title: 'The test of creating a manuscript via api 2',
                abstract: 'some text',
              },
              version: 1,
              collectionId: collectionID,
            },
          })
        })
        .then(response => {
          fragment = response.body
          fragmentID = fragment.id
          Cypress.env('fragmentIdV1', fragmentID)
          return fragmentID
        })
        .then(() => {
          cy.request({
            method: 'POST',
            url: `/api/collections/${collectionID}/fragments/${fragmentID}/users`,
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${userToken}`,
            },
            body: {
              email: `${Cypress.env('email')}auth@thinslices.com`,
              role: 'author',
              isSubmitting: true,
              isCorresponding: false,
              firstName: 'Author',
              lastName: 'Submittingson',
              affiliation: 'MIT',
              country: 'RO',
            },
          })
        })
        .then(() =>
          cy.uploadFileAPI({ fragmentId: fragmentID, token: userToken }),
        )
        .then(file => {
          cy.request({
            method: 'PATCH',
            url: `/api/collections/${collectionID}/fragments/${fragmentID}`,
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${userToken}`,
            },
            body: {
              ...fragment,
              files: {
                ...fragment.files,
                manuscripts: [file],
                supplementary: [file],
                coverLetter: [file],
              },
            },
          })
        })
        .then(() => {
          cy.request({
            method: 'POST',
            url: `/api/collections/${collectionID}/fragments/${fragmentID}/submit`,
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${userToken}`,
            },
          })
        })
    })

    cy.loginApi('admin', 'password').then(adminToken => {
      cy
        .request({
          method: 'GET',
          url: `/api/collections/${collectionID}`,
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${adminToken}`,
          },
        })
        .then(response => response.body.technicalChecks.token)
        .then(technicalChecks => {
          cy.request({
            method: 'PATCH',
            url: `/api/collections/${collectionID}/status`,
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${adminToken}`,
            },
            body: {
              token: technicalChecks,
              step: 'eqs',
              agree: true,
              customId: Math.random()
                .toString()
                .slice(-7),
            },
          })
        })
    })
  },
)
