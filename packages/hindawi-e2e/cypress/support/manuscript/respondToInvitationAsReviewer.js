const respondToInvitationAsReviewer = response => {
  cy.contains('Respond to Invitation to Review')
  cy.get(`[value=${response}`).click()
  cy
    .get('[data-test-id="radio-respond-to-invitation"]')
    .find('button')
    .click()
  cy.get(`[data-test-id="modal-root"]`).should('be.visible')
  cy.get('[data-test-id="modal-cancel"]').click()
  cy.get(`[data-test-id="modal-root"]`).should('not.be.visible')
  cy
    .get('[data-test-id="radio-respond-to-invitation"]')
    .find('button')
    .click()
  cy.get('[data-test-id="modal-confirm"]').click()
}

Cypress.Commands.add(
  'respondToInvitationAsReviewer',
  respondToInvitationAsReviewer,
)
