const checkStatus = status => {
  cy.get('[data-test-id="fragment-status"]').should('contain', status)
}

Cypress.Commands.add('checkStatus', checkStatus)
