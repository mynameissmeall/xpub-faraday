const approveManuscriptByEQA = ({ admin }) => {
  cy
    .get('[data-test-id="button-qa-manuscript-technical-checks"]')
    .should('be.visible')
    .click()
  cy.contains('Take a decision for manuscript .')
  cy.get('[data-test-id="eqs-no-button"]').click()
  cy.get('[data-test-id="modal-root"]').should('be.visible')
  cy.get('[data-test-id="modal-cancel"]').click()
  cy.get('[data-test-id="modal-root"]').should('not.be.visible')
  cy.get('[data-test-id="eqa-yes-button"]').click()

  cy.contains('Are you sure you want to accept this manuscript?')
  cy.get('[data-test-id="modal-confirm"]').click()

  cy.wait(2000)

  cy.contains('Manuscript decision submitted. Thank you!')
}

Cypress.Commands.add('approveManuscriptByEQA', approveManuscriptByEQA)
