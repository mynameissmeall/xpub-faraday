const submitReview = recommendation => {
  cy
    .get('[data-test-id="submit-report-box"]')
    .should('be.visible')
    .contains('Your report')
    .click()
  cy
    .get('[data-test-id="form-report-recommendation"]')
    .find('[role="listbox"]')
    .click()
  cy
    .get('[role="option"]')
    .contains(recommendation)
    .click()
    .wait(3000)

  cy
    .get('input[type="file"]')
    .first()
    .then(filePicker => {
      cy.uploadFile({ fileName: 'file1.pdf', filePicker })
    })

  cy.wait(2000)

  cy
    .get('[data-test-id="form-report-textarea"] textarea')
    .should('be.visible')
    .wait(2000)
    .click({ force: true })
    .type(Math.random().toString(22))
  cy
    .get('[data-test-id="button-submit-report"]')
    .should('be.visible')
    .click({ force: true })
  cy.wait(2000)
  cy.get(`[data-test-id="modal-root"]`).should('be.visible')
  cy.get('[data-test-id="modal-cancel"]').click()
  cy.get(`[data-test-id="modal-root"]`).should('not.be.visible')
  cy
    .get('[data-test-id="button-submit-report"]')
    .should('be.visible')
    .click({ force: true })
  cy.wait(2000)
  cy.get('[data-test-id="modal-confirm"]').click({ force: true })
  cy.wait(2000)
}

Cypress.Commands.add('submitReview', submitReview)
