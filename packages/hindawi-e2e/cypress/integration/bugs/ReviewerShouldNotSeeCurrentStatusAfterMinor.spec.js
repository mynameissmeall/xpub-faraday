describe('Second Reviewer should see the status the reviewer that previously made minor rivision', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
    cy.fixture('models/updatedFragment').as('updatedFragment')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he } = this
    cy.inviteHE({ eic, he })
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/').wait(3000)
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentId')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsHE('accept')
  })

  it('Invite reviewers as HE', function inviteReviewer() {
    const { reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/').wait(3000)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentId')}"]`).click()
    cy.inviteReviewer({ reviewer, he })
  })
  it('Give a review for minor revision as Reviewer', function submitMinReview() {
    const { reviewer, he, statuses } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('accept')

    cy.checkStatus(statuses.underReview.reviewer)

    cy.wait(3000)

    cy.submitReview('Minor Revision')
    cy.wait(3000)
    cy.checkStatus(statuses.reviewCompleted.reviewer)

    cy.loginApi(he.email, he.password)
    cy.visit(`/`)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentId')}"]`)
    cy.checkStatus(statuses.reviewCompleted.handlingEditor)

    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy
      .visit('/')
      .wait(2000)
      .get(`[data-test-id="fragment-${Cypress.env('fragmentId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('accept')

    cy.checkStatus(statuses.underReview.reviewer)

    cy.wait(3000)

    cy.submitReview('Minor Revision')
    cy.wait(3000)
    cy.checkStatus(statuses.reviewCompleted.reviewer)

    cy.loginApi(he.email, he.password)
    cy.visit(`/`)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentId')}"]`)
    cy.checkStatus(statuses.reviewCompleted.handlingEditor)
  })

  it('HE asks for minor revision', function heAsksForMinRev() {
    const { he, statuses } = this

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.reviewCompleted.handlingEditor)
    cy
      .get('[data-test-id="reviewer-details-and-reports-box"]')
      .should('be.visible')
      .click()
    cy.get('[data-test-id="reviewer-tab-reports"]').click()
    cy.HErecommendation('Request Minor Revision').then(() => {
      cy.checkStatus(statuses.revisionRequested.handlingEditor)
      cy
        .get(`[data-test-id="contextual-box-he-recommendation"]`)
        .should('be.visible')
        .click()
      cy
        .get('[data-test-id="contextual-box-he-recommendation"]')
        .find('[role="listbox"]')
        .click()
      cy
        .get('[data-test-id="contextual-box-he-recommendation"]')
        .find('[role="option"]')
        .should('contain', 'Reject')
        .should('not.contain', 'Publish')
        .should('not.contain', 'Request Major Revision')
        .should('not.contain', 'Request Minor Revision')
    })
  })
  it('Author submit revision', function authorSubmitRevision() {
    const { author, statuses, updatedFragment } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentId')}"]`).click()
    cy.checkStatus(statuses.revisionRequested.author)
    cy.authorSubmitRevision({ updatedFragment })
    cy.visit('/').wait(2000)
    cy
      .get(`[data-test-id="dashboard-list-items"]`)
      .find(`[data-test-id="row"]`)
      .first()
      .click()
    cy.location().then(loc => {
      Cypress.env(
        'fragmentIdV2',
        loc.pathname
          .replace('/details', '')
          .split('/')
          .pop(),
      )
    })
  })
  it('Invite reviewer that responded as HE', function inviteReviewerThatResponded() {
    const { reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/').wait(3000)
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV2')}"]`)
      .should('be.visible')
      .click()
    cy
      .wait(2000)
      .get('[data-test-id="invite-reviewer-email"]')
      .should('be.visible')
      .type(Cypress.env('email') + reviewer[1].email)
    cy
      .get('[data-test-id="invite-reviewer-first-name"]')
      .type(reviewer[1].firstName)
    cy
      .get('[data-test-id="invite-reviewer-last-name"]')
      .type(reviewer[1].lastName)
    cy
      .get('[data-test-id="invite-reviewer-affiliation"]')
      .type(reviewer[1].affiliation)
    cy.get('[placeholder="Please select"]').click()
    cy.wait(2000)
    cy.contains(reviewer[1].country).click()
    cy.get('[data-type-id="button-invite-reviewer-invite"]').click()
    cy.get('[data-test-id="modal-confirm"]').click()
    cy.wait(4000)
  })
  it('Other reviewers should see their status', function reviewerShouldSeeReviewCompletedStatus() {
    const { reviewer, statuses } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)

    cy
      .visit('/')
      .wait(2000)
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV2')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsReviewer('accept')
    cy.checkStatus(statuses.underReview.reviewer)

    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy
      .visit('/')
      .wait(2000)
      .get(`[data-test-id="fragment-${Cypress.env('fragmentId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.reviewCompleted.reviewer)
  })
})
