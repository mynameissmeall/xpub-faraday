describe('None can access deleted manuscripts', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
  })

  it('Submit manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Admin deletes manuscript', function adminDeletesManuscript() {
    const { admin } = this

    cy.loginApi(admin.username, admin.password).then(userToken => {
      cy
        .visit('/')
        .get(
          `[data-test-id="fragment-${Cypress.env(
            'fragmentIdV1',
          )}"] .deleteIcon`,
        )
        .click()

      cy
        .get(`[name="comments"]`)
        .type('Lorem impsum!')
        .get('[data-test-id="modal-confirm"]')
        .click()
    })
  })

  it('Admin should not be able to access deleted manuscript', function checkAdmin() {
    const { admin } = this
    cy
      .loginApi(admin.username, admin.password)
      .visit('/')
      .wait(2000)
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('not.exist')
      .visit(
        `/projects/${Cypress.env('collectionId')}/versions/${Cypress.env(
          'fragmentIdV1',
        )}/details`,
      )
      .wait(2000)
      .get('[data-test-id="fragment-status"]')
      .should('not.exist')
  })

  it('EiC should not be able to access deleted manuscript', function checkEiC() {
    const { eic } = this
    cy
      .loginApi(eic.username, eic.password)
      .visit('/')
      .wait(2000)
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('not.exist')
      .visit(
        `/projects/${Cypress.env('collectionId')}/versions/${Cypress.env(
          'fragmentIdV1',
        )}/details`,
      )
      .wait(2000)
      .get('[data-test-id="fragment-status"]')
      .should('not.exist')
  })

  it('HE should not be able to access deleted manuscript', function checkHE() {
    const { he } = this
    cy
      .loginApi(he.username, he.password)
      .visit('/')
      .wait(2000)
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('not.exist')
      .visit(
        `/projects/${Cypress.env('collectionId')}/versions/${Cypress.env(
          'fragmentIdV1',
        )}/details`,
      )
      .wait(2000)
      .get('[data-test-id="fragment-status"]')
      .should('not.exist')
  })

  it('Author should not be able to access deleted manuscript', function checkAuthor() {
    const { author } = this
    cy
      .loginApi(author.username, author.password)
      .visit('/')
      .wait(2000)
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('not.exist')
      .visit(
        `/projects/${Cypress.env('collectionId')}/versions/${Cypress.env(
          'fragmentIdV1',
        )}/details`,
      )
      .wait(2000)
      .get('[data-test-id="fragment-status"]')
      .should('not.exist')
  })
})
