describe('Create author', () => {
  beforeEach(() => {
    cy.fixture('users/author').as('author')
    cy.visit('/')
  })

  it('Create succesfully a new author account', function createAuthor() {
    const { author } = this

    cy
      .url()
      .should('include', 'login?next=/dashboard')
      .get('[href="/signup"]')
      .click()
      .url()
      .should('include', '/signup')

      .get('[name="firstName"]')
      .type(author.firstName)
      .get('[name="lastName"]')
      .type(author.lastName)
      .get('[role="listbox"]')
      .first()
      .click()
      .get('[role="option"]')
      .contains(author.title)
      .click()
      .get('[data-test-id="sign-up-country"]')
      .click()
      .get('[role="option"]')
      .contains(author.country)
      .click()
      .get('[name="affiliation"]')
      .type(author.affiliation)

      .get('[type="checkbox"]')
      .check({ force: true })
      .get('[data-test-id="sign-up-proceed-to-set-email-and-password"]')
      .click()
      .url()
      .should('include', '/signup')

      .get('[name="email"]')
      .type(
        `${Cypress.env('email') +
          Math.random()
            .toString(22)
            .substring(8)}@thinslices.com`,
      )
      .get('[name="password"]')
      .type(author.password)
      .get('[name="confirmNewPassword"]')
      .type(author.password)
      .get('[data-test-id="sign-up-confirm-button"]')
      .click()
      .wait(2000)
      .url()
      .should('include', '/dashboard')
      .get('[data-test-id="new-manuscript"]')
      .should('be.visible')
    cy.contains('Your account is not confirmed. Please check your email.')
  })
})
