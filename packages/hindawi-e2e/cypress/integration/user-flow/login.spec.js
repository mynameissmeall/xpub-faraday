describe('Login', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('models/fragment').as('fragment')
  })

  it('Login successfully as admin', function loginAdmin() {
    const { admin } = this
    cy.visit(`https://qa.review.hindawi.com`)

    cy.get('[name="username"]').type(admin.username)
    cy.get('[name="password"]').type(admin.password)
    cy.get('[type="submit"]').click()

    cy.url().should('include', '/dashboard')
  })

  it('Login failure as admin', () => {
    cy.visit(`https://qa.review.hindawi.com`)

    cy.get('[name="username"]').type('admin')
    cy.get('[name="password"]').type('password123')
    cy.get('[type="submit"]').click()

    cy.wait(2000)

    cy.contains('Unauthorized')
    cy.url().should('not.include', '.com/dashboard')
  })
})
