describe('Deactivate User', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
  })

  it('Deactivate succesfully a user', function deactivateUser() {
    const { admin } = this

    cy.loginApi(admin.username, admin.password)
    cy.visit('/')

    cy.get('.arrowDown').click()
    cy.get('[data-test-id="admin-dropdown-dashboard"]').click()
    cy.get('span[role="img"]').click()
    cy.get('table tbody tr:first button').click()
    cy.get('[data-test-id="modal-root"]').should('be.visible')
    cy.get('[data-test-id="modal-cancel"]').click()
    cy.get('[data-test-id="modal-root"]').should('not.be.visible')
    cy.get('table tbody tr:first button').click()
    cy.get('[data-test-id="modal-confirm"]').click()

    cy.get('#ps-modal-root').should('be.empty')
  })
})
