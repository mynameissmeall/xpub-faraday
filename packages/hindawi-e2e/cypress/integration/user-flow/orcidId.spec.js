describe('ORCID ID', () => {
  beforeEach(() => {
    cy.fixture('users/author').as('author')
  })

  it('ORCID ID', function orcidId() {
    const { author } = this

    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get('.arrowDown').click()
    cy.get('[data-test-id="admin-dropdown-profile"]').click()

    cy
      .request('/profile/api/users/orcid*', { followRedirect: true })
      .then(response => cy.log(response))
  })
})
