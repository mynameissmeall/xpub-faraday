describe('Edit User Profile', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
  })

  it('Edit user profile', function editUserProfile() {
    const { author } = this

    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get('.arrowDown').click()
    cy.get('[data-test-id="admin-dropdown-profile"]').click()
    cy.get('.editIcon').click()
    cy.get('input[name="firstName"]').type('new')
    cy.get('input[name="lastName"]').type('new')
    cy.get('[data-test-id="title"] button').click()
    cy
      .get('div[role="option"][aria-selected="true"]')
      .next()
      .click()
    cy
      .get('[data-test-id="country"]')
      .get('input[placeholder="Please select"]')
      .clear()
      .type('ro', { force: true })
      .type('{enter}', { force: true })

    cy.get('input[name="affiliation"]').type('new')
    cy.get('.saveIcon').click()
  })
})
