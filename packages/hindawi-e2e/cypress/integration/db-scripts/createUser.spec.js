describe('Create users into DB', () => {
  beforeEach(() => {
    cy.fixture('users/allUsers').as('users')
  })
  it('Create users except admin in DB', function createUsers() {
    const { users } = this
    cy.task('createUser', users).should('equal', true)
  })
})
