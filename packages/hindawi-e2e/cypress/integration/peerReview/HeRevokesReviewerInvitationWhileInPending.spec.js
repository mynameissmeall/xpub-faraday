describe('HE revokes reviewer invitation while invitation is in pending', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he } = this
    cy.inviteHE({ eic, he })
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsHE('accept')
  })

  it('Invite reviewers as HE', function inviteReviewer() {
    const { reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.inviteReviewer({ reviewer, he })
  })

  it('Revoke reviewer invitation while invitation is in pending as HE', function revokeReviewer() {
    const { statuses, reviewer, he } = this
    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.loginApi(he.email, he.password)
    cy.visit('/')
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()

    cy.checkStatus(statuses.reviewersInvited.handlingEditor)
    cy
      .get(`[ data-test-id="reviewer-details-and-reports-box"]`)
      .click()
      .contains('PENDING')
      .last()
      .click()
    cy
      .get(`[ data-test-id="revoke-button"]`)
      .should('be.visible')
      .last()
      .click()
    cy.get(`[data-test-id="modal-root"]`).should('be.visible')
    cy.get(`[ data-test-id="modal-cancel"]`).click()
    cy.get(`[data-test-id="modal-root"]`).should('not.be.visible')
    cy
      .get(`[ data-test-id="reviewer-details-and-reports-box"]`)
      .click()
      .contains('PENDING')
      .last()
      .click()
    cy
      .get(`[ data-test-id="revoke-button"]`)
      .should('be.visible')
      .last()
      .click()
    cy.get(`[ data-test-id="modal-confirm"]`).click()
    cy.wait(2000)

    cy
      .get(`[ data-test-id="reviewer-details-and-reports-box"]`)
      .contains('PENDING')
      .last()
      .click()

    cy
      .get(`[ data-test-id="revoke-button"]`)
      .should('be.visible')
      .last()
      .click()
    cy.get(`[data-test-id="modal-root"]`).should('be.visible')
    cy.get(`[ data-test-id="modal-cancel"]`).click()
    cy.get(`[data-test-id="modal-root"]`).should('not.be.visible')
    cy
      .get(`[ data-test-id="reviewer-details-and-reports-box"]`)
      .click()
      .contains('PENDING')
      .last()
      .click()
    cy
      .get(`[ data-test-id="revoke-button"]`)
      .should('be.visible')
      .last()
      .click()
    cy.get(`[ data-test-id="modal-confirm"]`).click()
    cy.wait(2000)

    cy
      .get(`[ data-test-id="reviewer-details-and-reports-box"]`)
      .contains('PENDING')
      .last()
      .click()
    cy
      .get(`[ data-test-id="revoke-button"]`)
      .should('be.visible')
      .last()
      .click()
    cy.get(`[data-test-id="modal-root"]`).should('be.visible')
    cy.get(`[ data-test-id="modal-cancel"]`).click()
    cy.get(`[data-test-id="modal-root"]`).should('not.be.visible')
    cy
      .get(`[ data-test-id="reviewer-details-and-reports-box"]`)
      .click()
      .contains('PENDING')
      .last()
      .click()
    cy
      .get(`[ data-test-id="revoke-button"]`)
      .should('be.visible')
      .last()
      .click()
    cy.get(`[ data-test-id="modal-confirm"]`).click()
    cy.wait(2000)

    cy
      .get(`[ data-test-id="reviewer-details-and-reports-box"]`)
      .should('be.visible')
      .should('not.contain', 'Reviewer1 Rev1')
      .should('not.contain', 'Revington2 Dude')
      .should('not.contain', 'Revington Dude')
    cy.checkStatus(statuses.heAssigned.handlingEditor)
    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')
    cy.wait(3000)
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('not.be.visible')
  })
})
