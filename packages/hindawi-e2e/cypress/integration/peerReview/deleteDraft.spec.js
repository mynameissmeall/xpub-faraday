describe('Delete Draft', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('models/fragment').as('fragment')
  })

  it('Create draft', function createDraft() {
    const { author } = this
    cy.createDraft({ author })
  })

  it('Admin deletes a draft', function adminDeletesDraft() {
    const { admin } = this

    cy.loginApi(admin.username, admin.password)
    cy.visit('/')

    cy
      .get(
        `[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"] .deleteIcon`,
      )
      .click()
    cy.get(`[data-test-id="modal-root"]`).should('be.visible')
    cy.get('[data-test-id="modal-cancel"]').click()
    cy.get(`[data-test-id="modal-root"]`).should('not.be.visible')
    cy
      .get(
        `[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"] .deleteIcon`,
      )
      .click()
    cy.get('[data-test-id="modal-confirm"]').click()
    cy.visit('/')
  })

  it('Create draft ', function createDraft() {
    const { author } = this
    cy.createDraft({ author })
  })

  it('Author deletes draft', function authorDeletesDraft() {
    const { author } = this

    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy
      .get(
        `[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"] .deleteIcon`,
      )
      .click()
    cy.get(`[data-test-id="modal-root"]`).should('be.visible')
    cy.get('[data-test-id="modal-cancel"]').click()
    cy.get(`[data-test-id="modal-root"]`).should('not.be.visible')
    cy
      .get(
        `[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"] .deleteIcon`,
      )
      .click()
    cy.get('[data-test-id="modal-confirm"]').click()
  })
})
