describe('Admin returns manuscript after EiC decision', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit('/')
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy
      .get('[data-test-id="respond-to-invitation-he-box"]')
      .should('be.visible')
      .should('contain', 'Accept')
      .should('contain', 'Decline')
      .should('contain', 'RESPOND TO INVITATION')
  })
  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { statuses, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('accept')
    cy.checkStatus(statuses.heAssigned.handlingEditor)
  })

  it('Invite reviewers as HE', function inviteReviewer() {
    const { statuses, reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()

    cy.inviteReviewer({ reviewer, he })
    cy.checkStatus(statuses.reviewersInvited.handlingEditor)
  })

  it('Give a review for publish as Reviewer', function submitPublishReview() {
    const { reviewer, he, statuses } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('accept')

    cy.checkStatus(statuses.underReview.reviewer)

    cy.submitReview('Publish')
    cy.wait(3000)
    // cy.checkStatus(statuses.reviewCompleted.reviewer) // Bug

    cy.loginApi(he.email, he.password)
    cy.visit('/')
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
    cy.checkStatus(statuses.reviewCompleted.handlingEditor)
  })

  it('HE makes recommendation to publish', function heMakesRecommendationToPublish() {
    const { he, eic, statuses } = this
    cy.loginApi(he.email, he.password)
    cy.wait(2000)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.wait(2000)
    cy.heMakesRecommendation('Publish')
    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.pendingApproval.editorInChief)
  })

  it('EiC makes decision to publish', function eicMakesDecisioToPublish() {
    const { eic, statuses } = this

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.checkStatus(statuses.pendingApproval.editorInChief)

    cy
      .get('[data-test-id="contextual-box-editorial-comments"]')
      .should('be.visible')
      .contains('Publish')

    cy.eicMakesDecision('Publish')
    cy.checkStatus(statuses.inQA.editorInChief)
  })

  it('Manuscript rejected by EQA', function adminRejectManuscript() {
    const { admin, eic } = this
    cy.loginApi(admin.username, admin.password)
    cy.visit('/')
    const id = Cypress.env('fragmentIdV1')
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.rejectManuscriptByEQA()
    cy.get('[data-test-id="journal-logo"]').click()
    cy.get(`[data-test-id="fragment-${id}"]`).click()
    cy
      .get(`[data-test-id="contextual-box-manuscript-eic-decision"]`)
      .contains('Your Editorial Decision')
      .click()
    cy
      .get('[data-test-id="form-eic-decision"]')
      .find('[role="listbox"]')
      .click()

    cy
      .get('[role="listbox"]')
      .should('contain', 'Return to Handling Editor')
      .should('contain', 'Publish')

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')
    cy.get(`[data-test-id="fragment-${id}"]`).click()
    cy
      .get(`[data-test-id="contextual-box-manuscript-eic-decision"]`)
      .contains('Your Editorial Decision')
      .click()
    cy
      .get(`[data-test-id="contextual-box-manuscript-eic-decision"]`)
      .find('[role="listbox"]')
      .click()
    cy
      .get('[role="listbox"]')
      .should('contain', 'Return to Handling Editor')
      .should('contain', 'Publish')
  })
})
