describe('HE Rejects manuscript while reviewers pending', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy
      .get('[data-test-id="respond-to-invitation-he-box"]')
      .should('be.visible')
      .should('contain', 'Accept')
      .should('contain', 'Decline')
      .should('contain', 'RESPOND TO INVITATION')
  })
  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { statuses, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('accept')
    cy.checkStatus(statuses.heAssigned.handlingEditor)
  })

  it('Invite reviewers as HE', function inviteReviewer() {
    const { statuses, reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()

    cy.inviteReviewer({ reviewer, he })
    cy.checkStatus(statuses.reviewersInvited.handlingEditor)
  })

  it('HE rejects manuscript while reviewers are pending', function HErejectsManuscriptReviewerPending() {
    const { he, eic, admin, statuses, author } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()

    cy.heMakesRecommendation('Reject').then(() => {
      cy.wait(3000)
      cy
        .get(`h3`)
        .contains('Your Editorial Recommendation')
        .should('not.be.visible')

      cy.loginApi(eic.email, eic.password)
      cy.visit('/')

      cy
        .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
        .should('be.visible')
        .click()
      cy.checkStatus(statuses.pendingApproval.editorInChief)
      cy
        .get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .should('contain', Cypress.env('heRecommendationText'))
        .should('contain', Cypress.env('heOptionalText'))

      cy.loginApi(admin.username, admin.password)
      cy.visit('/')

      cy
        .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
        .should('be.visible')
        .click()
      cy.checkStatus(statuses.pendingApproval.admin)
      cy
        .get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .should('contain', Cypress.env('heRecommendationText'))
        .should('contain', Cypress.env('heOptionalText'))

      cy.loginApi(author.email, author.password)
      cy.visit('/')

      cy
        .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
        .should('be.visible')
        .click()
      cy
        .get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('not.be.visible')
    })
  })

  it('EiC rejects the manuscript and Author sees response', function rejectManuscript() {
    const { eic, author, statuses } = this
    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.eicMakesDecision('Reject').then(() => {
      cy.checkStatus(statuses.rejected.editorInChief)

      cy.loginApi(author.email, author.password)
      cy.visit('/')

      cy.checkStatus(statuses.rejected.author)

      cy
        .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
        .should('be.visible')
        .click()

      cy
        .get(`h3`)
        .contains('Editorial Comments')
        .should('be.visible')
        .click()

      cy
        .get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .contains(Cypress.env('eicDecisionText'))
    })
  })
})
