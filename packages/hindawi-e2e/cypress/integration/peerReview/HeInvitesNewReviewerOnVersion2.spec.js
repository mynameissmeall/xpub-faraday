describe('He Invites New Reviewer on version 2', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit(`/`)

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy
      .get('[data-test-id="respond-to-invitation-he-box"]')
      .should('be.visible')
      .should('contain', 'Accept')
      .should('contain', 'Decline')
      .should('contain', 'RESPOND TO INVITATION')
  })
  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { statuses, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit(`/`)
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('accept')
    cy.checkStatus(statuses.heAssigned.handlingEditor)
  })

  it('Invite reviewers as HE', function inviteReviewer() {
    const { statuses, reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()

    cy.inviteReviewer({ reviewer, he })
    cy.checkStatus(statuses.reviewersInvited.handlingEditor)
  })

  it('Give a review for major revision as Reviewer', function majorRevisionReview() {
    const { reviewer, he, statuses } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('accept')

    cy.checkStatus(statuses.underReview.reviewer)

    cy.wait(3000)

    cy.submitReview('Major Revision')
    cy.wait(3000)
    // cy.checkStatus(statuses.reviewCompleted.reviewer) // Bug

    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
    cy.checkStatus(statuses.reviewCompleted.handlingEditor)
  })

  it('HE makes recommendation for major revision', function majorRevisionRecommendation() {
    const { he, eic, statuses } = this
    cy.loginApi(he.email, he.password)
    cy.visit('/')
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.heMakesRecommendation('Major Revision')

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.revisionRequested.editorInChief)
  })

  it('Author submit major revision', function majorRevisionSubmission() {
    const { author, statuses, updatedFragment } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.checkStatus(statuses.revisionRequested.author)
    cy.authorSubmitRevision({ updatedFragment })
    cy.visit('/').wait(2000)
    cy
      .get(`[data-test-id="dashboard-list-items"]`)
      .find(`[data-test-id="row"]`)
      .first()
      .click()
    cy.location().then(loc => {
      Cypress.env(
        'fragmentIdV2',
        loc.pathname
          .replace('/details', '')
          .split('/')
          .pop(),
      )
    })
  })

  it('He invites new reviewer on version 2 of the manuscript', function HeInvitesNewRevVersion2() {
    const { statuses, reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV2')}"]`).click()
    cy.checkStatus(statuses.underReview.handlingEditor)

    cy
      .get('[data-test-id="reviewer-details-and-reports-box"]')
      .should('be.visible')
      .click()

    cy.inviteReviewer({ reviewer, he })
    cy.checkStatus(statuses.underReview.handlingEditor)

    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV2')}"]`).click()
    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('accept')
    cy.checkStatus(statuses.underReview.reviewer)
    cy
      .get('[data-test-id="response-to-revision-request"]')
      .should('be.visible')
      .click()
      .should('contain', 'Author Reply')

    cy.get('[data-test-id="submit-report-box"]').should('be.visible')

    cy
      .get('[data-test-id="item-details-top"]')
      .find('[role="listbox"]')
      .click()
      .should('contain', 'Version 2')
      .should('not.contain', 'Version 1')
  })
})
