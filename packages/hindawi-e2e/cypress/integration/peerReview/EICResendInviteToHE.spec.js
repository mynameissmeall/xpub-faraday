describe('EiC revokes HE after HE accepted invitation', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
  })
  it('EIC should resend invite to HE', function resendHEInvite() {
    const { statuses, eic, he } = this
    cy.loginApi(eic.username, eic.password)
    cy.visit('/')
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
      .wait(2000)
    cy.resendHEInvite({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
  })
  it('EIC should resend invite to HE API', function resendHEInviteApi() {
    const { statuses, eic, he } = this
    cy.loginApi(eic.username, eic.password)
    cy.visit('/')

    cy.resendHEInviteApi({ he, role: 'handlingEditor' })
    cy.checkStatus(statuses.heInvited.editorInChief)
  })
})
