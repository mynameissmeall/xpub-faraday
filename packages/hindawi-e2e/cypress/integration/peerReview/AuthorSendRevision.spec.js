describe('Request Manuscript Revision EiC', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/eic').as('eic')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('EiC successfully request manuscript revision from author', function requestManuscriptRevision() {
    const { eic } = this
    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.eicMakesDecision('Request Revision')
  })

  it('Author submit revision', function authorSubmitRevision() {
    const { author, statuses, updatedFragment } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.checkStatus(statuses.revisionRequested.author)
    cy.authorSubmitRevision({ updatedFragment })
    cy.visit('/').wait(2000)
    cy
      .get(`[data-test-id="dashboard-list-items"]`)
      .find(`[data-test-id="row"]`)
      .first()
      .click()
    cy.location().then(loc => {
      Cypress.env(
        'fragmentIdV2',
        loc.pathname
          .replace('/details', '')
          .split('/')
          .pop(),
      )
    })
    cy
      .get(`[data-test-id="response-to-revision-request"]`)
      .should('be.visible')
      .click()
    cy
      .get(`[data-test-id="response-to-revision-request"]`)
      .contains('Author Reply')
    cy.get(`[data-test-id="response-to-revision-request"]`).contains('File')
    cy.checkStatus(statuses.heInvited.author)
    cy.get('[data-test-id="author-reply"]')
  })
})
