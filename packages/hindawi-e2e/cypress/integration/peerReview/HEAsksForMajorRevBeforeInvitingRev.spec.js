describe('HE asks for major revision before inviting reviewers', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy
      .get('[data-test-id="respond-to-invitation-he-box"]')
      .should('be.visible')
      .should('contain', 'Accept')
      .should('contain', 'Decline')
      .should('contain', 'RESPOND TO INVITATION')
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { statuses, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('accept')
    cy.checkStatus(statuses.heAssigned.handlingEditor)
  })

  it('HE asks for major revision before inviting reviewers', function heAsksForMajRevBeforeInviteingRev() {
    const { he, statuses, author } = this

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heAssigned.handlingEditor)
    cy
      .get('[data-test-id="reviewer-details-and-reports-box"]')
      .should('be.visible')
      .contains('No reviewers invited yet.')
    cy.heMakesRecommendation('Request Major Revision').then(() => {
      cy.checkStatus(statuses.revisionRequested.handlingEditor)
      cy
        .get(`[data-test-id="contextual-box-he-recommendation"]`)
        .should('be.visible')
        .click()
      cy
        .get('[data-test-id="contextual-box-he-recommendation"]')
        .find('[role="listbox"]')
        .click()
      cy
        .get('[data-test-id="contextual-box-he-recommendation"]')
        .find('[role="option"]')
        .should('contain', 'Reject')
        .should('not.contain', 'Publish')
        .should('not.contain', 'Request Major Revision')
        .should('not.contain', 'Request Minor Revision')

      cy.loginApi(author.email, author.password)
      cy.visit('/')

      cy.checkStatus(statuses.revisionRequested.author)
      cy
        .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
        .should('be.visible')
        .click()
      cy
        .get('[data-test-id="contextual-box-editorial-comments"]')
        .should('be.visible')
        .should('contain', 'Major Revision')
        .contains(Cypress.env('heRecommendationText'))
    })

    cy
      .get('[data-test-id="submit-revision"]')
      .should('be.visible')
      .contains('Submit Revision')
  })
})
