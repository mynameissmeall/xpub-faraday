describe('Admin edits during EQA', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he } = this
    cy.inviteHE({ eic, he })
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { he } = this

    cy.loginApi(he.username, he.password)
    cy.visit('/')
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsHE('accept')
  })

  it('Invite reviewers as HE', function inviteReviewer() {
    const { reviewer, he } = this

    cy.loginApi(he.username, he.password)
    cy.visit('/')
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.inviteReviewer({ reviewer, he })
  })

  it('Give a review for publish as Reviewer', function minorRevisionReview() {
    const { reviewer } = this

    cy.visit('/')
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsReviewer('accept')
    cy.submitReview('Publish')
    cy.wait(3000)
  })

  it('HE makes recommendation to publish', function heMakesRecommendationToPublish() {
    const { he } = this

    cy.loginApi(he.email, he.password)
    cy.visit('/')
    cy.wait(2000)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.wait(2000)
    cy.heMakesRecommendation('Publish')
  })

  it('EiC makes decision to publish', function eicMakesDecisioToPublish() {
    const { eic } = this

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.eicMakesDecision('Publish')
  })

  it('Admin edit manuscript during EQA', function approveManuscriptByEQA() {
    const { admin, fragment, statuses } = this

    const id = Cypress.env('fragmentIdV1')
    cy.loginApi(admin.username, admin.password)
    cy.visit('/')
    cy.get(`[data-test-id="fragment-${id}"]`).click()
    cy
      .get(`[data-test-id="button-qa-manuscript-edit"]`)
      .should('be.visible')
      .click()

    cy.wait(2000)
    cy.editManuscript(fragment)
    cy.get('[data-test-id="submission-next"]').click()
    cy.wait(2000)
    cy
      .get('[data-test-id="manuscript-title"]')
      .scrollIntoView()
      .contains('Fragment 2 - modified')

    cy.checkStatus(statuses.inQA.admin)
  })
})
