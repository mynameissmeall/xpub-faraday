const config = require('config')

const journalName = config.get('journal.name')
const getEmailCopy = ({ emailType, role }) => {
  let paragraph
  switch (emailType) {
    case 'user-signup':
      paragraph = `Thank you for creating an account on Hindawi’s review system.
        To submit a manuscript and access your dashboard, please confirm your account by clicking on the link below.`
      break
    case 'user-added-by-admin':
      paragraph = `You have been invited to join Hindawi as a ${role}.
        Please confirm your account and set your account details by clicking on the link below.`
      break
    case 'he-added-by-admin':
      paragraph = `You have been invited to become an Academic Editor for the journal ${journalName}.
        To begin performing your editorial duties, you will need to create an account on Hindawi’s review system.<br/><br/>
        Please confirm your account details by clicking on the link below.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return { paragraph, hasLink: true, hasIntro: true, hasSignature: true }
}

module.exports = {
  getEmailCopy,
}
