const logger = require('@pubsweet/logger')

const { services } = require('pubsweet-component-helper-service')

const notifications = require('./notifications')

module.exports = models => async (req, res) => {
  const { email, type, role = 'author' } = req.body
  if (!services.checkForUndefinedParams(email, type, role)) {
    res.status(400).json({ error: 'Email and type are required.' })
    logger.error('User ID and role are missing')
    return
  }

  if (!['signup', 'invite'].includes(type)) {
    return res.status(400).json({ error: `Email type ${type} is not defined.` })
  }

  let user
  try {
    user = await models.User.findByEmail(email)
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'User')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }

  if (type === 'signup') {
    if (!user.accessTokens.confirmation) {
      return res
        .status(400)
        .json({ error: 'User does not have a confirmation token.' })
    }
    notifications.sendSignupEmail({ user, baseUrl: services.getBaseUrl(req) })
  } else if (type === 'invite') {
    let emailRole
    switch (role) {
      case 'handlingEditor':
        emailRole = 'Handling Editor'
        break
      case 'editorInChief':
        emailRole = 'Editor in Chief'
        break
      case 'admin':
        emailRole = 'Administrator'
        break
      case 'author':
        emailRole = 'Author'
        break
      default:
        return res.status(400).json({
          error: `Role ${role} is not defined.`,
        })
    }
    notifications.sendNewUserEmail({
      user,
      baseUrl: services.getBaseUrl(req),
      role: emailRole,
    })
  }

  return res.status(200).json({})
}
