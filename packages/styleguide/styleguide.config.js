const path = require('path')

module.exports = {
  sections: [
    {
      name: 'Hindawi UI',
      sectionDepth: 1,
      components: ['../component-faraday-ui/src/[A-Z]*.js'],
    },
    {
      name: 'Modals',
      sectionDepth: 1,
      components: ['../component-faraday-ui/src/modals/[A-Z]*.js'],
    },
    {
      name: 'Manuscript Details',
      sectionDepth: 1,
      components: ['../component-faraday-ui/src/manuscriptDetails/[A-Z]*.js'],
    },
    {
      name: 'Contextual Boxes',
      sectionDepth: 1,
      components: ['../component-faraday-ui/src/contextualBoxes/[A-Z]*.js'],
    },
    {
      name: 'Submit Revision',
      sectionDepth: 1,
      components: ['../component-faraday-ui/src/submissionRevision/[A-Z]*.js'],
    },
    {
      name: 'Pending Items',
      sectionDepth: 1,
      components: ['../component-faraday-ui/src/pending/[A-Z]*.js'],
    },
    {
      name: 'Grid Items',
      sectionDepth: 1,
      components: ['../component-faraday-ui/src/gridItems/[A-Z]*.js'],
    },
  ],
  serverPort: process.env.NODE_ENV !== 'production' ? 6060 : 3000,
  skipComponentsWithoutExample: true,
  pagePerSection: true,
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'src/Wrapper'),
  },
}
