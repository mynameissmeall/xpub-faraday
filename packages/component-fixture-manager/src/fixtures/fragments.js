const Chance = require('chance')
const {
  submittingAuthor,
  reviewer,
  answerReviewer,
  recReviewer,
  handlingEditor,
  admin,
  inactiveReviewer,
  reviewer1,
  newReviewer,
} = require('./userData')
const {
  standardCollID,
  collectionReviewCompletedID,
  collectionNoInvitesID,
  noEditorRecomedationCollectionID,
} = require('./collectionIDs')
const { user } = require('./userData')

const chance = new Chance()
const fragments = {
  fragment: {
    id: chance.guid(),
    delete: jest.fn(),
    files: {
      manuscripts: [{ id: chance.guid() }],
      coverLetter: [],
      supplementary: [],
    },
    collectionId: standardCollID,
    metadata: {
      title: chance.sentence(),
      abstract: chance.paragraph(),
    },
    recommendations: [
      {
        recommendation: 'publish',
        recommendationType: 'review',
        comments: [
          {
            content: chance.paragraph(),
            public: chance.bool(),
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
          },
        ],
        id: chance.guid(),
        userId: answerReviewer.id,
        createdOn: chance.timestamp(),
        updatedOn: chance.timestamp(),
        submittedOn: chance.timestamp(),
      },
      {
        recommendation: 'reject',
        recommendationType: 'review',
        comments: [
          {
            content: chance.paragraph(),
            public: chance.bool(),
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
          },
        ],
        id: chance.guid(),
        userId: recReviewer.id,
        createdOn: chance.timestamp(),
        updatedOn: chance.timestamp(),
        submittedOn: chance.timestamp(),
      },
      {
        recommendation: 'minor',
        recommendationType: 'editorRecommendation',
        comments: [
          {
            content: chance.paragraph(),
            public: true,
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
          },
        ],
        id: chance.guid(),
        userId: handlingEditor.id,
        createdOn: chance.timestamp(),
        updatedOn: chance.timestamp(),
      },
      {
        recommendation: 'publish',
        recommendationType: 'editorRecommendation',
        comments: [
          {
            content: chance.paragraph(),
            public: true,
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
          },
        ],
        id: chance.guid(),
        userId: handlingEditor.id,
        createdOn: 1542361074012,
        updatedOn: chance.timestamp(),
      },
      {
        recommendation: 'return-to-handling-editor',
        recommendationType: 'editorRecommendation',
        comments: [
          {
            content: chance.paragraph(),
            public: true,
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
          },
        ],
        id: chance.guid(),
        userId: admin.id,
        createdOn: 1542361115749,
        updatedOn: chance.timestamp(),
      },
      {
        recommendation: 'publish',
        recommendationType: 'editorRecommendation',
        comments: [
          {
            content: chance.paragraph(),
            public: chance.bool(),
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
          },
        ],
        id: chance.guid(),
        userId: handlingEditor.id,
        createdOn: 1542361115750,
        updatedOn: chance.timestamp(),
      },
      {
        recommendation: 'publish',
        recommendationType: 'editorRecommendation',
        comments: [
          {
            content: chance.paragraph(),
            public: chance.bool(),
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
          },
        ],
        id: chance.guid(),
        userId: admin.id,
        createdOn: 1542361115751,
        updatedOn: chance.timestamp(),
      },
      {
        recommendation: 'revision',
        recommendationType: 'editorRecommendation',
        comments: [
          {
            content: chance.paragraph(),
            public: true,
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
          },
        ],
        id: chance.guid(),
        userId: admin.id,
        createdOn: chance.timestamp(),
        updatedOn: chance.timestamp(),
      },
    ],
    authors: [
      {
        email: chance.email(),
        id: submittingAuthor.id,
        isSubmitting: true,
        isCorresponding: false,
      },
      {
        email: inactiveReviewer.email,
        id: inactiveReviewer.id,
        isSubmitting: false,
        isCorresponding: false,
      },
    ],
    invitations: [
      {
        id: chance.guid(),
        role: 'reviewer',
        hasAnswer: false,
        isAccepted: false,
        userId: reviewer.id,
        invitedOn: chance.timestamp(),
        respondedOn: null,
        type: 'invitation',
      },
      {
        id: chance.guid(),
        role: 'reviewer',
        hasAnswer: true,
        isAccepted: true,
        userId: answerReviewer.id,
        invitedOn: chance.timestamp(),
        respondedOn: chance.timestamp(),
        type: 'invitation',
      },
      {
        id: chance.guid(),
        role: 'reviewer',
        hasAnswer: false,
        isAccepted: false,
        userId: inactiveReviewer.id,
        invitedOn: chance.timestamp(),
        respondedOn: chance.timestamp(),
        type: 'invitation',
      },
      {
        id: chance.guid(),
        role: 'reviewer',
        hasAnswer: true,
        isAccepted: true,
        userId: recReviewer.id,
        invitedOn: chance.timestamp(),
        respondedOn: chance.timestamp(),
        type: 'invitation',
      },
    ],
    save: jest.fn(() => fragments.fragment),
    owners: [user.id],
    type: 'fragment',
    revision: {
      collectionId: standardCollID,
      metadata: {
        title: chance.sentence(),
        abstract: chance.paragraph(),
      },
      authors: [
        {
          email: chance.email(),
          id: submittingAuthor.id,
          isSubmitting: true,
          isCorresponding: false,
        },
      ],
      owners: [user.id],
      type: 'fragment',
    },
  },
  noParentFragment: {
    id: chance.guid(),
    delete: jest.fn(),
    files: {
      manuscripts: [{ id: chance.guid() }],
      coverLetter: [],
      supplementary: [],
    },
    collectionId: '',
    metadata: {
      title: chance.sentence(),
      abstract: chance.paragraph(),
    },
    authors: [
      {
        email: chance.email(),
        id: submittingAuthor.id,
        isSubmitting: true,
        isCorresponding: false,
      },
    ],
    save: jest.fn(() => fragments.fragment),
    owners: [user.id],
    type: 'fragment',
  },
  reviewCompletedFragment: {
    id: chance.guid(),
    delete: jest.fn(),
    type: 'fragment',
    files: {
      manuscripts: [
        {
          id:
            '28ca8f73-2dcb-4a93-97c9-24d474d54691/a1107517-1d19-445d-90ee-315a026469c4',
          name: 'LinkedInProfileDemystified.pdf',
          size: 804871,
          originalName: 'LinkedInProfileDemystified.pdf',
        },
      ],
      coverLetter: [],
      supplementary: [],
    },
    owners: [user.id],
    authors: [
      {
        id: user.id,
        email: user.email,
        title: user.title,
        lastName: user.lastName,
        firstName: user.firstName,
        affiliation: user.affiliation,
        isSubmitting: true,
        isCorresponding: true,
      },
    ],
    created: '2018-10-08T12:01:46.384Z',
    version: 1,
    metadata: {
      type: 'research',
      title: chance.sentence(),
      journal: 'Bioinorganic Chemistry and Applications',
      abstract: chance.paragraph(),
    },
    conflicts: {
      hasFunding: 'yes',
      hasConflicts: 'no',
      hasDataAvailability: 'yes',
    },
    submitted: chance.timestamp(),
    invitations: [
      {
        id: chance.guid(),
        role: 'reviewer',
        type: 'invitation',
        userId: answerReviewer.id,
        hasAnswer: true,
        invitedOn: chance.timestamp(),
        isAccepted: true,
        respondedOn: chance.timestamp(),
        reviewerNumber: 2,
      },
      {
        id: chance.guid(),
        role: 'reviewer',
        type: 'invitation',
        userId: recReviewer.id,
        hasAnswer: false,
        invitedOn: chance.timestamp(),
        isAccepted: false,
        respondedOn: null,
      },
      {
        id: chance.guid(),
        role: 'reviewer',
        type: 'invitation',
        userId: reviewer.id,
        hasAnswer: true,
        invitedOn: chance.timestamp(),
        isAccepted: true,
        respondedOn: chance.timestamp(),
      },
    ],
    collectionId: collectionReviewCompletedID,
    declarations: {
      agree: true,
    },
    fragmentType: 'version',
    recommendations: [
      {
        id: chance.guid(),
        userId: answerReviewer.id,
        comments: [
          {
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
            public: true,
            content: 'A not so nice manuscript',
          },
        ],
        createdOn: chance.timestamp(),
        updatedOn: chance.timestamp(),
        submittedOn: chance.timestamp(),
        recommendation: 'publish',
        recommendationType: 'review',
      },
    ],
    save: jest.fn(() => fragments.fragment),
  },
  fragment1: {
    id: chance.guid(),
    collectionId: collectionNoInvitesID,
    metadata: {
      title: chance.sentence(),
      abstract: chance.paragraph(),
    },
    recommendations: [
      {
        recommendation: 'publish',
        recommendationType: 'review',
        comments: [
          {
            content: chance.paragraph(),
            public: chance.bool(),
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
          },
        ],
        id: chance.guid(),
        userId: reviewer1.id,
        createdOn: chance.timestamp(),
        updatedOn: chance.timestamp(),
        submittedOn: chance.timestamp(),
      },
      {
        recommendation: 'minor',
        recommendationType: 'editorRecommendation',
        comments: [
          {
            content: chance.paragraph(),
            public: true,
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
          },
        ],
        id: chance.guid(),
        userId: handlingEditor.id,
        createdOn: chance.timestamp(),
        updatedOn: chance.timestamp(),
      },
    ],
    authors: [
      {
        email: chance.email(),
        id: submittingAuthor.id,
        isSubmitting: true,
        isCorresponding: false,
      },
      {
        email: inactiveReviewer.email,
        id: inactiveReviewer.id,
        isSubmitting: false,
        isCorresponding: false,
      },
    ],
    invitations: [
      {
        id: chance.guid(),
        role: 'reviewer',
        hasAnswer: true,
        isAccepted: false,
        userId: reviewer1.id,
        invitedOn: chance.timestamp(),
        respondedOn: chance.timestamp(),
        type: 'invitation',
      },
    ],
    save: jest.fn(() => fragments.fragment),
    owners: [user.id],
    type: 'fragment',
    revision: {
      collectionId: collectionNoInvitesID,
      metadata: {
        title: chance.sentence(),
        abstract: chance.paragraph(),
      },
      authors: [
        {
          email: chance.email(),
          id: submittingAuthor.id,
          isSubmitting: true,
          isCorresponding: false,
        },
      ],
    },
  },
  noEditorRecomedationFragment: {
    id: chance.guid(),
    collectionId: noEditorRecomedationCollectionID,
    metadata: {
      title: chance.sentence(),
      abstract: chance.paragraph(),
    },
    recommendations: [
      {
        recommendation: 'publish',
        recommendationType: 'review',
        comments: [
          {
            content: chance.paragraph(),
            public: chance.bool(),
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
          },
        ],
        id: chance.guid(),
        userId: newReviewer.id,
        createdOn: chance.timestamp(),
        updatedOn: chance.timestamp(),
        submittedOn: chance.timestamp(),
      },
      {
        recommendation: 'publish',
        recommendationType: 'editorRecommendation',
        comments: [
          {
            content: chance.paragraph(),
            public: true,
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
          },
        ],
        id: chance.guid(),
        userId: handlingEditor.id,
        createdOn: 1542361074012,
        updatedOn: chance.timestamp(),
      },
      {
        recommendation: 'return-to-handling-editor',
        recommendationType: 'editorRecommendation',
        comments: [
          {
            content: chance.paragraph(),
            public: true,
            files: [
              {
                id: chance.guid(),
                name: 'file.pdf',
                size: chance.natural(),
              },
            ],
          },
        ],
        id: chance.guid(),
        userId: admin.id,
        createdOn: 1542361115749,
        updatedOn: chance.timestamp(),
      },
    ],
    authors: [
      {
        email: chance.email(),
        id: submittingAuthor.id,
        isSubmitting: true,
        isCorresponding: false,
      },
    ],
    invitations: [
      {
        id: chance.guid(),
        role: 'reviewer',
        hasAnswer: true,
        isAccepted: true,
        userId: newReviewer.id,
        invitedOn: chance.timestamp(),
        respondedOn: chance.timestamp(),
        type: 'invitation',
      },
    ],
    save: jest.fn(() => fragments.fragment),
    owners: [user.id],
    type: 'fragment',
  },
}

fragments.noInvitesFragment = {
  ...fragments.fragment1,
  recommendations: [],
  invites: [],
  id: chance.guid(),
}
fragments.noInvitesFragment1 = {
  ...fragments.fragment,
  recommendations: [],
  id: chance.guid(),
}
fragments.minorRevisionWithoutReview = {
  ...fragments.fragment,
  recommendations: [
    {
      recommendation: 'minor',
      recommendationType: 'editorRecommendation',
      comments: [
        {
          content: chance.paragraph(),
          public: true,
          files: [
            {
              id: chance.guid(),
              name: 'file.pdf',
              size: chance.natural(),
            },
          ],
        },
      ],
      id: chance.guid(),
      userId: handlingEditor.id,
      createdOn: chance.timestamp(),
      updatedOn: chance.timestamp(),
    },
  ],
  id: chance.guid(),
}
fragments.minorRevisionWithReview = {
  ...fragments.fragment,
  recommendations: [
    {
      recommendation: 'minor',
      recommendationType: 'editorRecommendation',
      comments: [
        {
          content: chance.paragraph(),
          public: true,
          files: [
            {
              id: chance.guid(),
              name: 'file.pdf',
              size: chance.natural(),
            },
          ],
        },
      ],
      id: chance.guid(),
      userId: handlingEditor.id,
      createdOn: chance.timestamp(),
      updatedOn: chance.timestamp(),
    },
    {
      recommendation: 'publish',
      recommendationType: 'review',
      comments: [
        {
          content: chance.paragraph(),
          public: chance.bool(),
          files: [
            {
              id: chance.guid(),
              name: 'file.pdf',
              size: chance.natural(),
            },
          ],
        },
      ],
      id: chance.guid(),
      userId: reviewer1.id,
      createdOn: chance.timestamp(),
      updatedOn: chance.timestamp(),
      submittedOn: chance.timestamp(),
    },
  ],
  id: chance.guid(),
}
fragments.majorRevisionWithReview = {
  ...fragments.fragment,
  recommendations: [
    {
      recommendation: 'major',
      recommendationType: 'editorRecommendation',
      comments: [
        {
          content: chance.paragraph(),
          public: true,
          files: [
            {
              id: chance.guid(),
              name: 'file.pdf',
              size: chance.natural(),
            },
          ],
        },
      ],
      id: chance.guid(),
      userId: handlingEditor.id,
      createdOn: chance.timestamp(),
      updatedOn: chance.timestamp(),
    },
    {
      recommendation: 'publish',
      recommendationType: 'review',
      comments: [
        {
          content: chance.paragraph(),
          public: chance.bool(),
          files: [
            {
              id: chance.guid(),
              name: 'file.pdf',
              size: chance.natural(),
            },
          ],
        },
      ],
      id: chance.guid(),
      userId: reviewer1.id,
      createdOn: chance.timestamp(),
      updatedOn: chance.timestamp(),
      submittedOn: chance.timestamp(),
    },
  ],
  id: chance.guid(),
}

module.exports = fragments
