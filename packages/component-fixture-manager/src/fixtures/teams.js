const users = require('./users')
const collections = require('./collections')
const fragments = require('./fragments')

const {
  heTeamID,
  revTeamID,
  rev1TeamID,
  authorTeamID,
  majorRevisionHeTeamID,
  revRecommendationTeamID,
  heNoRecommendationTeamID,
  revNoEditorRecommendationTeamID,
} = require('./teamIDs')
const { submittingAuthor } = require('./userData')

const {
  collection,
  majorRevisionCollection,
  noEditorRecomedationCollection,
} = collections

const {
  fragment,
  fragment1,
  reviewCompletedFragment,
  noEditorRecomedationFragment,
} = fragments

const {
  handlingEditor,
  reviewer,
  inactiveReviewer,
  answerReviewer,
  recReviewer,
  reviewer1,
  newReviewer,
  noRecommendationHE,
} = users
const teams = {
  heTeam: {
    teamType: {
      name: 'handlingEditor',
      permissions: 'handlingEditor',
    },
    group: 'handlingEditor',
    name: 'HandlingEditor',
    object: {
      type: 'collection',
      id: collection.id,
    },
    members: [handlingEditor.id],
    save: jest.fn(() => teams.heTeam),
    delete: jest.fn(),
    updateProperties: jest.fn(() => teams.heTeam),
    id: heTeamID,
  },
  heNoRecommendationTeam: {
    teamType: {
      name: 'handlingEditor',
      permissions: 'handlingEditor',
    },
    group: 'handlingEditor',
    name: 'HandlingEditor',
    object: {
      type: 'collection',
      id: noEditorRecomedationCollection.id,
    },
    members: [noRecommendationHE.id],
    save: jest.fn(() => teams.heNoRecommendationTeam),
    delete: jest.fn(),
    updateProperties: jest.fn(() => teams.heNoRecommendationTeam),
    id: heNoRecommendationTeamID,
  },
  revTeam: {
    teamType: {
      name: 'reviewer',
      permissions: 'reviewer',
    },
    group: 'reviewer',
    name: 'reviewer',
    object: {
      type: 'fragment',
      id: fragment.id,
    },
    members: [reviewer.id, inactiveReviewer.id, answerReviewer.id],
    save: jest.fn(() => teams.revTeam),
    delete: jest.fn(),
    updateProperties: jest.fn(() => teams.revTeam),
    id: revTeamID,
  },
  revRecommendationTeam: {
    teamType: {
      name: 'reviewer',
      permissions: 'reviewer',
    },
    group: 'reviewer',
    name: 'reviewer',
    object: {
      type: 'fragment',
      id: reviewCompletedFragment.id,
    },
    members: [reviewer.id, answerReviewer.id, recReviewer.id],
    save: jest.fn(() => teams.revRecommendationTeam),
    delete: jest.fn(),
    updateProperties: jest.fn(() => teams.revRecommendationTeam),
    id: revRecommendationTeamID,
  },
  rev1Team: {
    teamType: {
      name: 'reviewer',
      permissions: 'reviewer',
    },
    group: 'reviewer',
    name: 'reviewer',
    object: {
      type: 'fragment',
      id: fragment1.id,
    },
    members: [reviewer1.id],
    save: jest.fn(() => teams.rev1Team),
    updateProperties: jest.fn(() => teams.rev1Team),
    id: rev1TeamID,
  },
  authorTeam: {
    teamType: {
      name: 'author',
      permissions: 'author',
    },
    group: 'author',
    name: 'author',
    object: {
      type: 'fragment',
      id: fragment.id,
    },
    members: [submittingAuthor.id],
    save: jest.fn(() => teams.authorTeam),
    delete: jest.fn(),
    updateProperties: jest.fn(() => teams.authorTeam),
    id: authorTeamID,
  },
  revNoEditorRecommendationTeam: {
    teamType: {
      name: 'reviewer',
      permissions: 'reviewer',
    },
    group: 'reviewer',
    name: 'reviewer',
    object: {
      type: 'fragment',
      id: noEditorRecomedationFragment.id,
    },
    members: [newReviewer.id],
    save: jest.fn(() => teams.revNoEditorRecommendationTeam),
    updateProperties: jest.fn(() => teams.revNoEditorRecommendationTeam),
    id: revNoEditorRecommendationTeamID,
  },
  majorRevisionHeTeam: {
    teamType: {
      name: 'handlingEditor',
      permissions: 'handlingEditor',
    },
    group: 'handlingEditor',
    name: 'HandlingEditor',
    object: {
      type: 'collection',
      id: majorRevisionCollection.id,
    },
    members: [handlingEditor.id],
    save: jest.fn(() => teams.majorRevisionHeTeam),
    delete: jest.fn(),
    updateProperties: jest.fn(() => teams.majorRevisionHeTeam),
    id: majorRevisionHeTeamID,
  },
}
module.exports = teams
