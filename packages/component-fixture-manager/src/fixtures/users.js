const Chance = require('chance')
const usersData = require('./userData')

const chance = new Chance()
const {
  heTeamID,
  revTeamID,
  rev1TeamID,
  authorTeamID,
  majorRevisionHeTeamID,
  revRecommendationTeamID,
  heNoRecommendationTeamID,
  revNoEditorRecommendationTeamID,
} = require('./teamIDs')

const keys = Object.keys(usersData)
let users = {}
users = keys.reduce((obj, item) => {
  const userData = usersData[item]
  const isHE = item === 'answerHE' || item === 'handlingEditor'
  let teams = []

  switch (item) {
    case 'answerHE':
      teams = [heTeamID]
      break
    case 'handlingEditor':
      teams = [heTeamID, majorRevisionHeTeamID]
      break
    case 'noRecommendationHE':
      teams = [heNoRecommendationTeamID]
      break
    case 'author':
      teams = [authorTeamID]
      break
    case 'reviewer1':
      teams = [rev1TeamID]
      break
    case 'newReviewer':
      teams = [revNoEditorRecommendationTeamID]
      break
    case 'reviewer':
    case 'answerReviewer':
      teams = [revTeamID, revRecommendationTeamID]
      break
    case 'inactiveReviewer':
      teams.push(revTeamID)
      break
    case 'recReviewer':
      teams.push(revRecommendationTeamID)
      break
    default:
      teams = []
  }

  obj[item] = {
    ...userData,
    teams,
    type: 'user',
    username: item,
    password: 'password',
    handlingEditor: isHE,
    token: chance.hash(),
    admin: item === 'admin',
    affiliation: chance.company(),
    isConfirmed: item === 'author',
    isActive: !['inactiveUser', 'inactiveReviewer'].includes(item),
    editorInChief: item === 'editorInChief',
    updateProperties: jest.fn(() => users[item]),
    passwordResetTimestamp: item === 'author' ? Date.now() : undefined,
    save: jest.fn(function save() {
      return this
    }),
    notifications: {
      email: {
        user: true,
        system: true,
      },
    },
    accessTokens: {
      confirmation: chance.hash(),
      unsubscribe: chance.hash(),
      invitation: item === 'reviewer' ? chance.hash() : undefined,
      passwordReset: item === 'user' ? chance.hash() : undefined,
    },
    validPassword: jest.fn(function validPassword(password) {
      return this.password === password
    }),
  }

  return obj
}, {})

module.exports = users
