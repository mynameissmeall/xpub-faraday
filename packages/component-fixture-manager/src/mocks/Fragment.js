/* eslint-disable func-names-any */

function Fragment(properties) {
  this.type = 'fragment'
  this.id = properties.id
  this.collectionId = properties.collectionId
  this.metadata = properties.metadata
  this.recommendations = properties.recommendations
  this.authors = properties.authors
  this.invitations = properties.invitations
  this.owners = properties.owners
  this.revision = properties.revision
}

Fragment.prototype.save = jest.fn(function saveFragment() {
  this.id = '111222'
  return Promise.resolve(this)
})

module.exports = Fragment
