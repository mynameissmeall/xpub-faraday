const { get } = require('lodash')

const PackageManager = require('./PackageManager')

const {
  defaultConfig,
  defaultS3Config,
  defaultFTPConfig,
  defaultParseXmlOptions,
} = require('../config/default')

const { convertToXML, composeJson } = require('./helpers')

module.exports = {
  sendPackage({
    fragment = {},
    isEQA = false,
    fragmentUsers = [],
    config = defaultConfig,
    s3Config = defaultS3Config,
    ftpConfig = defaultFTPConfig,
    options = defaultParseXmlOptions,
  }) {
    const composedJson = composeJson({
      isEQA,
      config,
      options,
      fragment,
      fragmentUsers,
    })
    const xmlFile = convertToXML({
      options,
      json: composedJson,
      prefix: config.prefix,
    })

    return PackageManager.createFilesPackage(s3Config)({
      fragment,
      xmlFile,
      isEQA,
    })
      .then(() => {
        const packageName = get(xmlFile, 'name', '').replace('.xml', '')
        const filename = isEQA
          ? `ACCEPTED_${packageName}.${fragment.version}.zip`
          : `${packageName}.zip`

        return PackageManager.uploadFiles({
          filename,
          s3Config,
          config: ftpConfig,
        })
      })
      .catch(e => {
        throw new Error(e)
      })
  },
}
