const convert = require('xml-js')
const { get } = require('lodash')

const { getJsonTemplate } = require('./getJsonTemplate')

const {
  setFiles,
  setHistory,
  setMetadata,
  setQuestions,
  setReviewers,
  createFileName,
  setContributors,
} = require('./templateSetters')

module.exports = {
  convertToXML: ({ json = {}, options, prefix }) => {
    const content = convert.json2xml(json, options)
    const customId = get(
      json,
      'article.front.article-meta.article-id[0]._text',
      createFileName({ prefix }),
    )
    const name = `${customId}.xml`
    return {
      name,
      content,
    }
  },
  composeJson: ({
    config,
    options,
    isEQA = false,
    fragment = {},
    fragmentUsers = [],
  }) => {
    const {
      authors = [],
      files = [],
      metadata = { title: 'untitled', abstract: '' },
      submitted = new Date(),
      conflicts = {},
    } = fragment

    const jsonTemplate = getJsonTemplate(config)
    const fileName = createFileName({
      id: metadata.customId,
      prefix: config.prefix,
    })

    let composedJson = {
      ...jsonTemplate,
      ...setMetadata({
        options,
        metadata,
        fileName,
        jsonTemplate,
      }),
      ...setContributors(authors, jsonTemplate),
      ...setHistory(submitted, jsonTemplate),
      ...setFiles(files, jsonTemplate),
      ...setQuestions(conflicts, jsonTemplate),
    }

    if (isEQA) {
      composedJson = setReviewers(fragmentUsers, jsonTemplate)
    }

    return composedJson
  },
}
