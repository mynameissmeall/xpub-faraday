const config = require('../config/default')

const fragmentWithRecommendations = {
  type: 'fragment',
  files: {
    coverLetter: [],
    manuscripts: [
      {
        id:
          '9de6c313-1c3e-49eb-bc76-b5d2d045b271/f9916cb6-97a4-4d4c-8a2a-a89b0d10bbdf',
        name: 'scimakelatex.2898.adrian+onofrei.pdf',
        size: 76051,
        originalName: 'scimakelatex.2898.adrian+onofrei.pdf',
      },
    ],
    supplementary: [],
  },
  owners: ['34b0f0be-ea6a-4f5c-81b2-8eca299bdd51'],
  authors: [
    {
      id: '34b0f0be-ea6a-4f5c-81b2-8eca299bdd51',
      email: 'adrian.onofrei+auth1@thinslices.com',
      title: 'mr',
      country: 'AL',
      lastName: 'Adi',
      firstName: 'Auth',
      affiliation: 'TS',
      isSubmitting: true,
      isCorresponding: true,
    },
  ],
  created: '2018-10-18T11:39:40.722Z',
  version: 1,
  metadata: {
    customId: '9999999',
    type: 'research',
    title: 'Approve by he',
    journal: 'Bioinorganic Chemistry and Applications',
    abstract: 'test',
  },
  conflicts: {
    hasFunding: '',
    hasConflicts: 'no',
    hasDataAvailability: '',
  },
  submitted: 1539862801961,
  invitations: [
    {
      id: '625d7cef-201a-429a-8a91-97404e5e2f31',
      role: 'reviewer',
      type: 'invitation',
      userId: '41ccfd11-9ff8-49b7-a513-58bfede153e8',
      hasAnswer: true,
      invitedOn: 1539863340777,
      isAccepted: true,
      respondedOn: 1539863456467,
    },
  ],
  collectionId: '9c9d9cf1-179f-4f7a-87fa-6e70cffbd3f3',
  declarations: {
    agree: true,
  },
  fragmentType: 'version',
  recommendations: [
    {
      id: '9e77f9a8-ad88-4996-ae02-7bd2d7b42a00',
      userId: '41ccfd11-9ff8-49b7-a513-58bfede153e8',
      comments: [
        {
          files: [],
          public: true,
          content: 'revision pls - rev report',
        },
        {
          files: [],
          public: false,
          content: 'Rev confidential note for ET',
        },
      ],
      createdOn: 1539863473823,
      updatedOn: 1539863502691,
      submittedOn: 1539863502506,
      recommendation: 'major',
      recommendationType: 'review',
    },
    {
      id: 'eb4abe60-6484-40e2-b31e-4bf1f1592278',
      userId: '46841a2c-0b37-47c0-8847-9cedcaeeb082',
      comments: [
        {
          files: [],
          public: true,
          content: 'Author publish message',
        },
        {
          files: [],
          public: false,
          content: 'EiC publish message',
        },
      ],
      createdOn: 1539863568158,
      updatedOn: 1539863568158,
      recommendation: 'publish',
      recommendationType: 'editorRecommendation',
    },
    {
      id: '6e8cf67c-a969-48d3-9d90-52d02975a65a',
      userId: 'f12e0a14-4b6b-4d22-a506-d03c0ac7562f',
      comments: [
        {
          public: false,
          content: 'HE you have to change this',
        },
      ],
      createdOn: 1539863677902,
      updatedOn: 1539863677902,
      recommendation: 'return-to-handling-editor',
      recommendationType: 'editorRecommendation',
    },
    {
      id: '0e48444f-dd21-4e91-9b88-cdcfd9cc84d7',
      userId: '46841a2c-0b37-47c0-8847-9cedcaeeb082',
      comments: [
        {
          files: [],
          public: true,
          content: 'reject author',
        },
        {
          files: [],
          public: false,
          content: 'reject eic',
        },
      ],
      createdOn: 1539863792764,
      updatedOn: 1539863792764,
      recommendation: 'reject',
      recommendationType: 'editorRecommendation',
    },
    {
      id: '9730b9b0-fd37-46f6-943c-f3666984e2ab',
      userId: 'f12e0a14-4b6b-4d22-a506-d03c0ac7562f',
      comments: [
        {
          public: true,
          content: 'shit man',
        },
      ],
      createdOn: 1539864067558,
      updatedOn: 1539864067558,
      recommendation: 'reject',
      recommendationType: 'editorRecommendation',
    },
  ],
}

module.exports = {
  fragment: fragmentWithRecommendations,
  config,
}
