import React, { Fragment } from 'react'
import { get, isNull, last } from 'lodash'
import { connect } from 'react-redux'
import { th } from '@pubsweet/ui-toolkit'
import { actions } from 'pubsweet-client'
import { withJournal } from 'xpub-journal'
import { ConnectPage } from 'xpub-connect'
import { DragDropContext } from 'react-dnd'
import styled, { css } from 'styled-components'
import HTML5Backend from 'react-dnd-html5-backend'
import { Button, Spinner, Steps } from '@pubsweet/ui'
import {
  selectFragment,
  selectCollection,
  selectCurrentUser,
} from 'xpub-selectors'
import { Redirect } from 'react-router'
import {
  compose,
  toClass,
  withProps,
  withHandlers,
  withStateHandlers,
} from 'recompose'
import { Row, MultiAction, IconButton } from 'pubsweet-component-faraday-ui'
import { withModal } from 'pubsweet-component-modal/src/components'
import {
  getUserToken,
  currentUserIsAuthor,
} from 'pubsweet-component-faraday-selectors/src'

import {
  reduxForm,
  getFormValues,
  hasSubmitFailed,
  getFormSyncErrors,
  change as changeForm,
} from 'redux-form'
import {
  addAuthor,
  deleteAuthor,
  getAuthorError,
  getAuthorFetching,
} from 'pubsweet-components-faraday/src/redux/authors'
import {
  uploadFile,
  deleteFile,
  getSignedUrl,
  getFileFetching,
} from 'pubsweet-components-faraday/src/redux/files'

import { wizardSteps } from './'
import { submitManuscript } from '../redux/conversion'
import { getAutosaveFetching } from '../redux/autosave'
import { onChange, onSubmit, setInitialValues, validate } from './utils'

// #region wizard
const Wizard = ({
  step,
  history,
  journal,
  prevStep,
  isEditMode,
  isLastStep,
  isFirstStep,
  handleSubmit,
  getButtonText,
  isFilesFetching,
  canEditManuscript,
  isAuthorsFetching,
  journal: { manuscriptTypes = [] },
  ...rest
}) =>
  canEditManuscript ? (
    <Root className="wizard-root">
      <Steps currentStep={step}>
        {wizardSteps.map(({ stepTitle }) => (
          <Steps.Step key={stepTitle} title={stepTitle} />
        ))}
      </Steps>

      <StepRoot className="wizard-step" step={step}>
        {React.createElement(wizardSteps[step].component, {
          manuscriptTypes,
          journal,
          isAuthorsFetching,
          ...rest,
        })}
        <Row justify="center" mt={2}>
          {(isAuthorsFetching || isFilesFetching) && isLastStep ? (
            <Spinner />
          ) : (
            <Fragment>
              <Button
                data-test-id="submission-back"
                mr={1}
                onClick={isFirstStep ? history.goBack : prevStep}
              >
                <IconButton fontIcon="arrowLeft" mb={0.1} pb={0.3} />
                Back
              </Button>
              <Button
                data-test-id="submission-next"
                ml={isFirstStep ? 0 : 1}
                onClick={handleSubmit}
                primary
              >
                {getButtonText()}
                <IconButton fontIcon="arrowRight" mb={0.1} pb={0.1} />
              </Button>
            </Fragment>
          )}
        </Row>
      </StepRoot>
    </Root>
  ) : (
    <Redirect to="/404" />
  )
// #endregion

// #region export
export default compose(
  ConnectPage(({ match }) => [
    actions.getCollection({ id: match.params.project }),
    actions.getFragment(
      { id: match.params.project },
      { id: match.params.version },
    ),
  ]),
  withJournal,
  connect(
    (state, { match }) => ({
      token: getUserToken(state),
      isFetching: getAutosaveFetching(state),
      isFilesFetching: getFileFetching(state),
      reduxAuthorError: getAuthorError(state),
      formValues: getFormValues('submission')(state),
      submitFailed: hasSubmitFailed('submission')(state),
      formSyncErrors: getFormSyncErrors('submission')(state),
      isAdmin: get(selectCurrentUser(state), 'admin', false),
      isAuthor: currentUserIsAuthor(
        state,
        get(selectFragment(state, get(match, 'params.version')), 'id', null),
      ),
      fragment: selectFragment(state, get(match, 'params.version')),
      collection: selectCollection(state, get(match, 'params.project')),
      isAuthorsFetching: getAuthorFetching(state) || getAutosaveFetching(state),
    }),
    {
      changeForm,
      submitManuscript,
      updateFragment: actions.updateFragment,
    },
  ),
  withStateHandlers(
    { step: 0, authorEditIndex: null },
    {
      nextStep: ({ step }) => () => ({
        step: Math.min(wizardSteps.length - 1, step + 1),
      }),
      prevStep: ({ step }) => () => ({ step: Math.max(0, step - 1) }),
      setAuthorEditIndex: () => index => ({
        authorEditIndex: index,
      }),
    },
  ),
  withProps(setInitialValues),
  withProps(
    ({
      step,
      location,
      fragment,
      formValues,
      collection,
      submitFailed,
      formSyncErrors,
      authorEditIndex,
      reduxAuthorError,
    }) => ({
      addAuthor,
      deleteFile,
      uploadFile,
      deleteAuthor,
      getSignedUrl,
      isFirstStep: step === 0,
      isAuthorEdit: !isNull(authorEditIndex),
      isLastStep: step === wizardSteps.length - 1,
      isEditMode: get(location, 'state.editMode', false),
      filesError: submitFailed && get(formSyncErrors, 'files', ''),
      authorsError:
        (submitFailed && get(formSyncErrors, 'authors', '')) ||
        reduxAuthorError,
      isLastFragment:
        get(fragment, 'id', '') === last(get(collection, 'fragments', [])),
      status: get(collection, 'status', ''),
    }),
  ),
  withProps(({ status, isAdmin, isAuthor, isLastFragment }) => ({
    canEditManuscript:
      (isAdmin || (isAuthor && status === 'draft')) &&
      isLastFragment &&
      !(status === 'accepted' || status === 'rejected'),
  })),
  withHandlers({
    getButtonText: ({ isLastStep, isEditMode }) => () => {
      if (isEditMode && isLastStep) {
        return 'SAVE CHANGES'
      }
      return isLastStep ? `SUBMIT MANUSCRIPT` : `NEXT STEP`
    },
  }),
  withModal(({ isFetching }) => ({
    isFetching,
    modalComponent: MultiAction,
  })),
  reduxForm({
    validate,
    onChange,
    onSubmit,
    form: 'submission',
  }),
  DragDropContext(HTML5Backend),
  toClass,
)(Wizard)
// #endregion

// #region styled-components
const Root = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  padding-bottom: calc(${th('gridUnit')} * 3);
  margin: 0 auto;
`

const stepCSS = ({ step = 0 }) =>
  step === 1
    ? css`
        min-width: calc(${th('gridUnit')} * 96);
      `
    : css`
        width: calc(${th('gridUnit')} * 96);
      `

const StepRoot = styled.div`
  align-items: center;
  background-color: ${th('wizard.colorBackground')};
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  margin: ${th('gridUnit')} auto 0;
  padding: calc(${th('gridUnit')} * 5) calc(${th('gridUnit')} * 4);
  position: relative;
  ${stepCSS};
`
// #endregion
