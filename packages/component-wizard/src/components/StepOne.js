import React, { Fragment } from 'react'
import styled from 'styled-components'
import { get } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import { Checkbox, ValidatedField, H2 } from '@pubsweet/ui'
import { ActionLink, Text, Row } from 'pubsweet-component-faraday-ui'

const StepOne = ({ journal: { submission } }) => (
  <Fragment>
    <H2>1. Pre-Submission Checklist</H2>
    <Row mb={2}>
      <Text align="center" secondary>
        Before moving forward, please make sure you have reviewed the items on
        the list below.
      </Text>
    </Row>
    <Row alignItems="center" justify="flex-start" mb={1}>
      <Text bullet>
        I am aware that accepted manuscripts are subject to an
        <ActionLink
          pl={1 / 2}
          pr={1 / 2}
          to={get(submission, 'links.articleProcessing', '')}
        >
          Article Processing Charge of $1,250.
        </ActionLink>
      </Text>
    </Row>
    <Row alignItems="center" justify="flex-start" mb={1}>
      <Text bullet>
        All co-authors have read and agreed on the current version of this
        manuscript.
      </Text>
    </Row>
    <Row alignItems="center" justify="flex-start" mb={1}>
      <Text bullet>
        I have the email addresses of all co-authors of the manuscript.
      </Text>
    </Row>
    <Row alignItems="center" justify="flex-start" mb={1}>
      <Text bullet>
        I confirm the main manuscript file is in Microsoft Word or Adobe PDF
        format with the tables and figures integrated in the manuscript body.
      </Text>
    </Row>
    <Row alignItems="center" justify="flex-start" mb={1}>
      <Text bullet>
        I have all additional electronic files of supplementary materials (e.g.
        Datasets, images, audio, video) ready.
      </Text>
    </Row>
    <Row alignItems="center" justify="flex-start" mb={1}>
      <Text bullet>
        I am aware that an
        <ActionLink pl={1 / 2} pr={1 / 2} to="https://orcid.org/">
          ORCID
        </ActionLink>
        is required for the corresponding author before the article can be
        published (if accepted). The ORCID should be added via your user
        account.
      </Text>
    </Row>
    <Row alignItems="center" justify="flex-start" mb={1}>
      <Text bullet>
        I am aware that if my submission is covered by an
        <ActionLink
          pl={1 / 2}
          pr={1 / 2}
          to="https://about.hindawi.com/institutions/"
        >
          institutional membership
        </ActionLink>
        then Hindawi will share details of the manuscript with the administrator
        of the membership.
      </Text>
    </Row>

    <Row alignItems="center" justify="center" mb={4} mt={1}>
      <ValidatedField component={CustomCheckbox} name="declarations.agree" />
    </Row>
  </Fragment>
)

const CustomCheckbox = input => (
  <RootCheckbox data-test-id="agree-checkbox">
    <Checkbox
      checked={input.value}
      {...input}
      label="I have reviewed and understood all of the above."
    />
  </RootCheckbox>
)

export default StepOne

// #region styled-components
const RootCheckbox = styled.div.attrs(props => ({
  className: 'custom-checkbox',
}))`
  + div[role='alert'] {
    margin-top: 0;
  }
  & label {
    margin-bottom: calc(${th('gridUnit')} / 2);
    & span {
      color: ${th('colorText')};
      font-family: ${th('fontReading')};
      font-size: ${th('fontSizeBase')};
    }
  }
`
// #endregion
