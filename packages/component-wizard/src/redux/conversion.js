import moment from 'moment'
import { pick, get } from 'lodash'
import { actions } from 'pubsweet-client'
import journalConfig from 'xpub-faraday/app/config/journal'
import { create, update } from 'pubsweet-client/src/helpers/api'

/* constants */
export const CREATE_DRAFT_REQUEST = 'CREATE_DRAFT_REQUEST'
export const CREATE_DRAFT_SUCCESS = 'CREATE_DRAFT_SUCCESS'

/* action creators */
export const createDraftRequest = () => ({
  type: CREATE_DRAFT_REQUEST,
})

/* utils */
const generateCustomId = () =>
  moment
    .now()
    .toString()
    .slice(-7)

const redirectToRoute = (history, route) => {
  // redirect after a short delay
  window.setTimeout(() => {
    history.push(route)
  }, 10)
}
export const isRevisionFlow = (state, collection, fragment = {}) =>
  collection.fragments.length > 1 && !fragment.submitted

/* actions */
const addSubmittingAuthor = (user, collectionId, fragmentId) => {
  const author = {
    ...pick(user, [
      'id',
      'email',
      'affiliation',
      'firstName',
      'lastName',
      'country',
    ]),
    isSubmitting: true,
    isCorresponding: true,
  }
  return create(`/collections/${collectionId}/fragments/${fragmentId}/users`, {
    role: 'author',
    ...author,
  })
}

export const createDraftSubmission = history => (dispatch, getState) => {
  const currentUser = getState().currentUser.user
  return dispatch(
    actions.createCollection({ customId: generateCustomId() }),
  ).then(({ collection }) => {
    if (!collection.id) {
      throw new Error('Failed to create a project')
    }

    // TODO: rethrow errors so they can be caught here
    return dispatch(
      actions.createFragment(collection, {
        created: new Date(), // TODO: set on server
        collectionId: collection.id,
        files: {
          manuscripts: [],
          supplementary: [],
          coverLetter: [],
        },
        fragmentType: 'version',
        metadata: {
          journal: get(journalConfig, 'metadata.nameText', ''),
        },
        version: 1,
      }),
    ).then(({ fragment }) => {
      if (!fragment.id) {
        throw new Error('Failed to create a project')
      }
      const route = `/projects/${collection.id}/versions/${fragment.id}/submit`
      if (!currentUser.admin) {
        addSubmittingAuthor(currentUser, collection.id, fragment.id).then(() =>
          redirectToRoute(history, route),
        )
      } else {
        redirectToRoute(history, route)
      }
    })
  })
}

export const submitManuscript = ({ collectionId, fragmentId }) => dispatch =>
  create(`/collections/${collectionId}/fragments/${fragmentId}/submit`)

export const createRevision = (
  collection,
  previousVersion,
  history,
) => dispatch => {
  // copy invitations only if minor revision
  const {
    id,
    submitted,
    invitations,
    recommendations,
    ...prev
  } = previousVersion
  return dispatch(
    actions.createFragment(collection, {
      ...prev,
      invitations: invitations.filter(inv => inv.isAccepted),
      created: new Date(),
      version: previousVersion.version + 1,
    }),
  ).then(({ fragment }) => {
    const route = `/projects/${collection.id}/versions/${fragment.id}/submit`
    window.setTimeout(() => {
      history.push(route)
    }, 10)
    return fragment
  })
}

export const submitRevision = ({ collectionId, fragmentId }) =>
  update(`/collections/${collectionId}/fragments/${fragmentId}/submit`)

/* reducer */
const initialState = {
  complete: undefined,
  converting: false,
  error: undefined,
}

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state
  }
}
